//
//  TextFieldTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dropDownIcon: UIImageView!
    @IBOutlet weak var viewBullet: DesignableView!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        labelTitle.textColor = .darkGray
        textField.backgroundColor = .clear
        textField.font = CustomFont.regular(ofSize: 16)
        dropDownIcon.isHidden = true
        viewBullet.backgroundColor = .clear
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupData(title: String, text: String = "", placeholder: String, rightImage: UIImage? = nil) {
        labelTitle.text = title
        textField.text = text
        //textField.placeholder = placeholder
        if let image = rightImage {
            dropDownIcon.isHidden = false
            dropDownIcon.image = image
        }
        textField.isUserInteractionEnabled = true
    }
}
