//
//  ImageTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 23/08/21.
//

import UIKit
import Lottie
class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var animationView: AnimationView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        animationView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLottieAnimation() {
        img.image = Constants.image_noDataPlaceHolder
        /*animationView.isHidden = false
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.play()*/
    }
    
}
