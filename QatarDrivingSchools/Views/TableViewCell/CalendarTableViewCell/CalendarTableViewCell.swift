//
//  CalendarTableViewCell.swift
//  YobodTrainer
//
//  Created by C100-107 on 02/07/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import FSCalendar
class CalendarTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        labelTitle.textColor = Constants.color_darkApplicationThemeColor
        calendar.headerHeight = 0
        calendar.appearance.caseOptions = .weekdayUsesSingleUpperCase
        labelTitle.text = Constants.description_today.localized + " : " +  Date().toString(format: "dd-MM-yyyy")
        calendar.appearance.borderRadius = 30.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        for label in calendar.calendarWeekdayView.weekdayLabels {
            label.font = CustomFont.regular(ofSize: 14)
        }
    }
    //MARK:- Action methods
    @IBAction func buttonLeftTouchUpInside(_ sender: UIButton) {
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendar.currentPage) ?? Date()
        calendar.setCurrentPage(previousMonth, animated: true)
    }
    @IBAction func buttonRightTouchUpInside(_ sender: UIButton) {
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: calendar.currentPage) ?? Date()
        calendar.setCurrentPage(nextMonth, animated: true)
    }
}
