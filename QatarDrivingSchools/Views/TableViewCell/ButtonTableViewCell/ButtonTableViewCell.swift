//
//  ButtonTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var button: RoundButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var leading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = CustomFont.semiBold()
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    override func layoutIfNeeded() {
        
    }
    //MARK:- Class methods
    func setupBorder() {
        button.borderWidth = 1
        button.cornerRadius = 5
        button.borderColor = Constants.color_applicationThemeColor
        button.setBackgroundImage(nil, for: .normal)
        button.setTitleColor(Constants.color_applicationThemeColor, for: .normal)
    }
    func setupBackgroundWithRadiant() {
        button.cornerRadius = 0
        button.setTitleColor(.white, for: .normal)
        setGradient()
        //button.setBackgroundImage(#imageLiteral(resourceName: "button_bg"), for: .normal)
    }
    
    func setGradient() {
        let topColor = Constants.color_applicationThemeColor
        let bottomColor = Constants.color_darkApplicationThemeColor
        let gradientLayer: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            gradientLayer.frame = CGRect.zero
            return gradientLayer
        }()
        gradientLayer.frame = button.bounds
        button.layer.insertSublayer(gradientLayer, at: 0)
    }
}
