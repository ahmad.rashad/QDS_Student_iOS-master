//
//  CollectionWithTitleTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class CollectionWithTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonViewAll: UIButton!
    @IBOutlet weak var imageViewSeparator: UIImageView!
    @IBOutlet weak var imageViewBottomSeparator: UIImageView!
    
    @IBOutlet weak var leadingSpace: NSLayoutConstraint!
    @IBOutlet weak var trailingSpace: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        buttonViewAll.isHidden = true
        buttonViewAll.titleLabel?.font = CustomFont.semiBold(ofSize: 13)
        buttonViewAll.titleLabel?.textColor = Constants.color_applicationThemeColor
        labelTitle.font = CustomFont.semiBold()
        imageViewSeparator.backgroundColor = .clear
        imageViewBottomSeparator.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLayoutForIPad(){
        leadingSpace.priority = UILayoutPriority(1)
        trailingSpace.priority = UILayoutPriority(1)
        widthConstraint.priority = UILayoutPriority(1000)
    }
}
