//
//  TextViewTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class TextViewTableViewCell: UITableViewCell {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        labelTitle.textColor = .darkGray
        textView.font = CustomFont.regular(ofSize: 16)
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupData(title: String, text: String = "", placeholder: String? = nil){
        labelTitle.text = title
        if (placeholder != nil) && text.isEmpty {
            textView.text = placeholder ?? ""
            textView.textColor = .lightGray
        }else{
            textView.text = text
            textView.textColor = .black
        }
    }
}
