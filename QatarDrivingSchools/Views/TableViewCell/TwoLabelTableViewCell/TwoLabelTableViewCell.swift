//
//  TwoLabelTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class TwoLabelTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        label1.font = CustomFont.regular(ofSize: 14)
        label2.font = CustomFont.regular(ofSize: 14)
        
        label2.textColor = .darkGray
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupData(label1: String, label2: String) {
        self.label1.text = label1
        self.label2.text = label2
    }
    
}
