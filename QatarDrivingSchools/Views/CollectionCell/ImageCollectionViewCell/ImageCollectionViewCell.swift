//
//  ImageCollectionViewCell.swift
//  Bubbles
//
//  Created by C100-107 on 18/03/21.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    //MARK:- Variables
    @IBOutlet weak var imageViewMedia: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        
        contentView.backgroundColor = .clear
        self.scrollView.delegate = self
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 4.0
        self.scrollView.zoomScale = 1.0
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(zoom(tapGesture:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        self.scrollView.addGestureRecognizer(doubleTap)
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewMedia
    }
    @objc func zoom(tapGesture: UITapGestureRecognizer) {
        if (self.scrollView!.zoomScale == self.scrollView!.minimumZoomScale) {
            let center = tapGesture.location(in: self.scrollView!)
            let size = imageViewMedia?.image?.size
            let zoomRect = CGRect(x: center.x, y: center.y, width: ((size?.width ?? 100) / 4), height: ((size?.height ?? 100) / 4))
            self.scrollView!.zoom(to: zoomRect, animated: true)
        } else {
            self.scrollView!.setZoomScale(self.scrollView!.minimumZoomScale, animated: true)
        }
    }
}
