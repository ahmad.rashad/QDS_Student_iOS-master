//
//  PassedQuizCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class PassedQuizCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelResultTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelTitle.font = CustomFont.regular(ofSize: 12)
        labelResultTitle.font = CustomFont.regular(ofSize: 12)
        
        labelResultTitle.textColor = .gray
        
        labelResultTitle.text = Constants.description_result
        #if DEBUG
        labelTitle.text = "Qatar Driving Theory Test"
        setupAttributedText(text: "28/30")
        #endif
    }
    
    //MARK:- Class methods
    func setupAttributedText(title: String = Constants.description_result, text: String) {
        let mutableAttributedString = NSMutableAttributedString(string: title + "", attributes: [NSAttributedString.Key.font: CustomFont.semiBold(ofSize: 12)])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.regular(ofSize: 12),
                                                                                                  NSAttributedString.Key.foregroundColor: Constants.color_applicationThemeColor]))
    
        labelResultTitle.attributedText = mutableAttributedString
    }

}
