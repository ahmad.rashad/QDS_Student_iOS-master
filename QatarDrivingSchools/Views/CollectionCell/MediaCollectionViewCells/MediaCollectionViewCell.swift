//
//  MediaCollectionViewCell.swift
//  YobodTrainer
//
//  Created by C100-107 on 26/06/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import AVFoundation
class MediaCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var viewBackground: DesignableView!
    @IBOutlet weak var imageViewMedia: UIImageView!
    @IBOutlet weak var buttonPlayMedia: RoundButton!
    @IBOutlet weak var buttonDelete: RoundButton!
    @IBOutlet weak var buttonCounter: UIButton!
    
    var videoPlayer: AVPlayer!
    var videoPlayerLayer: AVPlayerLayer!
    
    var mediaUrl: URL! {
        didSet {
            videoPlayer = AVPlayer(url: mediaUrl)
            videoPlayerLayer = AVPlayerLayer(player: videoPlayer)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        viewBackground.backgroundColor = .clear
        buttonPlayMedia.isHidden = true
        buttonCounter.isHidden = true
        buttonDelete.isHidden = true
        removePlayer()
    }

    func addPlayerIntoView(isAutoPlayRequired: Bool = false){
        removePlayer()
        imageViewMedia.image = nil
        if videoPlayerLayer != nil && imageViewMedia != nil {
            videoPlayerLayer.frame = imageViewMedia.bounds
            videoPlayer.actionAtItemEnd = .none
            imageViewMedia.layer.addSublayer(videoPlayerLayer)
            if isAutoPlayRequired {
                videoPlayer.play()
            }
        }
    }
    func removePlayer(){
        buttonPlayMedia.isHidden = true
        for layer in imageViewMedia?.layer.sublayers ?? []{
            if let videoLayer = layer as? AVPlayerLayer{
                videoLayer.removeFromSuperlayer()
            }
        }
    }
}
