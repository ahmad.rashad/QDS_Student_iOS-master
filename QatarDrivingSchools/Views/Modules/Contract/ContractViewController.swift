//
//  ContractViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import WebKit
import NotificationBannerSwift
class ContractViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var imageViewPlaceholder: UIImageView!
    
    var dataSource : ContractDataSource?
    lazy var viewModel = ContractViewModel()

    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ContractDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        setupLayout()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        webView.layoutMargins.left = 20
        webView.layoutMargins.right = 20
        viewModel.isContractDetailsFetched.bind { isFetched in
            if isFetched {
                self.setContractDetails()
            }
            self.imageViewPlaceholder.isHidden = isFetched
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_contract.localized
        viewModel.getContract()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func setContractDetails() {
        self.activityIndicator.stopAnimating()
        webView.isHidden = false
        let details = viewModel.getContractDetails()
            webView.loadHTMLString(details, baseURL: nil)
        
        
//        switch viewModel.getContractDataType() {
//        case .png, .jpg:
//            webView.isHidden = true
//            collectionView.reloadData()
//        case .pdf:
//            webView.isHidden = false
//            if let url = viewModel.getContractURL() {
//                webView.load(URLRequest(url: url))
//            }
//        }
    }
}
