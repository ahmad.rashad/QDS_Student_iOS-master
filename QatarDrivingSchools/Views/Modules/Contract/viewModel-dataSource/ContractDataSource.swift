//
//  ContractDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import Foundation
import UIKit
import SDWebImage
import Photos
class ContractDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
      
    private let viewController: ContractViewController
    private let collectionView: UICollectionView
    private let viewModel: ContractViewModel

    private var previousIndex = 0
    
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: ContractViewModel, viewController: ContractViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView() {
        registerCollectionCell()
        collectionView.isPagingEnabled = true
        collectionView.reloadData()
    }
    
    func registerCollectionCell() {
        collectionView.registerNib(nibNames: [ImageCollectionViewCell.identifier])
    }
    
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as! ImageCollectionViewCell
        if let url = viewModel.getContractURL() {
            cell.imageViewMedia.contentMode = .scaleAspectFit
            cell.imageViewMedia.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "contract"), options: [], context: [:])
        }
        return cell
    }
}
extension ContractViewController: UIScrollViewDelegate {
    
}
