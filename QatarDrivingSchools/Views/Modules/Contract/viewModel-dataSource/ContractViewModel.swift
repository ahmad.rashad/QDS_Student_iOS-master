//
//  ContractViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import NISLRequestPackages

class ContractViewModel {
    
    // MARK: - Vars & Lets
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isContractDetailsFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var contractsData: [Contracts] = []
    
    //MARK:- Dependancy Injection
    init() {
    }
    
    //MARK:- API
    func getContract() {
        let param: Parameters = [
            "S_Code": Constants.loggedInUser?.scode ?? "",
            "Emailid": Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetContract, queryParameter: param) { (res: Swift.Result<BaseClass<ContractsData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if let contract = data.data {
                        self.contractsData = contract.contracts ?? []
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    self.isContractDetailsFetched.value = true
                } else {
                    //self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    self.isContractDetailsFetched.value = false
                }
            case .failure(let message):
                self.alertMessage.value = message
                self.isContractDetailsFetched.value = false
            }
        }
    }

    //MARK:- Class Methods

}
//MARK:- Getters and Setters
extension ContractViewModel {
    func getContractDataType() -> enumContractDocType {
        return enumContractDocType(rawValue: contractsData.first?.contractDocType ?? 0) ?? .jpg
    }
    func getContractURL() -> URL? {
        return Constants.getPathOfMedia(value: contractsData.first?.contractPdf ?? "")
    }
    func getContractDetails() -> String{
        return contractsData.first?.details ?? ""
    }
}

