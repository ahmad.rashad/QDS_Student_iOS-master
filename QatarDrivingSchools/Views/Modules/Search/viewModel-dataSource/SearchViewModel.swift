//
//  SearchViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit
import NISLRequestPackages
class SearchViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isEBookFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    var noResultMessage: Dynamic<String> = Dynamic("")
    
    private var eBookList: [EBooks] = []
    private var isMoreDataAvailable = true
    private var offset = 0
    private var searchText = ""
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - APIS
    func searchEBooks(isNeedToRefresh: Bool = false, offset: Int) {
        if isNeedToRefresh {
            //offset = 0
        }
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "BookName":searchText
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.SearchEBook, queryParameter: param) { (res: Swift.Result<BaseClass<EBooksBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.noResultMessage.value = ""
                    if offset == 0 {
                        self.eBookList.removeAll()
                    }
                    self.offset += 1
                    self.eBookList.append(contentsOf: data.data?.eBooks ?? [])
                    if self.eBookList.count == 0 {
                        self.noResultMessage.value = Constants.description_noEbook
                        self.isMoreDataAvailable = false 
                    }
                } else {
                    self.eBookList.removeAll()
                    self.noResultMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isEBookFetched.value = true
            case .failure(let message):
                self.eBookList.removeAll()
                self.alertMessage.value = message
                self.isEBookFetched.value = false
            }
        }
    }
}
//MARK:- Getters and Setters
extension SearchViewModel {
    func getTotalNumberOfEBooks() -> Int {
        eBookList.count
    }
    func getEBook(at index: Int) -> EBooks {
        eBookList[index]
    }
    func isMoreDataAvailableInEBook() -> Bool {
        isMoreDataAvailable
    }
    func getOffset() -> Int {
        offset
    }
    func setSearchText(value: String) {
        if searchText != value {
            searchText = value
            searchEBooks(isNeedToRefresh: true, offset: 0)
        }
    }
}
