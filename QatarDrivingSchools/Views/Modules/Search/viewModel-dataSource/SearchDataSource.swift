//
//  SearchDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import Foundation
import UIKit

class SearchDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let viewController: SearchViewController
    private let collectionView: UICollectionView
    private let viewModel: SearchViewModel
  
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: SearchViewModel, viewController: SearchViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
        self.viewController.textFieldSearch.delegate = self
        self.viewController.textFieldSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
    }
    
    //MARK: - Class methods
    func setupCollectionView(){
        registerCollectionCell()
    }
    func registerCollectionCell(){
        collectionView.registerNib(nibNames: [CoursesCollectionViewCell.identifier])
        collectionView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch viewController.viewControllerType {
        case .eBook:
            let count = viewModel.getTotalNumberOfEBooks()
            viewController.imageViewPlaceHolder.isHidden = count != 0
            return count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: collectionView.frame.width / 4 - 8, height: 180)
        }
        return CGSize(width: collectionView.frame.width / 2 - 2, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoursesCollectionViewCell.identifier, for: indexPath) as! CoursesCollectionViewCell
        switch viewController.viewControllerType {
        case .eBook:
            let eBook = viewModel.getEBook(at: indexPath.row)
            cell.setupUIForEBook(title: eBook.bookName ?? "")
            cell.imgView.sd_setImage(with: Constants.getPathOfMedia(value: eBook.thumbnailImage ?? ""), completed: nil)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch viewController.viewControllerType {
        case .eBook:
            let webKitViewController = WebKitViewController.loadFromNib()
            let eBook = viewModel.getEBook(at: indexPath.row)
            webKitViewController.url = Constants.getPathOfMedia(value: eBook.pdfLink ?? "")
            webKitViewController.titleString = eBook.bookName ?? ""
            webKitViewController.modalPresentationStyle = .overFullScreen
            viewController.present(webKitViewController, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        switch viewController.viewControllerType {
        case .eBook:
            let count = viewModel.getTotalNumberOfEBooks()
            let isLastCell = count - currentRow == 1
            let minY = collectionView.contentOffset.y
            if viewModel.isMoreDataAvailableInEBook() && isLastCell && minY > 0 {
                viewModel.searchEBooks(offset: viewModel.getOffset())
            }
        }
    }
}

//MARK:- TextField Delegate
extension SearchDataSource: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.setSearchText(value: textField.text ?? "")
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        viewModel.setSearchText(value: textField.text ?? "")
    }
}
