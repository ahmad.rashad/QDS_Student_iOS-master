//
//  SearchViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit

class SearchViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var labelNoMoreData: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewPlaceHolder: UIImageView!
    
    var dataSource: SearchDataSource?
    lazy var viewModel = SearchViewModel()
    
    enum enumViewControllerType {
        case eBook
    }
    var viewControllerType: enumViewControllerType = .eBook
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.text = Constants.title_search.localized
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        dataSource = SearchDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        switch viewControllerType {
        case .eBook:
            buttonFilter.isHidden = true
            viewModel.searchEBooks(offset: viewModel.getOffset())
        }
        viewModel.isEBookFetched.bind { isFetched in
            if isFetched {
                self.collectionView.reloadData()
            }
        }
        viewModel.noResultMessage.bind { message in
            //self.labelNoMoreData.text = message
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonFilterTouchUpInside(_ sender: UIButton) {
        let filterVC = FilterViewController.loadFromNib()
        filterVC.viewControllerType = .filter
        filterVC.viewModel.setTitle(value: Constants.description_filter.localized)
        filterVC.modalPresentationStyle = .overFullScreen
        present(filterVC, animated: true, completion: nil)
    }
    
}
