//
//  QRCodeViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-07 on 21/12/21.
//

import UIKit

class QRCodeViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewQRCode: UIImageView!
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        labelTitle.text = Constants.placeholder_personalQatariID
        imageViewQRCode.image = generateQRCode(from: Constants.loggedInUser?.personalQatariID ?? "")
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    //MARK:- Class Methods
    func generateQRCode(from string: String) -> UIImage?
    {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator")
        {
            filter.setValue(data, forKey: "inputMessage")
            
            guard let qrImage = filter.outputImage else {return nil}
            let scaleX = self.imageViewQRCode.frame.size.width / qrImage.extent.size.width
            let scaleY = self.imageViewQRCode.frame.size.height / qrImage.extent.size.height
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            
            if let output = filter.outputImage?.transformed(by: transform)
            {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
}
