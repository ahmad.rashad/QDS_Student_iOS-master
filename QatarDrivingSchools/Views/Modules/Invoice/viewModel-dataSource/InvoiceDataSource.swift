//
//  InvoiceDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import Foundation
import UIKit

class InvoiceDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let viewController: InvoiceViewController
    private let collectionView: UICollectionView
    private let viewModel: InvoiceViewModel
  
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: InvoiceViewModel, viewController: InvoiceViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView(){
        registerCollectionCell()
    }
    func registerCollectionCell(){
        collectionView.registerNib(nibNames: [CoursesCollectionViewCell.identifier])
        collectionView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTotalNumberOfInvoice()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: collectionView.frame.width / 4 - 8, height: 180)
        }
        return CGSize(width: collectionView.frame.width / 2 - 5, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoursesCollectionViewCell.identifier, for: indexPath) as! CoursesCollectionViewCell
        cell.setupUIForInvoice(title: Constants.title_invoice.localized)
        cell.imgView.image = Constants.image_invoice
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let webKitViewController = WebKitViewController.loadFromNib()
        webKitViewController.url = viewModel.getInvoice(at: indexPath.row)
        webKitViewController.titleString = Constants.title_invoice
        webKitViewController.modalPresentationStyle = .overFullScreen
        viewController.present(webKitViewController, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        /*let currentRow = indexPath.row
        let count = viewModel.getTotalNumberOfInvoices()
        
        let isLastCell = count - currentRow == 1
        let minY = collectionView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInInvoice() && isLastCell && minY > 0 {
            viewModel.getInvoices()
        }*/
    }
}



