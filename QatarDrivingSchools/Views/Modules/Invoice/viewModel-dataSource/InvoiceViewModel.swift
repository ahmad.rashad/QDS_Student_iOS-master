//
//  InvoiceViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import NISLRequestPackages

class InvoiceViewModel {
    
    // MARK: - Vars & Lets
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isInvoiceDataFetched: Dynamic<Bool> = Dynamic(false)
    
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var invoices: [Invoice] = []
    
    //MARK:- Dependancy Injection
    init() {
    }
    
    //MARK:- API
    func getInvoice() {
        let param: Parameters = [
            "S_Code": Constants.loggedInUser?.scode ?? "",
            "EmailId": Constants.getLoggedInUserEmailForAPI(),
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetInvoice, queryParameter: param) { (res: Swift.Result<BaseClass<InvoiceData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if let invoiceList = data.data {
                        self.invoices = invoiceList.invoice ?? []
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    self.isInvoiceDataFetched.value = true
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    self.isInvoiceDataFetched.value = false
                }
            case .failure(let message):
                self.alertMessage.value = message
                self.isInvoiceDataFetched.value = false
            }
        }
    }

    //MARK:- Class Methods

}
//MARK:- Getters and Setters
extension InvoiceViewModel {
    func getTotalNumberOfInvoice() -> Int {
        invoices.count
    }
    func getInvoice(at index: Int) -> URL? {
        Constants.getPathOfMedia(value: invoices[index].invoiceDoc ?? "")
    }
}
