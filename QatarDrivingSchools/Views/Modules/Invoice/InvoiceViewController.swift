//
//  InvoiceViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import NotificationBannerSwift
class InvoiceViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewPlaceHolder: UIImageView!
    
    var dataSource : InvoiceDataSource?
    lazy var viewModel = InvoiceViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = InvoiceDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        setupLayout()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isInvoiceDataFetched.bind { isFetched in
            self.collectionView.isHidden = self.viewModel.getTotalNumberOfInvoice() == 0
            if isFetched{
                self.imageViewPlaceHolder.image = self.viewModel.getTotalNumberOfInvoice() == 0 ? Constants.image_noDataPlaceHolder : UIImage()
                self.collectionView.reloadData()
            }else{
                self.imageViewPlaceHolder.image = Constants.image_noDataPlaceHolder
            }
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_invoice.localized
        viewModel.getInvoice()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
