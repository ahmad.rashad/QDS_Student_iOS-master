//
//  LoginViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import UIKit
import NotificationBannerSwift
class LoginViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
    
    var dataSource : LoginDataSource?
    lazy var viewModel = LoginViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       /* if UIDevice.current.userInterfaceIdiom  == .pad {
            leading.priority = UILayoutPriority(1.0)
            trailing.priority = UILayoutPriority(1.0)
        }else{
            width.priority = UILayoutPriority(1.0)
        }*/
        dataSource = LoginDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLayoutSubviews() {
        self.tableView.contentInset = .zero
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
