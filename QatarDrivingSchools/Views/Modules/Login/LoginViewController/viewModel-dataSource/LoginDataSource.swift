//
//  LoginDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class LoginDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: LoginViewController
    private let tableView: UITableView
    private let viewModel: LoginViewModel
    
    enum enumLoginTableRow: Int {
        case appIcon = 0
        case email
        case password
        case language
        case rememberMeForgotPassword
        case login
        static let count: Int = {
            var max: Int = 0
            while let _ = enumLoginTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    enum enumRememberMeCheckBoxStatus: Int {
        case checked = 0
        case unchecked
    }
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: LoginViewModel, viewController: LoginViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        registerTableCell()
    }
    
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         ButtonTableViewCell.identifier,
                                         AppIconTableViewCell.identifier,
                                         LanguageSettingsTableViewCell.identifier,
                                         RememberMeForgetPasswordTableViewCell.identifier])
        tableView.reloadData()
    }
    func showLanguageSelections(sender: UIView) {
        let optionMenu = UIAlertController(title: nil, message: Constants.title_selectLanguage, preferredStyle: .actionSheet)
//        optionMenu.addAction(UIAlertAction(title: "Default Language",
//                                           style: .default, handler: { action in
//        }))
        for i in 0..<viewModel.getLanguagesCount() {
            optionMenu.addAction(UIAlertAction(title: (Language(rawValue: self.viewModel.getLanguage(at: i)) ?? .ENGLISH).description,
                                               style: .default, handler: { action in
                let selectedLanguage  = self.viewModel.getLanguage(at: i);
                Language.language = Language(rawValue: selectedLanguage) ?? .ENGLISH
                /*self.viewModel.setLanguage(value: self.viewModel.getLanguage(at: i))
                self.tableView.reloadData()*/
            }))
        }

        
        optionMenu.addAction(UIAlertAction(title: Constants.title_cancel, style: .cancel, handler: { action in
            
        }))
        optionMenu.view.tintColor = Constants.color_applicationThemeColor
        if UIDevice.current.userInterfaceIdiom == .pad {
            optionMenu.popoverPresentationController?.sourceView = viewController.view
            optionMenu.popoverPresentationController?.sourceRect = sender.frame
        }
        viewController.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK:- Action methods
    @objc func buttonLoginTouchUpInside(_ sender: UIButton) {
        viewModel.login()
    }
    @objc func buttonRegisterTouchUpInside(_ sender: UIButton) {
    }
    @objc func buttonForgotPasswordTouchUpInside(_ sender: UIButton) {
        let forgotPasswordViewController = ForgotPasswordViewController.loadFromNib()
        viewController.navigationController?.pushViewController(forgotPasswordViewController, animated: true)
    }
    
    @objc func buttonCheckBoxRememberMeTouchUpInside(_ sender: UIButton){
        switch enumRememberMeCheckBoxStatus(rawValue: sender.tag) {
        case .checked:
            sender.tag = enumRememberMeCheckBoxStatus.unchecked.rawValue
            viewModel.setRememberMe(value: false)
        case .unchecked:
            sender.tag = enumRememberMeCheckBoxStatus.checked.rawValue
            viewModel.setRememberMe(value: true)
        default:
            break
        }
        tableView.reloadRows(at: [IndexPath(row: enumLoginTableRow.rememberMeForgotPassword.rawValue, section: 0)], with: .none)
    }
    
    
    //MARK:- Table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumLoginTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         switch enumLoginTableRow(rawValue: indexPath.row) {
         case .appIcon:
            return tableView.frame.height - 450
         default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumLoginTableRow(rawValue: indexPath.row)! {
        case .appIcon:
            let cell = tableView.dequeueReusableCell(withIdentifier: AppIconTableViewCell.identifier) as! AppIconTableViewCell
            return cell
        case .email:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_emailOrQID, placeholder: Constants.placeholder_emailOrQID)
            cell.textField.text = viewModel.getEmail()
            cell.textField.keyboardType = .default
            cell.textField.isSecureTextEntry = false
            cell.textField.tag = enumLoginTableRow.email.rawValue
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            cell.textField.textContentType = .emailAddress
            return cell
        case .password:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_password, placeholder: Constants.placeholder_password)
            cell.textField.text = viewModel.getPassword()
            cell.textField.isSecureTextEntry = true
            cell.textField.tag = enumLoginTableRow.password.rawValue
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            return cell
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.title_selectLanguage, placeholder: Constants.placeholder_password)
            cell.dropDownIcon.isHidden = false
            cell.textField.text = Language.language.description
            
            print("cell" + Language.language.description)
            print("cell" + Language.language.rawValue)
            
            cell.textField.isSecureTextEntry = false
            cell.textField.tag = enumLoginTableRow.language.rawValue
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            cell.textField.isUserInteractionEnabled = false
            return cell
            /*let cell = tableView.dequeueReusableCell(withIdentifier: LanguageSettingsTableViewCell.identifier) as! LanguageSettingsTableViewCell
            cell.labelSettings.font = CustomFont.semiBold(ofSize: 14)
            cell.labelSettings.textColor = .darkGray
            cell.languageChanged = { language in
                self.viewModel.setLanguage(value: language.rawValue)
                Language.language = language
            }
            return cell*/
        case .rememberMeForgotPassword:
            let cell = tableView.dequeueReusableCell(withIdentifier: RememberMeForgetPasswordTableViewCell.identifier) as! RememberMeForgetPasswordTableViewCell
            cell.switchRememberMe.isOn = viewModel.getRememberMe()
            if viewModel.getRememberMe() {
                cell.buttonRememberMe.tag = enumRememberMeCheckBoxStatus.checked.rawValue
            } else {
                cell.buttonRememberMe.tag = enumRememberMeCheckBoxStatus.unchecked.rawValue
            }
            cell.buttonForgetPassword.addTarget(self, action: #selector(buttonForgotPasswordTouchUpInside(_:)), for: .touchUpInside)
            cell.buttonRememberMe.addTarget(self, action: #selector(buttonCheckBoxRememberMeTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .login:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.setTitleColor(.white, for: .normal)
            cell.button.setTitle(Constants.title_login, for: .normal)
            cell.button.titleLabel?.font = CustomFont.bold(ofSize: 18)
            cell.button.addTarget(self, action: #selector(buttonLoginTouchUpInside(_:)), for: .touchUpInside)
            viewModel.isLoaderHidden.bind { (isHidden) in
                self.tableView.isUserInteractionEnabled = isHidden
                if isHidden {
                    cell.activityIndicator.stopAnimating()
                }else{
                    cell.activityIndicator.startAnimating()
                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumLoginTableRow(rawValue: indexPath.row)! {
        case .appIcon:
            break
        case .email:
            break
        case .password:
          break
        case .language:
            showLanguageSelections(sender: tableView.cellForRow(at: indexPath) ?? UIView())
        case .rememberMeForgotPassword:
            break
        case .login:
          break
        }
    }
}
//MARK:- TextField methods
extension LoginDataSource {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumLoginTableRow(rawValue: textField.tag) {
        case .email:
            viewModel.setEmail(value: textField.text ?? "")
        case .password:
            viewModel.setPassword(value: textField.text ?? "")
        case .language:
            viewModel.setLanguage(value: textField.text ?? "")
        default:
            break
        }
    }
}
