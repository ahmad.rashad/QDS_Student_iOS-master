//
//  LoginViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit
import NISLRequestPackages

class LoginViewModel: BaseViewModel{
    
    //MARK:- Variables
    private struct userDetailsStruct {
        var email = ""
        var password = ""
        var languageCode = "English"
        var rememberMe = false
    }
    
    private var userDetails = userDetailsStruct()
    var isContactMissing: Dynamic<User?> = Dynamic(nil)
    
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Dependancy Injection
    override init() {
        super.init()
        isLoaderHidden.value = false
        userDetails.email = UserDefaults.standard.string(forKey: enumUserDefaultsKey.rememberedUserEmail.rawValue) ?? ""
        userDetails.rememberMe = !userDetails.email.isEmpty
        #if DEBUG
   
            


        userDetails.email = "jogopon417@mediafate.com"
        userDetails.password = "password"
        
        userDetails.email = "csk@yopmail.com"
        userDetails.password = "Pg1i3Ju3"
        
        userDetails.email = "zp@narola.email"
        userDetails.password = "8E2CGM0f"
        
        userDetails.email = "hokkafomle@vusra.com"
        userDetails.password = "EywFrDZH"
        
        userDetails.email = "ahmad.rashad@idealsolutions.com"
        userDetails.password = "MVPzQrUo"
        
        userDetails.email = "pogeyow802@kahase.com"
        userDetails.password = "TGrdx3gz"
        
        userDetails.email = "heyome3235@runfons.com"
        userDetails.password = "X7AgbbdX"
        
        userDetails.email = "searchds@gmail.com"
        userDetails.password = "12345678"
        
        userDetails.email = "yelev84166@agrolivana.com"
        userDetails.password = "928068"
        
        userDetails.email = "demala3887@xitudy.com"
        userDetails.password = "683001"
        
        userDetails.email = "4564564544654"
        userDetails.password = "683977"
        
        userDetails.email = "fegaf25916@vpsrec.com"
        userDetails.password = "007247"
        
        userDetails.email = "60628401413"
        userDetails.password = "403304"
        
        //qa
        //userDetails.email = "Mohammad.ElSayed@idealsolutions.com"
        //userDetails.password = "UUx5wDUd"
        #endif
    }
    //MARK:- API
    func login() {
        if userDetails.email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty  {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_emailMissing.localized)
        }
//        else if !isValid(email: userDetails.email) {
//            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_emailIsNotValid.localized)
//        }
        else if userDetails.password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_passwordMissing.localized)
        } else if userDetails.languageCode.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_languageMissing.localized)
        } else {
            let param: Parameters = [
                "emailId": userDetails.email,
                "password": userDetails.password,
                "languageCode": Language.language.rawValue,
                "rememberMe": userDetails.rememberMe,
                "role":1
            ]
            isLoaderHidden.value = false
            apiManager.call(type: RequestItemsType.Account, params: param) { (res: Swift.Result<BaseClass<UserData>, AlertMessage>, response, jsonData) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        if let user = data.data {
                            if let tmpUser = user.user?.first{
                                self.saveUserInfoAndProceed(user: tmpUser)
                                if !self.userDetails.rememberMe {
                                    self.userDetails.email = ""
                                }
                                if Language.language == .DEFAULT_LANGUAGE {
                                    if let userLanguage = tmpUser.language {
                                        if let selectedLanguage = Language(rawValue: userLanguage) {
                                            UserDefaults.standard.set([selectedLanguage.rawValue], forKey: appleLanguagesKey)
                                            UserDefaults.standard.synchronize()
                                            UIView.appearance().semanticContentAttribute = selectedLanguage.semantic
                                            UITextField.appearance().textAlignment = selectedLanguage.semantic == .forceLeftToRight ? .left : .right
                                            UITextView.appearance().textAlignment = selectedLanguage.semantic == .forceLeftToRight ? .left : .right
                                            SideMenu.shared.setSideMenu()
                                        
                                        } else {
                                            UserDefaults.standard.set(["en"], forKey: appleLanguagesKey)
                                            UserDefaults.standard.synchronize()
                                            UIView.appearance().semanticContentAttribute = .forceLeftToRight
                                            UITextField.appearance().textAlignment = .left
                                            UITextView.appearance().textAlignment = .left
                                            SideMenu.shared.setSideMenu()
                                        }
                              
                                    }
                                }
                                Constants.BEARER_TOKEN = tmpUser.token
                                BaseViewModel.updateStudentActivity(moduleId: enumModuleType.StudentActivity.rawValue,
                                                                    action: "Logged in student iOS app at \(Date().toString(format: "dd-MM-YYYY hh:mm a"))",
                                                                    description: "Logged in student iOS app at \(Date().toString(format: "dd-MM-YYYY hh:mm a"))")
                                UserDefaults.standard.set(self.userDetails.email, forKey: enumUserDefaultsKey.rememberedUserEmail.rawValue)
                                if let fcmToken = UserDefaults.standard.string(forKey: enumUserDefaultsKey.fcmToken.rawValue) {
                                    BaseViewModel.updateAppToken(token: fcmToken)
                                    print(fcmToken);
                                }
                            }
                        } else {
                            self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                case .failure(let message):
                    self.alertMessage.value = message
                }
            }
        }
    }
    
    //MARK:- Class Methods
    func isValid(email: String) -> Bool {
        let regex : NSString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: email)
    }

    func saveUserInfoAndProceed(user: User){
        Constants.saveUserInfoAndProceed(user: user)
    }
}
//MARK:- Getters and Setters
extension LoginViewModel{
    func getEmail() -> String {
        userDetails.email
    }
    func getPassword() -> String {
        userDetails.password
    }
    func getLanguage() -> String {
        userDetails.languageCode
    }
    func getRememberMe() -> Bool {
        userDetails.rememberMe
    }
    func setEmail(value: String) {
        userDetails.email = value
    }
    func setPassword(value: String) {
        userDetails.password = value
    }
    func setLanguage(value: String) {
        userDetails.languageCode = value
    }
    func setRememberMe(value: Bool) {
        userDetails.rememberMe = value
    }
    func setUserDetails(value: Struct_KeyChainUser) {
        userDetails.email = value.email ?? ""
        userDetails.password = value.password ?? ""
    }
    
}
