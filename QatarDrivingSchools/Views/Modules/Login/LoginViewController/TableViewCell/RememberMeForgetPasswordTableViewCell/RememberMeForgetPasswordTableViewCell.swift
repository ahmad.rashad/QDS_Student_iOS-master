//
//  RememberMeForgetPasswordTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/03/21.
//

import UIKit

class RememberMeForgetPasswordTableViewCell: UITableViewCell {
    
    //MARK:- Variables
    
    @IBOutlet weak var switchRememberMe: UISwitch!
    @IBOutlet weak var buttonRememberMe: UIButton!
    @IBOutlet weak var buttonForgetPassword: UIButton!
    @IBOutlet weak var labelRememberMe: UILabel!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none
        
        labelRememberMe.font = CustomFont.semiBold(ofSize: 14)
        buttonForgetPassword.titleLabel?.font = CustomFont.semiBold(ofSize: 14)
        labelRememberMe.text = Constants.title_rememberMe.localized
        buttonForgetPassword.titleLabel?.textColor = Constants.color_applicationThemeColor
        buttonForgetPassword.setTitle(Constants.title_forgot_password.localized, for: .normal)
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
