//
//  AppIconTVC.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 23/06/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit

class AppIconTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
