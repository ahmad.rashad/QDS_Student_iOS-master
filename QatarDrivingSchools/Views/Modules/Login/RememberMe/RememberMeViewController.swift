//
//  RememberMeViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import UIKit

class RememberMeViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var dataSource: RememberMeDataSource?
    lazy var viewModel = RememberMeViewModel()
    var completionHandler: ((_ user: Struct_KeyChainUser)->Void)?
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = RememberMeDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        viewModel.isLoaderHidden.bind({ [weak self] in
            self?.shouldHideLoader(isHidden: $0)
        })
        viewModel.alertMessage.bind({ [weak self] in
            self?.showAlertWith(message: $0)
        })
        navigationController?.setNavigationBarHidden(true, animated: false)
        labelTitle.text = Constants.description_chooseFromSavedAccount
        labelTitle.font = CustomFont.bold(ofSize: 18)
        labelTitle.textColor = Constants.color_applicationThemeColor
    }
}

