//
//  RememberMeViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import UIKit
import NISLRequestPackages
class RememberMeViewModel {
    
    //MARK:- Variables
    private var users: [Struct_KeyChainUser] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
   // MARK: - Vars & Lets
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
}

//MARK:- Getters and Setters
extension RememberMeViewModel{
    func setUsers(value: [Struct_KeyChainUser]){
        users = value
    }
    func getTotalUsersCount() -> Int {
        users.count
    }
    func getUser(at index: Int) -> Struct_KeyChainUser {
        users[index]
    }
}

