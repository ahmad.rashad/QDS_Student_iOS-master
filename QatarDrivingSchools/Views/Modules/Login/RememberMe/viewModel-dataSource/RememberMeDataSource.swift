//
//  RememberMeDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import Foundation
import UIKit

class RememberMeDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: RememberMeViewController
    private let tableView: UITableView
    private let viewModel: RememberMeViewModel
   
    //MARK:- Init
    init(tableView: UITableView, viewModel: RememberMeViewModel, viewController: RememberMeViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [])
        tableView.reloadData()
    }
    //MARK:- Action methods
   
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getTotalUsersCount()
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "identifier")
        let user = viewModel.getUser(at: indexPath.row)
        cell.textLabel?.text = user.email
        if user.email?.isEmpty ?? true {

        }
        cell.detailTextLabel?.text = "********"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewController.completionHandler?(viewModel.getUser(at: indexPath.row))
        viewController.dismiss(animated: true, completion: nil)
    }
}
