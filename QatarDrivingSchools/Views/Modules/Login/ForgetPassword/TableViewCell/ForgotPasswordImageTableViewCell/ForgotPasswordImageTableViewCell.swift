//
//  ForgotPasswordImageTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 24/06/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit

class ForgotPasswordImageTableViewCell: UITableViewCell {

    //MARK:- Variables
    
    @IBOutlet weak var imageViewTop: UIImageView!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
        labelDescription.font = CustomFont.regular(ofSize: 16)
        labelDescription.textColor = .black
        labelDescription.text = Constants.description_forgotPassword.localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
