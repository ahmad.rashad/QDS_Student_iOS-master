//
//  ForgotPasswordViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import UIKit
import NISLRequestPackages
class ForgotPasswordViewModel {
    
    //MARK:- Variables
   
    struct userDetailsStruct {
        var email = ""
    }
    private var userDetails = userDetailsStruct()
    
    //MARK:- Dependancy Injection
    init() {    }
    
   // MARK: - Vars & Lets
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    
    func forgotPassword() {
        if userDetails.email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_emailMissing.localized)
        }
        else if !isValid(email: userDetails.email) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_emailIsNotValid.localized)
        } else {
            let param: Parameters = [
                "email": userDetails.email
            ]
            self.isLoaderHidden.value = false
            self.apiManager.call(type: RequestItemsType.ForgetPassword, params: param) { (res: Swift.Result<BaseClass<Bool>, AlertMessage>, response, jsonData) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        self.alertMessage.value = AlertMessage(title: Constants.description_mailSent.localized, body: data.message ?? "", isPopRequired: true)
                    } else{
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                case .failure( _):
                    self.alertMessage.value = AlertMessage(title: "", body: Constants.alert_genericErrorMessage.localized)
                }
            }
        }
    }
    
    func isValid(email: String) -> Bool {
        let regex : NSString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        
        return predicate.evaluate(with: email)
    }
}
//MARK:- Getters and Setters
extension ForgotPasswordViewModel{
    func getEmail() -> String {
        return userDetails.email
    }
    
    func setEmail(email: String){
        userDetails.email = email
    }
}
