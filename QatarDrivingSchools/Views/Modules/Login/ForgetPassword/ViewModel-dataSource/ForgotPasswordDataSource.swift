//
//  ForgotPasswordDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import Foundation
import UIKit

class ForgotPasswordDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ForgotPasswordViewController
    private let tableView: UITableView
    private let viewModel: ForgotPasswordViewModel
    
    enum enumForgotPasswordTableRow : Int {
        case description = 0
        case email
        case forgotPassword
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumForgotPasswordTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    //MARK:- Init
    init(tableView: UITableView, viewModel: ForgotPasswordViewModel, viewController: ForgotPasswordViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [ForgotPasswordImageTableViewCell.identifier,
                                         TextFieldTableViewCell.identifier,
                                         ButtonTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonForgotPasswordTouchUpInside(_ sender: UIButton){
        viewModel.forgotPassword()
    }
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumForgotPasswordTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumForgotPasswordTableRow(rawValue: indexPath.row)! {
        case .description:
            return UITableView.automaticDimension
        case .email:
            return 100
        case .forgotPassword:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumForgotPasswordTableRow(rawValue: indexPath.row)! {
        case .description:
            let cell = tableView.dequeueReusableCell(withIdentifier: ForgotPasswordImageTableViewCell.identifier) as! ForgotPasswordImageTableViewCell
            return cell
        case .email:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.labelTitle.text = Constants.placeholder_email.localized
            cell.textField.placeholder = Constants.placeholder_email.localized
            cell.textField.tag = enumForgotPasswordTableRow.email.rawValue
            cell.textField.textColor = .black
            cell.textField.keyboardType = .emailAddress
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            return cell
        case .forgotPassword:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.setTitle(Constants.title_submit.localized, for: .normal)
            cell.button.setTitleColor(.white, for: .normal)
            cell.button.titleLabel?.font = CustomFont.bold(ofSize: 18)
            cell.button.addTarget(self, action: #selector(buttonForgotPasswordTouchUpInside(_:)), for: .touchUpInside)
            viewModel.isLoaderHidden.bind { (isHidden) in
                self.tableView.isUserInteractionEnabled = isHidden
                if isHidden {
                    cell.activityIndicator.stopAnimating()
                }else{
                    cell.activityIndicator.startAnimating()
                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
//MARK:- TextField methods
extension ForgotPasswordDataSource {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumForgotPasswordTableRow(rawValue: textField.tag) {
        case .email:
            viewModel.setEmail(email: textField.text ?? "")
            break
        default:
            break
        }
    }
}
