//
//  ForgetPasswordViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/02/21.
//

import UIKit
import NotificationBannerSwift
class ForgotPasswordViewController: UIViewController {
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var dataSource: ForgotPasswordDataSource?
    lazy var viewModel = ForgotPasswordViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ForgotPasswordDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        viewModel.alertMessage.bind { (message) in
            if message.body == Constants.alert_emailMissing || message.body == Constants.alert_emailIsNotValid {
                let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
                banner.show()
            }else{
                if message.title == Constants.description_mailSent {
                    let alertVC = AlertViewController.loadFromNib()
                    alertVC.viewModel.setTitle(value: message.title)
                    alertVC.viewModel.setSubTitle(value: message.body)
                    alertVC.modalPresentationStyle = .overFullScreen
                    alertVC.callBackToCloseParentScreen = {
                        self.navigationController?.popViewController(animated: true)
                    }
                    self.present(alertVC, animated: true, completion: nil)
                }else{
                    self.showAlertWith(message: message)
                }
            }
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
        labelTitle.text = Constants.title_forgotPassword.localized
        labelTitle.font = CustomFont.bold(ofSize: 18)
    }
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

