//
//  CarouselDataSource.swift
//  Bubbles
//
//  Created by C100-107 on 20/03/21.
//

import Foundation
import UIKit
import SDWebImage
import Photos
class CarouselEventsDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
      
    private let viewController: CarouselViewController
    private let collectionView: UICollectionView
    private let viewModel: CarouselEventsViewModel

    private var previousIndex = 0
    
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: CarouselEventsViewModel, viewController: CarouselViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView() {
        registerCollectionCell()
        collectionView.isPagingEnabled = true
        collectionView.reloadData()
    }
    
    func registerCollectionCell() {
        collectionView.registerNib(nibNames: [ImageCollectionViewCell.identifier])
    }
    
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch viewController.mediaType {
        case .PHAsset:
            let count = viewController.media.count
            viewController.pageControl.numberOfPages = count
            viewController.pageControl.currentPage = viewController.index
            return count
        case .URL:
            let count = viewController.urls.count
            viewController.pageControl.numberOfPages = count
            viewController.pageControl.currentPage = viewController.index
            return count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as! ImageCollectionViewCell
        switch viewController.mediaType {
        case .PHAsset:
            let media = viewController.media[indexPath.row]
            switch media.mediaType {
            case .image:
                PHCachingImageManager().requestImage(for: media, targetSize: CGSize(width: cell.width, height: cell.height), contentMode: .aspectFill, options: nil) { (image, info) in
                    DispatchQueue.main.async {
                        cell.imageViewMedia?.contentMode = .scaleAspectFill
                        cell.imageViewMedia?.image = image
                    }
                }
            case .video:
                break
            default:
                print("Unknown")
            }
        case .URL:
           let url = Constants.getPathOfMedia(value: viewController.urls[indexPath.row].name)
            switch enumMediaType(rawValue: viewController.urls[indexPath.row].type ?? 0) {
            case .image:
                cell.imageViewMedia.sd_setImage(with: url, completed: nil)
            case .video:
                break
            default:
                break
            }
            
        }
       /* let url = Constants.getEventMediaURL(value: media.name ?? "")
        cell.imageViewMedia.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageViewMedia.sd_setImage(with: url, placeholderImage: Constants.image_placeholder, options: .refreshCached, completed: nil)*/
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        previousIndex = viewController.pageControl.currentPage
        viewController.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewController.pageControl.currentPage == indexPath.row {
            viewController.pageControl.currentPage = previousIndex
        }
    }
}
extension CarouselViewController: UIScrollViewDelegate {
    
}
