//
//  CarouselViewModel.swift
//  Bubbles
//
//  Created by C100-107 on 20/03/21.
//

import UIKit
import NISLRequestPackages

class CarouselEventsViewModel {
    
    //MARK:- Variables
    
    //MARK:- Dependancy Injection
    init() {    }
    
    // MARK: - Vars & Lets
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Class methods
    
    //MARK:- API methods
 
}
//MARK:- Getters and Setters
extension CarouselEventsViewModel{

}

