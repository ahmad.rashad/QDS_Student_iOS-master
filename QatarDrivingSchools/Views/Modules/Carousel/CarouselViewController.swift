//
//  CarouselViewController.swift
//  Bubbles
//
//  Created by C100-107 on 20/03/21.
//

import UIKit
import Photos
class CarouselViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var dataSource: CarouselEventsDataSource?
    lazy var viewModel = CarouselEventsViewModel()
    
    var media: [PHAsset] = []
    var urls: [Media] = []
    var index: Int = 0
    
    enum enumMediaType {
        case PHAsset
        case URL
    }
    var mediaType: enumMediaType = .PHAsset
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = CarouselEventsDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        print(IndexPath(row: index, section: 0))
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: false)
    }
    
    //MARK:- Action Methods
    @IBAction func buttonCloseTouchUpInside(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        collectionView.scrollToItem(at: IndexPath(row: sender.currentPage, section: 0), at: .left, animated: true)
    }
    @IBAction func buttonShareTouchUpInside(_ sender: RoundButton) {
        let text = "This is some text that I want to share."
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func buttonDownloadImageTouchUpInside(_ sender: RoundButton) {
      /*  let cell = collectionView.cellForItem(at: IndexPath(row: pageControl.currentPage, section: 0)) as? ImageCollectionViewCell
        
        if let pickedImage = cell?.imageViewMedia.image {
            UIImageWriteToSavedPhotosAlbum(pickedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }else{
            let ac = UIAlertController(title: Constants.alert_title_error, message: Constants.alert_tryAgainAfterImageLoad, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            present(ac, animated: true)
        }*/
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: Constants.alert_title_error, message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: Constants.alert_title_saved, message: Constants.alert_savedMessage, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            present(ac, animated: true)
        }
    }
}

