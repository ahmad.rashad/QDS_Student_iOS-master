//
//  ContactInfoViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//


import UIKit

class ContactInfoViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: ContactInfoDataSource?
    lazy var viewModel = ContactInfoViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_contactInfo.localized
                
        dataSource = ContactInfoDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
