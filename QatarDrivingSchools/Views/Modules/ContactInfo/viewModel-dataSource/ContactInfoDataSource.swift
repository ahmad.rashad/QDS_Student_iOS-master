//
//  ContactInfoDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import Foundation
import UIKit

class ContactInfoDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ContactInfoViewController
    private let tableView: UITableView
    private let viewModel: ContactInfoViewModel
    
    enum enumContactInfoTableRow: Int {
        case mobile
        case email
        case address
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumContactInfoTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: ContactInfoViewModel, viewController: ContactInfoViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         TextViewTableViewCell.identifier,])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumContactInfoTableRow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumContactInfoTableRow(rawValue: indexPath.row)! {
        case .mobile:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.description_mobile.localized,
                           text: Constants.loggedInUser?.phoneNumber ?? "",
                           placeholder: Constants.description_mobile.localized)
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .email:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_email.localized,
                           text: Constants.loggedInUser?.emailId ?? "",
                           placeholder: Constants.placeholder_email.localized)
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .address:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextViewTableViewCell.identifier) as! TextViewTableViewCell
            cell.setupData(title: Constants.description_address.localized,
                           text: Constants.loggedInUser?.address ?? "",
                           placeholder: Constants.description_address.localized)
            //cell.textView.delegate = self
            cell.textView.isUserInteractionEnabled = false
            return cell
        }
    }
}

extension ContactInfoDataSource: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.placeholder_enterDetails
            textView.textColor = UIColor.lightGray
        }
    }
}


