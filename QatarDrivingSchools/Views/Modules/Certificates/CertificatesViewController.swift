//
//  CertificatesViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 17/09/21.
//

import UIKit
import MessageUI
class CertificatesViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var labelDownload: UILabel!
    @IBOutlet weak var labelSendEmail: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viewDownload: UIView!
    @IBOutlet weak var viewEmail: UIView!
    
    var dataSource: CertificatesDataSource?
    lazy var viewModel = CertificatesViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = CertificatesDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        setupLayout()
        
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //SideMenu.shared.showSideMenu(sender: sender)
    }
    @IBAction func buttonDownloadTouchUpInside(_ sender: UIButton) {
        let cell = collectionView.cellForItem(at: IndexPath(item: pageControl.currentPage, section: 0)) as? MediaCollectionViewCell
        if let pickedImage = cell?.imageViewMedia.image {
            viewModel.isLoaderHidden.value = false
            UIImageWriteToSavedPhotosAlbum(pickedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }else{
            showPleaseLetImageLoadAlert()
        }
    }
    @IBAction func buttonEmailTouchUpInside(_ sender: UIButton) {
        let cell = collectionView.cellForItem(at: IndexPath(item: pageControl.currentPage, section: 0)) as? MediaCollectionViewCell
        if let pickedImage = cell?.imageViewMedia.image {
            sendMail(image: pickedImage)
        }else{
            showPleaseLetImageLoadAlert()
        }
    }
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        collectionView.scrollToItem(at: IndexPath(row: sender.currentPage, section: 0), at: .left, animated: true)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        viewModel.isLoaderHidden.value = true
        if let error = error {
            let ac = UIAlertController(title: Constants.alert_title_error, message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: Constants.alert_title_saved, message: Constants.alert_savedMessage, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            present(ac, animated: true)
        }
    }
    //MARK:- Class Methods
    func setupLayout(){
        labelTitle.text = Constants.description_certificate
        labelTitle.font = CustomFont.semiBold()
        labelNoData.font = CustomFont.semiBold()
        
        labelDownload.font = CustomFont.semiBold(ofSize: 18)
        labelSendEmail.font = CustomFont.semiBold(ofSize: 18)
        labelDownload.text = Constants.description_download
        labelSendEmail.text = Constants.description_sendEmail
        
        viewModel.noCertificateMessage.bind { message in
            self.labelNoData.text = message
        }
        viewModel.isCertificateDataFetched.bind { isFetched in
            if isFetched {
                self.collectionView.reloadData()
            }
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        //viewModel.getCertificates()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func showPleaseLetImageLoadAlert() {
        let ac = UIAlertController(title: Constants.alert_title_error, message: Constants.alert_tryAgainAfterImageLoad, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
        present(ac, animated: true)
    }
    func sendMail(image: UIImage) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setCcRecipients([Constants.loggedInUser?.emailId ?? ""])
            mail.setSubject("Certificate")
            mail.setMessageBody("", isHTML: false)
            if let imageData: Data = image.pngData() {
                mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "certificate.png")
                self.present(mail, animated: true, completion: nil)
            }
        }
    }
}
//MARK:- Mail Delegate
extension CertificatesViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
