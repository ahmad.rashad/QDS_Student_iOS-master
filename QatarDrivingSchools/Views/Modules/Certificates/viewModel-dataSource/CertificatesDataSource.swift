//
//  CertificatesDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 17/09/21.
//

import Foundation
import UIKit

class CertificatesDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let viewController: CertificatesViewController
    private let collectionView: UICollectionView
    private let viewModel: CertificatesViewModel
   
    private var previousIndex = 0
    
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: CertificatesViewModel, viewController: CertificatesViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView(){
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        registerCollectionCell()
    }
    func registerCollectionCell(){
        collectionView.registerNib(nibNames: [MediaCollectionViewCell.identifier])
        collectionView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = viewModel.getTotalCertificatesCount()
        viewController.pageControl.numberOfPages = count
        viewController.viewDownload.isHidden = count == 0
        viewController.viewEmail.isHidden = count == 0
        return count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return  CGSize(width: collectionView.frame.width, height: collectionView.frame.height)//CGSize(width: collectionView.frame.width / 4 - 8, height: 180)
        }
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediaCollectionViewCell.identifier, for: indexPath) as! MediaCollectionViewCell
        let certificate = viewModel.getCertificate(at: indexPath.row)//viewModel.getCertificate(at: indexPath.row)
        //let imageURL = Constants.getPathOfMedia(value: certificate.certificateLink ?? "")
        cell.imageViewMedia.contentMode = .scaleAspectFill
        //cell.imageViewMedia.sd_setImage(with: Constants.getPathOfMedia(value: certificate.certificateLink ?? ""), placeholderImage: Constants.image_noData, options: [], context: nil)
        cell.imageViewMedia.sd_setImage(with: Constants.getPathOfMedia(value: certificate.certificateLink ?? ""), placeholderImage: Constants.image_noData, options: []) { image, error, cache, url in
            cell.imageViewMedia.contentMode = .scaleAspectFit
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        previousIndex = viewController.pageControl.currentPage
        viewController.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewController.pageControl.currentPage == indexPath.row {
            viewController.pageControl.currentPage = previousIndex
        }
    }
}

