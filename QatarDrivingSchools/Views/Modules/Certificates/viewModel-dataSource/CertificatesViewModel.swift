//
//  CertificatesViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 17/09/21.
//

import UIKit
import NISLRequestPackages

class CertificatesViewModel {
    
    //MARK:- Variables
    private var selectedCategory = 0
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isCertificateDataFetched: Dynamic<Bool> = Dynamic(true)
    var noCertificateMessage: Dynamic<String> = Dynamic("")
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var certificatesList: [Certificates] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - API
    func getCertificates() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetCertificate, queryParameter: param) { (res: Swift.Result<BaseClass<CertificatesData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.certificatesList.append(contentsOf: data.data?.certificates ?? [])
                } else {
                    self.noCertificateMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isCertificateDataFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isCertificateDataFetched.value = false
            }
        }
    }
}
//MARK:- Getters and Setters
extension CertificatesViewModel {
    func getTotalCertificatesCount() -> Int {
        certificatesList.count
    }
    func getCertificate(at index: Int) -> Certificates {
        certificatesList[index]
    }
    func setCertificates(value: [Certificates]) {
        certificatesList = value
    }
}

