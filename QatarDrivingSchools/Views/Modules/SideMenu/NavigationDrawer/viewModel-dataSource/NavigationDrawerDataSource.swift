//
//  NavigationDrawerDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import Foundation
import UIKit

class NavigationDrawerDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: NavigationDrawerViewController
    private let tableView: UITableView
    private let viewModel: NavigationDrawerViewModel
    
    enum enumNavigationDrawerTableRow: Int{
        case dashboard
        case profile
        case course
        case eBook
        case complain
        case contract
        case invoice
        //case certificate
        case appointment
        case learningProgress
        case contactUS
        case QatarId
        case logout
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumNavigationDrawerTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    //MARK:- Init
    init(tableView: UITableView, viewModel: NavigationDrawerViewModel, viewController: NavigationDrawerViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [NavigationDrawerTableViewCell.identifier])
        tableView.reloadData()
    }
    func doLogout() {
        viewModel.updateLastVisitDate()
        viewController.showAlertAndDoLogout(message: Constants.alert_tryToLogout.localized)
        /*Constants.isUserLoggedIn = false
        Constants.loggedInUser = nil
        GlobalVariables.appDelegate?.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())*/
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumNavigationDrawerTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NavigationDrawerTableViewCell.identifier) as! NavigationDrawerTableViewCell
        var title = ""
        var image = #imageLiteral(resourceName: "profile_account_details")
        switch enumNavigationDrawerTableRow(rawValue: indexPath.row)! {
        case .dashboard:
            title = Constants.description_dashboard.localized
            image = #imageLiteral(resourceName: "menu_dashboard_ic")
        case .profile:
            title = Constants.description_profile.localized
            image = #imageLiteral(resourceName: "menu_profile_ic")
        case .course:
            title = Constants.description_course.localized
            image = #imageLiteral(resourceName: "menu_course_ic")
        case .eBook:
            title = Constants.description_eBook.localized
            image = #imageLiteral(resourceName: "menu_e-book_ic")
        case .complain:
            title = Constants.description_complain.localized
            image = #imageLiteral(resourceName: "menu_complain_ic")
        case .contract:
            title = Constants.description_contract.localized
            image = #imageLiteral(resourceName: "menu_contract_ic")
        case .invoice:
            title = Constants.description_invoice.localized
            image = #imageLiteral(resourceName: "menu_invoice_ic")
        /*case .certificate:
            title = Constants.description_certificate
            image = #imageLiteral(resourceName: "menu_certificate_ic")*/
        case .appointment:
            title = Constants.description_appointment.localized
            image = #imageLiteral(resourceName: "menu_appointment_ic")
        case .learningProgress:
            title = Constants.description_learningProgress.localized
            image = #imageLiteral(resourceName: "menu_learning_progress_ic")
        case .contactUS:
            title = Constants.description_contactUS.localized
            image = #imageLiteral(resourceName: "menu_contact_us_ic")
        case .QatarId:
            title = Constants.placeholder_personalQatariID.localized
            image = #imageLiteral(resourceName: "qr_code_ic")
        case .logout:
            title = Constants.description_logout.localized
            image = #imageLiteral(resourceName: "menu_logout_ic")
        }
        cell.imgView.image = image
        cell.labelTitle.text = title
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumNavigationDrawerTableRow(rawValue: indexPath.row)! {
        case .dashboard:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: DashboardViewController.loadFromNib())
        case .profile:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: ProfileViewController.loadFromNib())
        case .course:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: CoursesViewController.loadFromNib())
        case .eBook:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: EBookViewController.loadFromNib())
        case .complain:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: ComplaintsViewController.loadFromNib())
        case .contract:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: ContractViewController.loadFromNib())
        case .invoice:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: InvoiceViewController.loadFromNib())
        /*case .certificate:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: CertificatesViewController.loadFromNib())*/
        case .appointment:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: AppointmentCalendarViewController.loadFromNib())
        case .learningProgress:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: StudentLearningProgressViewController.loadFromNib())
        case .contactUS:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: ContactUSViewController.loadFromNib())
        case .QatarId:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController:  QRCodeViewController.loadFromNib())
        case .logout:
            doLogout()
        }
        SideMenu.shared.hideSideMenu()
    }
    
}

