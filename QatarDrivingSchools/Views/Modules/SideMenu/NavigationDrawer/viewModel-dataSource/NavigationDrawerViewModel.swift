//
//  NavigationDrawerViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit
import NISLRequestPackages
class NavigationDrawerViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- API
    func updateLastVisitDate() {
        let param: Parameters = [
                "emailId": Constants.getLoggedInUserEmailForAPI(),
                "S_Code":Constants.loggedInUser?.scode ?? "",
            ]


            apiManager.upload(type: RequestItemsType.UpdateLastVisitDate, params: param) { (res: Swift.Result<BaseClass<UserData>, AlertMessage>, response) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
//                        if let user = data.data {
//                            if let tmpUser = user.user?.first{
//                                Constants.loggedInUser = tmpUser
//                                self.isPopRequired.value = true
//                            }
//                        } else {
//                            self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
//                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    break
                case .failure(let message):
                    print(message)
                    //self.alertMessage.value = message
                    break
                //}
            }
        }
    }
    
   /* func logout() {
        let param: Parameters = [
            "emailId": userDetails.email,
            "password": userDetails.password,
            "languageCode": userDetails.languageCode,
            "rememberMe": userDetails.rememberMe
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.Logout, params: param) { (res: Swift.Result<BaseClass<UserData>, AlertMessage>, response) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if let user = data.data {
                        if let tmpUser = user.user?.first{
                            self.saveUserInfoAndProceed(user: tmpUser)
                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }*/
}
