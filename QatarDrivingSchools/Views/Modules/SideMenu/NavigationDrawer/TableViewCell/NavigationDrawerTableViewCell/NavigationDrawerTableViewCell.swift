//
//  NavigationDrawerTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class NavigationDrawerTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.medium()
        labelTitle.textColor = .white
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
