//
//  NavigationDrawerViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import UIKit
import NotificationBannerSwift
class NavigationDrawerViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: NavigationDrawerDataSource?
    lazy var viewModel = NavigationDrawerViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = NavigationDrawerDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        //setupLayout()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupLayout()
    }
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.hideSideMenu()
    }
    
    //MARK:- Class Methods
    func setupLayout(){
        let topColor = Constants.color_applicationThemeColor
        let bottomColor = Constants.color_darkApplicationThemeColor
        let gradientLayer: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            gradientLayer.frame = CGRect.zero
            return gradientLayer
        }()
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    

}

