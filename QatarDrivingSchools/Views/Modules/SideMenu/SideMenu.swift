//
//  SideMenu.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import LGSideMenuController

var HomeNavigationController = UINavigationController()

class SideMenu{
    
    static var shared = SideMenu()
    var sideMenuController : LGSideMenuController!
    
    func setSideMenu() {
        Language.language = Language.language
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let SideDrawerVC = NavigationDrawerViewController.loadFromNib()
        
        let HomeController = DashboardViewController.loadFromNib()
        HomeNavigationController = UINavigationController(rootViewController: HomeController)
        HomeNavigationController.setNavigationBarHidden(true, animated: false)
        
       
        
        switch Language.language {
        case .ARABIC, .PERSIAN, .PASHTO, .URDU:
            sideMenuController = LGSideMenuController(rootViewController: HomeNavigationController,
                                                      leftViewController: nil,
                                                      rightViewController: SideDrawerVC)
            if(UIScreen.main.bounds.height < 650 )
            {
                sideMenuController.rightViewWidth = 280
            }
            else
            {
                sideMenuController.rightViewWidth = 300
            }
            sideMenuController.isRightViewStatusBarHidden = true
            sideMenuController.leftViewBackgroundColor = UIColor.white
            sideMenuController.isRightViewSwipeGestureDisabled = true
            sideMenuController.isLeftViewSwipeGestureDisabled = false
        default:
            sideMenuController = LGSideMenuController(rootViewController: HomeNavigationController,
                                                      leftViewController: SideDrawerVC,
                                                      rightViewController: nil)
            if(UIScreen.main.bounds.height < 650 )
            {
                sideMenuController.leftViewWidth = 280
            }
            else
            {
                sideMenuController.leftViewWidth = 300
            }
            sideMenuController.isLeftViewStatusBarHidden = true
            sideMenuController.rightViewBackgroundColor = UIColor.white
            sideMenuController.isLeftViewSwipeGestureDisabled = true
            sideMenuController.isRightViewSwipeGestureDisabled = false
        }
        //sideMenuController.leftViewPresentationStyle = .slideAbove
        //sideMenuController.leftViewBackgroundBlurEffect = UIBlurEffect(style: .extraLight)
        //sideMenuController.leftViewBackgroundColor = UIColor.white
       
        
        //   sideMenuController.swipeGestureArea = .borders
        appDelegate.window?.rootViewController = sideMenuController
    }
    
    func showSideMenu(sender: UIButton){
        switch Language.language {
        case .ARABIC, .PERSIAN, .PASHTO, .URDU:
            SideMenu.shared.sideMenuController.showRightViewAnimated(sender: sender)
        default:
            SideMenu.shared.sideMenuController.showLeftViewAnimated(sender: sender)
        }
    }
    func hideSideMenu(){
        switch Language.language {
        case .ARABIC, .PERSIAN, .PASHTO, .URDU:
            SideMenu.shared.sideMenuController.hideRightView()
        default:
            SideMenu.shared.sideMenuController.hideLeftView()
        }
    }
}

