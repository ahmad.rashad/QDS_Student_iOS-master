//
//  AppointmentHistoryViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/09/21.
//

import UIKit

class AppointmentHistoryViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: AppointmentHistoryDataSource?
    lazy var viewModel = AppointmentHistoryViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = AppointmentHistoryDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_history
        labelTitle.font = CustomFont.medium(ofSize: 18)
       
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isLearningProgressFetched.bind { isFetched in
            self.tableView.reloadData()
        }
        viewModel.noLearningProgressMessage.bind { message in
            print(message)
        }
        viewModel.getAppointmentRequest()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
