//
//  AppointmentHistoryViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/09/21.
//

import UIKit
import NISLRequestPackages
class AppointmentHistoryViewModel {
    
    //MARK:- Variables
    var isLearningProgressFetched: Dynamic<Bool> = Dynamic(true)
    var noLearningProgressMessage: Dynamic<String> = Dynamic("")
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var isMoreDataAvailable = true
    private var appointments: [Appointments] = []
    private var offset: Int = 0
    private var appointmentCategories: [AppoinmentCategories] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - API
    func getAppointmentRequest() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetAppointmentsRequestList, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if self.offset == 0 {
                        self.appointments.removeAll()
                    }
                    let list = data.data?.appointments ?? []
                    self.offset += 1
                    self.appointments.append(contentsOf: list)
                    if list.count == 0{
                        self.isMoreDataAvailable = false
                    }
                } else {
                    self.isMoreDataAvailable = false
                    self.noLearningProgressMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isLearningProgressFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isLearningProgressFetched.value = false
            }
        }
    }
}

//MARK:- Getters and Setters
extension AppointmentHistoryViewModel {
    func getTotalAppointmentCount() -> Int {
        appointments.count
    }
    func getAppointment(at index: Int) -> Appointments {
        appointments[index]
    }
    func isMoreDataAvailableInAppointment() -> Bool {
        isMoreDataAvailable
    }
    func getAllAppointmentCategories() -> [AppoinmentCategories] {
        appointmentCategories
    }
    func setAppointmentCategories(value: [AppoinmentCategories]) {
        appointmentCategories = value
    }
    func removeAppointment(at index: Int) {
        appointments.remove(at: index)
    }
}



