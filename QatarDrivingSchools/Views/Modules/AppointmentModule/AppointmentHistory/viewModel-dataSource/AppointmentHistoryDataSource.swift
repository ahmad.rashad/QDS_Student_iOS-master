//
//  AppointmentHistoryDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/09/21.
//

import Foundation
import UIKit

class AppointmentHistoryDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: AppointmentHistoryViewController
    private let tableView: UITableView
    private let viewModel: AppointmentHistoryViewModel
    
    enum enumStudentLearningSectionType: Int {
        case learningProgress = 0
        case noData
        static let count: Int = {
            var max: Int = 0
            while let _ = enumStudentLearningSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: AppointmentHistoryViewModel, viewController: AppointmentHistoryViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [ComplaintsTableViewCell.identifier,
                                         ImageTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonEditTouchUpInside(_ sender: UIButton) {
        let editAppointmentVC = NewAppointmentViewController.loadFromNib()
        editAppointmentVC.viewControllerType = .edit
        editAppointmentVC.viewModel.setAppointmentCategories(value: viewModel.getAllAppointmentCategories())
        editAppointmentVC.appointmentIndex = sender.tag
        editAppointmentVC.completionHandlerForCancelAppointment = { index in
            if index < self.viewModel.getTotalAppointmentCount() {
                self.viewModel.removeAppointment(at: index)
                self.tableView.reloadData()
            }
        }
        editAppointmentVC.viewModel.setAppointmentDetails(value: viewModel.getAppointment(at: sender.tag))
        self.viewController.navigationController?.pushViewController(editAppointmentVC, animated: true)
    }
    
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumStudentLearningSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumStudentLearningSectionType(rawValue: section)! {
        case .learningProgress:
            return viewModel.getTotalAppointmentCount()
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalAppointmentCount() == 0 ? 1 : 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumStudentLearningSectionType(rawValue: indexPath.section)! {
        case .learningProgress:
            let cell = tableView.dequeueReusableCell(withIdentifier: ComplaintsTableViewCell.identifier) as! ComplaintsTableViewCell
            cell.setupLayoutForAppointmentHistory(appointment: viewModel.getAppointment(at: indexPath.row))
            cell.buttonEdit.tag = indexPath.row
            cell.buttonEdit.addTarget(self, action: #selector(buttonEditTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        let count = viewModel.getTotalAppointmentCount()
        
        let isLastCell = count - currentRow == 1
        let minY = tableView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInAppointment() && isLastCell && minY > 0 {
            //.viewModel.getAppointmentRequest()
        }
    }
}
