//
//  AppointmentSearchViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit
class AppointmentSearchViewModel {
    
    //MARK:- Variables
    private var selectedCategory = 0
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - Vars & Lets
 
}
//MARK:- Getters and Setters
extension AppointmentSearchViewModel {
    func getSelectedCategory() -> Int {
        selectedCategory
    }
    func setSelectedCategory(value: Int) {
        selectedCategory = value
    }
}

