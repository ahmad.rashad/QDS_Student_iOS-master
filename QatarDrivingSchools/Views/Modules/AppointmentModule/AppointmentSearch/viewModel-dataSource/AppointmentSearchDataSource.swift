//
//  AppointmentSearchDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import Foundation
import UIKit

class AppointmentSearchDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController: AppointmentSearchViewController
    private let tableView: UITableView
    private let viewModel: AppointmentSearchViewModel
    
    enum enumAppointmentRowType: Int {
        case searchIn = 0
        case what
        case who
        case whereLocation
        case whereEvent
        case dateTitle
        case dateFrom
        case dateTo
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAppointmentRowType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: AppointmentSearchViewModel, viewController: AppointmentSearchViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         LabelTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumAppointmentRowType.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
        switch enumAppointmentRowType(rawValue: indexPath.row)! {
        case .searchIn:
            cell.setupData(title: Constants.placeholder_searchIn,
                           placeholder: Constants.placeholder_searchIn,
                           rightImage: #imageLiteral(resourceName: "dropdown_ic"))
        case .what:
            cell.setupData(title: Constants.placeholder_what,
                           placeholder: Constants.placeholder_whatDesc)
        case .who:
            cell.setupData(title: Constants.placeholder_who,
                           placeholder: Constants.placeholder_whoDesc)
        case .whereLocation:
            cell.setupData(title: Constants.placeholder_where,
                           placeholder: Constants.placeholder_whereDesc)
        case .whereEvent:
            cell.setupData(title: Constants.placeholder_where,
                           placeholder: Constants.placeholder_whereDescEvent)
        case .dateTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.text = Constants.description_date
            return cell
        case .dateFrom:
            cell.setupData(title: Constants.description_from,
                           placeholder: Constants.placeholder_date,
                           rightImage: #imageLiteral(resourceName: "calendar_ic"))
        case .dateTo:
            cell.setupData(title: Constants.description_to,
                           placeholder: Constants.placeholder_date,
                           rightImage: #imageLiteral(resourceName: "calendar_ic"))
        }
        return cell
    }
}
extension AppointmentSearchDataSource: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.placeholder_enterDescription
            textView.textColor = UIColor.lightGray
        }
    }
}


