//
//  AppointmentSearchSearchViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//


import UIKit

class AppointmentSearchViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSearch: RoundButton!
    @IBOutlet weak var buttonReset: RoundButton!
    
    var dataSource: AppointmentSearchDataSource?
    lazy var viewModel = AppointmentSearchViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.text = Constants.title_appointmentSearch
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        dataSource = AppointmentSearchDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        buttonReset.setTitle(Constants.title_reset.localized, for: .normal)
        buttonSearch.setTitle(Constants.title_searchInCaps.localized, for: .normal)
        
        buttonReset.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        buttonSearch.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
}
