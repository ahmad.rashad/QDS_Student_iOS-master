//
//  AppointmentCalendarViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class AppointmentCalendarViewController: UIViewController {
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var buttonAddAppointment: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource:  AppointmentCalendarDataSource?
    lazy var viewModel =  AppointmentCalendarViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource =  AppointmentCalendarDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
  
    @objc func buttonAddAppointmentTouchUpInside(_ sender: UIButton) {
        let NewAppointmentVC = NewAppointmentViewController.loadFromNib()
        NewAppointmentVC.viewModel.setAppointmentCategories(value: viewModel.getAllAppointmentCategories())
        navigationController?.pushViewController(NewAppointmentVC, animated: true)
    }
    @IBAction func buttonAppointmentHistoryTouchUpInside(_ sender: UIButton) {
        let appointmentHistory = AppointmentHistoryViewController.loadFromNib()
        appointmentHistory.viewModel.setAppointmentCategories(value: viewModel.getAllAppointmentCategories())
        navigationController?.pushViewController(appointmentHistory, animated: true)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        
        labelTitle.text = Constants.description_appointment.localized
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        labelSubTitle.text = Date().toString(format: "MMMM").uppercased()
        labelSubTitle.textColor = Constants.color_darkApplicationThemeColor
        labelSubTitle.font = CustomFont.semiBold(ofSize: 18)
      
        buttonAddAppointment.titleLabel?.font = CustomFont.semiBold()
        buttonAddAppointment.setTitle(Constants.title_newAppointmentInCaps.localized, for: .normal)
        buttonAddAppointment.addTarget(self, action: #selector(buttonAddAppointmentTouchUpInside(_:)), for: .touchUpInside)
        viewModel.isAppointmentCategoriesFetched.bind { isFetched in
            self.tableView.reloadData()
        }
        viewModel.isAppointmentsFetched.bind { _ in
            self.tableView.reloadData()
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.getAppointmentCategories()
        viewModel.getAppointments(startDate: Date().getSpecificDate(byAdding: .month, value: -1).toString(format: "yyyy-MM-dd"),
                                  endDate: Date().getSpecificDate(byAdding: .month, value: 1).toString(format: "yyyy-MM-dd"))
    }
}
