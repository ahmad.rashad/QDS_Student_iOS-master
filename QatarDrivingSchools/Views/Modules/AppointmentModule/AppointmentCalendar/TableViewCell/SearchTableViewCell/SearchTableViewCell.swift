//
//  SearchTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 13/07/21.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        textField.font = CustomFont.regular()
        textField.placeholder = Constants.title_search
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
