//
//  AppointmentCalendarViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit
import Foundation
import NISLRequestPackages
class AppointmentCalendarViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isAppointmentCategoriesFetched: Dynamic<Bool> = Dynamic(true)
    var isAppointmentsFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    private var appointmentCategories: [AppoinmentCategories] = []
    private var appointments: [Appointments] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - APIs
    func getAppointmentCategories() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? ""
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetAppointmentsCategories, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentCategoriesBaseData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.appointmentCategories = data.data?.appoinmentCategories ?? []
                } else {
                }
                self.isAppointmentCategoriesFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isAppointmentCategoriesFetched.value = false
            }
        }
    }
    
    func getAppointments(startDate: String, endDate: String) {
        let param: Parameters = [
            "StartDate":startDate,
            "EndDate":endDate,
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetAllAppointments, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData
        ) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.appointments = data.data?.appointments ?? []
                } else {
                }
                self.isAppointmentsFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isAppointmentsFetched.value = false
            }
        }
    }

}
//MARK:- Getters and Setters
extension AppointmentCalendarViewModel {
    
    func getTotalCategoriesCount() -> Int {
        appointmentCategories.count
    }
    func getTotalAppointmentCount() -> Int {
        appointments.count
    }
    func getAppointmentCategory(at index: Int) -> AppoinmentCategories {
        appointmentCategories[index]
    }
    func getAppointment(at index: Int) -> Appointments {
        appointments[index]
    }
    func getAllAppointmentCategories() -> [AppoinmentCategories] {
        appointmentCategories
    }
    func numberOfAppointments(in date: String) -> Int {
        appointments.filter { appointment in
            if appointment.meetingDate?.toLocalDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "yyyy-MM-dd") == date {
                return true
            }
            return false
        }.count
    }
    func getAppointmentsForSelectedDate(date: String) -> [Appointments] {
        appointments.filter { appointment in
            if appointment.meetingDate?.toLocalDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "yyyy-MM-dd") == date {
                return true
            }
            return false
        }
    }
    func colorsForAppointment(in date: String) -> [UIColor] {
        let appointments = appointments.filter { appointment in
            if appointment.meetingDate?.toLocalDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "yyyy-MM-dd") == date {
                return true
            }
            return false
        }
        var colors: [UIColor] = []
        for appointment in appointments {
            colors.append(UIColor(hexStr: "\(appointment.eventColorCode ?? "#902C5D")ff") ?? UIColor())
        }
        return colors
    }
}
