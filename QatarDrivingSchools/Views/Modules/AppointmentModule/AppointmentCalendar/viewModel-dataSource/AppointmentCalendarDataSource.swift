//
//   AppointmentCalendarCalendarDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import Foundation
import UIKit
import FSCalendar
class  AppointmentCalendarDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController:  AppointmentCalendarViewController
    private let tableView: UITableView
    private let viewModel:  AppointmentCalendarViewModel
    
    enum enumAppointmentCalendarRowType: Int {
        case search
        case calendar
        case type
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAppointmentCalendarRowType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel:  AppointmentCalendarViewModel, viewController:  AppointmentCalendarViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TextViewTableViewCell.identifier,
                                         CalendarTableViewCell.identifier,
                                         CollectionWithTitleTableViewCell.identifier,
                                         SearchTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumAppointmentCalendarRowType.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumAppointmentCalendarRowType(rawValue: indexPath.row)! {
        case .search:
            return 0//UITableView.automaticDimension
        case .calendar:
            return 350
        case .type:
            return 200
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumAppointmentCalendarRowType(rawValue: indexPath.row)! {
        case .search:
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier) as! SearchTableViewCell
            return cell
        case .calendar:
            let cell = tableView.dequeueReusableCell(withIdentifier: CalendarTableViewCell.identifier) as! CalendarTableViewCell
            cell.calendar.delegate = self
            cell.calendar.dataSource = self
            cell.calendar.reloadData()
            return cell
        case .type:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.registerNib(nibNames: [AppointmentTypeCollectionViewCell.identifier])
            if let layout = cell.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
            cell.collectionView.reloadData()
            return cell
        }
    }
}
//MARK:- CollectionView delegate methods
extension AppointmentCalendarDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTotalCategoriesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppointmentTypeCollectionViewCell.identifier, for: indexPath) as! AppointmentTypeCollectionViewCell
        let detail = viewModel.getAppointmentCategory(at: indexPath.row)
        cell.setupData(color: detail.colorCode ?? "#ffffff", title: detail.type ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 10, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(0.0)
    }
}
//MARK:- TextView delegate
extension AppointmentCalendarDataSource: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.placeholder_enterDescription.localized
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK:- Calendar delegate
extension AppointmentCalendarDataSource: FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.viewController.labelSubTitle.text = calendar.currentPage.toString(format: "MMMM").uppercased()
        viewModel.getAppointments(startDate:  calendar.currentPage.getSpecificDate(byAdding: .month, value: -1).toString(format: "yyyy-MM-dd"),
                                  endDate:  calendar.currentPage.getSpecificDate(byAdding: .month, value: 1).toString(format: "yyyy-MM-dd"))
    }
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        viewModel.numberOfAppointments(in: date.toString(format: "yyyy-MM-dd"))
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        viewModel.colorsForAppointment(in: date.toString(format: "yyyy-MM-dd"))
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if viewModel.numberOfAppointments(in: date.toString(format: "yyyy-MM-dd")) > 0 {
            let appointmentListVC = AppointmentListViewController.loadFromNib()
            appointmentListVC.viewModel.setTitle(value: date.toString(format: "dd MMMM yyyy"))
            appointmentListVC.viewModel.setAppointments(value: viewModel.getAppointmentsForSelectedDate(date: date.toString(format: "yyyy-MM-dd")))
            appointmentListVC.viewModel.setAppointmentCategories(value: viewModel.getAllAppointmentCategories())
            let navVC = UINavigationController(rootViewController: appointmentListVC)
            navVC.modalPresentationStyle = .overFullScreen
            
            self.viewController.present(navVC, animated: false, completion: nil)
        }
    }
}
