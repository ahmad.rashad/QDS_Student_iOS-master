//
//  AppointmentTypeCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 13/07/21.
//

import UIKit

class AppointmentTypeCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewColor: DesignableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.regular(ofSize: 14)
    }

    //MARK:- Class Methods
    func setupData(color: String, title: String) {
        labelTitle.text = title
        viewColor.backgroundColor = UIColor(hexStr: "\(color)ff")
    }
}
