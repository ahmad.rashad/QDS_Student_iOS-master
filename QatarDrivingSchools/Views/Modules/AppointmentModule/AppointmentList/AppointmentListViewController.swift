//
//  AppointmentListViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class AppointmentListViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var dataSource: AppointmentListDataSource?
    lazy var viewModel = AppointmentListViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
           
        dataSource = AppointmentListDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        
        labelTitle.text = viewModel.getTitle()
        labelTitle.font = CustomFont.medium(ofSize: 18)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = Constants.color_lightTransparent
        }
    }
    
    //MARK:- Action Methods
    @IBAction func buttonCloseTouchUpInside(_ sender: UIButton) {
        self.view.backgroundColor = .clear
        UIView.animate(withDuration: 0.5) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

