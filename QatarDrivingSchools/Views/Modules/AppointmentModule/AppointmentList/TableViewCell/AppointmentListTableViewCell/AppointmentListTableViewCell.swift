//
//  AppointmentListTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class AppointmentListTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var viewBG: DesignableView!
    @IBOutlet weak var buttonJoin: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 16)
        labelTime.font = CustomFont.regular()
        buttonJoin.titleLabel?.font = CustomFont.semiBold(ofSize: 14)
        buttonJoin.backgroundColor = .white
        buttonJoin.setTitle(Constants.title_join, for: .normal)
        buttonJoin.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupDetails(appointment: Appointments){
        labelTitle.text = appointment.title ?? ""
        labelTime.text = appointment.meetingDate?.toDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "dd-MM-YYYY HH:mm")
        viewBG.backgroundColor = UIColor(hexStr: "\(appointment.eventColorCode ?? "#902C5D")ff") ?? UIColor()
        buttonJoin.isHidden = true
        buttonJoin.isHidden = false
        buttonEdit.isHidden = true
        if !(appointment.meetingId?.isEmpty ?? true) {
            buttonJoin.setTitle(Constants.title_join, for: .normal)
        }else{
            buttonJoin.setTitle(Constants.title_pending, for: .normal)
        }
    }
    
}
