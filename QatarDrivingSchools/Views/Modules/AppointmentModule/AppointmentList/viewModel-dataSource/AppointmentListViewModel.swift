//
//  AppointmentListViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

class AppointmentListViewModel {
    
    //MARK:- Variables
    private var title = ""
    private var appointments: [Appointments] = []
    private var appointmentCategories: [AppoinmentCategories] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - API
    
}
//MARK:- Getters and Setters
extension AppointmentListViewModel {
    func getTitle() -> String {
        title
    }
    func setTitle(value: String) {
        title = value
    }
    func setAppointments(value: [Appointments]) {
        appointments = value
    }
    func getTotalAppointmentsCount() -> Int {
        appointments.count
    }
    func getAppointment(at index: Int) -> Appointments {
        appointments[index]
    }
    func getAllAppointmentCategories() -> [AppoinmentCategories] {
        appointmentCategories
    }
    func setAppointmentCategories(value: [AppoinmentCategories]) {
        appointmentCategories = value
    }
}
