//
//  AppointmentDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import Foundation
import UIKit
import MobileRTC
class AppointmentListDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: AppointmentListViewController
    private let tableView: UITableView
    private let viewModel: AppointmentListViewModel
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: AppointmentListViewModel, viewController: AppointmentListViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [AppointmentListTableViewCell.identifier])
        tableView.reloadData()
    }
    func joinMeeting(meetingNumber: String, meetingPassword: String) {
        // Obtain the MobileRTCMeetingService from the Zoom SDK, this service can start meetings, join meetings, leave meetings, etc.
        if let meetingService = MobileRTC.shared().getMeetingService() {
            // Create a MobileRTCMeetingJoinParam to provide the MobileRTCMeetingService with the necessary info to join a meeting.
            // In this case, we will only need to provide a meeting number and password.
            meetingService.delegate = self
            let joinMeetingParameters = MobileRTCMeetingJoinParam()
            joinMeetingParameters.meetingNumber = meetingNumber
            joinMeetingParameters.password = meetingPassword
            
            // Call the joinMeeting function in MobileRTCMeetingService. The Zoom SDK will handle the UI for you, unless told otherwise.
            // If the meeting number and meeting password are valid, the user will be put into the meeting. A waiting room UI will be presented or the meeting UI will be presented.
            meetingService.joinMeeting(with: joinMeetingParameters)
        }
    }
    //MARK:- Action methods
    @objc func buttonEditTouchUpInside(_ sender: UIButton) {
        let editAppointmentVC = NewAppointmentViewController.loadFromNib()
        editAppointmentVC.viewControllerType = .edit
        editAppointmentVC.viewModel.setAppointmentCategories(value: viewModel.getAllAppointmentCategories())
        editAppointmentVC.viewModel.setAppointmentDetails(value: viewModel.getAppointment(at: sender.tag))
        self.viewController.navigationController?.pushViewController(editAppointmentVC, animated: true)
    }
    @objc func buttonJoinTouchUpInside(_ sender: UIButton) {
        let appointment = viewModel.getAppointment(at: sender.tag)
        if !(appointment.meetingId?.isEmpty ?? true) {
            joinMeeting(meetingNumber: appointment.meetingId ?? "", meetingPassword: appointment.meetingPassword ?? "")
        }else{
            let ac = UIAlertController(title:"", message: Constants.alert_meetingDetailsMissing, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            viewController.present(ac, animated: true)
        }
    }
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel.getTotalAppointmentsCount()
        var tempCount = count
        if tempCount > 3 {
            tempCount = 3
        }
        var newHeight = CGFloat(92 * tempCount) + 90
        if newHeight > (UIScreen.main.bounds.height - 100) {
            newHeight = UIScreen.main.bounds.height - 100
        }
        viewController.viewHeight.constant = newHeight
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentListTableViewCell.identifier) as! AppointmentListTableViewCell
        cell.buttonEdit.tag = indexPath.row
        cell.buttonJoin.tag = indexPath.row
        cell.buttonJoin.addTarget(self, action: #selector(buttonJoinTouchUpInside(_:)), for: .touchUpInside)
        cell.setupDetails(appointment: viewModel.getAppointment(at: indexPath.row))
        cell.buttonEdit.addTarget(self, action: #selector(buttonEditTouchUpInside(_:)), for: .touchUpInside)
        return cell
    }
}

// 1. Extend the ViewController class to adopt and conform to MobileRTCMeetingServiceDelegate. The delegate methods will listen for updates from the SDK about meeting connections and meeting states.
extension AppointmentListDataSource: MobileRTCMeetingServiceDelegate {

    // Is called upon in-meeting errors, join meeting errors, start meeting errors, meeting connection errors, etc.
    func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        switch error {
        case .passwordError:
            print("Could not join or start meeting because the meeting password was incorrect.")
        default:
            print("Could not join or start meeting with MobileRTCMeetError: \(error) \(message ?? "")")
        }
    }

    // Is called when the user joins a meeting.
    func onJoinMeetingConfirmed() {
        print("Join meeting confirmed.")
    }

    // Is called upon meeting state changes.
    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        print("Current meeting state: \(state)")
    }
}
