//
//  NewNewAppointmentViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 13/07/21.
//

import Foundation
import UIKit
import NISLRequestPackages
class NewAppointmentViewModel {
    
    //MARK:- Variables
    struct StructAppointment {
        var identifier: Int?
        var title, location, description : String?
        var startDate, endDate, startTime, endTime: Date?
        var appointmentCategory: AppoinmentCategories?
    }
    var appointmentDetails = StructAppointment()
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isAppointmentCreated: Dynamic<Bool> = Dynamic(true)
    var isAppointmentCancelled: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    private var appointmentCategories: [AppoinmentCategories] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- Class methods
    func isValidData() -> Bool {
        if((appointmentDetails.title?.isEmpty) ?? true) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentTitleMissing)
        }else if (appointmentDetails.startDate == nil) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentAvailabilityDateMissing)
        }else if (appointmentDetails.startDate ?? Date() <= Date()) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentAvailabilityDateShouldInFuture)
        }else if (appointmentDetails.startTime == nil) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentAvailabilityTimeMissing)
        }else if ((appointmentDetails.description?.isEmpty) ?? true) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentDescriptionMissing)
        }else if (appointmentDetails.appointmentCategory == nil) {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_appointmentEventTypeMissing)
        }else{
            return true
        }
        return false
    }
    
    //MARK:- API
    func createAppointment(){
        if(isValidData()) {
            let param: Parameters = [
                "Title": appointmentDetails.title ?? "",
                "EmailId": Constants.getLoggedInUserEmailForAPI(),
                "RequestDescription": appointmentDetails.description ?? "",
                "AvailabilityDate": appointmentDetails.startDate?.toString(format: "yyyy-MM-dd") ?? "00:00",
                "AvailabilityTime": appointmentDetails.startTime?.toString(format: "HH:mm") ?? "00:00",
                "S_Code": Constants.loggedInUser?.scode ?? "",
                "EventTypeId": appointmentDetails.appointmentCategory?.id ?? 0
            ]
            isLoaderHidden.value = false
            apiManager.call(type: RequestItemsType.CreateAppointment, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        //  self.appointments = data.data?.appointments ?? []
                    } else {
                    }
                    self.isAppointmentCreated.value = true
                case .failure(let message):
                    self.alertMessage.value = message
                    self.isAppointmentCreated.value = false
                }
            }
        }
    }
    
    func updateAppointment(){
        if(isValidData()) {
            let param: Parameters = [
                "Title": appointmentDetails.title ?? "",
                "EmailId": Constants.getLoggedInUserEmailForAPI(),
                "RequestDescription": appointmentDetails.description ?? "",
                "AvailabilityDate": appointmentDetails.startDate?.toString(format: "yyyy-MM-dd") ?? "00:00",
                "AvailabilityTime": appointmentDetails.startTime?.toString(format: "HH:mm") ?? "00:00",
                "S_Code": Constants.loggedInUser?.scode ?? "",
                "EventTypeId": appointmentDetails.appointmentCategory?.id ?? 0,
                "AppointmentId":appointmentDetails.identifier ?? 0
            ]
            
            isLoaderHidden.value = false
            apiManager.call(type: RequestItemsType.UpdateAppointment, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        //  self.appointments = data.data?.appointments ?? []
                    } else {
                    }
                    self.isAppointmentCreated.value = true
                case .failure(let message):
                    self.alertMessage.value = message
                    self.isAppointmentCreated.value = false
                }
            }
        }
    }
    func cancelAppointment() {
        let param: Parameters = [
            "EmailId": Constants.getLoggedInUserEmailForAPI(),
            "AppointmentId": appointmentDetails.identifier ?? 0,
            "S_Code": Constants.loggedInUser?.scode ?? ""]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.CancelAppointment, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    //  self.appointments = data.data?.appointments ?? []
                    self.isAppointmentCancelled.value = true
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    self.isAppointmentCancelled.value = false
                }
            case .failure(let message):
                self.alertMessage.value = message
                self.isAppointmentCancelled.value = false
            }
        }
    }
}
//MARK:- Getters and Setters
extension NewAppointmentViewModel {
    func getTitle() -> String {
        appointmentDetails.title ?? ""
    }
    func getLocation() -> String {
        appointmentDetails.location ?? ""
    }
    func getDescription() -> String {
        appointmentDetails.description ?? ""
    }
    func getStartDate() -> String {
        appointmentDetails.startDate?.toString(format: "dd-MM-YYYY") ?? ""
    }
    func getEndDate() -> String {
        appointmentDetails.endDate?.toString(format: "dd-MM-YYYY") ?? ""
    }
    func getStartTime() -> String {
        appointmentDetails.startTime?.toString(format: "hh:mm a") ?? ""
    }
    func getEndTime() -> String {
        appointmentDetails.endTime?.toString(format: "hh:mm a") ?? ""
    }
    func getAppointmentCategories() -> [AppoinmentCategories] {
        appointmentCategories
    }
    
    func setTitle(value: String) {
        appointmentDetails.title = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func setLocation(value: String) {
        appointmentDetails.location = value
    }
    func setDescription(value: String) {
        appointmentDetails.description = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func setStartDate(value: Date) {
        appointmentDetails.startDate = value
    }
    func setEndDate(value: Date) {
        appointmentDetails.endDate = value
    }
    func setStartTime(value: Date) {
        appointmentDetails.startTime = value
    }
    func setEndTime(value: Date) {
        appointmentDetails.endTime = value
    }
    func setAppointmentCategories(value: [AppoinmentCategories]) {
        appointmentCategories = value
    }
    func setSelectedAppointmentCategory(value: AppoinmentCategories?) {
        appointmentDetails.appointmentCategory = value
    }
    func getSelectedAppointmentCategory() -> AppoinmentCategories? {
        appointmentDetails.appointmentCategory
    }
    
    func setAppointmentDetails(value: Appointments) {
        appointmentDetails.identifier = value.appointmentId
        appointmentDetails.description = value.requestDescription
        appointmentDetails.title = value.eventName
        appointmentDetails.startDate = value.availiblityDate?.toDate()
        appointmentDetails.startTime = value.availiblityTime?.toDate(format: "HH:mm:ss")
        appointmentDetails.appointmentCategory = AppoinmentCategories(colorCode: value.eventColorCode ?? "ffffff", type: value.eventName ?? "", id: value.eventTypeId ?? 0)
    }
}
