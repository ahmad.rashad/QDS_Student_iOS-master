//
//  NewNewAppointmentDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 13/07/21.
//

import Foundation
import UIKit

class NewAppointmentDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController: NewAppointmentViewController
    private let tableView: UITableView
    private let viewModel: NewAppointmentViewModel
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var datePickerTag = enumDatePickerFor.startDate.rawValue
    
    enum enumDatePickerFor: Int {
        case startDate
        case endDate
        case startTime
        case endTime
    }
    
    enum enumAppointmentRowType: Int {
        case title
        //case location
       // case dateTitle
        case dateFrom
       // case dateTo
       // case TimeTitle
        case timeFrom
        //case timeTo
        case description
        case color
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAppointmentRowType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: NewAppointmentViewModel, viewController: NewAppointmentViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
        setupDatePicker()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    func setupDatePicker() {
        timePicker.datePickerMode = .time
        datePicker.minimumDate = Date().getSpecificDate(byAdding: .day, value: +1)
        datePicker.datePickerMode = .date
        if #available(iOS 14, *) {
            timePicker.preferredDatePickerStyle = .wheels
            datePicker.preferredDatePickerStyle = .wheels
        }
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TextViewTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func datePickerFinishEditing(_ sender: UIBarButtonItem){
        viewController.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat="yyyy-MM-dd"
        switch enumDatePickerFor(rawValue: datePickerTag)! {
        case .startDate:
            viewModel.setStartDate(value: datePicker.date)
        case .endDate:
            viewModel.setEndDate(value: datePicker.date)
        case .startTime:
            viewModel.setStartTime(value: timePicker.date)
        case .endTime:
            viewModel.setEndTime(value: timePicker.date)
        }
        tableView.reloadData()
    }
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumAppointmentRowType.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
        cell.textField.tag = indexPath.row
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: Constants.title_done, style: .done, target: self, action: #selector(datePickerFinishEditing(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceButton,doneButton], animated: false)
        
        switch enumAppointmentRowType(rawValue: indexPath.row)! {
        case .title:
            cell.setupData(title: Constants.description_title,
                           text: viewModel.getTitle(),
                           placeholder: Constants.placeholder_enterTitle)
        /*case .location:
            cell.setupData(title: Constants.description_addLocation,
                           text: viewModel.getLocation(),
                           placeholder: Constants.placeholder_addLocation,
                           rightImage: #imageLiteral(resourceName: "location_ic"))*/
        /*case .dateTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.text = Constants.description_date
            return cell*/
        case .dateFrom:
            cell.setupData(title: Constants.description_date,
                           text: viewModel.getStartDate(),
                           placeholder: Constants.placeholder_date,
                           rightImage: #imageLiteral(resourceName: "calendar_ic"))
            cell.textField.inputView = datePicker
            cell.textField.inputAccessoryView = toolbar
        /*case .dateTo:
            cell.setupData(title: Constants.description_to,
                           text: viewModel.getEndDate(),
                           placeholder: Constants.placeholder_date,
                           rightImage: #imageLiteral(resourceName: "calendar_ic"))
            cell.textField.inputView = datePicker
            cell.textField.inputAccessoryView = toolbar*/
        /*case .TimeTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.text = Constants.description_time
            return cell*/
        case .timeFrom:
            cell.setupData(title: Constants.description_time,
                           text: viewModel.getStartTime(),
                           placeholder: Constants.placeholder_time,
                           rightImage: #imageLiteral(resourceName: "clock_ic"))
            cell.dropDownIcon.tintColor = .black
            cell.textField.inputView = timePicker
            cell.textField.inputAccessoryView = toolbar
       /* case .timeTo:
            cell.setupData(title: Constants.description_to,
                           text: viewModel.getEndTime(),
                           placeholder: Constants.placeholder_time,
                           rightImage: #imageLiteral(resourceName: "clock1_ic"))
            cell.textField.inputView = timePicker
            cell.textField.inputAccessoryView = toolbar*/
        case .description:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextViewTableViewCell.identifier) as! TextViewTableViewCell
            cell.setupData(title: Constants.description_addDescription,
                           text: viewModel.getDescription(),
                           placeholder: Constants.placeholder_enterDescription)
            cell.textView.delegate = self
            return cell
        case .color:
            cell.labelTitle.text = Constants.placeholder_eventType
            cell.textField.isUserInteractionEnabled = false
            cell.dropDownIcon.isHidden = false
            if let category = viewModel.getSelectedAppointmentCategory() {
                cell.textField.text = "   \(category.type ?? "")"
                cell.viewBullet.backgroundColor = UIColor(hexStr: "\(category.colorCode ?? "fffff f")ff")
            }
        }
        cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumAppointmentRowType(rawValue: indexPath.row)! {
        case .color:
            let filterVC = FilterViewController.loadFromNib()
            filterVC.viewControllerType = .color
            filterVC.viewModel.setTitle(value: Constants.placeholder_eventType)
            filterVC.viewModel.setAppointmentCategories(value: viewModel.getAppointmentCategories())
            filterVC.selectedAppointmentCategory = { category in
                self.viewModel.setSelectedAppointmentCategory(value: category)
                self.tableView.reloadData()
            }
            filterVC.modalPresentationStyle = .overFullScreen
            viewController.present(filterVC, animated: true, completion: nil)
        default:
            break
        }
    }
}
extension NewAppointmentDataSource {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumAppointmentRowType(rawValue: textField.tag) {
        case .title:
            viewModel.setTitle(value: textField.text ?? "")
        case .description:
            break
        /*case .location:
            viewModel.setLocation(value: textField.text ?? "")*/
        case .dateFrom:
            datePickerTag = enumDatePickerFor.startDate.rawValue
            //viewModel.setStartDate(value: textField.text ?? "")
            break
        /*case .dateTo:
            datePickerTag = enumDatePickerFor.endDate.rawValue
            //viewModel.setEndDate(value: textField.text ?? "")
            break*/
        case .timeFrom:
            datePickerTag = enumDatePickerFor.startTime.rawValue
            break
      /*  case .timeTo:
            datePickerTag = enumDatePickerFor.endTime.rawValue
            break*/
        default:
            break
        }
    }
}
extension NewAppointmentDataSource: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.placeholder_enterDescription
            textView.textColor = UIColor.lightGray
        }else{
            viewModel.setDescription(value: textView.text)
        }
        
    }
}

