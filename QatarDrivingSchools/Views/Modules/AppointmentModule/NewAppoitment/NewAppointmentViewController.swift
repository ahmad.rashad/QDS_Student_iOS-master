//
//  NewAppointmentViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 13/07/21.
//

import UIKit
import NotificationBannerSwift
class NewAppointmentViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonCancel: RoundButton!
    @IBOutlet weak var buttonSave: RoundButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: NewAppointmentDataSource?
    lazy var viewModel = NewAppointmentViewModel()
    
    enum enumViewControllerType {
        case regular
        case edit
    }
    var viewControllerType: enumViewControllerType = .regular
    var appointmentIndex: Int = -1
    var completionHandlerForCancelAppointment: ((_ appointmentIndex: Int) -> Void)?
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = NewAppointmentDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonCancelTouchUpInside(_ sender: UIButton) {
        if viewControllerType == .edit {
            let alert = UIAlertController(title: "", message: Constants.alert_cancelAppointment, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Constants.title_yes, style: .destructive, handler: { _ in
                self.viewModel.cancelAppointment()
            }))
            alert.addAction(UIAlertAction(title: Constants.title_no, style: .default, handler: { _ in
                
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonSaveTouchUpInside(_ sender: RoundButton) {
        switch viewControllerType {
        case .regular:
            viewModel.createAppointment()
        case .edit:
            viewModel.updateAppointment()
        }
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = viewControllerType == .regular ? Constants.title_newAppointment : Constants.title_editAppointment
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        buttonCancel.setTitle(Constants.title_cancel.localized, for: .normal)
        buttonSave.setTitle(Constants.title_saveInCaps.localized, for: .normal)
        
        buttonSave.titleLabel?.font = CustomFont.semiBold(ofSize: 20)
        buttonCancel.titleLabel?.font = CustomFont.semiBold(ofSize: 20)
        
        
        switch viewControllerType {
        case .regular:
            buttonCancel.isHidden = true
            buttonSave.setTitle(Constants.title_addAppointment.localized, for: .normal)
        case .edit:
            break
        }
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isAppointmentCreated.bind { isCreated in
            if isCreated {
                self.navigationController?.popViewController(animated: true)
            } 
        }
        viewModel.isAppointmentCancelled.bind { isCancelled in
            if isCancelled {
                self.completionHandlerForCancelAppointment?(self.appointmentIndex)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
