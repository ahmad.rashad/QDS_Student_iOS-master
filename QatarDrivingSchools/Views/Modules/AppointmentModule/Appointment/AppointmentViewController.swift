//
//  AppointmentViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit

class AppointmentViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: AppointmentDataSource?
    lazy var viewModel = AppointmentViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = AppointmentDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_newAppointment.localized
        labelTitle.font = CustomFont.medium(ofSize: 18)
    }
}
