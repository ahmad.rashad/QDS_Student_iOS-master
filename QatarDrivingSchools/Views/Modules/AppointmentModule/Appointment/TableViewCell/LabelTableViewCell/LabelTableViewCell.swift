//
//  LabelTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit
 
class LabelTableViewCell: UITableViewCell {
     
    //MARK:- Variables
    @IBOutlet weak var imageViewUnderline: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        labelTitle.textColor = Constants.color_applicationThemeColor
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class Methods
    func setupAttributedText(title: String, text: String) {
        let mutableAttributedString = NSMutableAttributedString(string: title + ":", attributes: [NSAttributedString.Key.font: CustomFont.semiBold()])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.regular()]))
        imageViewUnderline.isHidden = true
        labelTitle.attributedText = mutableAttributedString
        labelTitle.textColor = .black
    }
    
    func setupAttributedText(text: String) {
        let mutableAttributedString = NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.semiBold()])
        imageViewUnderline.isHidden = true
        labelTitle.attributedText = mutableAttributedString
        labelTitle.textColor = .black
    }
}
