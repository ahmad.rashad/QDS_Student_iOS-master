//
//  AppointmentViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit
import NISLRequestPackages

class AppointmentViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - APIs

 
}
//MARK:- Getters and Setters
extension AppointmentViewModel {
}
