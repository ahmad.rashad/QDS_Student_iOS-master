//
//  AppointmentDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import Foundation
import UIKit

class  AppointmentDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController:  AppointmentViewController
    private let tableView: UITableView
    private let viewModel:  AppointmentViewModel
    
    enum enumAppointmentSectionType: Int {
        case space = 0
        case title
        case days
        case lessons
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAppointmentSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel:  AppointmentViewModel, viewController:  AppointmentViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LessonTableViewCell.identifier,
                                         LessonTitleTableViewCell.identifier,
                                         DaysTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumAppointmentSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumAppointmentSectionType(rawValue: section)! {
        case .space:
            return 1
        case .title:
            return 1
        case .days:
            return 1
        case .lessons:
            return 10
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumAppointmentSectionType(rawValue: indexPath.section)! {
        case .space:
            return 180
        case .title:
            return 40
        case .days:
            return 80
        case .lessons:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumAppointmentSectionType(rawValue: indexPath.section)! {
        case .space:
            let cell = UITableViewCell.defaultCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonTitleTableViewCell.identifier) as! LessonTitleTableViewCell
            return cell
        case .days:
            let cell = tableView.dequeueReusableCell(withIdentifier: DaysTableViewCell.identifier) as! DaysTableViewCell
            return cell
        case .lessons:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonTableViewCell.identifier) as! LessonTableViewCell
            return cell
        }
    }
}
