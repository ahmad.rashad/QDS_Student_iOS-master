//
//  LessonInFullScreenViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/10/21.
//

import Foundation
import NISLRequestPackages
class LessonInFullScreenViewModel {
    //MARK:- Variables
    private var questions: [Questions] = []
    private var englishQuestions: [Questions] = []
    private var videoId = ""
    private var timeDuration = ""
    private var lesson = Lessons()
    private var lastUpdateStudentProgressAPICalledTime = Date().timeIntervalSince1970 - (20)
    
    var isCompletedCalled: Bool = false
    
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Dependancy Injection
    init(){
        print("API init")
    }
    
    func setQuestionsInOrder() {
        questions.sort { question1, question2 in
            (question1.pauseTimeForQuestion?.secondFromString ?? 0) < (question2.pauseTimeForQuestion?.secondFromString ?? 0)
        }
    }
    
    //MARK:- APIs
    func updateStudentsProgress(isCompleted:Bool = false) {
       
        //api should not be called if it's not passed x seconds after last call
        if lastUpdateStudentProgressAPICalledTime + (10) > Date().timeIntervalSince1970 && isCompleted == false
        {
            print("API not called due to frequent call.",Date())
            return
        }else{
            print("API called.",Date())
        }
        
        lastUpdateStudentProgressAPICalledTime = Date().timeIntervalSince1970
        print("API",Date())
        if timeDuration.isEmpty {
            return
        }
        if lesson.progress == 100.0 {
            return
        }
        if isCompleted {
            if self.isCompletedCalled {
                print("Completed already called",Date())
                return
            } else {
                self.isCompletedCalled = true
            }
        }

        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "LessonId":lesson.lessonId ?? 0,
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "VideoTime":timeDuration
        ]
        isLoaderHidden.value = false
        apiManager.upload(type: RequestItemsType.UpdateStudentsProgress, params: param) { (res: Swift.Result<BaseClass<StudentsProgressData>, AlertMessage>, response) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    BaseViewModel.updateStudentActivity(moduleId: enumModuleType.StudentActivity.rawValue,
                                                        action: "Lesson watched till \(self.timeDuration)",
                                                        description: "Lesson watched till \(self.timeDuration)")
                    if isCompleted == true && data.data?.studentsProgress?.first?.lessonProgress == 100 {
                        //self.lesson.pauseFrom = "00:00:00"
                        self.setProgress(value: 100);
                    }
                    
                } else {
                    self.isCompletedCalled = false
                }
            case .failure(let message):
                self.isCompletedCalled = false
                self.isLoaderHidden.value = true
                print(message)
                break
            }
        }
    }
}

//MARK:- Getters and Setters
extension LessonInFullScreenViewModel {
    func getVideoId() -> String {
        videoId
    }
    func getQuestions() -> [Questions] {
        questions
    }
    
    func setQuestions(value: [Questions]) {
        questions = value
        setQuestionsInOrder()
    }
    func setEnglishQuestion(value: [Questions]) {
        englishQuestions = value
        englishQuestions.sort { question1, question2 in
            (question1.pauseTimeForQuestion?.secondFromString ?? 0) < (question2.pauseTimeForQuestion?.secondFromString ?? 0)
        }
    }
    func setVideoId(value: String) {
        videoId = value
    }
    func setLesson(value: Lessons) {
        lesson = value
    }
    func setTimeDuration(value: String) {
        timeDuration = value
    }
    func getSeekTime() -> String? {
        lesson.pausedFrom
    }
    
    func setSeekTime(value: String){
        lesson.pausedFrom = value
    }
    
    func getVideoDuration() -> String? {
        lesson.duration
    }
    
    func setProgress(value: Float) {
        lesson.progress = value
    }
    func getProgress() -> Float? {
        lesson.progress
    }
    
    func getQuestion(at time: Int) -> Questions? {
        return questions.first { question in
            question.pauseTimeForQuestion?.secondFromString == time
        }
    }
    func getEnglishQuestion(at time: Int) -> Questions? {
        return englishQuestions.first { question in
            question.pauseTimeForQuestion?.secondFromString == time
        }
    }
    func setQuestion(questionId: Int, updatedQuestionAnswer: Int?) {
        let index = questions.firstIndex { question in
            question.questionId == questionId
        }
        if let index = index {
            questions[index].selectedAnswer = updatedQuestionAnswer
            englishQuestions[index].selectedAnswer = updatedQuestionAnswer
        }
    }
}
