//
//  LessonInFullScreenViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 28/09/21.
//

import UIKit
import WebKit
import AVKit
import NotificationBannerSwift
class LessonInFullScreenViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var webView: WKWebView!
    var isFirstTime = true
    var currentTimeOfVideo: Int  = 0
    lazy var viewModel = LessonInFullScreenViewModel()
    var lessonProgressStatusUpdated: (()->())?
    var userContentControllers = [Constants.web_configuration_videoStatus,
                                  Constants.web_configuration_videoTime,
                                  Constants.web_configuration_videoSeek,
                                  Constants.web_configuration_setLanguage]
    var maxAllowedTime: Int = 0
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        WKWebsiteDataStore.default().removeData(ofTypes: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache], modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        URLCache.shared.removeAllCachedResponses()
        // Do any additional setup after loading the view.
        GlobalVariables.appDelegate?.myOrientation = .landscape
        self.rotateToLandsScapeDevice()
        if let seekTime = viewModel.getSeekTime() {
            self.setWebView(seekValue: "\(seekTime.secondFromString)")
        } else {
            self.setWebView(seekValue: "0")
        }
        
   
    }
    override func viewWillAppear(_ animated: Bool) {
        /*let alertController = UIAlertController(title: "", message: "This is test video, not coming from server.", preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.title_ok.localized, style: .destructive) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)*/
    }
   
    //MARK:- Class methods
    func setWebView(seekValue: String = ""){
        loadWebView()
        webView.configuration.userContentController = WKUserContentController()
        for content in userContentControllers {
            webView.configuration.userContentController.removeScriptMessageHandler(forName: content)
            webView.configuration.userContentController.add(self, name: content)
        }
        if !seekValue.isEmpty {
            maxAllowedTime = Int(seekValue) ?? 0
            webView.evaluateJavaScript("seek(\(seekValue))", completionHandler: nil)
        }
        webView.scrollView.isScrollEnabled = true
        

    }
    
    func loadWebView() {
        /*if let filePath = Bundle.main.path(forResource: "Wistia", ofType: "html") {
            do {
                let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
                let baseUrl = URL(fileURLWithPath: filePath)
                let htmlString = (contents as String).replacingOccurrences(of: Constants.description_zzzz, with: viewModel.getVideoId())
                webView.loadHTMLString(htmlString as String, baseURL: baseUrl)
            }
            catch {
                print(error.localizedDescription)
            }
        }*/
        #if DEBUG
        //viewModel.setVideoId(value: "QdrASkRHF16twkErciLEGK")
        #endif
        switch (Config.appConfiguration) {
        case .Debug:
            if let url = Constants.getPathOfMedia(value: viewModel.getVideoId()) {
                webView.load(URLRequest(url: url))
            }
        default:
            if let url = Constants.getPathOfMedia(value: viewModel.getVideoId()) {
                webView.load(URLRequest(url: url))
            }
        }

        
    }
    func rotateToLandsScapeDevice(){
        GlobalVariables.appDelegate?.myOrientation = .landscapeRight
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    func rotateToPortraitScapeDevice(){
        GlobalVariables.appDelegate?.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.rotateToPortraitScapeDevice()
        if maxAllowedTime <= currentTimeOfVideo {
            updateLessonProgress(timeInSecond: currentTimeOfVideo)
        }
        lessonProgressStatusUpdated?()
        webView.evaluateJavaScript("pauseVideo()", completionHandler: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- WebKit Script Handler Delegate
extension LessonInFullScreenViewController: WKScriptMessageHandler,WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         // This must be valid javascript!  Critically don't forget to terminate statements with either a newline or semicolon!
 
     }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error)
    {
        print(error)
    }
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("\(message.name): \(message.body)")


        
        if message.name == Constants.web_configuration_videoTime {
            if maxAllowedTime + 3 <= (Int((message.body as? Double) ?? 0.0)) {
                print("outside")
                webView.evaluateJavaScript("seek(\(maxAllowedTime))", completionHandler: nil)
                return
            }else{
                //if maxAllowedTime < (Int((message.body as? Double) ?? 0.0)) {
                  if maxAllowedTime < Int(Float("\(message.body)") ?? 0) {
                    maxAllowedTime = Int(Float("\(message.body)") ?? 0)
                    //maxAllowedTime = (Int((message.body as? Double) ?? 0.0))
                    let timeInSecond = Int(Float("\(message.body)") ?? 0)
                    if timeInSecond % 30 == 0 {
                        updateLessonProgress(timeInSecond: timeInSecond)
                    }
                }
                if message.name == Constants.web_configuration_videoTime {
                    let timeInSecond = Int(Float("\(message.body)") ?? 0)
                    currentTimeOfVideo = timeInSecond
                    if let question = viewModel.getQuestion(at: timeInSecond) {
                        if let selectedAnswer = question.selectedAnswer {
                            print("Right answer given already.", selectedAnswer)
                        }else{
                            webView.evaluateJavaScript("pauseVideo()", completionHandler: nil)
                            //self.rotateToPortraitScapeDevice()
                            let questionVC = QuestionsViewController.loadFromNib()
                            questionVC.viewControllerType = .videoQuestion
                            questionVC.viewModel.setQuestions(value: [question])
                            if let englishQuestion = viewModel.getEnglishQuestion(at: timeInSecond) {
                                questionVC.viewModel.setEnglishQuestions(value: [englishQuestion])
                            }
                            questionVC.completionHandlerForVideoQuestionAnswered = { (isRight,selectedAnswer) in
                                if isRight {
                                    self.viewModel.setQuestion(questionId: question.questionId ?? 0, updatedQuestionAnswer: selectedAnswer)
                                    self.webView.evaluateJavaScript("playVideo()", completionHandler: nil)
                                    let banner = NotificationBanner(title: Constants.alert_correctAnswer, style: .success, colors: CustomBannerColors())
                                    banner.show()
                                }else{
                                    let backTimeInSeconds = (question.backWardTime?.secondFromString ?? 0);
                                    self.webView.evaluateJavaScript("seek(\(backTimeInSeconds))", completionHandler: nil)
                                    let banner = NotificationBanner(title: Constants.alert_wrongAnswer, style: .danger, colors: CustomBannerColors())
                                    banner.show()
                                }
                                self.updateLessonProgress(timeInSecond: timeInSecond)
                            }
                            questionVC.modalPresentationStyle = .overFullScreen
                            present(questionVC, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        

        if maxAllowedTime <= currentTimeOfVideo {
            if message.name == Constants.web_configuration_videoStatus && (((message.body as? String) ?? "") == "videoPaused") {
                print("paused")
                updateLessonProgress(timeInSecond: currentTimeOfVideo)
            }
        }
        
        if message.name == Constants.web_configuration_videoStatus && (((message.body as? String) ?? "") == "ready") {
            print("ready")
            let langIndex =  getAudioIndex()
            self.webView.evaluateJavaScript("setLanguage(\(langIndex))", completionHandler: {e,d in
                print(e as Any)
                print(d as Any)
            })
        }
     
        if message.name == Constants.web_configuration_videoStatus && (((message.body as? String) ?? "") == "videoCompleted") {
            print("Completed")
            print("Video duration: " + self.viewModel.getVideoDuration()!.secondFromString.description)
            print("maxAllowedTime: " + maxAllowedTime.description)
            print("currentTimeOfVideo: " + currentTimeOfVideo.description)

            if let videoDuration = self.viewModel.getVideoDuration()?.secondFromString {
                updateLessonProgress(timeInSecond: videoDuration, isCompleted: true)
                //maxAllowedTime = videoDuration
            } else {
                updateLessonProgress(timeInSecond: currentTimeOfVideo)
            }
          
            //currentTimeOfVideo = 0
        }
        
        if message.name == Constants.web_configuration_videoStatus && (((message.body as? String) ?? "") == "videoPlaying") {
            print("videoPlaying")
            if isFirstTime {
                isFirstTime = false
                if let currentProgress = viewModel.getProgress() {
                    if currentProgress == 100 {
                        viewModel.setSeekTime(value: "00:00:00")
                    }
                }
                    
                if let seekTime = viewModel.getSeekTime() {
                    currentTimeOfVideo = seekTime.secondFromString
                    webView.evaluateJavaScript("seek(\(seekTime.secondFromString))", completionHandler: nil)
                }
            }
          /*  let videoURL = URL(string: "")
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }*/
            /*let timeInSecond = Int(Float(currentTimeOfVideo) ?? 0)
            hmsFrom(seconds: timeInSecond) { hours, minutes, seconds in
                let hours = self.getStringFrom(seconds: hours)
                let minutes = self.getStringFrom(seconds: minutes)
                let seconds = self.getStringFrom(seconds: seconds)
                print("\(hours):\(minutes):\(seconds)")
                self.completionForLastTime?("\(hours):\(minutes):\(seconds)")
            }*/
        }
       
    }
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    func updateLessonProgress(timeInSecond: Int, isCompleted: Bool = false) {
        self.hmsFrom(seconds: timeInSecond) { hours, minutes, seconds in
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
            print("API - \(hours):\(minutes):\(seconds) ",Date())
            self.viewModel.setTimeDuration(value: "\(hours):\(minutes):\(seconds)")
            self.viewModel.updateStudentsProgress(isCompleted: isCompleted)
        }
    }
    
    func getAudioIndex() -> String
    {
        var index = "0";
        
        if Language.language == .ARABIC {index = "0";}
        else if Language.language ==  .ENGLISH {index = "1";}
        else if Language.language ==  .HINDI {index = "2";}
        else if Language.language ==  .URDU {index = "3";}
        else if Language.language ==  .MALAYALAM {index = "4";}
        else if Language.language ==  .NEPALI {index = "5";}
        else if Language.language ==  .BANGALI {index = "6";}
        else if Language.language ==  .TAMIL {index = "7"; }
        else if Language.language ==  .SINHALA {index = "8";}
        else if Language.language ==  .FRENCH {index = "9";}
        else if Language.language ==  .SPANISH {index = "10";}
        else if Language.language ==  .RUSSIAN {index = "12";}
        else if Language.language ==  .PERSIAN {index = "13";}
        else if Language.language ==  .TURKISH {index = "14";}
        else if Language.language ==  .CHINESE {index = "15";}
        else if Language.language ==  .FILIPINO {index = "16";}
        else if Language.language ==  .PASHTO {index = "17";}
        else if Language.language ==  .TELUGU {index = "18";}
        else { index = "0"; }
        
 
        return index
        
    }
}
