//
//  LessonsListViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit
import NotificationBannerSwift
class LessonsListViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewHeader: UIImageView!
    
    @IBOutlet weak var buttonPrevious: RoundButton!
    @IBOutlet weak var buttonNext: ApplicationThemeButton!
    
    var dataSource : LessonsListDataSource?
    lazy var viewModel = LessonsListViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = LessonsListDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getLessonByDay()
    }
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func buttonPreviousTouchUpInside(_ sender: UIButton) {
        if viewModel.isPreviousDayAllowed() {
            self.buttonPrevious.isUserInteractionEnabled = false
            self.buttonNext.isUserInteractionEnabled = false
            viewModel.updateSelectedDayToPreviousDay()
        }
        updatePreviousNextButtons()
    }
    @objc func buttonNextTouchUpInside(_ sender: UIButton) {
        if viewModel.isNextDayAllowed() {
            self.buttonPrevious.isUserInteractionEnabled = false
            self.buttonNext.isUserInteractionEnabled = false
            viewModel.updateSelectedDayToNextDay()
        }
        updatePreviousNextButtons()
    }
    //MARK:- Class Methods
    func setupLayout() {
        self.buttonPrevious.isUserInteractionEnabled = false
        self.buttonNext.isUserInteractionEnabled = false
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_courseDetails.localized
        
        buttonPrevious.setTitle(Constants.title_previous, for: .normal)
        buttonNext.setTitle(Constants.title_next, for: .normal)
        
        buttonPrevious.titleLabel?.font = CustomFont.regular()
        buttonNext.titleLabel?.font = CustomFont.semiBold()
        
        buttonPrevious.addTarget(self, action: #selector(buttonPreviousTouchUpInside(_:)), for: .touchUpInside)
        buttonNext.addTarget(self, action: #selector(buttonNextTouchUpInside(_:)), for: .touchUpInside)
        
        updatePreviousNextButtons()
        viewModel.isLessonsFetched.bind { isFetched in
            self.buttonPrevious.isUserInteractionEnabled = true
            self.buttonNext.isUserInteractionEnabled = true
            self.tableView.reloadData()
            self.updatePreviousNextButtons()
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        imageView.sd_setImage(with: viewModel.getCourseImageURL(), placeholderImage: Constants.image_noData, options: [], context: nil)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func updatePreviousNextButtons() {
        buttonPrevious.isHidden = viewModel.isFirstDaySelected()
        buttonNext.isHidden = viewModel.isLastDaySelected()
        buttonPrevious.alpha = viewModel.isPreviousDayAllowed() ? 1 : 0.5
        buttonNext.alpha = viewModel.isNextDayAllowed() ? 1 : 0.5
        
        if viewModel.getTotalNumberOfDays() == 0 {
            buttonNext.isHidden = true
            buttonPrevious.isHidden = true
        }
    }
}
