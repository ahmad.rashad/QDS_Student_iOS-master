//
//  NumberCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import UIKit

class NumberCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgView: DesignableView!
    @IBOutlet weak var labelNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        labelNumber.font = CustomFont.regular()
        bgView.borderColor = Constants.color_darkApplicationThemeColor
    }

}
