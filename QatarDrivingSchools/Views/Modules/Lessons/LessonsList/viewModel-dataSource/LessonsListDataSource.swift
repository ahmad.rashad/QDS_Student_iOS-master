//
//  LessonsListDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import Foundation
import UIKit
import NISLRequestPackages
class LessonsListDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController: LessonsListViewController
    private let tableView: UITableView
    private let viewModel: LessonsListViewModel
    
    enum enumAppointmentSectionType: Int {
        case space = 0
        case title
        case days
        case lessons
        case noData
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAppointmentSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: LessonsListViewModel, viewController: LessonsListViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LessonTableViewCell.identifier,
                                         LessonTitleTableViewCell.identifier,
                                         DaysTableViewCell.identifier,
                                         ImageTableViewCell.identifier,
                                         TestOverviewTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonLessonTouchUpInside(_ sender: UIButton) {
        let lesson = viewModel.getLesson(at: sender.tag)
        switch enumLessonState(rawValue: lesson.status ?? 0) {
        case .inactive,.active,.resume:
            let lessonDescriptionVC = LessonDescriptionViewController.loadFromNib()
            lessonDescriptionVC.viewModel.setLesson(value: lesson)
            self.viewController.navigationController?.pushViewController(lessonDescriptionVC, animated: true)
        case .completed:
            
            if (lesson.testResultDetail != nil) && (lesson.testResultDetail?.count ?? 0) > 0 {
                if let result = lesson.testResultDetail {
                    if result.first?.resultStatus == enumResultType.failed.rawValue {
                        if let test = lesson.tests?.first {
                            let alertController = UIAlertController(title: "", message: Constants.alert_startQuiz.localized, preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .default, handler: { _ in
                                let questionVC = QuestionsViewController.loadFromNib()
                                questionVC.viewModel.setTestDetails(value: test)
                                self.viewController.navigationController?.pushViewController(questionVC, animated: true)
                            }))
                            alertController.addAction(UIAlertAction(title: Constants.title_no.localized, style: .destructive, handler: nil))
                            viewController.present(alertController, animated: true, completion: nil)
                        }
                    }else{
                        let examResultVC = ExamResultViewController.loadFromNib()
                        examResultVC.viewModel.setResult(value: result)
                        examResultVC.isFromLesson = true
                        self.viewController.navigationController?.pushViewController(examResultVC, animated: true)
                       
                    }
                }
            }else{
                if let test = lesson.tests?.first {
                    let alertController = UIAlertController(title: "", message: Constants.alert_startQuiz.localized, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .default, handler: { _ in
                        let questionVC = QuestionsViewController.loadFromNib()
                        questionVC.viewModel.setTestDetails(value: test)
                        self.viewController.navigationController?.pushViewController(questionVC, animated: true)
                    }))
                    alertController.addAction(UIAlertAction(title: Constants.title_no.localized, style: .destructive, handler: nil))
                    viewController.present(alertController, animated: true, completion: nil)
                }else{
                    let alertVC = AlertViewController.loadFromNib()
                    alertVC.viewModel.setImage(value: #imageLiteral(resourceName: "lock_ic"))
                    alertVC.viewModel.setTitle(value: "")
                    alertVC.viewModel.setSubTitle(value: Constants.alert_thereIsNoQuiz.localized)
                    alertVC.modalPresentationStyle = .overFullScreen
                    self.viewController.present(alertVC, animated: true, completion: nil)
                }
            }
        case .none:
            break
        }
    }
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumAppointmentSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumAppointmentSectionType(rawValue: section)! {
        case .space:
            return 1
        case .title:
            return 1
        case .days:
            return 1
        case .lessons:
            /*if viewModel.getTotalNumberOfDays() == viewModel.getSelectedDay() {
                return viewModel.getTotalTestForLastDay()
            }*/
            return viewModel.getTotalLessonCount()
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalLessonCount() == 0 ? 1 : 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumAppointmentSectionType(rawValue: indexPath.section)! {
        case .space:
            return 180
        case .title:
            return 60
        case .days:
            if viewModel.getTotalNumberOfDays() == 0 {
                return 0
            }
            return 55
        case .lessons:
            return UITableView.automaticDimension
        case .noData:
            return viewModel.getTotalLessonCount() == 0 ? 300 : 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumAppointmentSectionType(rawValue: indexPath.section)! {
        case .space:
            let cell = UITableViewCell.defaultCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonTitleTableViewCell.identifier) as! LessonTitleTableViewCell
            cell.titleLabel.text = viewModel.getCourseTitle()
            return cell
        case .days:
            let cell = tableView.dequeueReusableCell(withIdentifier: DaysTableViewCell.identifier) as! DaysTableViewCell
            cell.collectionView.registerNib(nibNames: [NumberCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            return cell
        case .lessons:
            /*if viewModel.getTotalNumberOfDays() == viewModel.getSelectedDay() {
                let cell = tableView.dequeueReusableCell(withIdentifier: TestOverviewTableViewCell.identifier) as! TestOverviewTableViewCell
                if let test = viewModel.getTestDetailsForLastDay(at: indexPath.row) {
                    cell.setupDetails(test: test)
                }
                return cell
            }*/
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonTableViewCell.identifier) as! LessonTableViewCell
            cell.setDetails(lesson: viewModel.getLesson(at: indexPath.row))
            cell.buttonStartLesson.tag = indexPath.row
            cell.buttonStartLesson.addTarget(self, action: #selector(buttonLessonTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumAppointmentSectionType(rawValue: indexPath.section)! {
        case .lessons:
            /*if viewModel.getTotalNumberOfDays() == viewModel.getSelectedDay() {
                if let test = viewModel.getTestDetailsForLastDay(at: indexPath.row) {
                    if test.totalQuestion == 0 {
                        viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_noQuestions)
                    }else{
                        let alertController = UIAlertController(title: "", message: Constants.alert_startQuiz, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: Constants.title_yes, style: .default, handler: { _ in
                            let questionVC = QuestionsViewController.loadFromNib()
                            questionVC.viewModel.setTestDetails(value: test)
                            self.viewController.navigationController?.pushViewController(questionVC, animated: true)
                        }))
                        alertController.addAction(UIAlertAction(title: Constants.title_no, style: .destructive, handler: nil))
                        viewController.present(alertController, animated: true, completion: nil)
                    }
                }
                return 
            }*/
            let cell = tableView.cellForRow(at: indexPath) as? LessonTableViewCell
            if !(cell?.buttonStartLesson.isEnabled ?? true) {
                let alertVC = AlertViewController.loadFromNib()
                alertVC.viewModel.setImage(value: #imageLiteral(resourceName: "lock_ic"))
                alertVC.viewModel.setTitle(value: "")
                alertVC.viewModel.setSubTitle(value: Constants.alert_watchPreviousLessonBefore.localized)
                alertVC.modalPresentationStyle = .overFullScreen
                self.viewController.present(alertVC, animated: true, completion: nil)
            }else{
                /*let zoomVC = ZoomViewController.loadFromNib()
                 viewController.navigationController?.pushViewController(zoomVC, animated: true)*/
                /*let questionVC = QuestionsViewController.loadFromNib()
                 viewController.navigationController?.pushViewController(questionVC, animated: true)*/
                let lessonDetails = LessonDescriptionViewController.loadFromNib()
                lessonDetails.viewModel.setLesson(value: viewModel.getLesson(at: indexPath.row))
                viewController.navigationController?.pushViewController(lessonDetails, animated: true)
            }
        default:
            break
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print(scrollView.contentOffset.y)
        viewController.imageViewHeader.isHidden = false
        if scrollView.contentOffset.y > 0 {
            viewController.imageViewHeader.alpha = (scrollView.contentOffset.y / 180)
        }
        if scrollView.contentOffset.y < 1{
            viewController.imageViewHeader.alpha = 0
        }
    }
}

//MARK:- Collection methods
extension LessonsListDataSource : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTotalNumberOfDays()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NumberCollectionViewCell.identifier, for: indexPath) as! NumberCollectionViewCell
        cell.labelNumber.text = "\(indexPath.row + 1)"
        cell.bgView.cornerRadius = 15
        cell.bgView.borderWidth = viewModel.getSelectedDay() - 1 == indexPath.row ? 1 : 0
        cell.labelNumber.textColor = viewModel.getSelectedDay() - 1 == indexPath.row ? Constants.color_darkApplicationThemeColor : UIColor.lightGray
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Uncomment below code to enable tap on numbers of day
       /*let cell = tableView.cellForRow(at: IndexPath(row: 0, section: enumAppointmentSectionType.days.rawValue)) as? DaysTableViewCell
        cell?.setupData(value: (indexPath.row + 1))
        viewModel.setSelectedDay(value: indexPath.row + 1)
        viewModel.getLessonByDay()
        collectionView.reloadData()*/
    }
}
