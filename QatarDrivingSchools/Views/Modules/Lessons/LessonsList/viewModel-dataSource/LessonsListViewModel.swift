//
//  LessonsListViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit
import NISLRequestPackages

class LessonsListViewModel {
    
    //MARK:- Variables
    private var selectedDay = 1
    private var course: Courses?
        var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isLessonsFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    private var lessons: [Lessons] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- Class methods
    func isNextDayAllowed() -> Bool {
        var isAllowed = true
        for lesson in lessons {
            if (lesson.tests?.isEmpty ?? true) || (lesson.tests?.count == 0) {
                isAllowed = false
            }else{
                if let result = lesson.testResultDetail {
                    if result.first?.resultStatus == enumResultType.failed.rawValue {
                        isAllowed = false
                    }
                }
            }
        }
        return isAllowed
    }
    func updateSelectedDayToNextDay() {
        selectedDay += 1
        getLessonByDay()
    }
    func isPreviousDayAllowed() -> Bool {
        selectedDay != 1
    }
    func updateSelectedDayToPreviousDay() {
        selectedDay -= 1
        getLessonByDay()
    }
     
    //MARK:- API to fetch all lessons by day
    func getLessonByDay() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "Day":selectedDay,
            "CourseId":course?.courseId ?? 0,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetLessonByDay, queryParameter: param) { (res: Swift.Result<BaseClass<LessonsBaseClass>, AlertMessage>, response, jsonData) in
            switch res {
            case .success(let data):
                if Language.language == .ENGLISH {
                    self.isLoaderHidden.value = true
                    self.lessons.removeAll()
                    if data.status == Constants.api_success{
                        self.lessons.append(contentsOf: data.data?.lessons ?? [])
                    } else {
                    }
                    self.isLessonsFetched.value = true
                }else{
                    var tmpLessons = data.data?.lessons ?? []
                    for i in 0..<tmpLessons.count {
                        var localizeLesson: [LocalizeLessonBaseClass] = []
                        if let translation = tmpLessons[i].translation {
                            let jsonData = Data(translation.utf8)
                            do {
                                localizeLesson = try JSONDecoder().decode([LocalizeLessonBaseClass].self, from: jsonData)
                                print(localizeLesson)
                            } catch let error{
                                print(error.localizedDescription)
                            }
                        }
                        for lesson in localizeLesson {
                            if lesson.code == Language.language.rawValue {
                                tmpLessons[i].lessonTitle = lesson.lessonTitle
                                tmpLessons[i].descriptionValue = lesson.description
                                tmpLessons[i].courseTitle = lesson.courseTitle
                                break
                            }
                        }
                    }
                    self.lessons.removeAll()
                    self.lessons.append(contentsOf: tmpLessons)
                    self.isLoaderHidden.value = true
                    self.isLessonsFetched.value = true
                    /*if let jsonData = jsonData {
                        var request = URLRequest(url: URL(string: "http://clientapp.narola.online:9564/process_post?languageCode=\(Language.language.rawValue)")!)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        let session = URLSession.shared
                        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                            DispatchQueue.main.async {
                                self.isLoaderHidden.value = true
                                do {
                                    if let data = data {
                                        let data = try JSONDecoder().decode(BaseClass<LessonsBaseClass>.self, from: data)
                                        self.lessons.removeAll()
                                        if data.status == Constants.api_success{
                                            self.lessons.append(contentsOf: data.data?.lessons ?? [])
                                        } else {
                                            // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                                        }
                                    }
                                    self.isLessonsFetched.value = true
                                    
                                } catch {
                                    print("error")
                                }
                            }
                        })
                        task.resume()
                    }*/
                }
                
            case .failure(let message):
                self.isLoaderHidden.value = true
                self.alertMessage.value = message
            }
        }
    }
}
//MARK:- Getters and Setters
extension LessonsListViewModel {
    func getSelectedDay() -> Int {
        selectedDay
    }
    func setSelectedDay(value: Int) {
        selectedDay = value
    }
    func setCourse(value: Courses) {
        course = value
    }
    func getTotalLessonCount() -> Int {
        lessons.count
    }
    func getCourseTitle() -> String {
        course?.courseTitle ?? ""
    }
    func getLesson(at index: Int) -> Lessons {
        lessons[index]
    }
    func getCourseImageURL() -> URL? {
        Constants.getPathOfMedia(value: course?.courseImage)
    }
    func getTotalNumberOfDays() -> Int {
        return course?.totalNumberOfDays ?? 0
    }
    func getTotalTestForLastDay() -> Int {
        return lessons.first?.tests?.count ?? 0
    }
    func getTestDetailsForLastDay(at index: Int) -> Tests? {
        return lessons.first?.tests?[index]
    }
    func isFirstDaySelected() -> Bool {
        selectedDay == 1
    }
    func isLastDaySelected() -> Bool {
        selectedDay == getTotalNumberOfDays()
    }
}


