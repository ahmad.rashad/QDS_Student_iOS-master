//
//  QuestionProgressTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class QuestionProgressTableViewCell: UITableViewCell {
    
    //MARK:- Variables
    @IBOutlet weak var labelQuestionNumber: UILabel!
    @IBOutlet weak var labelTotalQuestionNumber: UILabel!
    @IBOutlet weak var buttonTimer: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
