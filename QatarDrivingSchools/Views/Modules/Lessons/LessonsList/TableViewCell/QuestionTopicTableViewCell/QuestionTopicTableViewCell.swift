//
//  QuestionTopicTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class QuestionTopicTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.bold(ofSize: 18)
        labelValue.font = CustomFont.regular()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
