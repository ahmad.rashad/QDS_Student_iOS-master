//
//  LessonTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class LessonTableViewCell: UITableViewCell {
    
    //MARK:- Variables
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonTime: UIButton!
    @IBOutlet weak var buttonPerson: UIButton!
    @IBOutlet weak var buttonStartLesson: RoundButton!
    @IBOutlet weak var imageViewPassed: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        buttonTime.titleLabel?.font = CustomFont.regular(ofSize: 12)
        buttonPerson.titleLabel?.font = CustomFont.regular(ofSize: 12)
        buttonStartLesson.titleLabel?.font = CustomFont.bold(ofSize: 10)
        
        buttonStartLesson.backgroundColor = Constants.color_darkApplicationThemeColor
        buttonStartLesson.setTitleColor(.white, for: .normal)
        
        buttonTime.setTitleColor(Constants.color_applicationThemeColor, for: .normal)
        buttonPerson.setTitleColor(Constants.color_applicationThemeColor, for: .normal)
        
        buttonStartLesson.setTitle(Constants.title_startLesson.localized, for: .normal)
        buttonStartLesson.setTitleColor(.white, for: .normal)
        
        buttonTime.setTitleColor(Constants.color_darkApplicationThemeColor, for: .normal)
        buttonPerson.setTitleColor(Constants.color_darkApplicationThemeColor, for: .normal)
        imageViewPassed.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDetails(lesson: Lessons) {
        labelTitle.text = lesson.lessonTitle ?? ""
        buttonTime.setTitle(" \(lesson.duration ?? "") ", for: .normal)
        buttonPerson.setTitle(" 0 ", for: .normal)
        imgView.sd_setImage(with: Constants.getPathOfMedia(value: lesson.thumbnailImage), placeholderImage: Constants.image_noData, options: [], context: nil)
        //imgView.sd_setImage(with: , completed: nil)
        buttonStartLesson.isEnabled = true
        buttonStartLesson.backgroundColor = Constants.color_darkApplicationThemeColor
        switch enumLessonState(rawValue: lesson.status ?? 0) {
        case .inactive:
            buttonStartLesson.isEnabled = false
            buttonStartLesson.setTitle(Constants.title_startLesson.localized, for: .normal)
            buttonStartLesson.backgroundColor = Constants.color_applicationDisableColor
        case .active:
            buttonStartLesson.setTitle(Constants.title_startLesson.localized, for: .normal)
        case .resume:
            buttonStartLesson.setTitle(Constants.title_resume.localized, for: .normal)
        case .completed:
            buttonStartLesson.setTitle(Constants.title_completed.localized, for: .normal)
           
            if (lesson.tests?.isEmpty ?? true) || (lesson.tests?.count == 0) {
                buttonStartLesson.setTitle(Constants.title_attendQuiz.localized, for: .normal)
            }else{
                if (lesson.testResultDetail?.first?.resultStatus ?? 0) == enumResultType.failed.rawValue {
                    buttonStartLesson.setTitle(Constants.title_attendQuiz.localized, for: .normal)
                }else{
                    buttonStartLesson.setTitle(Constants.title_viewResult, for: .normal)
                }
            }
        case .none:
            break
        }
        switch enumResultType(rawValue: lesson.testResultDetail?.first?.resultStatus ?? 0) {
        case .pass:
            imageViewPassed.isHidden = false
        default:
            imageViewPassed.isHidden = true
        }
        
    }
    
}
