//
//  TestOverviewTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class TestOverviewTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonDuration: UIButton!
    @IBOutlet weak var buttonNumberOfQuestions: UIButton!
    @IBOutlet weak var imageViewQuiz: UIImageView!
    @IBOutlet weak var topImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sideImageWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonArrow: UIButton!
    @IBOutlet weak var buttonCertificate: UIButton!
    
    //MARK:- Activity life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        backgroundColor = .white
        labelTitle.font = CustomFont.medium()
        buttonDuration.titleLabel?.font = CustomFont.regular(ofSize: 14)
        buttonNumberOfQuestions.titleLabel?.font = CustomFont.regular(ofSize: 14)
        buttonCertificate.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupDetails(test: Tests) {
        labelTitle.text = test.examName ?? ""
        buttonDuration.setTitle(" \(test.timeDuration ?? "")", for: .normal)
        let questionText = (test.numberOfQuestionsToDisplay ?? 0) < 2 ? Constants.description_question.localized : Constants.description_questions.localized
        buttonNumberOfQuestions.setTitle(" \(test.numberOfQuestionsToDisplay ?? 0) \(questionText)", for: .normal)
    }
    
    func setStatus(test: Tests) {
        switch enumTestStatus(rawValue: test.status ?? 0)! {
        case .deactivate:
            buttonArrow.isEnabled = false
        case .activate:
            buttonArrow.isEnabled = true
        }
    }
}
