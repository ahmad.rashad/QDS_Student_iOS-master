//
//  DaysTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 09/07/21.
//

import UIKit

class DaysTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var selectedDay: UILabel!
    @IBOutlet weak var labelDays: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        selectedDay.font = CustomFont.semiBold()
        labelDays.font = CustomFont.regular(ofSize: 14)
        setupData(value: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(value: Int) {
        selectedDay.text = "\(value)"
        labelDays.text = "Day \(value)"
    }
}
