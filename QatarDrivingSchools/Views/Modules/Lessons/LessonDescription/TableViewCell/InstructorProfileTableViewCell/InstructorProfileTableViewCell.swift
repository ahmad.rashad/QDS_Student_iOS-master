//
//  InstructorProfileTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit

class InstructorProfileTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelInstructor: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelInstructorName: UILabel!
    @IBOutlet weak var labelInstructorPosition: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelInstructor.font = CustomFont.semiBold(ofSize: 14)
        labelInstructorName.font = CustomFont.semiBold(ofSize: 14)
        labelInstructorPosition.font = CustomFont.regular(ofSize: 14)
        
        labelInstructor.textColor = .lightGray
        labelInstructorName.textColor = .black
        labelInstructorPosition.textColor = .lightGray
        
        labelInstructor.text = Constants.description_instructor
        #if DEBUG
        labelInstructorName.text = "Sarah Musk"
        labelInstructorPosition.text = "Driving Trainer"
        #endif
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
