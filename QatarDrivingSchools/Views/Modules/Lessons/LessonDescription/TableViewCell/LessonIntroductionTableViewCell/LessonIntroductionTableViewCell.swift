//
//  LessonIntroductionTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit

class LessonIntroductionTableViewCell: UITableViewCell {
   
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelRegistrationDate: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        labelTitle.font = CustomFont.regular()
        labelDescription.font = CustomFont.regular()
        labelRegistrationDate.font = CustomFont.regular()
        labelDate.font = CustomFont.regular()
        
        labelDescription.textColor = .darkGray
        labelDate.textColor = .darkGray
        
        labelTitle.text = Constants.description_introduction.localized
        labelRegistrationDate.text = Constants.description_registrationDate.localized
        
        labelRegistrationDate.text = ""
        
        #if DEBUG
        labelDescription.text = "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum"
        labelDate.text = "15 October 2020"
        #endif
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- Class methods
    func setDetails(description: String, date: String) {
        if description.isEmpty {
            labelTitle.text = ""
        }
        labelDescription.text = description
        labelDate.text = date
    }
}
