//
//  LessonDescriptionTitleTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit

class LessonDescriptionTitleTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelLesson: UILabel!
    @IBOutlet weak var labelLessonTitle: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var progressView: CircularProgressView!
    @IBOutlet weak var labelProgress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelLesson.font = CustomFont.regular()
        labelLessonTitle.font = CustomFont.semiBold(ofSize: 20)
        labelDuration.font = CustomFont.regular(ofSize: 14)
        labelProgress.font = CustomFont.bold(ofSize: 16)
        
        labelDuration.textColor = Constants.color_darkApplicationThemeColor 
        
        #if DEBUG
        labelLesson.text = "Lesson 2"
        labelLessonTitle.text = "Traffic Signals"
        labelDuration.text = "2m 15s - Released On: 03-11-2020"
        labelProgress.text = "75%"
        #endif
        setProgressView(isAnimationRequired: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setProgressView(value: Float = 0.0, isAnimationRequired: Bool){
        progressView.trackWidth = 10.0
        progressView.trackClr = UIColor(red: 198/255, green: 199/255, blue: 203/255, alpha:0.6)
        progressView.progressClr = Constants.color_applicationThemeColor //UIColor(red: 233/255, green: 172/255, blue: 0/255, alpha: 1.0)
        if isAnimationRequired {
            progressView.setProgressWithAnimation(duration: 1.0, value: value)
        }else{
            progressView.setProgressWithAnimation(duration: 0, value: value)
        }
    }
    
    func setDetails(lessonTitle: String, courseTitle: String, duration: String, progress: Float, isAnimationRequired: Bool){
        labelLesson.text = lessonTitle
        labelLessonTitle.text = courseTitle
        labelDuration.text = "\(Constants.description_duration.localized): \(duration)"//"2m 15s - Released On: 03-11-2020"
        labelProgress.text = "\(progress)%"
        setProgressView(value: Float(progress)/100, isAnimationRequired: isAnimationRequired)
    }
    
}
