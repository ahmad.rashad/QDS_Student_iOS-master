//
//  LessonVideoTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 21/09/21.
//

import UIKit
import WebKit
import JavaScriptCore

class LessonVideoTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var webView: WKWebView!
    
    var completionForLastTime: ((_ time: String)->())?
    var currentTimeOfVideo = ""
    var userContentControllers = [Constants.web_configuration_videoStatus,
                                  Constants.web_configuration_videoTime,
                                  Constants.web_configuration_videoSeek,
                                  Constants.web_configuration_setLanguage]
    
    //MARK:- Activity life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        //setWebView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Class methods
    func setWebView(seekValue: String = "", videoId: String){
        loadWebView(videoId: videoId)
        webView.configuration.userContentController = WKUserContentController()
        for content in userContentControllers {
            webView.configuration.userContentController.removeScriptMessageHandler(forName: content)
            webView.configuration.userContentController.add(self, name: content)
        }
        if !seekValue.isEmpty {
            webView.evaluateJavaScript("seek(\(seekValue))", completionHandler: nil)
        }
       /* let uuid = "sAGCzvjEFw7wmL7scnJni789"
        
        webView.navigationDelegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.webView.evaluateJavaScript("seek(50)", completionHandler: nil)
            self.webView.evaluateJavaScript("setImageSource(\(uuid))", completionHandler: nil)
        }*/
    }
    func loadWebView(videoId: String) {
        /*if let filePath = Bundle.main.path(forResource: "vidyard", ofType: "html") {
            do {
                let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
                let baseUrl = URL(fileURLWithPath: filePath)
                let htmlString = (contents as String).replacingOccurrences(of: "ZZZZZZZZZZZZZZZZZZ", with: videoId)
                webView.loadHTMLString(htmlString as String, baseURL: baseUrl)
            }
            catch {
                print(error.localizedDescription)
            }
        }*/
        if videoId != "" {
            if let url = Constants.getPathOfMedia(value: videoId) {
                webView.load(URLRequest(url: url))
            }
        }
    }
}
//MARK:- WebKit Script Handler Delegate
extension LessonVideoTableViewCell: WKScriptMessageHandler,WKNavigationDelegate {
    /*func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       webView.evaluateJavaScript("setImageSource(\"sAGCzvjEFw7wmL7scnJni789\")", completionHandler: nil)
    }*/
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("\(message.name): \(message.body)")
        if message.name == Constants.web_configuration_videoTime {
            currentTimeOfVideo = "\(message.body)"
        }
        if message.name == Constants.web_configuration_videoStatus && (((message.body as? String) ?? "") == "videoPaused") {
            let timeInSecond = Int(Float(currentTimeOfVideo) ?? 0)
            hmsFrom(seconds: timeInSecond) { hours, minutes, seconds in
                let hours = self.getStringFrom(seconds: hours)
                let minutes = self.getStringFrom(seconds: minutes)
                let seconds = self.getStringFrom(seconds: seconds)
                print("\(hours):\(minutes):\(seconds)")
                self.completionForLastTime?("\(hours):\(minutes):\(seconds)")
            }
        }
    }
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
}
/*
class ViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet var buttonSeekTo: [UIButton]!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK: --
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.configuration.userContentController = WKUserContentController()
        webView.configuration.userContentController.add(self, name: "videoStatus")
        webView.configuration.userContentController.add(self, name: "videoTime")
        webView.configuration.userContentController.add(self, name: "videoSeek")
        loadWebView()
        for i in 0...buttonSeekTo.count - 1 {
            buttonSeekTo[i].addTarget(self, action: #selector(actionSeek(_:)), for: .touchUpInside)
        }
    }
        
    //MARK: --
    @IBAction func actionPlay(_ sender: Any) {
        webView.evaluateJavaScript("playVideo()", completionHandler: nil)
    }
    
    @IBAction func actionPause(_ sender: Any) {
        webView.evaluateJavaScript("pauseVideo()", completionHandler: nil)
    }
    
    @objc func actionSeek(_ sender: UIButton) {
        webView.evaluateJavaScript("seek(\(sender.tag))", completionHandler: nil)
    }
    
    @IBAction func actionForward(_ sender: Any) {
        webView.evaluateJavaScript("forward()", completionHandler: nil)
    }
    
    //MARK: --
    func loadWebView() {
        if let filePath = Bundle.main.path(forResource: "vidyard", ofType: "html") {
            do {
                let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
                let baseUrl = URL(fileURLWithPath: filePath)
                webView.loadHTMLString(contents as String, baseURL: baseUrl)
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension ViewController: WKScriptMessageHandler  {
    //MARK: --
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("\(message.name): \(message.body)")
        if message.name == "videoStatus" {
            let alert = UIAlertController(title: message.body as? String, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

*/
