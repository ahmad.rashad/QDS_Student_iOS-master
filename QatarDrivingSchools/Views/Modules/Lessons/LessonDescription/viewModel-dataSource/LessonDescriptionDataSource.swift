//
//  LessonDescriptionDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import Foundation
import UIKit
import NISLRequestPackages
class LessonDescriptionDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: LessonDescriptionViewController
    private let tableView: UITableView
    private let viewModel: LessonDescriptionViewModel
    
    var isAnimationRequired = true
    
    enum enumLessonDescriptionSection: Int {
        case space
        case title
        case introduction
        case quiz
        static let count : Int = {
            var max = 0
            while let _ = enumLessonDescriptionSection(rawValue: max) { max += 1 }
            return max
        }()
    }
    //MARK:- Init
    init(tableView: UITableView, viewModel: LessonDescriptionViewModel, viewController: LessonDescriptionViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LessonDescriptionTitleTableViewCell.identifier,
                                         LessonIntroductionTableViewCell.identifier,
                                         InstructorProfileTableViewCell.identifier,
                                         RadioLabelTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TestOverviewTableViewCell.identifier,
                                         LessonVideoTableViewCell.identifier])
        //tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonPlayCourseTouchUpInside(_ sender: UIButton) {
        let alert = UIAlertController(title: "In-progress feature", message: "Would you like to mark course as complete?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.viewModel.updateStudentsProgress(isMarkAsComplete: true)
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    @objc func buttonLessonVideoTouchUpInside(_ sender: UIButton){
        let lessonInFullScreen = LessonInFullScreenViewController.loadFromNib()
        lessonInFullScreen.viewModel.setQuestions(value: viewModel.getQuestions())
        lessonInFullScreen.viewModel.setEnglishQuestion(value: viewModel.getEnglishQuestions())
        lessonInFullScreen.viewModel.setVideoId(value: viewModel.getVideoId())
        lessonInFullScreen.viewModel.setLesson(value: viewModel.getLesson())
        lessonInFullScreen.lessonProgressStatusUpdated = {
            self.viewModel.getLessonDetail(isFullUpdateRequire: true)
        }
        self.viewController.navigationController?.pushViewController(lessonInFullScreen, animated: true)
    }
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumLessonDescriptionSection.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumLessonDescriptionSection(rawValue: section) {
        /*case .skillsList:
            return 0*/
        case .quiz:
            return viewModel.getTestCount()
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumLessonDescriptionSection(rawValue: indexPath.section)! {
        case .space:
            return tableView.frame.height / 3
        case .introduction:
            let isEmpty = "\(viewModel.getDescription())\(viewModel.getDate())".isEmpty
            return isEmpty ? 0 : UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumLessonDescriptionSection(rawValue: indexPath.section)! {
        case .space:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonVideoTableViewCell.identifier) as! LessonVideoTableViewCell
            cell.buttonPlay.isHidden = true
            cell.buttonPlay.addTarget(self, action: #selector(buttonPlayCourseTouchUpInside(_:)), for: .touchUpInside)
            cell.setWebView(seekValue: viewModel.getProgressedDuration(), videoId: viewModel.getVideoId())
            cell.completionForLastTime = { time in
                print("time: ",time)
                self.viewModel.setTimeDuration(value: time)
                self.viewModel.updateStudentsProgress(isMarkAsComplete: false)
            }
            cell.button.addTarget(self, action: #selector(buttonLessonVideoTouchUpInside(_:)), for: .touchUpInside)
            return cell
            /*let cell = UITableViewCell.defaultCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell*/
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonDescriptionTitleTableViewCell.identifier, for: indexPath) as! LessonDescriptionTitleTableViewCell
            cell.setDetails(lessonTitle: viewModel.getLessonTitle(), courseTitle: viewModel.getCourseTitle(), duration: viewModel.getDuration(), progress: viewModel.getProgress(), isAnimationRequired: isAnimationRequired)
            isAnimationRequired = false
            return cell
        case .introduction:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonIntroductionTableViewCell.identifier) as! LessonIntroductionTableViewCell
            cell.setDetails(description: viewModel.getDescription(), date: viewModel.getDate())
            return cell
        case .quiz:
            let cell = tableView.dequeueReusableCell(withIdentifier: TestOverviewTableViewCell.identifier) as! TestOverviewTableViewCell
            cell.setupDetails(test: viewModel.getTestDetails(at: indexPath.row))
            cell.topImageViewHeight.constant = 0
            cell.sideImageWidth.constant = 50
            if viewModel.getProgress() >= 100.0 {
                cell.buttonArrow.isEnabled = true
            }else{
                cell.buttonArrow.isEnabled = false
            }
            return cell
        /*case .skillsTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.setupAttributedText(text: "Skills Covered in this video")
            return cell
        case .skillsList:
            let cell = tableView.dequeueReusableCell(withIdentifier: RadioLabelTableViewCell.identifier) as! RadioLabelTableViewCell
            cell.removeTopBottomSpace()
            return cell
        case .instructor:
            let cell = tableView.dequeueReusableCell(withIdentifier: InstructorProfileTableViewCell.identifier) as! InstructorProfileTableViewCell
            return cell*/
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumLessonDescriptionSection(rawValue: indexPath.section)! {
        case .space:
          break
        case .title:
            break
        case .introduction:
            break
        case .quiz:
            let test = viewModel.getTestDetails(at: indexPath.row)
            if viewModel.getProgress() < 100.0 {
                let alertVC = AlertViewController.loadFromNib()
                alertVC.viewModel.setImage(value: #imageLiteral(resourceName: "lock_ic"))
                alertVC.viewModel.setTitle(value: "")
                alertVC.viewModel.setSubTitle(value: Constants.alert_completeLessonFirstForQuiz.localized)
                alertVC.modalPresentationStyle = .overFullScreen
                self.viewController.present(alertVC, animated: true, completion: nil)
            }else if test.totalQuestion == 0 {
                viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_noQuestions.localized)
            }else{
                let alertController = UIAlertController(title: "", message: Constants.alert_startQuiz.localized, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .default, handler: { _ in
                    let questionVC = QuestionsViewController.loadFromNib()
                    questionVC.viewModel.setTestDetails(value: test)
                    questionVC.viewModel.setLesson(value: self.viewModel.getLesson())
                    questionVC.viewModel.setPreviousResultStatus(value: self.viewModel.getTestResultStatus())
                    self.viewController.navigationController?.pushViewController(questionVC, animated: true)
                }))
                alertController.addAction(UIAlertAction(title: Constants.title_no.localized, style: .destructive, handler: nil))
                viewController.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
