//
//  LessonDescriptionViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import Foundation
import NISLRequestPackages
class LessonDescriptionViewModel {
    
    //MARK:- Variables
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isLessonsFetched: Dynamic<Bool> = Dynamic(true)
    var isLessonsDetailsUpdateRequired: Dynamic<Bool> = Dynamic(true)
    var isQuestionFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())

    private var lesson = Lessons()
    private var timeDuration = ""
    private var questions: [Questions] = []
    private var englishQuestion: [Questions] = []
    private var videoId = ""
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - APIs
    func getLessonDetail(isFullUpdateRequire: Bool) {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "lessonId":lesson.lessonId ?? 0,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetLessonDetail, queryParameter: param) { (res: Swift.Result<BaseClass<LessonsBaseClass>, AlertMessage>, response, jsonData) in
            //self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if (Language.language == .ENGLISH) {
                    self.isLoaderHidden.value = true
                    if data.status == Constants.api_success{
                        if let lesson = data.data?.lessons?.first {
                            self.lesson = lesson
                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    if isFullUpdateRequire {
                        self.isLessonsFetched.value = true
                    }else{
                        self.isLessonsDetailsUpdateRequired.value = true
                    }
                    self.getVideoQuestions()
                }else{
                    if data.status == Constants.api_success{
                        if let lesson = data.data?.lessons?.first {
                            self.lesson = lesson
                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    
                    var localizeLesson: [LocalizeLessonBaseClass] = []
                    if let translation = data.data?.lessons?.first?.translation {
                        let jsonData = Data(translation.utf8)
                        do {
                            localizeLesson = try JSONDecoder().decode([LocalizeLessonBaseClass].self, from: jsonData)
                            print(localizeLesson)
                        } catch let error{
                            print(error.localizedDescription)
                        }
                    }
                    
                    for lesson in localizeLesson {
                        if lesson.code == Language.language.rawValue {
                            self.lesson.lessonTitle = lesson.lessonTitle
                            self.lesson.descriptionValue = lesson.description
                            self.lesson.courseTitle = lesson.courseTitle
                            if (self.lesson.tests?.count ?? 0) > 0 {
                                var localize: [EntityTranslation] = []
                                if let translation = lesson.entity?.first?.examName {
                                    let jsonData = Data(translation.utf8)
                                    do {
                                        localize = try JSONDecoder().decode([EntityTranslation].self, from: jsonData)
                                        print(localizeLesson)
                                    } catch let error{
                                        print(error.localizedDescription)
                                    }
                                    for tmp in localize {
                                        if tmp.code == Language.language.rawValue {
                                            self.lesson.tests?[0].examName = tmp.examName
                                            break
                                        }
                                    }
                                }
                            }
                            break
                        }
                    }
                    
                    
                    if isFullUpdateRequire {
                        self.isLessonsFetched.value = true
                    }else{
                        self.isLessonsDetailsUpdateRequired.value = true
                    }
                    self.getVideoQuestions()
                    
                    /*if let jsonData = jsonData {
                        var request = URLRequest(url: URL(string: "http://clientapp.narola.online:9564/process_post?languageCode=\(Language.language.rawValue)")!)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        let session = URLSession.shared
                        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                            DispatchQueue.main.async {
                                self.isLoaderHidden.value = true
                                do {
                                    if let data = data {
                                        let data = try JSONDecoder().decode(BaseClass<LessonsBaseClass>.self, from: data)
                                      
                                        if data.status == Constants.api_success{
                                            if let lesson = data.data?.lessons?.first {
                                                self.lesson = lesson
                                            }
                                        } else {
                                            self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                                        }
                                        if isFullUpdateRequire {
                                            self.isLessonsFetched.value = true
                                        }else{
                                            self.isLessonsDetailsUpdateRequired.value = true
                                        }
                                        self.getVideoQuestions()
                                    }
                                } catch {
                                    print("error")
                                }
                            }
                        })
                        task.resume()
                    }*/
                }
                
               
            case .failure(let message):
                self.isLoaderHidden.value = true
                self.alertMessage.value = message
                //self.getVideoQuestions()
            }
        }
    }
    func updateStudentsProgress(isMarkAsComplete: Bool) {
        if isMarkAsComplete {
            timeDuration = lesson.duration ?? ""
        }
        if timeDuration.isEmpty {
            return
        }
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "LessonId":lesson.lessonId ?? 0,
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "VideoTime":timeDuration
        ]
        isLoaderHidden.value = false
        apiManager.upload(type: RequestItemsType.UpdateStudentsProgress, params: param) { (res: Swift.Result<BaseClass<StudentsProgressData>, AlertMessage>, response) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.lesson.progress = data.data?.studentsProgress?.first?.lessonProgress ?? self.lesson.progress
                    self.getLessonDetail(isFullUpdateRequire: false)
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
    
    func getVideoQuestions() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "lessonId":lesson.lessonId ?? 0,
            "CourseId":lesson.courseId ?? 0
        ]
        apiManager.call(type: RequestItemsType.GetVideoQuestions, queryParameter: param) { (res: Swift.Result<BaseClass<QuestionsBaseClass>, AlertMessage>, response, jsonData) in
            switch res {
            case .success(let data):
                if (Language.language == .ENGLISH) {
                    self.isLoaderHidden.value = true
                    if data.status == Constants.api_success{
                        self.questions = data.data?.questions ?? []
                        self.englishQuestion = self.questions
                        for question in self.questions {
                            self.videoId = question.videoId ?? ""
                            break
                        }
                    }
                    self.isQuestionFetched.value = true
                }else{
                    if data.status == Constants.api_success{
                        self.questions = data.data?.questions ?? []
                        self.englishQuestion = self.questions
                    }
                    var languageQuestions: [LanguageQuestion] = []
                    if let translation = data.data?.questions?.first?.translation {
                        let jsonData = Data(translation.utf8)
                        do {
                            languageQuestions = try JSONDecoder().decode([LanguageQuestion].self, from: jsonData)
                            print(languageQuestions)
                        } catch let error{
                            print(error.localizedDescription)
                        }
                    }
                    
                    var selectedLanguagesQuestion: LanguageQuestion?
                    for question in languageQuestions {
                        if question.code == Language.language.rawValue {
                            selectedLanguagesQuestion = question
                            break
                        }
                    }
                    for index in 0..<self.questions.count {
                        if let selectedLanguagesQuestion = selectedLanguagesQuestion {
                            self.questions[index].question = selectedLanguagesQuestion.question ?? ""
                            for optionIndex in 0..<(self.questions[index].options?.count ?? 0) {
                                let option = self.questions[index].options?[optionIndex].option?.lowercased()
                                if option == "Option1".lowercased() {
                                    self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option1 ?? ""
                                }
                                else if option == "Option2".lowercased() {
                                    self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option2 ?? ""
                                }
                                else if option == "Option3".lowercased() {
                                    self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option3 ?? ""
                                }
                                else if option == "Option4".lowercased() {
                                    self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option4 ?? ""
                                }
                            }
                        }
                        
                    }
                    self.isLoaderHidden.value = true
                    
                    
                    /*if let jsonData = jsonData {
                        var request = URLRequest(url: URL(string: "http://clientapp.narola.online:9564/process_post?languageCode=\(Language.language.rawValue)")!)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        let session = URLSession.shared
                        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                            DispatchQueue.main.async {
                                self.isLoaderHidden.value = true
                                do {
                                    if let data = data {
                                        let data = try JSONDecoder().decode(BaseClass<QuestionsBaseClass>.self, from: data)
                                        if data.status == Constants.api_success{
                                            self.questions = data.data?.questions ?? []
                                            for question in self.questions {
                                                self.videoId = question.videoId ?? ""
                                                break
                                            }
                                        } else {
                                        }
                                        self.isQuestionFetched.value = true
                                    }
                                } catch {
                                    print("error")
                                }
                            }
                        })
                        task.resume()
                    }*/
                }
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
}
//MARK:- Getters and Setters
extension LessonDescriptionViewModel {
    func setLesson(value: Lessons) {
        lesson = value
    }
    func getLessonTitle() -> String {
        lesson.lessonTitle ?? ""
    }
    func getCourseTitle() -> String {
        lesson.courseTitle ?? ""
    }
    func getDuration() -> String {
        lesson.duration ?? ""
    }
    func getProgressedDuration() -> String {
        lesson.pausedFrom ?? "00:00:00"
    }
    func getProgress() -> Float {
        lesson.progress ?? 0.0
    }
    func getDescription() -> String {
        return lesson.descriptionValue ?? ""
    }
    func getDate() -> String {
        ""
    }
    func getLessonMediaURL() -> URL? {
        Constants.getPathOfMedia(value: lesson.thumbnailImage ?? "")
    }
    func  getTestDetails(at index: Int) -> Tests {
        lesson.tests?[index] ?? Tests()
    }
    func getTestCount() -> Int {
        lesson.tests?.count ?? 0
    }
    func setTimeDuration(value: String) {
        timeDuration = value
    }
    func getCourseId() -> Int {
        lesson.courseId ?? 0
    }
    func getLessonId() -> Int {
        lesson.lessonId ?? 0
    }
    func getQuestions() -> [Questions] {
        questions
    }
    func getEnglishQuestions() -> [Questions] {
        englishQuestion
    }
    func getVideoId() -> String {
        lesson.vidyardLink ?? ""
    }
    func getLesson() -> Lessons {
        lesson
    }
    func getTestResultStatus() -> enumResultType {
        enumResultType(rawValue: lesson.testResultDetail?.first?.resultStatus ?? enumResultType.failed.rawValue) ?? .failed
    }
}


