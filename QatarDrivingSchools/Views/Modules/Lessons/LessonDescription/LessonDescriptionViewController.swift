//
//  LessonDescriptionViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit
import NotificationBannerSwift
class LessonDescriptionViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewTop: UIImageView!
    
    var dataSource: LessonDescriptionDataSource?
    lazy var viewModel = LessonDescriptionViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = LessonDescriptionDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func buttonMarkAsComplete(_ sender: Any) {
        let alert = UIAlertController(title: "In-progress feature", message: "Would you like to mark course as complete?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.viewModel.updateStudentsProgress(isMarkAsComplete: true)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Class Methods
    func setupLayout(){
        labelTitle.text = viewModel.getLessonTitle()
        labelTitle.font = CustomFont.medium(ofSize: 18)
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }else{
                self.activityIndicator.startAnimating()
                self.view.isUserInteractionEnabled = false
            }
        }
        viewModel.isLessonsFetched.bind { _ in
            self.tableView.reloadData()
        }
        viewModel.isQuestionFetched.bind { _ in
            self.tableView.reloadSections(IndexSet([LessonDescriptionDataSource.enumLessonDescriptionSection.space.rawValue]), with: .none)
        }
        viewModel.alertMessage.bind { message in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isLessonsDetailsUpdateRequired.bind { _ in
            self.tableView.reloadSections([LessonDescriptionDataSource.enumLessonDescriptionSection.introduction.rawValue,
                                           LessonDescriptionDataSource.enumLessonDescriptionSection.title.rawValue,
                                           LessonDescriptionDataSource.enumLessonDescriptionSection.quiz.rawValue],
                                          with: .none)
        }
        viewModel.getLessonDetail(isFullUpdateRequire: true)
        
        imageViewTop.sd_setImage(with: viewModel.getLessonMediaURL(), placeholderImage: Constants.image_noData, options: [.refreshCached], progress: nil, completed: nil)
    }
}
