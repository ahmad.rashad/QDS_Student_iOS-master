//
//  QuestionsViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit
import NISLRequestPackages
class QuestionsViewModel {
    
    //MARK: - Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isQuestionFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var questions: [Questions] = []
    private var englishQuestions: [Questions] = []
    private var totalTimeDuration: String = ""
    private var test = Tests()
    private var previousResultStatus: enumResultType = .failed
    private var lesson = Lessons()
    
    //MARK: - Dependancy Injection
    init() {    }
    
    //MARK: - API
    func getQuestionsForTest() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "ExamId":test.examId ?? 0,
            "emailId":Constants.getLoggedInUserEmailForAPI()
        ]
        /*#if DEBUG
        param = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "ExamId":3,
        ]
        #endif*/
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetQuestionsForTest, queryParameter: param) { (res: Swift.Result<BaseClass<QuestionsBaseClass>, AlertMessage>, response, jsonData) in
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.englishQuestions = data.data?.questions ?? []
                    
                    if Language.language == .ENGLISH {
                        self.isLoaderHidden.value = true
                        if data.status == Constants.api_success{
                            self.questions = data.data?.questions ?? []
                            self.englishQuestions = self.questions
                            if data.data?.resultStatus == 1
                            {
                                self.previousResultStatus = .pass
                            } else {
                                self.previousResultStatus = .failed
                            }
                    

                        }
                        self.isQuestionFetched.value = true
                    }else{
                        if data.status == Constants.api_success{
                            self.englishQuestions = data.data?.questions ?? []
                            
                            if data.data?.resultStatus == 1
                            {
                                self.previousResultStatus = .pass
                            } else {
                                self.previousResultStatus = .failed
                            }
                        }
                        self.questions = data.data?.questions ?? []
                        
                        for index in 0..<self.questions.count {
                            var languageQuestions: [LanguageQuestion] = []
                            if let translation = self.questions[index].translation {
                                let jsonData = Data(translation.utf8)
                                do {
                                    languageQuestions = try JSONDecoder().decode([LanguageQuestion].self, from: jsonData)
                                    print(languageQuestions)
                                } catch let error{
                                    print(error.localizedDescription)
                                }
                            }
                            var selectedLanguagesQuestion: LanguageQuestion?
                            for question in languageQuestions {
                                if question.code == Language.language.rawValue {
                                    selectedLanguagesQuestion = question
                                    break
                                }
                            }
                            
                            
                            if let selectedLanguagesQuestion = selectedLanguagesQuestion {
                                self.questions[index].question = selectedLanguagesQuestion.question ?? ""
                                for optionIndex in 0..<(self.questions[index].options?.count ?? 0) {
                                    let option = self.questions[index].options?[optionIndex].option?.lowercased()
                                    if option == "Option1".lowercased() {
                                        self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option1 ?? ""
                                    }
                                    else if option == "Option2".lowercased() {
                                        self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option2 ?? ""
                                    }
                                    else if option == "Option3".lowercased() {
                                        self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option3 ?? ""
                                       
                                    
                                    }
                                    else if option == "Option4".lowercased() {
                                        self.questions[index].options?[optionIndex].text = selectedLanguagesQuestion.option?.option4 ?? ""
                                        
                                    }
                                }
                            }
                            
                        }
                        
                        
                        
                        self.isLoaderHidden.value = true
                        self.isQuestionFetched.value = true
                        /*if let jsonData = jsonData {
                            var request = URLRequest(url: URL(string: "http://clientapp.narola.online:9564/process_post?languageCode=\(Language.language.rawValue)")!)
                            request.httpMethod = "POST"
                            request.httpBody = jsonData
                            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                            let session = URLSession.shared
                            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                                DispatchQueue.main.async {
                                    self.isLoaderHidden.value = true
                                    do {
                                        if let data = data {
                                            let data = try JSONDecoder().decode(BaseClass<QuestionsBaseClass>.self, from: data)
                                            if data.status == Constants.api_success{
                                                self.questions = data.data?.questions ?? []
                                            } else {
                                               // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                                            }
                                            self.isQuestionFetched.value = true
                                        }
                                    } catch {
                                        print("error")
                                    }
                                }
                            })
                            task.resume()
                        }*/
                    }
                 
                   
               
                    
                } else {
                    self.isLoaderHidden.value = true
                    //self.alertMessage.value = AlertMessage(title: Constants.alert_title_alert, body: data.message ?? "")
                }
                
                
                
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
    
    func updateQuestionsOptions(){
        for index in 0..<self.questions.count {
            
            if(self.questions[index].options != nil && self.questions[index].options!.count > 0){
                self.questions[index].options!.removeAll(where: { $0.text == ""})
            }
        }
    }
}
//MARK: - Getters and Setters
extension QuestionsViewModel {
    func getLesson() -> Lessons {
        lesson
    }
    func getTotalNumberOfQuestions() -> Int {
        questions.count
    }
    func getQuestion(at index: Int) -> Questions {
        updateQuestionsOptions()
        return questions[index]
    }
    func getAllQuestions() -> [Questions] {
        englishQuestions
    }
    func getLastQuestionCountWithAnswer() -> Int {
        for i in 0..<questions.count{
            if questions[i].selectedAnswer == nil {
                return i
            }
        }
        return questions.count
    }
    func getQuestionOption(at index: Int, optionIndex: Int) -> String {
        questions[index].options?[optionIndex].text ?? ""
    }
    func getPreviousResultStatus() -> enumResultType {
        print(previousResultStatus)
        return previousResultStatus
    }
    func setTotalTimeDuration(value: String) {
        totalTimeDuration = value
    }
    func setTestDetails(value: Tests) {
        test = value
    }
    func setPreviousResultStatus(value: enumResultType) {
        previousResultStatus = value
    }
    func setLesson(value: Lessons) {
        lesson = value
    }
    func setSelectedAnswer(at index: Int, selectedIndex: Int) {
        questions[index].selectedAnswer = selectedIndex
        englishQuestions[index].selectedAnswer = selectedIndex
    }
    func isAnswerSelected(at index: Int, optionIndex: Int) -> Bool {
        questions[index].selectedAnswer == optionIndex
    }
    func isAnswerRight(at index: Int, optionIndex: Int) -> Bool {
        if optionIndex == 0 {
            return "option1" == englishQuestions[index].correctAnswer
        }else if optionIndex == 1 {
            return "option2" == englishQuestions[index].correctAnswer
        }else if optionIndex == 2 {
            return "option3" == englishQuestions[index].correctAnswer
        }else if optionIndex == 3 {
            return "option4" == englishQuestions[index].correctAnswer
        }
        return false
    }
    func isAnyAnswerSelected(at index: Int) -> Bool {
        questions[index].selectedAnswer != nil
    }
    func setQuestions(value: [Questions]) {
        questions = value
    }
    func setEnglishQuestions(value: [Questions]){
        englishQuestions = value
    }
    func isQuestionAnswerRight(at index: Int) -> Bool {
       let optionIndex = englishQuestions[index].selectedAnswer ?? -1
       return isAnswerRight(at: index, optionIndex: optionIndex)
    }
    func getTotalTimeDuration() -> String {
        totalTimeDuration
    }
    func getTestDetails() -> Tests {
        test
    }
    
    func getCorrectAnswerIndex(index: Int) -> Int{
        var answerIndex = -1
        if let correctAnswer = questions[index].correctAnswer{
            switch correctAnswer {
            case "option1":
                answerIndex = 0
            case "option2":
                answerIndex = 1
            case "option3":
                answerIndex = 2
            case "option4":
                answerIndex = 3

            default:
                answerIndex = -1
            }
        }
 
        return answerIndex
    }
}


