//
//  QuestionsDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import Foundation
import UIKit

class QuestionsDataSource : NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private let viewController: QuestionsViewController
    private let tableView: UITableView
    private let collectionViewQuestion: UICollectionView
    private let viewModel: QuestionsViewModel
    
    var questionCount = 0
    var lastQuestionWithoutAnswer = 0
    var timer: Timer?
    var startTime: Date = Date()
    var isSelected = false
    
    enum enumQuestionSection: Int {
        case question = 0
        case options
        static let count: Int = {
            var max: Int = 0
            while let _ = enumQuestionSection(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    func updateQuestionsOptions() {
      
    }
    

    


    
    //MARK:- Init
    init(tableView: UITableView, collectionViewQuestion: UICollectionView, viewModel: QuestionsViewModel, viewController: QuestionsViewController) {
        self.viewController = viewController
        self.collectionViewQuestion = collectionViewQuestion
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        self.updateQuestionsOptions()
        setupTableView()
        registerCollectionCell()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        registerTableCell()
    }
    
    func registerCollectionCell() {
        collectionViewQuestion.registerNib(nibNames: [NumberCollectionViewCell.identifier])
    }
    
    func registerTableCell(){
        tableView.registerNib(nibNames: [QuestionTableViewCell.identifier,
                                         QuestionOptionsTableViewCell.identifier])
        tableView.reloadData()
    }
    
    func setTimer() {
        if viewController.viewControllerType == .normal {
            startTime = Date()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
        }
    }
    func updateBottomButtons(){
        if lastQuestionWithoutAnswer == questionCount {
            viewController.buttonNext.alpha = 0.5
        }else{
            viewController.buttonNext.alpha = 1
        }
        if questionCount == 0 {
            viewController.buttonPrevious.isHidden = true
            viewController.buttonPrevious.alpha = 0.5
        }else{
            viewController.buttonPrevious.isHidden = false
            viewController.buttonPrevious.alpha = 1
        }
        if questionCount == viewModel.getTotalNumberOfQuestions() - 1 {
            if lastQuestionWithoutAnswer == questionCount {
                viewController.buttonSubmit.alpha = 0.5
            }else{
                viewController.buttonSubmit.alpha = 1
            }
        }
        viewController.buttonPrevious.isHidden = true
        viewController.buttonPrevious.alpha = 0.5
        //viewController.buttonNext.isEnabled = viewController.buttonNext.alpha == 1
        //viewController.buttonPrevious.isEnabled = viewController.buttonPrevious.alpha == 1
    }
    @objc func updateCountDown() {
        /*let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.zeroFormattingBehavior = .dropLeading
        formatter.unitsStyle = .short*/
        
        var runningTime: TimeInterval = 0
        var time: Date = Date()
        let cal: Calendar = Calendar.current
        time = cal.date(byAdding: .second, value: 1, to: time)!
        runningTime = time.timeIntervalSince(startTime)        
        viewController.buttonTimer.setTitle(" \(timeString(time: runningTime)) ", for: .normal)
    }
    func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if questionCount >= lastQuestionWithoutAnswer {
            viewController.buttonNext.isEnabled = false
            viewController.buttonSubmit.isEnabled = false
        }else{
            viewController.buttonNext.isEnabled = true
            viewController.buttonSubmit.isEnabled = true
        }
        if questionCount == viewModel.getTotalNumberOfQuestions() - 1 {
            viewController.stackViewNext.isHidden = true
            viewController.buttonSubmit.isHidden = false
        }else{
            viewController.stackViewNext.isHidden = false
            viewController.buttonSubmit.isHidden = true
        }
        if viewController.viewControllerType == .reviewAnswer {
            viewController.buttonNext.isEnabled = true
            viewController.buttonPrevious.isEnabled = true
            viewController.buttonSubmit.isEnabled = true
        }
        return enumQuestionSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumQuestionSection(rawValue: section)! {
        case .question:
            if viewModel.getTotalNumberOfQuestions() > 0 {
                return 1 
            }
            return 0
        case .options:
            if viewModel.getTotalNumberOfQuestions() > 0 {
                
                return viewModel.getQuestion(at: questionCount).options?.count ?? 0;
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumQuestionSection(rawValue: indexPath.section)! {
        case .question:
            viewController.labelCurrentQuestion.text = "\(questionCount + 1)"
            let cell = tableView.dequeueReusableCell(withIdentifier: QuestionTableViewCell.identifier) as! QuestionTableViewCell
            let question = viewModel.getQuestion(at: questionCount)
            cell.setDetails(questionNumber: questionCount + 1, question: question.question ?? "", image: question.sign ?? "")
            if viewController.viewControllerType == .videoQuestion {
                cell.labelQuestionTopSpace.constant = -40
                cell.labelQuestionTitle.text = ""
            }
            return cell
        case .options:
            let cell = tableView.dequeueReusableCell(withIdentifier: QuestionOptionsTableViewCell.identifier) as! QuestionOptionsTableViewCell
            cell.labelOption.text = viewModel.getQuestionOption(at: questionCount, optionIndex: indexPath.row)
            var image = Constants.image_unselectedRadio
            if viewModel.isAnswerSelected(at: questionCount, optionIndex: indexPath.row) {
                image = Constants.image_selectedRadio
            }
            cell.labelOption.textColor = .black
            
            if isSelected == true {
                let selectedQuestion = viewModel.getQuestion(at: questionCount)
                if let selectAnswer = selectedQuestion.selectedAnswer {
                    let correctAnswerIndex = viewModel.getCorrectAnswerIndex(index: questionCount)
                    if correctAnswerIndex == indexPath.row {
                        cell.labelOption.textColor = .green
                    } else if selectAnswer == indexPath.row {
                        if selectAnswer != correctAnswerIndex {
                            cell.labelOption.textColor = .red
                        }
                    }
                }
            }
            
            if viewController.viewControllerType == .reviewAnswer {
                if viewModel.isAnswerRight(at: questionCount, optionIndex: indexPath.row) {
                    cell.labelOption.textColor = .green
                }else{
                    cell.labelOption.textColor = .black
                }
            }
            cell.buttonCheckBox.setBackgroundImage(image, for: .normal)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumQuestionSection(rawValue: indexPath.section)! {
        case .question:
            break
        case .options:
            if viewController.viewControllerType != .reviewAnswer {
                if isSelected == false {
                    viewModel.setSelectedAnswer(at: questionCount, selectedIndex: indexPath.row)
                    lastQuestionWithoutAnswer = viewModel.getLastQuestionCountWithAnswer()
                    tableView.reloadData()
                    collectionViewQuestion.reloadData()
                    updateBottomButtons()
                }

            }
        }
    }
}

//MARK:- Collection methods
extension QuestionsDataSource : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTotalNumberOfQuestions()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NumberCollectionViewCell.identifier, for: indexPath) as! NumberCollectionViewCell
        cell.labelNumber.text = "\(indexPath.row + 1)"
        cell.bgView.cornerRadius = 15
        if viewModel.isAnyAnswerSelected(at: indexPath.row) {
            cell.bgView.borderWidth = questionCount == indexPath.row ? 1 : 0
            cell.bgView.backgroundColor = questionCount == indexPath.row ? .white : Constants.color_darkApplicationThemeColor
            cell.labelNumber.textColor = questionCount == indexPath.row ? Constants.color_darkApplicationThemeColor : .white
        }else{
            cell.bgView.backgroundColor = .white
            cell.bgView.borderWidth = questionCount == indexPath.row ? 1 : 0
            cell.labelNumber.textColor = questionCount == indexPath.row ? Constants.color_darkApplicationThemeColor : UIColor.lightGray
        }
        if viewController.viewControllerType == .reviewAnswer {
            cell.bgView.backgroundColor = viewModel.isQuestionAnswerRight(at: indexPath.row) ? .green : .red
            cell.labelNumber.textColor =  viewModel.isQuestionAnswerRight(at: indexPath.row) ? .black : .white
            cell.bgView.borderColor = Constants.color_darkApplicationThemeColor
            cell.bgView.borderWidth = questionCount == indexPath.row ? 1 : 0
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let lastQuestionCount = questionCount
//        questionCount = indexPath.row
//        if questionCount > lastQuestionWithoutAnswer && viewController.viewControllerType != .reviewAnswer {
//            questionCount = lastQuestionCount
//        }else{
//            viewController.stopSpeech()
//            collectionView.reloadData()
//            tableView.reloadData()
//        }
//        updateBottomButtons()
    }
}
