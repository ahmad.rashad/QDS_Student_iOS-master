//
//  QuestionTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelQuestionTitle: UILabel!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTopic: UILabel!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var labelQuestionTopSpace: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelQuestionTitle.font = CustomFont.semiBold()
        labelQuestion.font = CustomFont.regular()
        
        /*#if DEBUG
        setupAttributedText(text: "Signals")
        labelQuestionTitle.text = "Question 3:"
        labelQuestion.text = "What is the meaning of this sign?"
        #endif*/
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupAttributedText(title: String = Constants.description_topic, text: String) {
        let mutableAttributedString = NSMutableAttributedString(string: title + ":", attributes: [NSAttributedString.Key.font: CustomFont.semiBold()])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.regular()]))
    
        labelTopic.attributedText = mutableAttributedString
        labelTopic.textColor = .black
    }
    func setDetails(questionNumber: Int, question: String, image: String) {
        labelQuestion.text = question
        if !image.isEmpty {
            imgView.sd_setImage(with: Constants.getPathOfMedia(value: image), placeholderImage: #imageLiteral(resourceName: "not_found_ic"), options: [], completed: nil)
            imgView.isHidden = false
            imgViewHeight.constant = 200
        }else{
            imgView.isHidden = true
            imgViewHeight.constant = 0
        }
        
        labelQuestionTitle.text = "\(Constants.description_question) \(questionNumber):"
    }
}
