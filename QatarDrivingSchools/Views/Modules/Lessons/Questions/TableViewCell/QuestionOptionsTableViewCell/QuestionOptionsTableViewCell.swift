//
//  QuestionOptionsTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit

class QuestionOptionsTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var buttonCheckBox: UIButton!
    @IBOutlet weak var labelOption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelOption.font = CustomFont.regular()
        #if DEBUG
        labelOption.text = "You must stop."
        #endif
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
