//
//  QuestionsViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 15/07/21.
//

import UIKit
import AVFoundation
import NotificationBannerSwift
import NISLRequestPackages
class QuestionsViewController: UIViewController {
    
    //MARK: - Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonPrevious: RoundButton!
    @IBOutlet weak var buttonNext: RoundButton!
    @IBOutlet weak var labelCurrentQuestion: UILabel!
    @IBOutlet weak var labelTotalQuestion: UILabel!
    @IBOutlet weak var buttonTimer: UIButton!
    @IBOutlet weak var collectionViewQuestions: UICollectionView!
    @IBOutlet weak var buttonSubmit: RoundButton!
    @IBOutlet weak var stackViewNext: UIStackView!
    @IBOutlet weak var topCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var timerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonBackTime: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeader: UIImageView!
    @IBOutlet weak var buttonSpeaker: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let synthesizer = AVSpeechSynthesizer()
    
    var dataSource : QuestionsDataSource?
    lazy var viewModel = QuestionsViewModel()
    var completionHandlerForVideoQuestionAnswered: ((_ isCorrect: Bool, _ selectedAnswer: Int?)-> Void)?
    
    //var isForReviewAnswers = false
    enum enumViewControllerType {
        case normal
        case reviewAnswer
        case videoQuestion
    }
    var viewControllerType: enumViewControllerType = .normal
    
    //MARK: - Activity .life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = QuestionsDataSource(tableView: tableView,collectionViewQuestion: collectionViewQuestions, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        collectionViewQuestions.dataSource = dataSource
        collectionViewQuestions.delegate = dataSource
        setupLayout()
    }
    
    //MARK: - Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        synthesizer.stopSpeaking(at: .immediate)
        SpeechService.shared.audioPlayerDidFinishPlaying(nil, successfully: true)
        buttonSpeaker.isEnabled = true
        switch viewControllerType {
        case .reviewAnswer:
            self.navigationController?.popViewController(animated: true)
        case .normal:
            let alert = UIAlertController(title: "", message: Constants.alert_exitExam.localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .destructive, handler: { _ in
                self.dataSource?.timer?.invalidate()
                self.navigationController?.popViewController(animated: true)
            }))
            alert.addAction(UIAlertAction(title: Constants.title_no.localized, style: .default, handler: { _ in
                
            }))
            present(alert, animated: true, completion: nil)
        case .videoQuestion:
            rotateToLandsScapeDevice()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonNextTouchUpInside(_ sender: RoundButton) {
        stopSpeech()
        //let isCorrectAnswer = dataSource?.validateSelectedOption(index: dataSource?.questionCount ?? 0);
        
        if dataSource?.isSelected == true {
            dataSource?.isSelected = false
            dataSource?.questionCount += 1
            if (dataSource?.questionCount ?? 0) == viewModel.getTotalNumberOfQuestions() {
                dataSource?.questionCount = viewModel.getTotalNumberOfQuestions() - 1
            }
            tableView.reloadData()
            collectionViewQuestions.reloadData()
            collectionViewQuestions.scrollToItem(at: IndexPath(item: (dataSource?.questionCount ?? 0) - 1, section: 0), at: .left, animated: true)
            dataSource?.updateBottomButtons()
        } else {
            dataSource?.isSelected = true
            tableView.reloadData()
        }
        
//        dataSource?.questionCount += 1
//        if (dataSource?.questionCount ?? 0) == viewModel.getTotalNumberOfQuestions() {
//            dataSource?.questionCount = viewModel.getTotalNumberOfQuestions() - 1
//        }
        //tableView.reloadData()
//        collectionViewQuestions.reloadData()
//        collectionViewQuestions.scrollToItem(at: IndexPath(item: (dataSource?.questionCount ?? 0) - 1, section: 0), at: .left, animated: true)
//        dataSource?.updateBottomButtons()
    }
    
    @IBAction func buttonPreviousTouchUpInside(_ sender: RoundButton) {
        stopSpeech()
        dataSource?.questionCount -= 1
        if (dataSource?.questionCount  ?? 0) < 0 {
            dataSource?.questionCount = 0
        }
        tableView.reloadData()
        collectionViewQuestions.reloadData()
        collectionViewQuestions.scrollToItem(at: IndexPath(item: (dataSource?.questionCount ?? 0) - 1, section: 0), at: .left, animated: true)
        dataSource?.updateBottomButtons()
    }
    
    @IBAction func buttonSpeakerTouchUpInside(_ sender: UIButton) {
        buttonSpeaker.isEnabled = false
        let questionCount = viewModel.getTotalNumberOfQuestions()
        let currentQuestionNumber = dataSource?.questionCount ?? -1
        
        if questionCount > currentQuestionNumber && currentQuestionNumber >= 0 {
            let question = viewModel.getQuestion(at: currentQuestionNumber)
            var stringToSpeech = question.question ?? ""
            for option in question.options ?? [] {
                stringToSpeech.append(" " + (option.option ?? ""))
                stringToSpeech.append(" " + (option.text ?? ""))
            }
            let utterance = AVSpeechUtterance(string: stringToSpeech)
            utterance.rate = 0.5
            switch Language.language {
            case .DEFAULT_LANGUAGE:
                var stringToSpeech = question.question ?? ""
                for option in question.options ?? [] {
                    // " - - - - - - - " used for pause in speech
                    stringToSpeech.append(" - - - - - - - ")
                    stringToSpeech.append(option.option ?? "")
                    stringToSpeech.append(" - - - - - - - ")
                    stringToSpeech.append(option.text ?? "")
                }
                let utterance = AVSpeechUtterance(string: stringToSpeech)
                utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
                utterance.rate = 0.5
                synthesizer.speak(utterance)
            case .ENGLISH:
                var stringToSpeech = question.question ?? ""
                for option in question.options ?? [] {
                    // " - - - - - - - " used for pause in speech
                    stringToSpeech.append(" - - - - - - - ")
                    stringToSpeech.append(option.option ?? "")
                    stringToSpeech.append(" - - - - - - - ")
                    stringToSpeech.append(option.text ?? "")
                }
                let utterance = AVSpeechUtterance(string: stringToSpeech)
                utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
                utterance.rate = 0.5
                synthesizer.speak(utterance)
            case .ARABIC:
                utterance.voice = AVSpeechSynthesisVoice(language: "ar")
                synthesizer.speak(utterance)
            case .BANGALI:
                //not working
                SpeechService.shared.speak(text: stringToSpeech, voiceType: "bn") { }
                //utterance.voice = AVSpeechSynthesisVoice(language: "bn")
                //synthesizer.speak(utterance)
            case .CHINESE:
                utterance.voice = AVSpeechSynthesisVoice(language: "zh")
                synthesizer.speak(utterance)
            case .FILIPINO:
                utterance.voice = AVSpeechSynthesisVoice(language: "fil")
                synthesizer.speak(utterance)
            case .FRENCH:
                utterance.voice = AVSpeechSynthesisVoice(language: "fr")
                synthesizer.speak(utterance)
            case .HINDI:
                for option in question.options ?? [] {
                    stringToSpeech.append(" " + (option.option ?? ""))
                    stringToSpeech.append(" " + (option.text ?? ""))
                }
                let utterance = AVSpeechUtterance(string: stringToSpeech)
                utterance.voice = AVSpeechSynthesisVoice(language: "hi")
                utterance.rate = 0.5
                synthesizer.speak(utterance)
            case .MALAYALAM:
                // TODO : Google
                SpeechService.shared.speak(text: stringToSpeech, voiceType: "ml") { }
               // utterance.voice = AVSpeechSynthesisVoice(language: "ml")
               // synthesizer.speak(utterance)
            case .NEPALI:
                // TODO : Google
                SpeechService.shared.speak(text: stringToSpeech, voiceType: "ne") { }
                //utterance.voice = AVSpeechSynthesisVoice(language: "ne")
                //synthesizer.speak(utterance)
            case .PASHTO:
                // TODO:  both not supported (apple-google) - show alert need to verify once
                buttonSpeaker.isEnabled = true
                viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_textToSpeechNotSupported)
                //SpeechService.shared.speak(text: stringToSpeech, voiceType: "ps") { }
                //utterance.voice = AVSpeechSynthesisVoice(language: "ps")
                //synthesizer.speak(utterance)
            case .PERSIAN:
                // TODO:  both not supported (apple-google) - show alert
                buttonSpeaker.isEnabled = true
                viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_textToSpeechNotSupported)
                //utterance.voice = AVSpeechSynthesisVoice(language: "fa")
                //synthesizer.speak(utterance)
            case .RUSSIAN:
                utterance.voice = AVSpeechSynthesisVoice(language: "ru")
                synthesizer.speak(utterance)
            case .SINHALA:
                // TODO:  both not supported (apple-google) - show alert need to verify once
                buttonSpeaker.isEnabled = true
                viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_textToSpeechNotSupported)
                //utterance.voice = AVSpeechSynthesisVoice(language: "si")
                //synthesizer.speak(utterance)
            case .SPANISH:
                utterance.voice = AVSpeechSynthesisVoice(language: "es")
                synthesizer.speak(utterance)
            case .TAMIL:
                // TODO: Google
                SpeechService.shared.speak(text: stringToSpeech, voiceType: "ta") { }
                //utterance.voice = AVSpeechSynthesisVoice(language: "ta")
                //synthesizer.speak(utterance)
            case .TELUGU:
                // TODO: Google
                SpeechService.shared.speak(text: stringToSpeech, voiceType: "te") { }
                //utterance.voice = AVSpeechSynthesisVoice(language: "te")
                //synthesizer.speak(utterance)
            case .TURKISH:
                utterance.voice = AVSpeechSynthesisVoice(language: "tr")
                synthesizer.speak(utterance)
            case .URDU:
                // TODO:  both not supported (apple-google) - show alert
                buttonSpeaker.isEnabled = true
                viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_textToSpeechNotSupported)
                //utterance.voice = AVSpeechSynthesisVoice(language: "ur")
                //synthesizer.speak(utterance)
            }
           
            //utterance.voice = AVSpeechSynthesisVoice(language: "hi")
          
        }
    }
    
    @objc func buttonSubmitTouchUpInside(_ sender: UIButton) {
        switch viewControllerType {
        case .reviewAnswer:
            self.navigationController?.popViewController(animated: true)
        case .normal:
            let alert = UIAlertController(title: "", message: Constants.alert_confirmToSubmitAnswers.localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .default, handler: { _ in
                self.dataSource?.timer?.invalidate()
                let resultVC = ExamResultViewController.loadFromNib()
                resultVC.viewModel.setTestDetails(value: self.viewModel.getTestDetails())
                resultVC.viewModel.setTimeDuration(value: self.buttonTimer.title(for: .normal) ?? "")
                resultVC.viewModel.setQuestionsAnswers(value: self.viewModel.getAllQuestions())
                resultVC.viewModel.setPreviousResultStatus(value: self.viewModel.getPreviousResultStatus())
                resultVC.viewModel.setLesson(value: self.viewModel.getLesson())
                
                self.navigationController?.pushViewController(resultVC, animated: true)
            }))
            alert.addAction(UIAlertAction(title: Constants.title_no.localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .videoQuestion:
            rotateToLandsScapeDevice()
            if viewModel.getTotalNumberOfQuestions() > 0 {
                completionHandlerForVideoQuestionAnswered?(viewModel.isQuestionAnswerRight(at: 0), viewModel.getQuestion(at: 0).selectedAnswer)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.description_quiz.localized
        buttonPrevious.setTitle(Constants.title_previous.localized, for: .normal)
        buttonNext.setTitle(Constants.title_next.localized, for: .normal)
        buttonPrevious.titleLabel?.font = CustomFont.regular()
        buttonNext.titleLabel?.font = CustomFont.semiBold()
        buttonSubmit.titleLabel?.font = CustomFont.semiBold()
        buttonSubmit.setTitle(Constants.title_submit, for: .normal)
        buttonSubmit.isHidden = true
        labelCurrentQuestion.font = CustomFont.bold(ofSize: 22)
        labelTotalQuestion.font = CustomFont.regular(ofSize: 22)
        buttonTimer.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        buttonSubmit.isEnabled = false
        buttonSubmit.addTarget(self, action: #selector(buttonSubmitTouchUpInside(_:)), for: .touchUpInside)
        viewModel.isQuestionFetched.bind { _ in
            self.tableView.reloadData()
            self.collectionViewQuestions.reloadData()
            self.labelTotalQuestion.text = "Of \(self.viewModel.getTotalNumberOfQuestions())"
            self.dataSource?.setTimer()
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        if viewControllerType == .reviewAnswer {
            labelTotalQuestion.text = "Of \(viewModel.getTotalNumberOfQuestions())"
            buttonTimer.setTitle(viewModel.getTotalTimeDuration(), for: .normal)
            buttonSubmit.setTitle(Constants.title_done, for: .normal)
            buttonSubmit.isEnabled = true
        }else{
            viewModel.getQuestionsForTest()
        
        }
        if viewControllerType == .videoQuestion {
            timerViewHeight.constant = 0
            buttonBackTime.constant = 0
            topCollectionViewHeight.constant = 0
            imageViewHeader.isHidden = true
        }
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        synthesizer.delegate = self
        
        buttonPrevious.alpha = 0.5
        buttonNext.alpha = 0.5
        buttonPrevious.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func rotateToLandsScapeDevice(){
        GlobalVariables.appDelegate?.myOrientation = .landscapeRight
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    func stopSpeech() {
        synthesizer.stopSpeaking(at: .immediate)
        SpeechService.shared.audioPlayerDidFinishPlaying(nil, successfully: true)
        buttonSpeaker.isEnabled = true
    }
}
//MARK:- Speak Delegate
extension QuestionsViewController: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        buttonSpeaker.isEnabled = true
    }
}
