//
//  CompletedLessonsViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import UIKit

class CompletedLessonsViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: CompletedLessonsDataSource?
    lazy var viewModel = CompletedLessonsViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
              
        dataSource = CompletedLessonsDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Class methods
    func setupLayout() {
        labelTitle.text = Constants.description_completedLessons
        labelTitle.font = CustomFont.medium(ofSize: 18)
        viewModel.isLessonsFetched.bind { _ in
            self.tableView.reloadData()
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.getCompletedLessons()
    }
}
