//
//  CompletedLessonsDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import Foundation
import UIKit

class CompletedLessonsDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: CompletedLessonsViewController
    private let tableView: UITableView
    private let viewModel: CompletedLessonsViewModel
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: CompletedLessonsViewModel, viewController: CompletedLessonsViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [CompletedLessonTableViewCell.identifier,
                                         LessonTableViewCell.identifier])
        tableView.reloadData()
    }
   
    //MARK:- Action methods
    @objc func buttonLessonTouchUpInside(_ sender: UIButton) {
        let lessonDescriptionVC = LessonDescriptionViewController.loadFromNib()
        let lesson = viewModel.getLesson(at: sender.tag)
        lessonDescriptionVC.viewModel.setLesson(value: lesson)
        self.viewController.navigationController?.pushViewController(lessonDescriptionVC, animated: true)
    }
  
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getTotalLessonCount()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LessonTableViewCell.identifier) as! LessonTableViewCell
        cell.setDetails(lesson: viewModel.getLesson(at: indexPath.row))
        cell.buttonStartLesson.tag = indexPath.row
        cell.buttonStartLesson.addTarget(self, action: #selector(buttonLessonTouchUpInside(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        let count = viewModel.getTotalLessonCount()
        
        let isLastCell = count - currentRow == 1
        let minY = tableView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInLesson() && isLastCell && minY > 0 {
            viewModel.getCompletedLessons()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
