//
//  CompletedLessonsViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import Foundation
import NISLRequestPackages
class CompletedLessonsViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isLessonsFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var lessons: [Lessons] = []
    private var isMoreDataAvailable = true
    private var offset = 0
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- API Methods
    func getCompletedLessons() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetCompletedLessons, queryParameter: param) { (res: Swift.Result<BaseClass<LessonsBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                self.lessons.removeAll()
                if data.status == Constants.api_success{
                    let list = data.data?.lessons ?? []
                    self.offset += 1
                    self.lessons.append(contentsOf: list)
                    if list.count == 0{
                        self.isMoreDataAvailable = false
                    }
                } else {
                   // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isLessonsFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
}
extension CompletedLessonsViewModel {
    func getTotalLessonCount() -> Int {
        lessons.count
    }
    func getLesson(at index: Int) -> Lessons {
        lessons[index]
    }
    func isMoreDataAvailableInLesson() -> Bool {
        isMoreDataAvailable
    }
}
