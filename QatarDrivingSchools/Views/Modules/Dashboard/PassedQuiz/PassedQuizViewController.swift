//
//  PassedQuizViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import UIKit

class PassedQuizViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataSource: PassedQuizDataSource?
    lazy var viewModel = PassedQuizViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.text = Constants.description_passedQuiz
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        dataSource = PassedQuizDataSource(collectionView: collectionView, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
