//
//  PassedQuizDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/07/21.
//

import Foundation
import UIKit

class PassedQuizDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let viewController: PassedQuizViewController
    private let collectionView: UICollectionView
    private let viewModel: PassedQuizViewModel
    
    enum enumCollectionType: Int {
        case header
        case data
    }
    
    enum enumPassedQuizType: Int {
        case all
        case inProgress
        case saved
        case skills
        static let count: Int = {
            var max: Int = 0
            while let _ = enumPassedQuizType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(collectionView: UICollectionView, viewModel: PassedQuizViewModel, viewController: PassedQuizViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView(){
        registerCollectionCell()
    }
    func registerCollectionCell(){
        collectionView.registerNib(nibNames: [PassedQuizCollectionViewCell.identifier])
        collectionView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: collectionView.frame.width / 4 - 10, height: collectionView.frame.width / 4 - 10)
        }
        return CGSize(width: collectionView.frame.width / 2 - 5, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PassedQuizCollectionViewCell.identifier, for: indexPath) as! PassedQuizCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
}
