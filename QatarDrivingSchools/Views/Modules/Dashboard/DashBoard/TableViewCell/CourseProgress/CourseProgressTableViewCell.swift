//
//  CourseProgressTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/09/21.
//

import UIKit

class CourseProgressTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var progressView: CircularProgressView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelProgress: UILabel!
    
    //MARK:- Activity life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 22)
        labelTitle.textColor = .black
        labelProgress.font = CustomFont.semiBold(ofSize: 20)
        labelProgress.textColor = Constants.color_darkApplicationThemeColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setProgressView(value: Float = 0.0){
        progressView.trackWidth = 10.0
        progressView.trackClr = .white//UIColor(red: 198/255, green: 199/255, blue: 203/255, alpha:0.6)
        progressView.progressClr = Constants.color_applicationThemeColor //UIColor(red: 233/255, green: 172/255, blue: 0/255, alpha: 1.0)
        progressView.setProgressWithAnimation(duration: 1.0, value: value)
    }
    func setupDetails(title: String, progress: Float){
        setProgressView(value: (progress)/100.0)
        labelTitle.text = title
        labelProgress.text = "\(progress)%"
    }
}
