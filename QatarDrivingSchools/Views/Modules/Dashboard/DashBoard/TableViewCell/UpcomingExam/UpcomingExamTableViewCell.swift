//
//  UpcomingExamTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/09/21.
//

import UIKit

class UpcomingExamTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelExamName: UILabel!
    @IBOutlet weak var labelExamDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.medium(ofSize: 20)
        labelExamName.font = CustomFont.regular()
        labelExamDate.font = CustomFont.regular()
        
        labelTitle.textColor = .white
        labelExamName.textColor = .white
        labelExamDate.textColor = .white
        
        labelTitle.text = Constants.description_upcomingExample
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
