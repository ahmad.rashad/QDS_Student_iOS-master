//
//  ResultCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/09/21.
//

import UIKit

class ResultCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    //MARK:- Activity life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.medium()
        labelScore.font = CustomFont.medium()
        labelDate.font = CustomFont.medium()
        //labelDate.textColor = .darkGray
    }
    
    //MARK:- Class methods
    func setupDetails(result: QuizResult) {
        labelTitle.attributedText = getAttributedText(title: "\(Constants.description_exam.localized):", text: result.examName ?? "")
        labelScore.attributedText = getAttributedText(title: "\(Constants.description_score.localized):", text: "\(result.score ?? 0)")
        labelDate.attributedText = getAttributedText(title: "\(Constants.description_date.localized):", text: (result.examDate ?? "").toDate()?.toString(format: "dd-MM-yyyy HH:mm:ss") ?? "")
    }
    
    func getAttributedText(title: String, text: String) -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString(string: title + "", attributes: [NSAttributedString.Key.font: CustomFont.semiBold(ofSize: 14)])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.regular(ofSize: 14),
                                                                                                  NSAttributedString.Key.foregroundColor: Constants.color_darkApplicationThemeColor]))
    
        return mutableAttributedString
    }
}
