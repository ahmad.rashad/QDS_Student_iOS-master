//
//  ComplaintOverViewCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-07 on 29/12/21.
//

import UIKit

class ComplaintOverViewCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    //MARK:- Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.regular(ofSize: 16)
        labelDescription.font = CustomFont.bold(ofSize: 25)
        labelDescription.textColor = Constants.color_darkApplicationThemeColor
    }
    //MARK:- Class methods
    func setupDetails(title: String, description: String, image: UIImage) {
        labelTitle.text = title
        labelDescription.text = description
        imageViewIcon.image = image
    }
}
