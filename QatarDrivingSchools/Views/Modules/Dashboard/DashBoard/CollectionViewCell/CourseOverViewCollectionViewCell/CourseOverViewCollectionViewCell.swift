//
//  CourseOverViewCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-07 on 29/12/21.
//

import UIKit

class CourseOverViewCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    //MARK:- Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.semiBold()
        labelDescription.font = CustomFont.regular()
        
        labelTitle.textColor = .gray
        labelDescription.textColor = .black
    }
    
    //MARK:- Class methods
    func setupDetails(title: String, description: String, image: UIImage) {
        labelTitle.text = title
        labelDescription.text = description
        imageViewIcon.image = image
    }

}
