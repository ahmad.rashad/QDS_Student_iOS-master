//
//  TestOverViewCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 29/09/21.
//

import UIKit

class TestOverViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonDuration: UIButton!
    @IBOutlet weak var buttonNumberOfQuestions: UIButton!
    @IBOutlet weak var imageViewQuiz: UIImageView!
    @IBOutlet weak var topImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sideImageWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonArrow: UIButton!
    
    //MARK:- Activity life cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .white
        labelTitle.font = CustomFont.medium()
        buttonDuration.titleLabel?.font = CustomFont.regular(ofSize: 14)
        buttonNumberOfQuestions.titleLabel?.font = CustomFont.regular(ofSize: 14)
    }

    //MARK:- Class methods
    func setupDetails(test: Tests) {
        labelTitle.text = test.examName ?? ""
        buttonDuration.setTitle(" \(test.timeDuration ?? "")", for: .normal)
        let questionText = (test.totalQuestion ?? 0) < 2 ? Constants.description_question.localized : Constants.description_questions.localized
        buttonNumberOfQuestions.setTitle(" \(test.totalQuestion ?? 0) \(questionText)", for: .normal)
    }
}
