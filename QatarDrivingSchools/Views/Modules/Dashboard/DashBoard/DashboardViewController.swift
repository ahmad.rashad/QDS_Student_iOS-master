//
//  DashboardViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import UIKit

class DashboardViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var buttonViewAll: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    
    var dataSource: DashboardDataSource?
    lazy var viewModel = DashboardViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = DashboardDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    @IBAction func buttonNotificationTouchUpInside(_ sender: UIButton) {
        /*let alertVC = AlertViewController.loadFromNib()
        alertVC.viewModel.setTitle(value: Constants.description_mailSent)
        alertVC.viewModel.setSubTitle(value: Constants.description_mailSentDesc)
        alertVC.modalPresentationStyle = .overFullScreen
        self.present(alertVC, animated: true, completion: nil)*/
        //self.navigationController?.pushViewController(LessonDescriptionViewController.loadFromNib(), animated: true)
        //self.navigationController?.pushViewController(ExamResultViewController.loadFromNib(), animated: true)
        self.navigationController?.pushViewController(NotificationViewController.loadFromNib(), animated: true)
    }
    
    //MARK:- Class methods
    func setupLayout() {
        
        labelTitle.text = Constants.description_dashboard
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        labelSubtitle.text = ""//Constants.description_completedLessons
        labelSubtitle.font = CustomFont.semiBold()
        
        buttonViewAll.titleLabel?.font = CustomFont.semiBold(ofSize: 14)
        buttonViewAll.titleLabel?.textColor = Constants.color_applicationThemeColor
        buttonViewAll.setTitle(Constants.title_viewAll, for: .normal)
        buttonViewAll.isHidden = true
        
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isDashboardDataFetched.bind { _ in
            self.tableView.reloadData()
        }
        viewModel.isQuizDataFetched.bind { _ in
            self.tableView.reloadData()
        }
        viewModel.getDashBoardData()
        //viewModel.getQuiz(isNeedToRefresh: true)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
