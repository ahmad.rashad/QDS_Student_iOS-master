//
//  DashboardViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit
import NISLRequestPackages

class DashboardViewModel: UserViewModel {
    
    //MARK:- Variable
    var isDashboardDataFetched: Dynamic<Bool> = Dynamic(false)
    var isQuizDataFetched: Dynamic<Bool> = Dynamic(false)
    
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var isNoData: Bool = false
    private var dashboardData: DashboardData?
    private var quizData: QuizData?
    
    private var isMoreLessonDataAvailable = true
    private var offset = 0
    
    //MARK:- Dependancy Injection
    override init() {
        super.init()
        getUserDetails()
    }

    //MARK:- API
    func getDashBoardData() {
        let param: Parameters = [
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "S_Code":Constants.loggedInUser?.scode ?? ""
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetDashboard, queryParameter: param) { (res: Swift.Result<BaseClass<DashboardBaseData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            self.isNoData = false
            switch res {
            case .success(let data):
                print(data);
                if data.status == Constants.api_success{
                     self.dashboardData = data.data?.dashboardData?.first
                } else {
                    self.isNoData = true
                   // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isDashboardDataFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isNoData = true
                self.isDashboardDataFetched.value = false
            }
            self.getQuiz(isNeedToRefresh: true)
        }
    }
    
    func getQuiz(isNeedToRefresh: Bool = false) {
        if isNeedToRefresh {
            offset = 0
        }
        let param: Parameters = [
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetQuiz, queryParameter: param) { (res: Swift.Result<BaseClass<QuizData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if self.offset == 0 {
                        self.quizData = data.data
                    }else{
                        if (self.quizData?.quiz?.count ?? 0) > 0 {
                            self.quizData?.quiz?[0].lessonQuiz?.append(contentsOf: data.data?.quiz?.first?.lessonQuiz ?? [])
                            self.quizData?.quiz?[0].courseQuiz?.append(contentsOf: data.data?.quiz?.first?.courseQuiz ?? [])
                        }
                    }
                    if data.data?.quiz?.first?.lessonQuiz?.count == 0 {
                        self.isMoreLessonDataAvailable = false
                    }
                    self.offset += 1
                } else {
                    self.isMoreLessonDataAvailable = false
                    //self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
               // self.isQuizDataFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
               // self.isQuizDataFetched.value = false
            }
            self.isDashboardDataFetched.value = true
        }
    }
}

//MARK:- Getters and Setters
extension DashboardViewModel {
    func getCourseTitle() -> String {
        dashboardData?.courseProgress?.first?.courseTitle ?? ""
    }
    func getCourseProgress() -> Float {
        (dashboardData?.courseProgress?.first?.courseProgress ?? 0.0) 
    }
    func getResultCount() -> Int {
        dashboardData?.quizResult?.count ?? 0
    }
    func getResult(at index: Int) -> QuizResult {
        dashboardData?.quizResult?[index] ?? QuizResult()
    }
    func getInProgressLessonCount() -> Int {
        dashboardData?.inprogressLesson?.count ?? 0
    }
    func getInProgressLesson(at index: Int) -> Lessons {
        dashboardData?.inprogressLesson?[index] ?? Lessons()
    }
    func isDataMissing() -> Bool {
        isNoData
    }
    func getLessonQuizCount() -> Int {
        quizData?.quiz?.first?.lessonQuiz?.count ?? 0
    }
    func getCourseQuizCount() -> Int {
        quizData?.quiz?.first?.courseQuiz?.count ?? 0
    }
    func getLessonQuiz(at index: Int) -> Tests? {
        quizData?.quiz?.first?.lessonQuiz?[index]
    }
    
    func getCourseQuiz() -> Tests? {
        quizData?.quiz?.first?.courseQuiz?.first
    }
    func getTotalComplaint() -> String {
        "\(dashboardData?.studentAdditionalDetail?.first?.totalComplaints ?? 0)"
    }
    func getPendingComplaint() -> String {
        "\(dashboardData?.studentAdditionalDetail?.first?.pendingComplaints ?? 0)"
    }
    func getCompletedComplaint() -> String {
        "\(dashboardData?.studentAdditionalDetail?.first?.completedComplaints ?? 0)"
    }
    func getLessonProgress() -> String {
        dashboardData?.studentAdditionalDetail?.first?.lessonProgress ?? ""
    }
    func getQuizReview() -> String {
        dashboardData?.studentAdditionalDetail?.first?.quizReview ?? ""
    }
    func getDueCourseFee() -> String {
        "\(dashboardData?.studentAdditionalDetail?.first?.dueCourseFees ?? 0)"
    }
    func isDashBoardDataMissing() -> Bool {
        dashboardData == nil
    }
    func isMoreDataAvailableInLesson() -> Bool {
        isMoreLessonDataAvailable
    }
}
