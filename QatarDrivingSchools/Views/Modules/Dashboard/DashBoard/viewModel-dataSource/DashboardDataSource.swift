//
//  DashboardDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import Foundation
import UIKit
import NISLRequestPackages
class DashboardDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: DashboardViewController
    private let tableView: UITableView
    private let viewModel: DashboardViewModel
    
    enum enumCollectionViewType: Int {
        case passedExams
        case lessonQuiz
        case courseOverView
        case complaintsOverView
    }
    enum enumDashboardTableSection: Int{
        case noData
        case courseProgress
        case upcomingExam
        case passedExams
        case inProgressLessonTitle
        case inProgressLesson
        case lessonQuiz
        case courseQuizTitle
        case courseQuiz
        case courseOverView
        case complaintsOverView
        static let count: Int = {
            var max: Int = 0
            while let _ = enumDashboardTableSection(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    enum enumComplainCellType: Int, CaseIterable {
        case totalComplain
        case pendingComplain
        case completedComplain
    }
    
    enum enumCourseCellType: Int, CaseIterable {
        case lessonProgress
        case quizReview
        case dueCourseFee
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: DashboardViewModel, viewController: DashboardViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        self.viewController.buttonViewAll.addTarget(self, action: #selector(buttonViewAllCompletedLessonTouchUpInside(_:)), for: .touchUpInside)
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LessonTableViewCell.identifier,
                                         CollectionWithTitleTableViewCell.identifier,
                                         CourseProgressTableViewCell.identifier,
                                         UpcomingExamTableViewCell.identifier,
                                         ImageTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TestOverviewTableViewCell.identifier])
        tableView.reloadData()
    }
    
    func startQuiz(test: Tests){
        if test.status == enumTestStatus.deactivate.rawValue {
            let alertVC = AlertViewController.loadFromNib()
            alertVC.viewModel.setImage(value: #imageLiteral(resourceName: "lock_ic"))
            alertVC.viewModel.setTitle(value: "")
            alertVC.viewModel.setSubTitle(value: Constants.alert_completeLessonFirstForQuiz.localized)
            alertVC.modalPresentationStyle = .overFullScreen
            self.viewController.present(alertVC, animated: true, completion: nil)
        }else if test.totalQuestion == 0 {
            viewModel.alertMessage.value = AlertMessage(title: "", body: Constants.alert_noQuestions.localized)
        }else{
            if let messageStr = test.errorMesg {
                if !messageStr.isEmpty {
                    let alertVC = AlertViewController.loadFromNib()
                    alertVC.viewModel.setImage(value: #imageLiteral(resourceName: "lock_ic"))
                    alertVC.viewModel.setTitle(value: "")
                    alertVC.viewModel.setSubTitle(value: messageStr)
                    alertVC.modalPresentationStyle = .overFullScreen
                    self.viewController.present(alertVC, animated: true, completion: nil)
                    return
                }
            }
            let alertController = UIAlertController(title: "", message: Constants.alert_startQuiz.localized, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Constants.title_yes.localized, style: .default, handler: { _ in
                let questionVC = QuestionsViewController.loadFromNib()
                questionVC.viewModel.setTestDetails(value: test)
                questionVC.viewModel.setPreviousResultStatus(value: enumResultType(rawValue: test.resultStatus ?? 0) ?? .failed)
                self.viewController.navigationController?.pushViewController(questionVC, animated: true)
            }))
            alertController.addAction(UIAlertAction(title: Constants.title_no.localized, style: .destructive, handler: nil))
            viewController.present(alertController, animated: true, completion: nil)
           
        }
    }
    //MARK:- Action methods
    @objc func buttonViewAllQuizTouchUpInside(_ sender: UIButton) {
        let passedQuizVC = PassedQuizViewController.loadFromNib()
        self.viewController.navigationController?.pushViewController(passedQuizVC, animated: true)
    }
    @objc func buttonViewAllCompletedLessonTouchUpInside(_ sender: UIButton) {
        let completedLessonVC = CompletedLessonsViewController.loadFromNib()
        self.viewController.navigationController?.pushViewController(completedLessonVC, animated: true)
    }
    @objc func buttonLessonTouchUpInside(_ sender: UIButton) {
        let lessonDescriptionVC = LessonDescriptionViewController.loadFromNib()
        let lesson = viewModel.getInProgressLesson(at: sender.tag)
        lessonDescriptionVC.viewModel.setLesson(value: lesson)
        self.viewController.navigationController?.pushViewController(lessonDescriptionVC, animated: true)
    }
    @objc func buttonCertificateTouchUpInside(_ sender: UIButton) {
        if let test = viewModel.getCourseQuiz() {
            let certificateVC =
                CertificatesViewController.loadFromNib()
            certificateVC.viewModel.setCertificates(value: test.certificate ?? [])
            self.viewController.navigationController?.pushViewController(certificateVC, animated: true)
        }
    }
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumDashboardTableSection.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumDashboardTableSection(rawValue: section)! {
        case .noData:
            return viewModel.isDataMissing() ? 1 : 0
        case .courseProgress:
            if !viewModel.isDashboardDataFetched.value {
                return 0
            }
            return viewModel.isDataMissing() ? 0 : 1
        case .upcomingExam:
            return 0
        case .passedExams:
            if viewModel.isDataMissing() {
                return 0
            }
            return viewModel.getResultCount() == 0 ? 0 : 1
        case .inProgressLessonTitle:
            if viewModel.isDataMissing() {
                return 0
            }
            return viewModel.getInProgressLessonCount() == 0 ? 0 : 1
        case .inProgressLesson:
            if viewModel.isDataMissing() {
                return 0
            }
            return viewModel.getInProgressLessonCount()
        case .lessonQuiz:
            if viewModel.isDataMissing() {
                return 0
            }
            return viewModel.getLessonQuizCount() == 0 ? 0 : 1
        case .courseQuiz, .courseQuizTitle:
            if viewModel.isDataMissing() {
                return 0
            }
            return viewModel.getCourseQuizCount() == 0 ? 0 : 1
        case .courseOverView:
            return viewModel.isDashBoardDataMissing() ? 0 : 1
        case .complaintsOverView:
            return viewModel.isDashBoardDataMissing() ? 0 : 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumDashboardTableSection(rawValue: indexPath.section)! {
        case .noData:
            return tableView.frame.height
        case .courseProgress:
            return viewModel.isDataMissing() ? 0 : 200
        case .upcomingExam,.inProgressLesson:
            return UITableView.automaticDimension
        case .inProgressLessonTitle,.courseQuizTitle:
            return 30
        case .passedExams:
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 150//UIScreenmain.bounds.width / 4 + 50
            }
            return 150
        case .lessonQuiz:
            return 140
        case .courseOverView, .complaintsOverView:
            return viewModel.isDataMissing() ? 0 : 140
        case .courseQuiz:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumDashboardTableSection(rawValue: indexPath.section)! {
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        case .courseProgress:
            let cell = tableView.dequeueReusableCell(withIdentifier: CourseProgressTableViewCell.identifier) as! CourseProgressTableViewCell
            cell.setupDetails(title: viewModel.getCourseTitle(), progress: viewModel.getCourseProgress())
            return cell
        case .upcomingExam:
            let cell = tableView.dequeueReusableCell(withIdentifier: UpcomingExamTableViewCell.identifier) as! UpcomingExamTableViewCell
            return cell
        case .inProgressLessonTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.font = CustomFont.semiBold()
            cell.labelTitle.textColor = .black
            cell.imageViewUnderline.isHidden = true
            cell.leading.constant = 10.0
            cell.labelTitle.text = Constants.description_continueWatchingLesson.localized
            cell.leading.priority = UILayoutPriority(1000)
            return cell
        case .inProgressLesson:
            let cell = tableView.dequeueReusableCell(withIdentifier: LessonTableViewCell.identifier) as! LessonTableViewCell
            cell.setDetails(lesson: viewModel.getInProgressLesson(at: indexPath.row))
            cell.buttonStartLesson.tag = indexPath.row
            cell.buttonStartLesson.addTarget(self, action: #selector(buttonLessonTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .passedExams:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.imageViewSeparator.backgroundColor = .lightGray
            cell.labelTitle.text = Constants.description_passedQuiz.localized
            cell.collectionView.tag = enumCollectionViewType.passedExams.rawValue
            cell.collectionView.registerNib(nibNames: [ResultCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.buttonViewAll.isHidden = true
            cell.buttonViewAll.setTitle(Constants.title_viewAll.localized, for: .normal)
            cell.buttonViewAll.addTarget(self, action: #selector(buttonViewAllQuizTouchUpInside(_:)), for: .touchUpInside)
            cell.imageViewBottomSeparator.backgroundColor = .lightGray
            cell.collectionView.reloadData()
            return cell
        case .lessonQuiz:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.imageViewSeparator.backgroundColor = .lightGray
            cell.labelTitle.text = Constants.description_lessonQuiz.localized
            cell.collectionView.tag = enumCollectionViewType.lessonQuiz.rawValue
            cell.collectionView.registerNib(nibNames: [TestOverViewCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.buttonViewAll.isHidden = true
            cell.buttonViewAll.setTitle(Constants.title_viewAll, for: .normal)
            cell.buttonViewAll.addTarget(self, action: #selector(buttonViewAllQuizTouchUpInside(_:)), for: .touchUpInside)
            cell.imageViewBottomSeparator.backgroundColor = .lightGray
            cell.collectionView.reloadData()
            return cell
        case .courseOverView:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.imageViewSeparator.backgroundColor = .lightGray
            cell.labelTitle.text = Constants.description_courseOverView.localized
            cell.collectionView.tag = enumCollectionViewType.courseOverView.rawValue
            cell.collectionView.registerNib(nibNames: [CourseOverViewCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.buttonViewAll.isHidden = true
            cell.imageViewBottomSeparator.backgroundColor = .lightGray
            cell.collectionView.reloadData()
            return cell
        case .complaintsOverView:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.imageViewSeparator.backgroundColor = .lightGray
            cell.labelTitle.text = Constants.description_complaintOverView.localized
            cell.collectionView.tag = enumCollectionViewType.complaintsOverView.rawValue
            cell.collectionView.registerNib(nibNames: [ComplaintOverViewCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.buttonViewAll.isHidden = true
            cell.imageViewBottomSeparator.backgroundColor = .lightGray
            cell.collectionView.reloadData()
            return cell
        case .courseQuizTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.font = CustomFont.semiBold()
            cell.labelTitle.textColor = .black
            cell.imageViewUnderline.isHidden = true
            cell.leading.constant = 10.0
            cell.labelTitle.text = Constants.description_courseQuiz.localized
            cell.leading.priority = UILayoutPriority(1000)
            return cell
        case .courseQuiz:
            let cell = tableView.dequeueReusableCell(withIdentifier: TestOverviewTableViewCell.identifier) as! TestOverviewTableViewCell
            if let test = viewModel.getCourseQuiz() {
                cell.setupDetails(test: test)
                cell.setStatus(test: test)
                if let certificate = test.certificate?.first {
                    cell.buttonCertificate.isHidden = (certificate.certificateLink ?? "").isEmpty
                    cell.buttonCertificate.addTarget(self, action: #selector(buttonCertificateTouchUpInside(_:)), for: .touchUpInside)
                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumDashboardTableSection(rawValue: indexPath.section)! {
        case .courseProgress:
            SideMenu.shared.sideMenuController.rootViewController = UINavigationController(rootViewController: CoursesViewController.loadFromNib())
        case .inProgressLesson:
            let lessonDescriptionVC = LessonDescriptionViewController.loadFromNib()
            let lesson = viewModel.getInProgressLesson(at: indexPath.row)
            lessonDescriptionVC.viewModel.setLesson(value: lesson)
            self.viewController.navigationController?.pushViewController(lessonDescriptionVC, animated: true)
        case .courseQuiz:
            if let test = viewModel.getCourseQuiz() {
                startQuiz(test: test)
            }
        default:
            break
        }
        /*let completedLessonsVC = PassedQuizViewController.loadFromNib()
        self.viewController.navigationController?.pushViewController(completedLessonsVC, animated: true)*/
    }
}

//MARK:- CollectionView Delegate & DataSource
extension DashboardDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch enumCollectionViewType(rawValue: collectionView.tag)! {
        case .passedExams:
            return viewModel.getResultCount()
        case .lessonQuiz:
            return viewModel.getLessonQuizCount()
        case .courseOverView:
            return enumCourseCellType.allCases.count
        case .complaintsOverView:
            return enumComplainCellType.allCases.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch enumCollectionViewType(rawValue: collectionView.tag)! {
        case .passedExams:
            if UIDevice.current.userInterfaceIdiom == .pad{
                return CGSize(width: 300, height: 110) //return CGSize(width: collectionView.frame.width / 4 - 10, height: collectionView.frame.width / 4 - 10)
            }
            return CGSize(width: 300, height: 110)
        case .lessonQuiz:
            return CGSize(width: 310, height: 90)
        case .courseOverView, .complaintsOverView:
            return CGSize(width: 310, height: 100)
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch enumCollectionViewType(rawValue: collectionView.tag)! {
        case .passedExams:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ResultCollectionViewCell.identifier, for: indexPath) as! ResultCollectionViewCell
            cell.setupDetails(result: viewModel.getResult(at: indexPath.row))
            return cell
        case .lessonQuiz:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TestOverViewCollectionViewCell.identifier, for: indexPath) as! TestOverViewCollectionViewCell
            cell.topImageViewHeight.constant = 0
            cell.sideImageWidth.constant = 50
            if let test = viewModel.getLessonQuiz(at: indexPath.row) {
                cell.setupDetails(test: test)
                switch enumTestStatus(rawValue: test.status ?? 0)! {
                case .deactivate:
                    cell.buttonArrow.isEnabled = false
                case .activate:
                    cell.buttonArrow.isEnabled = true
                }
            }
            return cell
        case .courseOverView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CourseOverViewCollectionViewCell.identifier, for: indexPath) as! CourseOverViewCollectionViewCell
            switch enumCourseCellType(rawValue: indexPath.row)! {
            case .lessonProgress:
                cell.setupDetails(title: Constants.description_courseLessonProgress, description: viewModel.getLessonProgress(), image: #imageLiteral(resourceName: "lesson_progress_ic"))
            case .quizReview:
                cell.setupDetails(title: Constants.description_courseQuizReview, description: viewModel.getQuizReview(), image: #imageLiteral(resourceName: "course_placeholder"))
            case .dueCourseFee:
                cell.setupDetails(title: Constants.description_courseDueFee, description: viewModel.getDueCourseFee(), image: #imageLiteral(resourceName: "quiz_type_ic"))
            }
            return cell
        case .complaintsOverView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ComplaintOverViewCollectionViewCell.identifier, for: indexPath) as! ComplaintOverViewCollectionViewCell
            switch enumComplainCellType(rawValue: indexPath.row)! {
            case .totalComplain:
                cell.setupDetails(title: Constants.description_totalComplain, description: viewModel.getTotalComplaint(), image: #imageLiteral(resourceName: "total_complain_ic"))
            case .pendingComplain:
                cell.setupDetails(title: Constants.description_pendingComplain, description: viewModel.getPendingComplaint(), image: #imageLiteral(resourceName: "pending_complain_ic"))
            case .completedComplain:
                cell.setupDetails(title: Constants.description_completedComplain, description: viewModel.getCompletedComplaint(), image: #imageLiteral(resourceName: "completed_complain_ic"))
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        switch enumCollectionViewType(rawValue: collectionView.tag)! {
        case .passedExams, .courseOverView, .complaintsOverView:
            break
        case .lessonQuiz:
            let currentRow = indexPath.row
            let count = viewModel.getLessonQuizCount()
            
            let isLastCell = count - currentRow == 1
            let minY = collectionView.contentOffset.y
            
            if viewModel.isMoreDataAvailableInLesson() && isLastCell && minY >= 0 {
                viewModel.getQuiz()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch enumCollectionViewType(rawValue: collectionView.tag)! {
        case .passedExams, .courseOverView, .complaintsOverView:
            break
        case .lessonQuiz:
            if let test = viewModel.getLessonQuiz(at: indexPath.row) {
                startQuiz(test: test)
            }
        }
    }
    
}
