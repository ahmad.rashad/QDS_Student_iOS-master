//
//  SearchListDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import Foundation
import UIKit

class SearchListDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: SearchListViewController
    private let tableView: UITableView
    private let viewModel: SearchListViewModel
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: SearchListViewModel, viewController: SearchListViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [RadioLabelTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RadioLabelTableViewCell.identifier) as! RadioLabelTableViewCell
        return cell
    }
}
