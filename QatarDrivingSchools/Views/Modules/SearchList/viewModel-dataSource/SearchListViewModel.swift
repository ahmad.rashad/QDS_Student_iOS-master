//
//  SearchListViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import Foundation
import UIKit
class SearchListViewModel {
    
    //MARK:- Variables
    private var screenTitle = ""
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - Vars & Lets
 
}

//MARK:- Getters and Setters
extension SearchListViewModel {
    func getTitle() -> String {
        screenTitle
    }
    func setTitle(value: String) {
        screenTitle = value
    }
}
