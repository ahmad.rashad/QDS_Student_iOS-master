//
//  RadioLabelTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import UIKit

class RadioLabelTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var buttonCheckBox: UIButton!
    @IBOutlet weak var buttonRightCheckBox: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewColor: DesignableView!
    
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.regular()
        buttonRightCheckBox.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setRightCheckBox(){
        buttonCheckBox.isHidden = true
        buttonRightCheckBox.isHidden = false
    }
    func setLeftCheckBox() {
        buttonCheckBox.isHidden = false
        buttonRightCheckBox.isHidden = true
        viewColor.backgroundColor = .clear
    }
    func removeTopBottomSpace() {
        topHeight.constant = 0
        bottomHeight.constant = 0
    }
}
