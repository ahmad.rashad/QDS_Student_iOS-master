//
//  SearchListViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import UIKit

class SearchListViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var buttonSelect: UIButton!
    
    var dataSource: SearchListDataSource?
    lazy var viewModel = SearchListViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
           
        dataSource = SearchListDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        
        labelTitle.text = viewModel.getTitle()
        labelTitle.font = CustomFont.medium(ofSize: 18)
        buttonSelect.setTitle(Constants.title_select, for: .normal)
        buttonSelect.titleLabel?.font = CustomFont.medium()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
        //MARK:- Action Methods
    @IBAction func buttonCloseTouchUpInside(_ sender: UIButton) {
        self.view.backgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
    }

}
