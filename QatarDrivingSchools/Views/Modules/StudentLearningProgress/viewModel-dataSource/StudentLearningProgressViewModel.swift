//
//  StudentLearningProgressViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit
import NISLRequestPackages
class StudentLearningProgressViewModel {
    
    //MARK:- Variables
    var isLearningProgressFetched: Dynamic<Bool> = Dynamic(true)
    var noLearningProgressMessage: Dynamic<String> = Dynamic("")
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    private var isMoreDataAvailable = true
    private var lessons: [Lessons] = []
    private var offset: Int = 0
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - API
    func getLearningProgress() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetLearningProgress, queryParameter: param) { (res: Swift.Result<BaseClass<LessonsBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if self.offset == 0 {
                        self.lessons.removeAll()
                    }
                    let list = data.data?.lessons ?? []
                    self.offset += 1
                    self.lessons.append(contentsOf: list)
                    if list.count == 0{
                        self.isMoreDataAvailable = false
                    }
                } else {
                    self.isMoreDataAvailable = false
                    self.noLearningProgressMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isLearningProgressFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isLearningProgressFetched.value = false
            }
        }
    }
}

//MARK:- Getters and Setters
extension StudentLearningProgressViewModel {
    func getTotalLessonCount() -> Int {
        lessons.count
    }
    func getLesson(at index: Int) -> Lessons {
        lessons[index]
    }
    func isMoreDataAvailableInLesson() -> Bool {
        isMoreDataAvailable
    }
}


