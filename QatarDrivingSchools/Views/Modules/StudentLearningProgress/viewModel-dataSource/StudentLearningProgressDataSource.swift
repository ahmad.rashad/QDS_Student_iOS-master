//
//  StudentLearningProgressDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import Foundation
import UIKit

class StudentLearningProgressDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: StudentLearningProgressViewController
    private let tableView: UITableView
    private let viewModel: StudentLearningProgressViewModel
    
    enum enumStudentLearningSectionType: Int {
        case learningProgress = 0
        case noData
        static let count: Int = {
            var max: Int = 0
            while let _ = enumStudentLearningSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: StudentLearningProgressViewModel, viewController: StudentLearningProgressViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LearningProgressTableViewCell.identifier,
                                         ImageTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumStudentLearningSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumStudentLearningSectionType(rawValue: section)! {
        case .learningProgress:
            return viewModel.getTotalLessonCount()
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalLessonCount() == 0 ? 1 : 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumStudentLearningSectionType(rawValue: indexPath.section)! {
        case .learningProgress:
            let cell = tableView.dequeueReusableCell(withIdentifier: LearningProgressTableViewCell.identifier) as! LearningProgressTableViewCell
            cell.setupDetails(lesson: viewModel.getLesson(at: indexPath.row))
            return cell
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        let count = viewModel.getTotalLessonCount()
        
        let isLastCell = count - currentRow == 1
        let minY = tableView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInLesson() && isLastCell && minY > 0 {
            viewModel.getLearningProgress()
        }
    }
}
