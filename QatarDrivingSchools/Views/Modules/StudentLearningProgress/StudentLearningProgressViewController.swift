//
//  StudentLearningProgressViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit

class StudentLearningProgressViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: StudentLearningProgressDataSource?
    lazy var viewModel = StudentLearningProgressViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = StudentLearningProgressDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    @IBAction func buttonSearchTouchUpInside(_ sender: UIButton) {
        //self.navigationController?.pushViewController(SearchViewController.loadFromNib(), animated: true)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_studentLearningProgress
        labelTitle.font = CustomFont.medium(ofSize: 18)
       
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isLearningProgressFetched.bind { isFetched in
            self.tableView.reloadData()
        }
        viewModel.noLearningProgressMessage.bind { message in
            print(message)
        }
        viewModel.getLearningProgress()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
