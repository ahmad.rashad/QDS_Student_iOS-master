//
//  LearningProgressTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit

class LearningProgressTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCourseTypeTitle: UILabel!
    @IBOutlet weak var labelLessonProgressTitle: UILabel!
    @IBOutlet weak var labelQuizReviewTitle: UILabel!
    @IBOutlet weak var labelQuizEvaluationTitle: UILabel!
    
    @IBOutlet weak var labelCourseType: UILabel!
    @IBOutlet weak var labelLessonProgress: UILabel!
    @IBOutlet weak var labelQuizReview: UILabel!
    @IBOutlet weak var labelQuizEvaluation: UILabel!
    @IBOutlet weak var labelCourseStatus: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelTitle.font = CustomFont.bold(ofSize: 18)
        labelCourseStatus.font = CustomFont.bold(ofSize: 14)
        
        let labelFont = CustomFont.regular(ofSize: 14)
        labelCourseTypeTitle.font = labelFont
        labelLessonProgressTitle.font = labelFont
        labelQuizReviewTitle.font = labelFont
        labelQuizEvaluationTitle.font = labelFont
        
        labelCourseType.font = labelFont
        labelLessonProgress.font = labelFont
        labelQuizReview.font = labelFont
        labelQuizEvaluation.font = labelFont
        
        let labelColor = UIColor.darkGray
        labelCourseTypeTitle.textColor = labelColor
        labelLessonProgressTitle.textColor = labelColor
        labelQuizReviewTitle.textColor = labelColor
        labelQuizEvaluationTitle.textColor = labelColor
        
        labelCourseType.textColor = labelColor
        labelLessonProgress.textColor = labelColor
        labelQuizReview.textColor = labelColor
        labelQuizEvaluation.textColor = labelColor
        
        labelCourseTypeTitle.text = Constants.description_courseType + ":"
        labelLessonProgressTitle.text = Constants.description_lessonProgress + ":"
        labelQuizReviewTitle.text = ""//Constants.description_quizReview + ":"
        labelQuizEvaluationTitle.text = ""//Constants.description_quizTypeWiseEvaluation + ":"
        
        /*labelCourseType.text = "Driving"
        labelLessonProgress.text = "5/10"
        labelQuizReview.text = "5/10"
        labelQuizEvaluation.text = "5/10"*/
        
        progressView.transform = CGAffineTransform(scaleX: 1.0, y: 1.2)
        
        clearCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupDetails(lesson: Lessons){
        labelTitle.text = lesson.lessonTitle ?? ""
        labelCourseType.text = lesson.courseTitle ?? ""
        labelLessonProgress.text = "\(lesson.progress ?? 0.0) %"
        labelQuizReview.text = ""
        labelQuizEvaluation.text = ""
        progressView.progress = (lesson.progress ?? 0.0)/100.0
    
        labelCourseStatus.text = Constants.description_courseInProgress
        let progress = lesson.progress ?? 0
        if progress < 25 {
            progressView.tintColor = Constants.color_red
        }else if progress < 75 {
            progressView.tintColor = Constants.color_yellow
        }else if progress < 99 {
            progressView.tintColor = Constants.color_blue
        }else{
            progressView.tintColor = Constants.color_darkGreen
            labelCourseStatus.text = Constants.description_courseCompleted
        }
        if progress == 0 {
            labelCourseStatus.text = Constants.description_coursePending
        }
    }
  
}
