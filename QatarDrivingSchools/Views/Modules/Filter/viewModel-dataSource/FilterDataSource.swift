//
//  FilterDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import Foundation
import UIKit

class FilterDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: FilterViewController
    private let tableView: UITableView
    private let viewModel: FilterViewModel
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: FilterViewModel, viewController: FilterViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [RadioLabelTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch viewController.viewControllerType {
        case .color:
            return viewModel.getTotalAppointmentsCount()
        case .filter:
            let height = 54 * (viewModel.getFilterCount() + 2) + 10 
            viewController.viewHeight.constant = CGFloat(height)
            viewController.viewHeight.priority = UILayoutPriority(1000)
            viewController.topHeight.priority = UILayoutPriority(1)
            return viewModel.getFilterCount()
        case .government:
            return 4
        case .complainCategory:
            return viewModel.getComplainCategoryCount()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RadioLabelTableViewCell.identifier) as! RadioLabelTableViewCell
        switch viewController.viewControllerType {
        case .color:
            cell.setRightCheckBox()
            let appointmentCategory = viewModel.getAppointmentCategory(at: indexPath.row)
            cell.viewColor.backgroundColor = UIColor(hexStr: "\(appointmentCategory.colorCode ?? "#902C5D")ff")
            if viewModel.isIndexSelected(value: indexPath.row) {
                //cell.buttonRightCheckBox.setImage(<#T##image: UIImage?##UIImage?#>, for: <#T##UIControl.State#>)
            }
            cell.labelTitle.text = appointmentCategory.type
        case .filter:
            cell.labelTitle.text = viewModel.getFilterValue(at: indexPath.row)
            cell.setLeftCheckBox()
        case .government:
            cell.setLeftCheckBox()
        case .complainCategory:
            cell.labelTitle.text = viewModel.getCategoryName(at: indexPath.row)
            cell.setLeftCheckBox()
        }
        cell.buttonCheckBox.setImage(Constants.image_unselectedRadio, for: .normal)
        cell.buttonRightCheckBox.setImage(Constants.image_unselectedRadio, for: .normal)
        cell.buttonCheckBox.isUserInteractionEnabled = false
        cell.buttonRightCheckBox.isUserInteractionEnabled = false
        if viewModel.isIndexSelected(value: indexPath.row) {
            cell.buttonCheckBox.setImage(Constants.image_selectedRadio, for: .normal)
            cell.buttonRightCheckBox.setImage(Constants.image_selectedRadio, for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch viewController.viewControllerType {
        case .complainCategory:
            viewModel.removeSelectedIndex()
        case .color:
            viewModel.removeSelectedIndex()
        default:
            break
        }
        viewModel.changeSelectionState(index: indexPath.row)
        tableView.reloadData()
    }
}

