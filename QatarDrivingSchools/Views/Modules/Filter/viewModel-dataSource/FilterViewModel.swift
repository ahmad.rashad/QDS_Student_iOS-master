//
//  FilterViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import Foundation
import UIKit
class FilterViewModel {
    
    //MARK:- Variables
    private var filterType = [Constants.description_all,
                              Constants.description_course,
                              Constants.description_lessons,
                              Constants.description_quiz]
    private var screenTitle = ""
    private var selectedIndexes: [Int] = []
    private var complainCategoryTypes: [ComplaintsCategory] = []
    private var appointmentCategories: [AppoinmentCategories] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - Vars & Lets
 
}

//MARK:- Getters and Setters
extension FilterViewModel {
    func getTitle() -> String {
        screenTitle
    }
    func getFilterCount() -> Int {
        filterType.count
    }
    func getFilterValue(at index: Int) -> String {
        filterType[index]
    }
    func getComplainCategoryCount() -> Int {
        complainCategoryTypes.count
    }
    func getCategoryName(at index: Int) -> String {
        complainCategoryTypes[index].complainType ?? ""
    }
    func getSelectedCategory() -> ComplaintsCategory? {
        if let selectedIndex = selectedIndexes.first {
            if selectedIndex == -1 {
                return nil
            }
            if complainCategoryTypes.count > selectedIndex {
                return complainCategoryTypes[selectedIndex]
            }
        }
        return nil
    }
    func setTitle(value: String) {
        screenTitle = value
    }
    func setComplainCategory(value: [ComplaintsCategory], index: Int){
        selectedIndexes = [index]
        complainCategoryTypes = value
    }
    func removeSelectedIndex(){
        selectedIndexes.removeAll()
    }
    func changeSelectionState(index: Int) {
        if selectedIndexes.contains(index){
            selectedIndexes.removeAll { value in
                value == index
            }
        }else{
            selectedIndexes.append(index)
        }
    }
    func isIndexSelected(value: Int) -> Bool {
        return selectedIndexes.contains(value)
    }
    func setAppointmentCategories(value: [AppoinmentCategories]) {
        appointmentCategories = value
    }
    func getTotalAppointmentsCount() -> Int {
        appointmentCategories.count
    }
    func getAppointmentCategory(at index: Int) -> AppoinmentCategories {
        appointmentCategories[index]
    }
    func getSelectedAppointmentCategory() -> AppoinmentCategories? {
        if let selectedIndex = selectedIndexes.first {
            if appointmentCategories.count > selectedIndex {
                return appointmentCategories[selectedIndex]
            }
        }
        return nil
    }
}
