//
//  FilterViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class FilterViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSelect: UIButton!
    
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var dataSource: FilterDataSource?
    lazy var viewModel = FilterViewModel()
    
    enum enumViewControllerType {
        case filter
        case government
        case complainCategory
        case color
    }
    var viewControllerType: enumViewControllerType = .filter
    var selectedCategory: ((_ category: ComplaintsCategory?)-> Void)?
    var selectedAppointmentCategory: ((_ category: AppoinmentCategories?)-> Void)?
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = FilterDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = Constants.color_lightTransparent
        }
    }
    //MARK:- Action Methods
    @IBAction func buttonCloseTouchUpInside(_ sender: UIButton) {
        view.backgroundColor = .clear
        dismiss(animated: true, completion: nil)
    }

    @IBAction func buttonSelectTouchUpInside(_ sender: Any) {
        switch viewControllerType {
        case .complainCategory:
            selectedCategory?(viewModel.getSelectedCategory())
        case .color:
            selectedAppointmentCategory?(viewModel.getSelectedAppointmentCategory())
        default:
            break
        }
        view.backgroundColor = .clear
        dismiss(animated: true, completion: nil)
    }
    //MARK:- Class Methods
    func setupLayout() {
        view.backgroundColor = .clear
        labelTitle.text = viewModel.getTitle()
        labelTitle.font = CustomFont.medium(ofSize: 18)
        buttonSelect.setTitle(Constants.title_select, for: .normal)
        buttonSelect.titleLabel?.font = CustomFont.medium()
        viewHeight.priority = UILayoutPriority(1)
    }
}
