//
//  AlertViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import UIKit

class AlertViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var buttonOk: UIButton!
    
    let viewModel = AlertViewModel()
    
    var callBackToCloseParentScreen:(()->Void)?
    
    //MARK:- Activity Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupLayout()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = Constants.color_lightTransparent
        }
    }
    
    //MARK:- Action Methods
    @IBAction func buttonCloseTouchUpInside(_ sender: UIButton) {
        self.view.backgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonOkTouchUpInside(_ sender: Any) {
        self.view.backgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
        callBackToCloseParentScreen?()
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        view.backgroundColor = .clear
        labelTitle.font = CustomFont.medium()
        labelSubtitle.font = CustomFont.regular()
        buttonOk.titleLabel?.font = CustomFont.bold(ofSize: 18)
        buttonOk.setTitle(Constants.title_ok.localized, for: .normal)
        
        labelTitle.text = viewModel.getTitle()
        labelSubtitle.text = viewModel.getSubTitle()
        imgView.image = viewModel.getImage()
    }
    
}
