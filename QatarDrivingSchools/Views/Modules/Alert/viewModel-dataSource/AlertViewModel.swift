//
//  AlertViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 12/07/21.
//

import Foundation
import UIKit
class AlertViewModel {
    private var title = ""
    private var subTitle = ""
    private var image = #imageLiteral(resourceName: "tick_ic")
}

//MARK:- Getters and Setters
extension AlertViewModel {
    func getTitle() -> String {
        title
    }
    func getSubTitle() -> String {
        subTitle
    }
    func getImage() -> UIImage {
        image
    }
    func setTitle(value: String) {
        title = value
    }
    func setSubTitle(value: String) {
        subTitle = value
    }
    func setImage(value: UIImage) {
        image = value
    }
}
