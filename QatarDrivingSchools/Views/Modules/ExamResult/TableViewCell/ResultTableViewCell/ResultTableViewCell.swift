//
//  ResultTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPercentage: UILabel!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold(ofSize: 18)
        labelTitle.text = Constants.title_result + " : "
        labelPercentage.font = CustomFont.semiBold(ofSize: 20)
        labelPercentage.textColor = Constants.color_greenText
     
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
