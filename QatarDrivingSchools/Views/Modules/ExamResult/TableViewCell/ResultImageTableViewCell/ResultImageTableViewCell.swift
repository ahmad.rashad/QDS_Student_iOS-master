//
//  ResultImageTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class ResultImageTableViewCell: UITableViewCell {
   
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.regular()
        labelSubTitle.font = CustomFont.semiBold(ofSize: 18)
        labelSubTitle.textColor = Constants.color_greenText
        labelTitle.text = Constants.description_testTitle
        
        #if DEBUG
        labelSubTitle.text = "Congratulations you have passed the theory exam."
        #endif
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:- Class methods
    func setupDetails(subtitle: String, isPassed: Bool){
        labelSubTitle.text = subtitle
        labelSubTitle.textColor =  isPassed ? Constants.color_greenText : .red
    }
    
}
