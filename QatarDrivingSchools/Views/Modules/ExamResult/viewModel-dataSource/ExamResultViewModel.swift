//
//  ExamResultViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import Foundation
import UIKit
import NISLRequestPackages
class ExamResultViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isResultFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    private var result: ResultData?
    private var questions: [Questions] = []
    private var test = Tests()
    private var lesson = Lessons()
    private var previousResultStatus: enumResultType = .failed
    
    struct StructQuestionData {
        struct Questions {
            var id = 0
            var answer = ""
            var isRightAnswer = false
        }
        var timeDuration = ""
        var questionList:[Questions] = []
    }
    var questionData = StructQuestionData()
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - Class methods
    func calculateResult() {
        var result = TestResultDetail()
        result.submittedDate = Date().toString()
        result.totalQuestion = questionData.questionList.count
        result.timeDuration = questionData.timeDuration
        result.numberOfQuestionsToDisplay = lesson.testResultDetail?.first?.numberOfQuestionsToDisplay
        var score = 0
        for question in questionData.questionList {
            if question.isRightAnswer {
                score += 1
            }
        }
        result.score = score
        let resultPercentage = Float(100 * score) / Float(questionData.questionList.count)
        result.result = resultPercentage
       
        if let testResultDetails = lesson.testResultDetail {
            result.examName = testResultDetails.first?.examName ?? ""
            result.examId = testResultDetails.first?.examId ?? 0
            result.resultStatus = (lesson.tests?.first?.passingLimitAnswer ?? 0) <= score ? enumResultType.pass.rawValue : enumResultType.failed.rawValue
        }else{
            result.examName = test.examName
            result.examId = test.examId
            result.resultStatus = (test.passingLimitAnswer ?? 0) <= score ? enumResultType.pass.rawValue : enumResultType.failed.rawValue
        }
        
        self.result = ResultData(result: [result])
        isLoaderHidden.value = true
        isResultFetched.value = true
    }
    //MARK: - API
    func getResult() {
        
        if previousResultStatus == .pass {
           calculateResult()
           return
        }
        if let certificate = test.certificate?.first {
           if !(certificate.certificateLink?.isEmpty ?? true) {
               calculateResult()
              return
           }
        }
        var strQuestion = "["
        for question in questionData.questionList {
            strQuestion.append("{")
            strQuestion.append("\"id\":\(question.id),\"answer\":\"\(question.answer)\"")
            strQuestion.append("},")
        }
        strQuestion.removeLast()
        strQuestion.append("]")
        let param: Parameters = [
            "s_Code":Constants.loggedInUser?.scode ?? "",
            "examId":test.examId ?? 0,
            "emailId":Constants.getLoggedInUserEmailForAPI(),
            "timeDuration":questionData.timeDuration,
            "questions":strQuestion
        ]
        print(param)
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetResult, params: param) { (res: Swift.Result<BaseClass<ResultData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    print(data)
                    self.result = data.data
                } else {
                   // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isResultFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
}

//MARK:- Getters and Setters
extension ExamResultViewModel {
    func setTimeDuration(value: String){
        questionData.timeDuration = value
    }
    func setQuestionsAnswers(value: [Questions]) {
        questions = value
        for tmpValue in value {
            let selectedAnswer = tmpValue.options?[tmpValue.selectedAnswer ?? 0].text ?? ""
            
            let isRightAnswer = tmpValue.options?.filter({ option in
                option.option == tmpValue.correctAnswer
            }).first?.text == selectedAnswer
            questionData.questionList.append(StructQuestionData.Questions(id: tmpValue.questionId ?? 0, answer: selectedAnswer, isRightAnswer: isRightAnswer))
        }
    }
    func setTestDetails(value: Tests) {
        test = value
    }
    func setLesson(value: Lessons) {
        lesson = value
    }
    func getLesson() -> Lessons {
        lesson
    }
    func getExamCategory() -> String {
        if (result?.result?.count ?? 0) > 0 {
            return result?.result?.first?.examName ?? ""
        }
        return ""
    }
    func getNumberOfQuestions() -> String {
        if (result?.result?.count ?? 0) > 0 {
            return "\(result?.result?.first?.totalQuestion ?? 0)"
        }
        return ""
    }
    
    func getNumberOfQuestionsToDisplay() -> String {
        if (result?.result?.count ?? 0) > 0 {
            return "\(result?.result?.first?.numberOfQuestionsToDisplay ?? 0)"
        }
        return ""
    }
    
    func getScore() -> String {
        if (result?.result?.count ?? 0) > 0 {
            return "\(result?.result?.first?.score ?? 0)/\(getNumberOfQuestionsToDisplay())"
        }
        return ""
    }
    func getResultText() -> String {
        if (result?.result?.count ?? 0) > 0 {
            return "\(String(format: "%.2f", result?.result?.first?.result ?? 0)) % \(Constants.description_marks)"
        }
        return ""
    }
    func getTimeDuration() -> String {
        questionData.timeDuration
    }
    func getQuestions() -> [Questions] {
        questions
    }
    func isUserPassed() -> Bool {
        result?.result?.first?.resultStatus == enumResultType.pass.rawValue
    }
    func getSubmittedDate() -> String {
        let date = result?.result?.first?.submittedDate ?? ""
        if !date.isEmpty {
            return date.toDate()?.toString(format: "dd-MM-yyyy HH:mm:ss") ?? ""
        }
        return date
    }
    func setResult(value: [TestResultDetail]) {
        result = ResultData(result: value)
    }
    func setPreviousResultStatus(value: enumResultType) {
        previousResultStatus = value
    }
}

