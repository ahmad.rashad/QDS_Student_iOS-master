//
//  ExamResultDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import Foundation
import UIKit

class ExamResultDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ExamResultViewController
    private let tableView: UITableView
    private let viewModel: ExamResultViewModel
    
    enum enumExamResultTableRow: Int {
        case image = 0
        case category
        case date
        case numberOfQuestions
        case score
        case result
        case finish
        /*case getCertificate
        case showResult
        case theoreticalTest
        case practicalExam*/
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumExamResultTableRow(rawValue: max) { max += 1}
            return max
        }()
    }
    //MARK:- Init
    init(tableView: UITableView, viewModel: ExamResultViewModel, viewController: ExamResultViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [ButtonTableViewCell.identifier,
                                         ResultImageTableViewCell.identifier,
                                         ResultTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         ButtonTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonShowResultTouchUpInside(_ sender: UIButton) {
        let questionVC = QuestionsViewController.loadFromNib()
        questionVC.viewControllerType = .reviewAnswer
        questionVC.viewModel.setTotalTimeDuration(value: viewModel.getTimeDuration())
        questionVC.viewModel.setQuestions(value: viewModel.getQuestions())
        viewController.navigationController?.pushViewController(questionVC, animated: true)
    }
    
    @objc func buttonFinishTouchUpInside(_ sender: UIButton) {
        if viewController.isFromLesson {
            //viewController.navigationController?.popViewController(animated: true)
            if (viewController.navigationController != nil) {
                        for vc in  viewController.navigationController!.viewControllers {
                            if vc is LessonsListViewController {
                                viewController.navigationController?.popToViewController(vc, animated: false)
                            }
                        }
              }
        }else{
            let count = (viewController.navigationController?.viewControllers.count ?? 0) - 3
            if count < viewController.navigationController?.viewControllers.count ?? 0 {
                if let vc = viewController.navigationController?.viewControllers[count] {
                    if viewController.navigationController?.viewControllers.count == 5 {
                        if (viewController.navigationController != nil) {
                                    for vc in  viewController.navigationController!.viewControllers {
                                        if vc is LessonsListViewController {
                                            viewController.navigationController?.popToViewController(vc, animated: false)
                                        }
                                    }
                          }
                    } else {
                        viewController.navigationController?.popToViewController(vc, animated: true)
                    }
                }else{
                    viewController.navigationController?.popToRootViewController(animated: true)
                }
                
            }else{
                viewController.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumExamResultTableRow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumExamResultTableRow(rawValue: indexPath.row)! {
        case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: ResultImageTableViewCell.identifier) as! ResultImageTableViewCell
            let isPassed = viewModel.isUserPassed()
            if viewController.activityIndicator.isAnimating {
                cell.setupDetails(subtitle: "", isPassed: isPassed)
            }else{
                cell.setupDetails(subtitle: isPassed ? Constants.description_passedExam : Constants.description_failedExam, isPassed: isPassed)
            }
            return cell
        case .category:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.setupAttributedText(title: Constants.description_examCategory, text: viewModel.getExamCategory())
            return cell
        case .date:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.setupAttributedText(title: Constants.description_dateTime, text: viewModel.getSubmittedDate())
            return cell
        case .numberOfQuestions:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.setupAttributedText(title: Constants.description_numberOfQuestion, text: viewModel.getNumberOfQuestionsToDisplay())
            return cell
        case .score:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.setupAttributedText(title: Constants.description_score, text: viewModel.getScore())
            return cell
        case .result:
            let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.identifier) as! ResultTableViewCell
            cell.labelPercentage.text = viewModel.getResultText()
            cell.labelPercentage.textColor = viewModel.isUserPassed() ? Constants.color_greenText : .red
            return cell
        case .finish:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.setTitleColor(.white, for: .normal)
            cell.button.setTitle(Constants.title_finish, for: .normal)
            cell.button.titleLabel?.font = CustomFont.bold(ofSize: 18)
            cell.button.addTarget(self, action: #selector(buttonFinishTouchUpInside(_:)), for: .touchUpInside)
            return cell
            
        /*case .getCertificate:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.tag = indexPath.row
            cell.button.setTitle(Constants.title_getCertificate, for: .normal)
            cell.setupBackgroundWithRadiant()
            return cell
        case .showResult:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.tag = indexPath.row
            cell.button.setTitle(Constants.title_showResult, for: .normal)
            cell.setupBackgroundWithRadiant()
            cell.button.addTarget(self, action: #selector(buttonShowResultTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .theoreticalTest:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.tag = indexPath.row
            cell.button.setTitle(Constants.title_theoreticalTest, for: .normal)
            cell.setupBorder()
            return cell
        case .practicalExam:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.tag = indexPath.row
            cell.button.setTitle(Constants.title_practicalExam, for: .normal)
            cell.setupBorder()
            return cell*/
        }
    }
}


