//
//  ExamResultViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 14/07/21.
//

import UIKit

class ExamResultViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: ExamResultDataSource?
    lazy var viewModel = ExamResultViewModel()
    var isFromLesson = false
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ExamResultDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
        viewModel.getResult()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        if isFromLesson {
            self.navigationController?.popViewController(animated: true)
          
        }else{
            let count = (self.navigationController?.viewControllers.count ?? 0) - 3
            if count < self.navigationController?.viewControllers.count ?? 0 {
                if let vc = self.navigationController?.viewControllers[count] {
                    self.navigationController?.popToViewController(vc, animated: true)
                }else{
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
            }else{
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        /*for controller in navigationController?.viewControllers ?? [] {
            if let descriptionViewController = controller as? LessonDescriptionViewController {
                self.navigationController?.popToViewController(descriptionViewController, animated: true)
            }
        }*/
    }

    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_result
        labelTitle.font = CustomFont.medium(ofSize: 18)
        viewModel.isResultFetched.bind { isFetched in
            self.tableView.reloadData() 
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        if isFromLesson {
            self.tableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
}

