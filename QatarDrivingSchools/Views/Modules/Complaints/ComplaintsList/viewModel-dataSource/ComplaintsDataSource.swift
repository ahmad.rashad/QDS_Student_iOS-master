//
//  ComplaintsDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import Foundation
import UIKit

class ComplaintsDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ComplaintsViewController
    private let tableView: UITableView
    private let viewModel: ComplaintsViewModel
    
    enum enumComplainSectionType: Int {
        case complaints = 0
        case noData
        static let count: Int = {
            var max: Int = 0
            while let _ = enumComplainSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: ComplaintsViewModel, viewController: ComplaintsViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [ComplaintsTableViewCell.identifier,
                                         ImageTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func buttonEditTouchUpInside(_ sender: UIButton) {
        let vc = NewComplainViewController.loadFromNib()
        vc.viewControllerType = .edit
        vc.completionHandler = { isReloadRequired in
            if isReloadRequired {
                self.viewModel.resetData()
            }
        }
        vc.viewModel.setCurrentComplainDetails(value: viewModel.getComplain(at: sender.tag))
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func buttonInfoTouchUpInside(_ sender: UIButton) {
        let complain = viewModel.getComplain(at: sender.tag)
        if let response = complain.response {
            let ac = UIAlertController(title:"", message: response, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.title_ok, style: .default))
            viewController.present(ac, animated: true)
        }
    }
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return enumComplainSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumComplainSectionType(rawValue: section)! {
        case .complaints:
            return viewModel.getTotalNumberOfComplains()
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalNumberOfComplains() == 0 ? 1 : 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumComplainSectionType(rawValue: indexPath.section)! {
        case .complaints:
            return UITableView.automaticDimension
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalNumberOfComplains() == 0 ? tableView.frame.height : 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumComplainSectionType(rawValue: indexPath.section)! {
        case .complaints:
            let cell = tableView.dequeueReusableCell(withIdentifier: ComplaintsTableViewCell.identifier) as! ComplaintsTableViewCell
            cell.setDetails(detail: viewModel.getComplain(at: indexPath.row))
            cell.buttonEdit.tag = indexPath.row
            cell.buttonEdit.addTarget(self, action: #selector(buttonEditTouchUpInside(_:)), for: .touchUpInside)
            cell.buttonInfo.tag = indexPath.row
            cell.buttonInfo.addTarget(self, action: #selector(buttonInfoTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumComplainSectionType(rawValue: indexPath.section)! {
        case .complaints:
            let vc = NewComplainViewController.loadFromNib()
            vc.viewControllerType = .viewOnly
            vc.viewModel.setCurrentComplainDetails(value: viewModel.getComplain(at: indexPath.row))
            viewController.navigationController?.pushViewController(vc, animated: true)
        case .noData:
            break
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        let count = viewModel.getTotalNumberOfComplains()
        
        let isLastCell = count - currentRow == 1
        let minY = tableView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInComplain() && isLastCell && minY > 0 {
            viewModel.getComplainList()
        }
    }
}

