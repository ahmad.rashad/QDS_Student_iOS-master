//
//  ComplaintsViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit
import NISLRequestPackages
class ComplaintsViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isDataFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var complainList: [ComplainData] = []
    private var noDataPlaceholder = ""
    private var isMoreDataAvailable = true
    private var offset = 0
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- API
    func getComplainList() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetComplainList, queryParameter: param) { (res: Swift.Result<BaseClass<ComplainBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if self.offset == 0 {
                        self.isMoreDataAvailable = true
                        self.complainList.removeAll()
                    }
                    let list = data.data?.complainData ?? []
                    self.offset += 1
                    self.complainList.append(contentsOf: list)
                    if list.count == 0{
                        self.isMoreDataAvailable = false
                    }
                } else {
                    self.isMoreDataAvailable = false
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.noDataPlaceholder = ""
                if self.complainList.count == 0 {
                    self.noDataPlaceholder = Constants.description_noComplaintsYet
                }
                self.isDataFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.noDataPlaceholder = Constants.description_noComplaintsYet
                self.isDataFetched.value = false
                self.isMoreDataAvailable = false
                self.isDataFetched.value = true
            }
        }
    }
}
//MARK:- Getters and Setters
extension ComplaintsViewModel {
    func  getTotalNumberOfComplains() -> Int {
        complainList.count
    }
    func getComplain(at index: Int) -> ComplainData {
        complainList[index]
    }
    func getNoDataPlaceholder() -> String {
        noDataPlaceholder
    }
    func isMoreDataAvailableInComplain() -> Bool {
        isMoreDataAvailable
    }
    func resetData() {
        offset = 0
        getComplainList()
    }
}
