//
//  ComplaintsTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class ComplaintsTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelComplainTypeTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelAttachment: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var viewStatus: DesignableView!
    @IBOutlet weak var imageViewAttachment: UIImageView!
    @IBOutlet weak var buttonInfo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        
        labelComplainTypeTitle.font = CustomFont.medium(ofSize: 16)
        labelDescription.font = CustomFont.regular(ofSize: 16)
        labelDate.font = CustomFont.medium(ofSize: 16)
        labelAttachment.font = CustomFont.regular(ofSize: 16)
        labelStatus.font = CustomFont.medium(ofSize: 14)
        
        labelDescription.textColor = .gray
        labelAttachment.textColor = .darkGray
        buttonInfo.isHidden = true
        labelComplainTypeTitle.text = Constants.description_complainType
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setDetails(detail: ComplainData) {
        labelDate.attributedText = setupAttributedDateText(text: (detail.complainDate)?.toDateString(outputFormat: "dd-MM-yyyy") ?? "")
        labelComplainTypeTitle.text = "\(Constants.description_complainType) \(detail.categoryName ?? "")"
        labelDescription.text = detail.descriptionValue ?? ""
        let mediaCount = detail.media?.count ?? 0
        if mediaCount > 1 {
            labelAttachment.text = "\(mediaCount) \(Constants.description_filesAttached)"
        }else{
            labelAttachment.text = "\(mediaCount) \(Constants.description_fileAttached)"
        }
        if mediaCount == 0 {
            labelAttachment.text = Constants.description_noFilesAttached
        }
        buttonEdit.isHidden = false
        let status = detail.status ?? 0
        switch enumComplainStatus(rawValue: status)! {
        case .pending:
            labelStatus.text = Constants.description_pending
            viewStatus.backgroundColor = Constants.color_yellow
        case .done:
            buttonEdit.isHidden = true
            labelStatus.text = Constants.title_done
            viewStatus.backgroundColor = Constants.color_darkGreen
        case .inProgress:
            labelStatus.text = Constants.description_inProgress
            viewStatus.backgroundColor = Constants.color_applicationThemeColor
        }
        buttonInfo.isHidden = true
        if !(detail.response?.isEmpty ?? true) {
            buttonInfo.isHidden = false
        }
    }
 
    
    func setupLayoutForAppointmentHistory(appointment: Appointments)
    {
        labelComplainTypeTitle.attributedText = setupAttributedDateText(title: "\(appointment.title ?? "")\n\n" + Constants.description_eventType + ": ",
                                                                        titleFont: CustomFont.medium(ofSize: 16),
                                                                        text: appointment.eventName ?? "",
                                                                        textFont: CustomFont.regular(ofSize: 16))
        let date = "\(appointment.availiblityDate?.toDate()?.toString(format: "dd-MM-YYYY") ?? "") \(appointment.availiblityTime?.toDate(format: "hh:mm a")?.toString(format: "hh:mm a") ?? "")"
        labelDescription.attributedText = setupAttributedDateText(title: Constants.description_issueDate + " : ", text: date)
        labelDate.text = appointment.requestDescription ?? ""
        labelDate.font = CustomFont.regular()
        labelDate.textColor = .darkGray
        labelDescription.textColor = .black
        imageViewAttachment.isHidden = true
        if appointment.meetingDate?.isEmpty ?? true {
            buttonEdit.isHidden = false
        }else{
            buttonEdit.isHidden = true
        }
        switch enumAppointmentStatus(rawValue: appointment.status ?? -1){
        case .pending:
            labelStatus.text = Constants.description_pending
            viewStatus.backgroundColor = Constants.color_yellow
        case .completed:
            labelStatus.text = Constants.description_completed
            viewStatus.backgroundColor = Constants.color_darkGreen
        case .cancelled:
            labelStatus.text = Constants.description_cancelled
            viewStatus.backgroundColor = Constants.color_red
        default:
            labelStatus.text = Constants.description_notAssigned
            viewStatus.backgroundColor = .black
        }
    }
    
    func setupAttributedDateText(
        title: String = Constants.description_dateTitle,
                                 titleFont: UIFont = CustomFont.regular(ofSize: 16),
                                 text: String,
                                 textFont: UIFont = CustomFont.medium(ofSize: 16)) -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString(string: title + "", attributes: [NSAttributedString.Key.font: titleFont])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: textFont,
                                                                                                  NSAttributedString.Key.foregroundColor: UIColor.black]))
        return mutableAttributedString
    }
    
}
