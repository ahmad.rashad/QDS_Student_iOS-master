//
//  ComplaintsViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class ComplaintsViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: ComplaintsDataSource?
    lazy var viewModel = ComplaintsViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ComplaintsDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    @IBAction func buttonAddTouchUpInside(_ sender: UIButton) {
        let vc = NewComplainViewController.loadFromNib()
        vc.completionHandler = { isReloadRequired in
            if isReloadRequired {
                self.viewModel.resetData()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Class methods
    func setupLayout() {
        labelTitle.text = Constants.title_complaints
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelNoData.font = CustomFont.semiBold()
        
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.stopAnimating()
            }else{
                self.view.isUserInteractionEnabled = false
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isDataFetched.bind { isFetched in
            if isFetched {
                self.tableView.reloadData()
                
            }else{
              //  self.labelNoData.text = self.viewModel.getNoDataPlaceholder()
            }
        }
        viewModel.getComplainList()
    }
}
