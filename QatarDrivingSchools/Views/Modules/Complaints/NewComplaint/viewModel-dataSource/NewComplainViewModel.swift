//
//  NewComplainViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit
import NISLRequestPackages

class NewComplainViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isCategoryDataFetched: Dynamic<Bool> = Dynamic(false)
    var isComplainAdded: Dynamic<Bool> = Dynamic(false)
    private var apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var complainCategories: [ComplaintsCategory] = []
    private var currentComplainDetails = ComplainData()
    private var mediaParams : NSMutableDictionary = [:]
    private var deletedMediaId = ""
    //MARK:- Dependancy Injection
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 100000
        let sessionManager = SessionManager(configuration: configuration)
        apiManager = RequestManager(sessionManager: sessionManager, retrier: RequestManagerRetrier())
    }
    
    func getComplainCategoryList() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? ""
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetComplainCategories, queryParameter: param) { (res: Swift.Result<BaseClass<ComplaintsCategoryBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.complainCategories.append(contentsOf: data.data?.complaintsCategory ?? [])
                    self.isCategoryDataFetched.value = true
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }
    
    func isValidData() -> Bool {
        if currentComplainDetails.categoryId == nil || currentComplainDetails.categoryId == 0 {
            self.alertMessage.value = AlertMessage(title: "", body: Constants.alert_pleaseSelectComplainCategory)
            isLoaderHidden.value = true
            return false
        }
        else if currentComplainDetails.complainTitle?.isEmpty ?? true {
            self.alertMessage.value = AlertMessage(title: "", body: Constants.alert_complainTitleMissing)
            isLoaderHidden.value = true
            return false
        }
        else if currentComplainDetails.descriptionValue?.isEmpty ?? true {
            self.alertMessage.value = AlertMessage(title: "", body: Constants.alert_complainDescMissing)
            isLoaderHidden.value = true
            return false
        }
        return true
    }
    func addComplain() {
        if isValidData() {
            var param: Parameters = [
                "S_Code":Constants.loggedInUser?.scode ?? "",
                "CategoryId":currentComplainDetails.categoryId ?? 0,
                "EmailId":Constants.getLoggedInUserEmailForAPI(),
                "Description":currentComplainDetails.descriptionValue ?? "",
                "ComplainTitle":currentComplainDetails.complainTitle ?? ""
            ]
            var media: [Any] = []
            for (_,value) in mediaParams {
                //param[key as? String ?? ""] = value
                media.append(value)
            }
            param["Attachment"] = media
            isLoaderHidden.value = false
            apiManager.upload(type: RequestItemsType.AddComplain, params: param) { (res: Swift.Result<BaseClass<ComplainBaseClass>, AlertMessage>, response) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        self.isComplainAdded.value = true
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                case .failure(let message):
                    self.alertMessage.value = message
                }
            }
        }
    }
    func editComplain() {
        if isValidData() {
            var param: Parameters = [
                "S_Code":Constants.loggedInUser?.scode ?? "",
                "CategoryId":currentComplainDetails.categoryId ?? 0,
                "ComplainId":currentComplainDetails.complainId ?? 0,
                "ComplainTitle":currentComplainDetails.complainTitle ?? "",
                "EmailId":Constants.getLoggedInUserEmailForAPI(),
                "Description":currentComplainDetails.descriptionValue ?? "",
                "DeletedMediaId":deletedMediaId
            ]
            var media: [Any] = []
            for (_,value) in mediaParams {
                //param[key as? String ?? ""] = value
                media.append(value)
            }
            param["Attachment"] = media
            isLoaderHidden.value = false
            apiManager.upload(type: RequestItemsType.EditComplain, params: param) { (res: Swift.Result<BaseClass<ComplainBaseClass>, AlertMessage>, response) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        self.isComplainAdded.value = true
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    break
                case .failure(let message):
                    self.alertMessage.value = message
                    break
                }
            }
        }
    }
    
}
//MARK:- Getters and Setters
extension NewComplainViewModel {
    func getOldComplainMediaCount() -> Int {
        return currentComplainDetails.media?.count ?? 0
    }
    func getOldComplainMedia(at index: Int) -> Media {
        return currentComplainDetails.media?[index] ?? Media()
    }
    func getComplaintsCategory() -> [ComplaintsCategory] {
        complainCategories
    }
    func getComplaintTitle() -> String {
        currentComplainDetails.complainTitle ?? ""
    }
    func getCategoryName() -> String {
        if currentComplainDetails.categoryName == "" {
            return Constants.placeholder_selectCategory
        }
        return currentComplainDetails.categoryName ?? Constants.placeholder_selectCategory
    }
    func getDescription() -> String {
        currentComplainDetails.descriptionValue ?? ""
    }
    func setCategoryDetails(value: ComplaintsCategory){
        currentComplainDetails.categoryId = value.id ?? 0
        currentComplainDetails.categoryName = value.complainType ?? ""
    }
    func setMediaParam(value: NSMutableDictionary) {
        mediaParams = value
    }
    func setTitle(value: String) {
        currentComplainDetails.complainTitle = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func setDescription(value: String) {
        currentComplainDetails.descriptionValue = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func addDeletedMedia(index: Int) {
        let value = "\(currentComplainDetails.media?[index].id ?? 0)"
        if deletedMediaId.isEmpty {
            deletedMediaId.append(value)
        }else{
            deletedMediaId.append(","+value)
        }
        currentComplainDetails.media?.remove(at: index)
    }
    func getSelectedCategoryIndex() -> Int {
        for i in 0..<complainCategories.count {
            if complainCategories[i].id  == currentComplainDetails.categoryId{
                return i
            }
        }
        return -1
    }
    func setCurrentComplainDetails(value: ComplainData) {
        currentComplainDetails = value
    }
    
}

