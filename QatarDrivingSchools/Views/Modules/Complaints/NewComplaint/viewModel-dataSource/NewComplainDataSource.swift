//
//  NewComplainDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import Foundation
import UIKit
import Photos
import AVKit
class NewComplainDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: NewComplainViewController
    private let tableView: UITableView
    private let viewModel: NewComplainViewModel
    
    enum enumNewComplainTableRow: Int {
        case heading
        case title
        case category
        case description
        case attachment
        case attachmentMedia
        static let count: Int = {
            var max: Int = 0
            while let _ = enumNewComplainTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: NewComplainViewModel, viewController: NewComplainViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [ComplainDetailsTableViewCell.identifier,
                                         TextFieldTableViewCell.identifier,
                                         TextViewTableViewCell.identifier,
                                         AttachFileTableViewCell.identifier,
                                         CollectionWithTitleTableViewCell.identifier])
        tableView.reloadData()
    }
    func openGallery(type: Int)
    {
        viewController.showMediaPicker(mediaType: type)
    }
    func optionsToSelectMedia() {
        viewController.showMediaPicker()
       /*let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Constants.title_images, style: .default, handler: { _ in
            self.openGallery(type: PHAssetMediaType.image.rawValue)
        }))
        alert.addAction(UIAlertAction(title: Constants.title_video, style: .default, handler: { _ in
            self.openGallery(type: PHAssetMediaType.video.rawValue)
        }))
        alert.addAction(UIAlertAction(title: Constants.title_cancel, style: .cancel, handler: { _ in
            
        }))
        viewController.present(alert, animated: true)*/
    }
    func openVideoInFullScreen(url: URL){
        DispatchQueue.main.async {
            let playerViewController = AVPlayerViewController()
            let player = AVPlayer(url: url)
            playerViewController.player = player
            self.viewController.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    //MARK:- Action methods
    @objc func addAttachmentTouchUpInside(_ sender: UIButton) {
        optionsToSelectMedia()
    }
    @objc func buttonDeleteMediaTouchUpInside(_ sender: UIButton){
        if viewController.viewControllerType == .edit{
            if sender.tag < viewModel.getOldComplainMediaCount() {
                viewModel.addDeletedMedia(index: sender.tag)
            }else{
                viewController.selectedMedia.remove(at: sender.tag - viewModel.getOldComplainMediaCount())
                //viewController.setMediaParams()
            }
        }else{
            viewController.selectedMedia.remove(at: sender.tag)
            //viewController.setMediaParams()
        }
        tableView.reloadRows(at: [IndexPath(row: enumNewComplainTableRow.attachmentMedia.rawValue, section: 0)], with: .none)
    }
    
    @objc func buttonPlayMediaTouchUpInside(_ sender: UIButton) {
        if sender.tag < viewModel.getOldComplainMediaCount() {
            let media = viewModel.getOldComplainMedia(at: sender.tag)
            let url = Constants.getPathOfMedia(value: media.name ?? "")
            if let url = url {
                openVideoInFullScreen(url: url)
            }
        }else{
            let asset = viewController.selectedMedia[sender.tag - viewModel.getOldComplainMediaCount()]
            PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (AVAsset, audio, info) in
                let asset = AVAsset as! AVURLAsset
                let url = asset.url
                self.openVideoInFullScreen(url: url)
            }
        }
    }
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumNewComplainTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumNewComplainTableRow(rawValue: indexPath.row)! {
        case .attachmentMedia:
            return UIScreen.main.bounds.width / 3
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumNewComplainTableRow(rawValue: indexPath.row)! {
        case .heading:
            let cell = tableView.dequeueReusableCell(withIdentifier: ComplainDetailsTableViewCell.identifier) as! ComplainDetailsTableViewCell
            return cell
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.textField.tag = enumNewComplainTableRow.title.rawValue
            cell.labelTitle.text = Constants.placeholder_complaintTitle
            cell.textField.placeholder = Constants.placeholder_complaintTitle
            cell.textField.text = viewModel.getComplaintTitle()
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            cell.textField.isUserInteractionEnabled = viewController.viewControllerType != .viewOnly
            return cell
        case .category:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.description_complainCategory,
                           text: viewModel.getCategoryName(),
                           placeholder: Constants.placeholder_selectCategory,
                           rightImage: #imageLiteral(resourceName: "dropdown_ic"))
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .description:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextViewTableViewCell.identifier) as! TextViewTableViewCell
            cell.setupData(title: Constants.description_complainDescription,
                           text: viewModel.getDescription(),
                           placeholder: Constants.placeholder_enterDetails)
            cell.textView.delegate = self
            //cell.textView.isUserInteractionEnabled = viewController.viewControllerType != .viewOnly
            if viewController.viewControllerType == .viewOnly {
                cell.textView.isEditable = false
            }
            return cell
        case .attachment:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttachFileTableViewCell.identifier) as! AttachFileTableViewCell
            cell.buttonAttachment.addTarget(self, action: #selector(addAttachmentTouchUpInside(_:)), for: .touchUpInside)
            if viewController.viewControllerType == .viewOnly {
                cell.stackViewAttachment.isHidden = true
                cell.heightAttachment.constant = 0
            }
            return cell
        case .attachmentMedia:
            let cell = tableView.dequeueReusableCell(withIdentifier: CollectionWithTitleTableViewCell.identifier) as! CollectionWithTitleTableViewCell
            cell.collectionView.registerNib(nibNames: [MediaCollectionViewCell.identifier])
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            if UIDevice.current.userInterfaceIdiom == .pad {
                cell.setLayoutForIPad()
            }
            if let layout = cell.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            cell.collectionView.reloadData()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewController.viewControllerType != .viewOnly {
            switch enumNewComplainTableRow(rawValue: indexPath.row)! {
            case .category:
                let filterVC = FilterViewController.loadFromNib()
                filterVC.viewControllerType = .complainCategory
                viewModel.isCategoryDataFetched.bind { isFetched in
                    if isFetched {
                        filterVC.viewModel.setComplainCategory(value: self.viewModel.getComplaintsCategory(), index: self.viewModel.getSelectedCategoryIndex())
                        filterVC.tableView.reloadData()
                    }
                }
                filterVC.selectedCategory = { category in
                    if let category = category {
                        self.viewModel.setCategoryDetails(value: category)
                        self.tableView.reloadData()
                    }
                }
                filterVC.viewModel.setComplainCategory(value: self.viewModel.getComplaintsCategory(), index: viewModel.getSelectedCategoryIndex())
                filterVC.viewModel.setTitle(value: Constants.placeholder_selectCategory)
                filterVC.modalPresentationStyle = .overFullScreen
                viewController.present(filterVC, animated: true, completion: nil)
            default:
                break
            }
        }
    }
 
}
//MARK:- CollectionView Methods
extension NewComplainDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch viewController.viewControllerType {
        case .regular:
            let count = viewController.selectedMedia.count + 1
            return count > 3 ? 3 : count
        case .edit:
            let count = viewModel.getOldComplainMediaCount() + viewController.selectedMedia.count + 1
            return count > 3 ? 3 : count
        case .viewOnly:
            return viewModel.getOldComplainMediaCount()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: collectionView.frame.width / 3 - 15, height: collectionView.frame.width / 3 - 15)
        }
        return CGSize(width: collectionView.frame.width / 3 - 10, height: collectionView.frame.width / 3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediaCollectionViewCell.identifier, for: indexPath) as! MediaCollectionViewCell
        cell.buttonDelete.isHidden = false
        if viewController.viewControllerType == .viewOnly {
            cell.buttonDelete.isHidden = true
        }
        cell.buttonDelete.tag = indexPath.row
        cell.buttonDelete.addTarget(self, action: #selector(buttonDeleteMediaTouchUpInside(_:)), for: .touchUpInside)
        cell.buttonCounter.isHidden = true
        cell.buttonCounter.setTitle("\(indexPath.row + 1)", for: .normal)
        cell.removePlayer()
        cell.imageViewMedia.image = Constants.image_addImage
        if indexPath.row < viewModel.getOldComplainMediaCount() {
            let media = viewModel.getOldComplainMedia(at: indexPath.row)
            switch enumMediaType(rawValue: media.type ?? 0) {
            case .image:
                cell.imageViewMedia?.contentMode = .scaleAspectFill
                cell.imageViewMedia.sd_setImage(with: Constants.getPathOfMedia(value: media.name), placeholderImage: #imageLiteral(resourceName: "not_found_ic"), options: [], context: [:])
            case .video:
                cell.mediaUrl = Constants.getPathOfMedia(value: media.name)
                cell.addPlayerIntoView()
                cell.buttonPlayMedia.tag = indexPath.row
                cell.buttonPlayMedia.addTarget(self, action: #selector(buttonPlayMediaTouchUpInside(_:)), for: .touchUpInside)
                cell.buttonPlayMedia.isUserInteractionEnabled = true
                cell.buttonPlayMedia.isHidden = false
            case .other:
                cell.imageViewMedia?.contentMode = .scaleAspectFit
                cell.imageViewMedia.image = #imageLiteral(resourceName: "pdf")
            default:
                cell.imageViewMedia.image = #imageLiteral(resourceName: "not_found_ic")
            }
        }
        else if indexPath.row < (viewController.selectedMedia.count + viewModel.getOldComplainMediaCount()) {
            let asset = viewController.selectedMedia[indexPath.row - viewModel.getOldComplainMediaCount()]
            switch asset.mediaType {
            case .image:
                PHCachingImageManager().requestImage(for: asset, targetSize: CGSize(width: cell.width, height: cell.height), contentMode: .aspectFill, options: nil) { (image, info) in
                    DispatchQueue.main.async {
                        cell.imageViewMedia?.contentMode = .scaleAspectFill
                        cell.imageViewMedia?.image = image
                        cell.buttonPlayMedia.isHidden = true
                    }
                }
            case .video:
                PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (AVAsset, audio, info) in
                    let asset = AVAsset as! AVURLAsset
                    DispatchQueue.main.async {
                        cell.mediaUrl = asset.url
                        cell.addPlayerIntoView()
                        cell.buttonPlayMedia.isUserInteractionEnabled = true
                        cell.buttonPlayMedia.isHidden = false
                        cell.buttonPlayMedia.tag = indexPath.row
                        cell.buttonPlayMedia.addTarget(self, action: #selector(self.buttonPlayMediaTouchUpInside(_:)), for: .touchUpInside)
                    }
                }
                break
            default:
                print("Unknown")
            }
        }else{
            cell.imageViewMedia.contentMode = .scaleAspectFit
            cell.imageViewMedia.image = Constants.image_addImage
            //cell.imageViewMedia.backgroundColor = Constants.color_lightOrangeForCalendar
            cell.buttonDelete.isHidden = true
            cell.buttonCounter.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < viewModel.getOldComplainMediaCount() {
            let media = viewModel.getOldComplainMedia(at: indexPath.row)
            if media.type == enumMediaType.other.rawValue {
                let webKitViewController = WebKitViewController.loadFromNib()
                webKitViewController.url = Constants.getPathOfMedia(value: media.name ?? "")
                webKitViewController.titleString = Constants.description_pdf
                webKitViewController.modalPresentationStyle = .overFullScreen
                viewController.present(webKitViewController, animated: true, completion: nil)
            }else{
                let carouselVC = CarouselViewController.loadFromNib()
                carouselVC.urls = [media]
                carouselVC.mediaType = .URL
                self.viewController.present(carouselVC, animated: true, completion: nil)
            }
        }
        else if indexPath.row < (viewController.selectedMedia.count + viewModel.getOldComplainMediaCount()) {
            //viewController.showMediaPicker()
            let carouselVC = CarouselViewController.loadFromNib()
            carouselVC.media = [viewController.selectedMedia[indexPath.row]]
            //carouselVC.index = index
            self.viewController.present(carouselVC, animated: true, completion: nil)
        }else{
            viewController.showMediaPicker()
        }
    }
        
}
//MARK:- TextView Methods
extension NewComplainDataSource: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.placeholder_enterDetails
            textView.textColor = UIColor.lightGray
            viewModel.setDescription(value: "")
        }else{
            viewModel.setDescription(value: textView.text.trimmingCharacters(in: .whitespacesAndNewlines))
        }
    }
}
//MARK:- TextField Delegate methods
extension NewComplainDataSource  {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumNewComplainTableRow(rawValue: textField.tag) {
        case .title:
            viewModel.setTitle(value: textField.text ?? "")
        default:
            break
        }
    }
}

