//
//  AttachFileTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class AttachFileTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var stackViewAttachment: UIStackView!
    
    @IBOutlet weak var heightAttachment: NSLayoutConstraint!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        
        labelTitle.font = CustomFont.semiBold(ofSize: 14)
        labelTitle.textColor = .darkGray
        
        buttonAttachment.titleLabel?.font = CustomFont.semiBold(ofSize: 12)
        
        labelTitle.text = Constants.description_attachFile
        buttonAttachment.setTitle(" " + Constants.title_addAttachment, for: .normal)
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
