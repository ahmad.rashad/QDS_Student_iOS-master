//
//  ComplainDetailsTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class ComplainDetailsTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold()
        labelDescription.font = CustomFont.regular(ofSize: 14)
        
        labelDescription.textColor = .lightGray
        
        labelTitle.text = Constants.description_complainDetails
        labelDescription.text = "Lorem ipsum dolor sit amet. Sed impedit repudiandae aut perferendis consequatur est modi nemo non quas obcaecati id quod voluptates. Id enim asperiores et laboriosam natus a laboriosam dolores ex omnis molestiae a nobis deserunt. In beatae reprehenderit et velit ipsum qui mollitia distinctio et minima voluptatum At libero laboriosam et tempore sunt."
        labelDescription.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
