//
//  NewComplainViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit
import Photos
import AssetsPickerViewController
import NotificationBannerSwift
class NewComplainViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonCancel: RoundButton!
    @IBOutlet weak var buttonSave: ApplicationThemeButton!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    
    var dataSource: NewComplainDataSource?
    lazy var viewModel = NewComplainViewModel()
    
    var selectedMedia : [PHAsset] = []
    var mediaCountThatNeedToRemove = 0
    var viewControllerType = enumViewControllerType.regular
    var completionHandler: ((_ isReloadRequired: Bool)->Void)?
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = NewComplainDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }

    //MARK:- Class Methods
    func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url) else {
                assertionFailure("Not able to open App privacy settings")
                return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    func showUI(for status: PHAuthorizationStatus, VC: UIViewController) {
        
        switch status {
        case .authorized:
           // showFullAccessUI()
            break
        case .limited:
            showAlertToSelectOtherImages(VC: VC)

        case .restricted:
            //showRestrictedAccessUI()
            break
        case .denied:
           // showAccessDeniedUI()
            break
        case .notDetermined:
            break

        @unknown default:
            break
        }
    }
    func showAlertToSelectOtherImages(VC: UIViewController) {
        let actionSheet = UIAlertController(title: "",
                                            message: "Select more photos or go to Settings to allow access to all photos.",
                                            preferredStyle: .actionSheet)
        
        let selectPhotosAction = UIAlertAction(title: "Select more photos",
                                               style: .default) {  [unowned self] (_) in
            // Show limited library picker
            //PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self)
            self.gotoAppPrivacySettings()
        }
        actionSheet.addAction(selectPhotosAction)
        
        let allowFullAccessAction = UIAlertAction(title: "Allow access to all photos",
                                                  style: .default) { [unowned self] (_) in
            // Open app privacy settings
            gotoAppPrivacySettings()
        }
        actionSheet.addAction(allowFullAccessAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        
        VC.present(actionSheet, animated: true, completion: nil)
    }
    func showAccessDeniedUI() {
        let alert = UIAlertController(title: "Allow access to your photos",
                                      message: "This lets you share from your camera roll and enables other features for photos and videos. Go to your settings and tap \"Photos\".",
                                      preferredStyle: .alert)
        
        let notNowAction = UIAlertAction(title: "Not Now",
                                         style: .cancel,
                                         handler: nil)
        alert.addAction(notNowAction)
        
        let openSettingsAction = UIAlertAction(title: "Open Settings",
                                               style: .default) { [unowned self] (_) in
            // Open app privacy settings
            gotoAppPrivacySettings()
        }
        alert.addAction(openSettingsAction)
        
        present(alert, animated: true, completion: nil)
    }

    func showMediaPicker(mediaType: Int = PHAssetMediaType.image.rawValue){
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //showFullAccessUI()
            break
        case .limited:
            //showLimittedAccessUI()
            break
        case .restricted:
            //showLimittedAccessUI()
            break
        case .denied:
            showAccessDeniedUI()
            
        case .notDetermined:
            break
        @unknown default:
            break
        }
        
        
        let picker =  AssetsPickerViewController()
        if !Constants.IS_FIRST_TIME_IMAGE_PICKER {
            if #available(iOS 14, *) {
                PHPhotoLibrary.requestAuthorization(for: .readWrite) { [unowned self] (status) in
                    DispatchQueue.main.async { [unowned self] in
                        showUI(for: status, VC: picker)
                    }
                }
            } else {
                // Fallback on earlier versions
            }
        }else{
            Constants.IS_FIRST_TIME_IMAGE_PICKER = !Constants.IS_FIRST_TIME_IMAGE_PICKER
        }
        
        /*let smartAlbumFilter: ((PHAssetCollection, PHFetchResult<PHAsset>) -> Bool) = { (album, fetchResult) in
         if album.assetCollectionSubtype == .smartAlbumBursts { return false }
         if album.assetCollectionSubtype == .smartAlbumTimelapses { return false }
         if album.assetCollectionSubtype == .smartAlbumFavorites { return false }
         if album.assetCollectionSubtype == .smartAlbumVideos { return false }
         if album.assetCollectionSubtype == .smartAlbumSlomoVideos { return false }
         return true
         }*/
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "mediaType = %d",  mediaType)
        let pickerConfig = AssetsPickerConfig()
        // pickerConfig.assetCellType = CustomAssetCell.classForCoder()
        pickerConfig.assetPortraitColumnCount = 3
        pickerConfig.assetLandscapeColumnCount = 5
        //pickerConfig.albumFilter = [.smartAlbum: smartAlbumFilter]
        
        // Uncomment to apply media filter
        /* pickerConfig.assetFetchOptions = [  .smartAlbum: options,
                                            .album: options]*/
        pickerConfig.assetIsShowCameraButton = false
        picker.pickerConfig = pickerConfig
        picker.pickerDelegate = self
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    
    func setMediaParams(){
        let param : NSMutableDictionary = [:]
        let myGroup = DispatchGroup()
        for i in 0..<selectedMedia.count{
            let asset = selectedMedia[i]
            switch asset.mediaType {
            case .image:
                #if DEBUG
                //myGroup.enter()
                #endif
                myGroup.enter()
                PHCachingImageManager().requestImage(for: asset, targetSize: preferredContentSize, contentMode: .default, options: nil) { (image, info) in
                    let key = "Attachment[\(i)]"
                    if !((param.allKeys as? [String])?.contains(key) ?? true) {
                        param[key] = image
                        myGroup.leave()
                    }
                }
                if #available(iOS 14, *) {
                   
                }else{
                    /*PHCachingImageManager().requestImageData(for: asset, options: nil) { (data, string, orientation, info) in
                        param["workout_media[\(i)]"] = UIImage(data: data!)
                        myGroup.leave()
                    }*/
                }
            case .video:
                myGroup.enter()
                PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (AVAsset, audio, info) in
                    let asset = AVAsset as! AVURLAsset
                    encodeVideo(at: asset.url) { (url, error) in
                        if error == nil {
                            let key = "Attachment[\(i)]"
                            if !((param.allKeys as? [String])?.contains(key) ?? true) {
                                param[key] = url
                                myGroup.leave()
                            }
                        }else{
                            myGroup.leave()
                        }
                    }
                }
            break
            default:
                print("Unknown")
            }
        }
        myGroup.notify(queue: .main) {
            self.viewModel.setMediaParam(value: param)
            switch self.viewControllerType {
            case .regular:
                self.viewModel.addComplain()
            case .edit:
                self.viewModel.editComplain()
            case .viewOnly:
                break
            }
        }
    }
    
    func setupLayout() {
        
        labelTitle.text = viewControllerType == .regular ? Constants.title_newComplain : Constants.title_editComplain
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        buttonCancel.setTitle(Constants.title_cancel.localized, for: .normal)
        buttonSave.setTitle(Constants.title_submit.localized, for: .normal)
        
        buttonCancel.titleLabel?.font = CustomFont.regular(ofSize: 20)
        buttonSave.titleLabel?.font = CustomFont.semiBold(ofSize: 20)
        
        switch viewControllerType {
        case .edit:
            buttonSave.setTitle(Constants.title_saveInCaps.localized, for: .normal)
        case .regular:
            buttonSave.setTitle(Constants.title_submit.localized, for: .normal)
        case .viewOnly:
            labelTitle.text = Constants.title_complainDetails
            bottomViewHeight.constant = 0
            buttonCancel.isHidden = true
            buttonSave.isHidden = true
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.stopAnimating()
            }else{
                self.view.isUserInteractionEnabled = false
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isComplainAdded.bind { isAdded in
            if isAdded {
                self.completionHandler?(true)
                self.navigationController?.popViewController(animated: true)
            }
        }
        viewModel.getComplainCategoryList()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSaveTouchUpInside(_ sender: RoundButton) {
        viewModel.isLoaderHidden.value = false
        setMediaParams()
    }
    @IBAction func buttonCancelTouchUpInside(_ sender: RoundButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- Media picker library delegate
extension NewComplainViewController: AssetsPickerViewControllerDelegate{
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        var isVideoSelected = false
        for media in selectedMedia {
            switch media.mediaType {
            case .video:
                isVideoSelected = true
                break
            default:
                break
            }
        }
        switch enumViewControllerType(rawValue: viewControllerType.rawValue) {
        case .edit:
            for i in 0..<viewModel.getOldComplainMediaCount(){
                let media = viewModel.getOldComplainMedia(at: i)
                if media.type == enumMediaType.video.rawValue{
                    isVideoSelected = true
                }
            }
            if viewModel.getOldComplainMediaCount() + selectedMedia.count > 2 {
                let alertController = UIAlertController(title: Constants.alert_title_alert, message: Constants.alert_canNotSelectMoreThanGivenLimit, preferredStyle: .alert)
                let action = UIAlertAction(title: Constants.title_ok, style: .default) { (action) in }
                alertController.addAction(action)
                controller.present(alertController, animated: true, completion: nil)
                return false
            }
            break
        case .regular:
            if selectedMedia.count > 2 {
                let alertController = UIAlertController(title: Constants.alert_title_alert, message: Constants.alert_canNotSelectMoreThanGivenLimit, preferredStyle: .alert)
                let action = UIAlertAction(title: Constants.title_ok, style: .default) { (action) in }
                alertController.addAction(action)
                controller.present(alertController, animated: true, completion: nil)
                return false
            }
            break
        default:
            break
        }
       /* //to avoid video
        if asset.mediaType == .video {
            return false
        }*/
        if asset.mediaType == .video && isVideoSelected {
            let alertController = UIAlertController(title: Constants.alert_title_alert, message: Constants.alert_canNotSelectMoreThanGivenLimitOfVideo, preferredStyle: .alert)
            let action = UIAlertAction(title: Constants.title_ok, style: .default) { (action) in }
            alertController.addAction(action)
            controller.present(alertController, animated: true, completion: nil)
            return false
        }
        return true
    }
    func assetsPicker(controller: AssetsPickerViewController, didSelect asset: PHAsset, at indexPath: IndexPath) {
        selectedMedia.append(asset)
        mediaCountThatNeedToRemove = mediaCountThatNeedToRemove + 1
    }
    func assetsPicker(controller: AssetsPickerViewController, didDeselect asset: PHAsset, at indexPath: IndexPath) {
        for i in 0..<selectedMedia.count {
            if selectedMedia[i] == asset{
                selectedMedia.remove(at: i)
                break
            }
        }
    }
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {
        selectedMedia = selectedMedia.dropLast(mediaCountThatNeedToRemove)
    }
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        //selectedMedia = assets
        mediaCountThatNeedToRemove = 0
        //setMediaParams()
        tableView.reloadData()
    }
    func assetsPickerMorePhotos(controller: AssetsPickerViewController) {
        showAlertToSelectOtherImages(VC: controller)
    }
    
}
