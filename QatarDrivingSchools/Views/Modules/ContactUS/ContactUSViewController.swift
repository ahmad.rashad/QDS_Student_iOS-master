//
//  ContactUSViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import NotificationBannerSwift
class ContactUSViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource : ContactUSDataSource?
    lazy var viewModel = ContactUSViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ContactUSDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isContactUSDataFetched.bind { isFetched in
            if isFetched{
                self.tableView.reloadData()
            }
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_contactUS.localized
        viewModel.getContactUS()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
