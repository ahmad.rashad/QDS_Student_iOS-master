//
//  ContactInfoTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit

class ContactInfoTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelContact: UILabel!
    
    //MARK:- Activity life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelEmail.font = CustomFont.semiBold()
        labelContact.font = CustomFont.semiBold()
        labelEmail.isUserInteractionEnabled = true
        labelContact.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Class methods
    func setupDetails(email: String, contact: String) {
        labelEmail.attributedText = setupAttributedText(title: Constants.description_email.localized, text: email)
        labelContact.attributedText = setupAttributedText(title: Constants.description_contact.localized, text: contact)
    }
    
    func setupAttributedText(title: String, text: String) -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString(string: title + "", attributes: [NSAttributedString.Key.font: CustomFont.medium(ofSize: 16)])
        mutableAttributedString.append(NSMutableAttributedString(string: " " + text, attributes: [NSAttributedString.Key.font: CustomFont.regular(ofSize: 16),
                                                                                                  NSAttributedString.Key.foregroundColor: Constants.color_applicationThemeColor]))
        return mutableAttributedString
    }

}
