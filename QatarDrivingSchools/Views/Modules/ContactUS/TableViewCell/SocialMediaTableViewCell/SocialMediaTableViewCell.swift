//
//  SocialMediaTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit

class SocialMediaTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var buttonInstagram: UIButton!
    @IBOutlet weak var buttonYoutube: UIButton!
    @IBOutlet weak var buttonTwitter: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    
    //MARK:- Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.semiBold()
        labelTitle.textColor = .darkGray
        labelTitle.text = Constants.description_followUsOn.localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
