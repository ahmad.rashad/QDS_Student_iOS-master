//
//  ContactUSDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import MessageUI
class ContactUSDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ContactUSViewController
    private let tableView: UITableView
    private let viewModel: ContactUSViewModel

    enum enumContactUSTableRow: Int {
        case socialMedia
        case contactInfo
        static let count: Int = {
            var max: Int = 0
            while let _ = enumContactUSTableRow(rawValue: max) { max += 1}
            return max
        }()
    }
    
    enum enumSocialMediaType: Int {
        case facebook
        case instagram
        case youtube
        case twitter
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: ContactUSViewModel, viewController: ContactUSViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        registerTableCell()
    }
    
    func registerTableCell(){
        tableView.registerNib(nibNames: [SocialMediaTableViewCell.identifier,
                                         ContactInfoTableViewCell.identifier])
        tableView.reloadData()
    }
    
    //MARK:- Action methods
    @objc func emailTapped() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setCcRecipients([viewModel.getContactEmail()])
            mail.setSubject("")
            mail.setMessageBody("", isHTML: false)
            viewController.present(mail, animated: true, completion: nil)
        }
    }
    @objc func contactTapped() {
        if let url = URL(string: "tel://\(viewModel.getContactNumber())"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @objc func buttonSocialMediaTouchUpInside(_ sender: UIButton) {
        var url = URL(string: "https://www.google.com")
        switch enumSocialMediaType(rawValue: sender.tag)! {
        case .facebook:
            url = viewModel.getFacebookURL()
        case .instagram:
            url = viewModel.getInstagramURL()
        case .youtube:
            url = viewModel.getYoutubeURL()
        case .twitter:
            url = viewModel.getTwitterURL()
        }
        if let url = url {
            UIApplication.shared.open(url)
        }
    }
    //MARK:- Table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumContactUSTableRow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumContactUSTableRow(rawValue: indexPath.row)! {
        case .socialMedia:
            let cell = tableView.dequeueReusableCell(withIdentifier: SocialMediaTableViewCell.identifier) as! SocialMediaTableViewCell
            cell.buttonFacebook.tag = enumSocialMediaType.facebook.rawValue
            cell.buttonYoutube.tag = enumSocialMediaType.youtube.rawValue
            cell.buttonInstagram.tag = enumSocialMediaType.instagram.rawValue
            cell.buttonTwitter.tag = enumSocialMediaType.twitter.rawValue
            cell.buttonFacebook.addTarget(self, action: #selector(buttonSocialMediaTouchUpInside(_:)), for: .touchUpInside)
            cell.buttonYoutube.addTarget(self, action: #selector(buttonSocialMediaTouchUpInside(_:)), for: .touchUpInside)
            cell.buttonInstagram.addTarget(self, action: #selector(buttonSocialMediaTouchUpInside(_:)), for: .touchUpInside)
            cell.buttonTwitter.addTarget(self, action: #selector(buttonSocialMediaTouchUpInside(_:)), for: .touchUpInside)
            return cell
        case .contactInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: ContactInfoTableViewCell .identifier) as! ContactInfoTableViewCell
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(emailTapped))
            let contactTap = UITapGestureRecognizer(target: self, action: #selector(contactTapped))
            cell.labelEmail.addGestureRecognizer(emailTap)
            cell.labelContact.addGestureRecognizer(contactTap)
            cell.setupDetails(email: viewModel.getContactEmail(),
                              contact: viewModel.getContactNumber())
            return cell
        }
    }
}

//MARK:- Email Composer
extension ContactUSDataSource: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


