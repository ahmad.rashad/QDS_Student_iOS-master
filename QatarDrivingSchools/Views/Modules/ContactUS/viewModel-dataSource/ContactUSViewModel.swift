//
//  ContactUSViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import UIKit
import NISLRequestPackages

class ContactUSViewModel {
    
    // MARK: - Vars & Lets
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isContactUSDataFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var contactDetails = ContactData()
    //MARK:- Dependancy Injection
    init() {
    }
    
    //MARK:- API
    func getContactUS() {
        let param: Parameters = [
            "S_Code": Constants.loggedInUser?.scode ?? "",
            "EmailId": Constants.getLoggedInUserEmailForAPI(),
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetSchoolContact, queryParameter: param) { (res: Swift.Result<BaseClass<ContactData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if let _ = data.data {
                        self.contactDetails = data.data ?? ContactData()
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    self.isContactUSDataFetched.value = true
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    self.isContactUSDataFetched.value = false
                }
            case .failure(let message):
                self.alertMessage.value = message
            }
        }
    }

    //MARK:- Class Methods
    func makeValidURLString(value: String) -> String {
        var url = value
        if !url.contains("https://") {
            url = "https://\(url)"
        }
        return url
    }
}
//MARK:- Getters and Setters
extension ContactUSViewModel {
    func getContactEmail() -> String {
        contactDetails.contact?.first?.emailid ?? ""
    }
    func getContactNumber() -> String {
        contactDetails.contact?.first?.contactNumber ?? ""
    }
    func getFacebookURL() -> URL? {
        return URL(string: makeValidURLString(value: contactDetails.contact?.first?.socialMediaLinks?.first?.facebook ?? ""))
    }
    func getInstagramURL() -> URL? {
        return URL(string: makeValidURLString(value: contactDetails.contact?.first?.socialMediaLinks?.first?.instagram ?? ""))
    }
    func getYoutubeURL() -> URL? {
        return URL(string: makeValidURLString(value: contactDetails.contact?.first?.socialMediaLinks?.first?.youtube ?? ""))
    }
    func getTwitterURL() -> URL? {
        return URL(string: makeValidURLString(value: contactDetails.contact?.first?.socialMediaLinks?.first?.twitter ?? ""))
    }
}

