//
//  AccountDetailsViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import UIKit

class AccountDetailsViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSave: UIButton!
    
    var dataSource: AccountDetailsDataSource?
    lazy var viewModel = AccountDetailsViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = AccountDetailsDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_accountDetails
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        buttonSave.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        buttonSave.setTitle(Constants.title_save.localized, for: .normal)
    }
}
