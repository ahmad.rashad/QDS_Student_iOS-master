//
//  AccountDetailsDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 07/07/21.
//

import Foundation
import UIKit

class AccountDetailsDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: AccountDetailsViewController
    private let tableView: UITableView
    private let viewModel: AccountDetailsViewModel
    
    enum enumAccountDetailsTableRow: Int {
        case username
        case password
        case language
        case space
        case otherDetails
        case registrationDate
        case lastVisitedDate
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumAccountDetailsTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: AccountDetailsViewModel, viewController: AccountDetailsViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TwoLabelTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumAccountDetailsTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumAccountDetailsTableRow(rawValue: indexPath.row)! {
        case .space:
            return 30
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumAccountDetailsTableRow(rawValue: indexPath.row)! {
        case .username:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_username,
                           placeholder: Constants.placeholder_username)
            return cell
        case .password:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_password,
                           placeholder: Constants.placeholder_password)
            return cell
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_language,
                           placeholder: Constants.title_selectLanguage,
                           rightImage: #imageLiteral(resourceName: "dropdown_ic"))
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .space:
            return UITableViewCell.defaultCell
        case .otherDetails:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.font = CustomFont.bold(ofSize: 18)
            cell.labelTitle.text = Constants.description_otherDetails
            cell.labelTitle.textColor = .black
            cell.imageViewUnderline.isHidden = true
            return cell
        case .registrationDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: TwoLabelTableViewCell.identifier) as! TwoLabelTableViewCell
            cell.setupData(label1: Constants.description_registrationDate, label2: "15 October 2020")
            return cell
        case .lastVisitedDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: TwoLabelTableViewCell.identifier) as! TwoLabelTableViewCell
            cell.setupData(label1: Constants.description_lastVisitDate, label2: "15 November 2020")
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumAccountDetailsTableRow(rawValue: indexPath.row)! {
        case .language:
            let searchListVC = SearchListViewController.loadFromNib()
            searchListVC.viewModel.setTitle(value: Constants.title_selectLanguage)
            searchListVC.modalPresentationStyle = .overFullScreen
            self.viewController.present(searchListVC, animated: true, completion: nil)
        default:
            break
        }
    }
}
//MARK:- TextField Methods
extension AccountDetailsDataSource: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch enumAccountDetailsTableRow(rawValue: textField.tag)! {
        case .language:
            let searchListVC = SearchViewController.loadFromNib()
            self.viewController.present(searchListVC, animated: true, completion: nil)
        default:
            break
        }
    }
}
