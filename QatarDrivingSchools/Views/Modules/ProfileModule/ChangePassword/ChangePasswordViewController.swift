//
//  ChangePasswordViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 16/07/21.
//

import UIKit
import NotificationBannerSwift
class ChangePasswordViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSave: UIButton!
    
    var dataSource: ChangePasswordDataSource?
    lazy var viewModel = ChangePasswordViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ChangePasswordDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        
        setupLayout()
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonChangePasswordTouchUpInside(_ sender: UIButton) {
        view.endEditing(true)
        viewModel.changePassword()
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            if message.body == Constants.alert_passwordIsNotValid {
                self.showAlertWith(message: message)
            }else{
                let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
                banner.show()
            }
        }
        viewModel.isPasswordUpdated.bind { isSuccess in
            if isSuccess {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.description_changePassword.localized
        
        buttonSave.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        buttonSave.setTitle(Constants.title_saveInCaps.localized, for: .normal)
        
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
