//
//  ChangePasswordViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 16/07/21.
//

import UIKit
import NISLRequestPackages

class ChangePasswordViewModel {
    
    //MARK:- Variables
    private var currentPassword = ""
    private var newPassword = ""
    private var confirmPassword = ""
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isPasswordUpdated: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    
    //MARK:- Dependancy Injection
    init() {
        isLoaderHidden.value = false
    }

    //MARK:- API
    func changePassword() {
        if currentPassword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_oldPasswordMissing.localized)
        }
        else if newPassword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_newPasswordMissing.localized)
        }
        else if !newPassword.isPasswordValid() {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_passwordIsNotValid)
        }
        else if confirmPassword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_confirmPasswordMissing.localized)
        }
        else if newPassword != confirmPassword {
            self.alertMessage.value = AlertMessage(title: Constants.alert_title_error, body: Constants.alert_passwordDoesNotMatch.localized)
        }
        else {
            let param: Parameters = [
                "oldPassword":currentPassword,
                "newPassword":newPassword,
                "emailId":Constants.getLoggedInUserEmailForAPI()
            ]
            isLoaderHidden.value = false
            apiManager.call(type: RequestItemsType.ChangePassword, params: param) { (res: Swift.Result<BaseClass<String>, AlertMessage>, response, data)  in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                        self.isPasswordUpdated.value = true
                    }else{
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    break
                case .failure(let message):
                    self.alertMessage.value = message
                    break
                }
            }
        }
    }
}
//MARK:- Getters and Setters
extension ChangePasswordViewModel{
    func getCurrentPassword() -> String{
        return currentPassword
    }
    func getNewPassword() -> String{
        return newPassword
    }
    func getConfirmPassword() -> String{
        return confirmPassword
    }
    
    func setCurrentPassword(value: String){
        currentPassword = value
    }
    func setNewPassword(value: String){
        newPassword = value
    }
    func setConfirmPassword(value: String){
        confirmPassword = value
    }
}

