//
//  ChangePasswordDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 16/07/21.
//

import Foundation
import UIKit

class ChangePasswordDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ChangePasswordViewController
    private let tableView: UITableView
    private let viewModel: ChangePasswordViewModel
    
    enum enumChangePasswordTableRow: Int {
        case oldPassword
        case newPassword
        case newConfirmPassword
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumChangePasswordTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: ChangePasswordViewModel, viewController: ChangePasswordViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TwoLabelTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumChangePasswordTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumChangePasswordTableRow(rawValue: indexPath.row)! {
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
        switch enumChangePasswordTableRow(rawValue: indexPath.row)! {
        case .oldPassword:
            cell.textField.text = viewModel.getCurrentPassword()
            cell.setupData(title: Constants.placeholder_oldPassword.localized,
                           placeholder: Constants.placeholder_oldPassword.localized)
        case .newPassword:
            cell.textField.text = viewModel.getNewPassword()
            cell.setupData(title: Constants.placeholder_newPassword.localized,
                           placeholder: Constants.placeholder_newPassword.localized)
        case .newConfirmPassword:
            cell.textField.text = viewModel.getConfirmPassword()
            cell.setupData(title: Constants.placeholder_confirmPassword.localized,
                           placeholder: Constants.placeholder_confirmPassword.localized)
        }
        cell.textField.isSecureTextEntry = true
        cell.textField.tag = indexPath.row
        cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
        return cell
    }

}
//MARK:- TextField Methods
extension ChangePasswordDataSource: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumChangePasswordTableRow(rawValue: textField.tag) {
        case .oldPassword:
            viewModel.setCurrentPassword(value: textField.text ?? "")
            break
        case .newPassword:
            viewModel.setNewPassword(value: textField.text ?? "")
            break
        case .newConfirmPassword:
            viewModel.setConfirmPassword(value: textField.text ?? "")
            break
        default:
            break
        }
    }
}
