//
//  UserUserProfileViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit
import NotificationBannerSwift
class UserProfileViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonSave: ApplicationThemeButton!
    
    enum enumIDMediaType: Int {
        case front
        case back
    }
    
    var dataSource : UserProfileDataSource?
    lazy var viewModel = UserProfileViewModel()
    private var imagePicker = UIImagePickerController()
    var pickerTag = enumIDMediaType.front
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = UserProfileDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func buttonSaveTouchUpInside(_ sender: UIButton) {
        viewModel.updateUserProfile()
    }
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isPopRequired.bind { _ in
            self.navigationController?.popViewController(animated: true)
        }
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_userProfile.localized
        
        buttonSave.titleLabel?.font = CustomFont.semiBold(ofSize: 18)
        buttonSave.setTitle(Constants.title_saveInCaps.localized, for: .normal)
        buttonSave.addTarget(self, action: #selector(buttonSaveTouchUpInside(_:)), for: .touchUpInside)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func opneMediaPicker() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        }
    }
}
//MARK:- Image picker delegate
extension UserProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            switch pickerTag {
            case .front:
                viewModel.setFrontImage(value: image)
            case .back:
                viewModel.setBackImage(value: image)
            }
            self.tableView.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
}
