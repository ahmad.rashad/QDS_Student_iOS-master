//
//  GenderTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit

class GenderTableViewCell: UITableViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonMale: UIButton!
    @IBOutlet weak var buttonFemale: UIButton!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    var completionHandlerForGender: ((_ gender: Bool)->Void)?
    
    enum enumGender:Int {
        case male = 0
        case female
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelTitle.font = CustomFont.regular(ofSize: 12)
        buttonMale.titleLabel?.font = CustomFont.regular(ofSize: 14)
        buttonFemale.titleLabel?.font = CustomFont.regular(ofSize: 14)
        
        labelTitle.text = Constants.description_gender.localized
        buttonMale.setTitle(Constants.title_male.localized, for: .normal)
        buttonFemale.setTitle(Constants.title_female.localized, for: .normal)
        
        buttonMale.tag = enumGender.male.rawValue
        buttonFemale.tag = enumGender.female.rawValue
        
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- Class methods
    func setSelectedGender(gender: Bool) {
        if gender {
            buttonMale.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            buttonFemale.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            completionHandlerForGender?(true)
        }else{
            buttonMale.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            buttonFemale.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            completionHandlerForGender?(false)
        }
    }
    //MARK:- Action methods
    
    @IBAction func buttonGenderTouchUpInside(_ sender: UIButton) {
        switch enumGender(rawValue: sender.tag)! {
        case .male:
            buttonMale.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            buttonFemale.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            completionHandlerForGender?(true)
        case .female:
            buttonMale.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            buttonFemale.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            completionHandlerForGender?(false)
        }
    }
    
}
