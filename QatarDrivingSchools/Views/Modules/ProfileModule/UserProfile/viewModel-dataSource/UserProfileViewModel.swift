//
//  UserUserProfileViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//


import UIKit
import NISLRequestPackages

class UserProfileViewModel {
    
    // MARK: - Vars & Lets
    private struct UserDetails {
        var fullNameInEnglish: String = Constants.loggedInUser?.fullNameEnglish ?? ""
        var fullNameInArabic: String = Constants.loggedInUser?.fullNameArabic ?? ""
        var dob: Date? = (Constants.loggedInUser?.dateOfBirth ?? "").toDate()
        var gender: Bool? = Constants.loggedInUser?.gender
        var frontImage: UIImage?
        var backImage: UIImage?
    }
    private var userDetails = UserDetails()
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isPopRequired: Dynamic<Bool> = Dynamic(true)
    
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Dependancy Injection
    init() {
        isLoaderHidden.value = false
    }
    
    //MARK:- API
    func updateUserProfile() {
        if userDetails.fullNameInEnglish.isEmpty {
            alertMessage.value = AlertMessage(title: "", body: Constants.alert_fullNameEnglishMissing)
        }else{
            var param: Parameters = [
                "userId": Constants.loggedInUser?.id ?? 0,
                "emailId": Constants.getLoggedInUserEmailForAPI(),
                "fullNameEnglish": userDetails.fullNameInEnglish,
                "fullNameArabic": userDetails.fullNameInArabic,
                "gender":userDetails.gender ?? true,
                "dateOfBirth":userDetails.dob?.toString() ?? "",
            ]
            if let image = userDetails.frontImage {
                param["front_image"] = image
            }
            if let image = userDetails.backImage {
                param["back_image"] = image
            }
            
            isLoaderHidden.value = false
            apiManager.upload(type: RequestItemsType.UpdateUserProfile, params: param) { (res: Swift.Result<BaseClass<UserData>, AlertMessage>, response) in
                self.isLoaderHidden.value = true
                switch res {
                case .success(let data):
                    if data.status == Constants.api_success{
                        if let user = data.data {
                            if let tmpUser = user.user?.first{
                                Constants.loggedInUser = tmpUser
                                self.isPopRequired.value = true
                            }
                        } else {
                            self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                        }
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                    break
                case .failure(let message):
                    self.alertMessage.value = message
                    break
                }
            }
        }
    }


    
    //MARK:- Class Methods

}
//MARK:- Getters and Setters
extension UserProfileViewModel {
    func getFullNameInEnglish() -> String {
        userDetails.fullNameInEnglish
    }
    func getFullNameInArabic() -> String {
        userDetails.fullNameInArabic
    }
    func getDob() -> String {
        userDetails.dob?.toString(format: "dd/MM/yyyy") ?? ""
    }
    func getGender() -> Bool {
        userDetails.gender ?? true
    }
    func getFrontImage() -> UIImage? {
        userDetails.frontImage
    }
    func getBackImage() -> UIImage? {
        userDetails.backImage
    }
    
    func setFullNameInEnglish(value: String) {
        userDetails.fullNameInEnglish = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func setFullNameInArabic(value: String) {
        userDetails.fullNameInArabic = value.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func setDob(value: Date) {
        userDetails.dob = value
    }
    func setGender(value: Bool) {
        userDetails.gender = value
    }
    func setFrontImage(value: UIImage) {
        userDetails.frontImage = value
    }
    func setBackImage(value: UIImage) {
        userDetails.backImage = value
    }
}
