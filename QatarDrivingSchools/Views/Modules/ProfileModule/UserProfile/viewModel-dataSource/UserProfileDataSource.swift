//
//  UserUserProfileDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit

class UserProfileDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: UserProfileViewController
    private let tableView: UITableView
    private let viewModel: UserProfileViewModel
        
    let datePicker = UIDatePicker()
    
    enum enumUserProfileTableRow: Int {
        case id = 0
        case nameInEnglish
        case nameInArabic
        case dob
        case gender
        /*case governmentWork
        case frontIDPhoto
        case backIDPhoto*/
        static let count: Int = {
            var max: Int = 0
            while let _ = enumUserProfileTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: UserProfileViewModel, viewController: UserProfileViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
        setupDatePicker()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        registerTableCell()
    }
    func setupDatePicker() {
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
        if #available(iOS 14, *) {
          datePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    func registerTableCell(){
        tableView.registerNib(nibNames: [TextFieldTableViewCell.identifier,
                                         TwoLabelTableViewCell.identifier,
                                         IDPhotoTableViewCell.identifier,
                                         GenderTableViewCell.identifier,
                                         ButtonTableViewCell.identifier])
        tableView.reloadData()
    }
    
    //MARK:- Action methods
    @objc func datePickerFinishEditing(_ sender: UIBarButtonItem) {
        viewController.view.endEditing(true)
        viewModel.setDob(value: datePicker.date)
        tableView.reloadData()
    }
  
    //MARK:- Table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumUserProfileTableRow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumUserProfileTableRow(rawValue: indexPath.row)! {
        case .id:
            let cell = tableView.dequeueReusableCell(withIdentifier: TwoLabelTableViewCell.identifier, for: indexPath) as! TwoLabelTableViewCell
            cell.label1.text = Constants.placeholder_personalQatariID.localized
            cell.label2.text = Constants.loggedInUser?.personalQatariID ?? ""
            return cell
        case .nameInEnglish:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier, for: indexPath) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_fullNameInEnglish.localized,
                           text: viewModel.getFullNameInEnglish(),
                           placeholder: Constants.placeholder_fullNameInEnglish.localized)
            cell.textField.tag = indexPath.row
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .nameInArabic:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier, for: indexPath) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_fullNameInArabic.localized,
                           text: viewModel.getFullNameInArabic(),
                           placeholder: Constants.placeholder_fullNameInArabic.localized)
            cell.textField.tag = indexPath.row
            cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .allEditingEvents)
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .dob:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier, for: indexPath) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_dob.localized,
                           text: viewModel.getDob(),
                           placeholder: Constants.placeholder_date.localized,
                           rightImage: #imageLiteral(resourceName: "calendar_ic"))
            cell.textField.inputView = datePicker
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: Constants.title_done, style: .done, target: self, action: #selector(datePickerFinishEditing(_:)))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            toolbar.setItems([spaceButton,doneButton], animated: false)
            cell.textField.inputAccessoryView = toolbar
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .gender:
        let cell = tableView.dequeueReusableCell(withIdentifier: GenderTableViewCell.identifier, for: indexPath) as! GenderTableViewCell
            cell.setSelectedGender(gender: viewModel.getGender())
            cell.completionHandlerForGender = { gender in
                self.viewModel.setGender(value: gender)
            }
            cell.isUserInteractionEnabled = false
            return cell
       /* case .governmentWork:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier, for: indexPath) as! TextFieldTableViewCell
            cell.setupData(title: Constants.placeholder_governmentWork,
                           placeholder: Constants.placeholder_governmentWork,
                           rightImage: #imageLiteral(resourceName: "dropdown_ic"))
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .frontIDPhoto:
            let cell = tableView.dequeueReusableCell(withIdentifier: IDPhotoTableViewCell.identifier, for: indexPath) as! IDPhotoTableViewCell
            cell.labelTitle.text = Constants.description_frontIdPhoto
            if let image = viewModel.getFrontImage() {
                cell.imgView?.image = image
            }else{
                cell.imgView?.sd_setImage(with: Constants.getPathOfMedia(value: Constants.loggedInUser?.idPhotoFront ?? ""), completed: nil)
            }
            return cell
        case .backIDPhoto:
            let cell = tableView.dequeueReusableCell(withIdentifier: IDPhotoTableViewCell.identifier, for: indexPath) as! IDPhotoTableViewCell
            cell.labelTitle.text = Constants.description_backIdPhoto
            if let image = viewModel.getBackImage() {
                cell.imgView?.image = image
            }else{
                cell.imgView?.sd_setImage(with: Constants.getPathOfMedia(value: Constants.loggedInUser?.idPhotoBack ?? ""), completed: nil)
            }
            return cell*/
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*switch enumUserProfileTableRow(rawValue: indexPath.row)! {
        case .governmentWork:
            let filterVC = FilterViewController.loadFromNib()
            filterVC.viewControllerType = .government
            filterVC.viewModel.setTitle(value: Constants.placeholder_governmentWork)
            filterVC.modalPresentationStyle = .overFullScreen
            viewController.present(filterVC, animated: true, completion: nil)
        case .frontIDPhoto:
            viewController.pickerTag = .front
            viewController.opneMediaPicker()
        case .backIDPhoto:
            viewController.pickerTag = .back
            viewController.opneMediaPicker()
        default:
            break
        }*/
    }
}

//MARK:- TextField methods
extension UserProfileDataSource {
    @objc func textFieldDidChange(_ textField: UITextField){
        switch enumUserProfileTableRow(rawValue: textField.tag) {
        case .nameInEnglish:
            viewModel.setFullNameInEnglish(value: textField.text ?? "")
        case .nameInArabic:
            viewModel.setFullNameInArabic(value: textField.text ?? "")
        default:
            break
        }
    }
}
