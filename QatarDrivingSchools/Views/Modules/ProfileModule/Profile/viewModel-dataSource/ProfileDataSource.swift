//
//  ProfileDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class ProfileDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: ProfileViewController
    private let tableView: UITableView
    private let viewModel: ProfileViewModel
    
    private let icons = [#imageLiteral(resourceName: "profile_uesr_profile_ic"),#imageLiteral(resourceName: "profile_account_details"),#imageLiteral(resourceName: "profile_contact_info_ic")]
    
    enum enumProfileTableRow: Int {
        case profile = 0
        case userProfile
        case changePassword
        case contactInfo
        case languageSettings
        case otherDetails
        case registrationDate
        //case lastVisitedDate
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumProfileTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: ProfileViewModel, viewController: ProfileViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        registerTableCell()
    }
    
    func registerTableCell(){
        tableView.registerNib(nibNames: [ProfileOverViewTableViewCell.identifier,
                                         LabelWithTitleTableViewCell.identifier,
                                         LanguageSettingsTableViewCell.identifier,
                                         LabelTableViewCell.identifier,
                                         TwoLabelTableViewCell.identifier,
                                         TextFieldTableViewCell.identifier])
        tableView.reloadData()
    }
    func showLanguageSelections(sender: UIView) {
        let optionMenu = UIAlertController(title: nil, message: Constants.title_selectLanguage, preferredStyle: .actionSheet)
        for i in 0..<viewModel.getLanguagesCount() {
            if Language(rawValue: self.viewModel.getLanguage(at: i)) != .DEFAULT_LANGUAGE {
                optionMenu.addAction(UIAlertAction(title: (Language(rawValue: self.viewModel.getLanguage(at: i)) ?? .ENGLISH).description, style: .default, handler: { action in
                    Language.language = Language(rawValue: self.viewModel.getLanguage(at: i)) ?? .ENGLISH
                    self.tableView.reloadData()
                }))
            }
    
        }
        
        optionMenu.addAction(UIAlertAction(title: Constants.title_cancel.localized, style: .cancel, handler: { action in
            
        }))
        optionMenu.view.tintColor = Constants.color_applicationThemeColor
        if UIDevice.current.userInterfaceIdiom == .pad {
            optionMenu.popoverPresentationController?.sourceView = viewController.view
            optionMenu.popoverPresentationController?.sourceRect = sender.frame
        }
        viewController.present(optionMenu, animated: true, completion: nil)
    }
    //MARK:- Action methods
    
    //MARK:- Table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumProfileTableRow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         switch enumProfileTableRow(rawValue: indexPath.row) {
         case .profile, .languageSettings:
            return UITableView.automaticDimension
         case .otherDetails,.registrationDate:
            return UITableView.automaticDimension
         default:
            return 60
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumProfileTableRow(rawValue: indexPath.row)! {
        case .profile:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileOverViewTableViewCell.identifier) as! ProfileOverViewTableViewCell
            cell.setupData(username: viewModel.getUserName(), email: viewModel.getEmail(), profileURL: viewModel.getUserProfileURL())
            return cell
        case .userProfile:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelWithTitleTableViewCell.identifier) as! LabelWithTitleTableViewCell
            cell.setupDate(image: icons[0], title: Constants.description_userProfile.localized)
            cell.setTopCorner()
            return cell
        case .changePassword:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelWithTitleTableViewCell.identifier) as! LabelWithTitleTableViewCell
            cell.setupDate(image: icons[1], title: Constants.description_changePassword.localized)
            return cell
        case .contactInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelWithTitleTableViewCell.identifier) as! LabelWithTitleTableViewCell
            cell.setupDate(image: icons[2], title: Constants.description_contactInfo.localized)
            return cell
        case .languageSettings:
            /*let cell = tableView.dequeueReusableCell(withIdentifier: LanguageSettingsTableViewCell.identifier) as! LanguageSettingsTableViewCell
            cell.languageChanged = { language in
               Language.language = language
            }
            return cell*/
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setupData(title: Constants.title_selectLanguage, placeholder: Constants.placeholder_password.localized)
            cell.dropDownIcon.isHidden = false
            cell.textField.text = Language.language.description
            cell.textField.isUserInteractionEnabled = false
            return cell
        case .otherDetails:
            let cell = tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
            cell.labelTitle.font = CustomFont.bold(ofSize: 18)
            cell.labelTitle.text = Constants.description_otherDetails.localized
            cell.labelTitle.textColor = .black
            cell.imageViewUnderline.isHidden = true
            return cell
        case .registrationDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: TwoLabelTableViewCell.identifier) as! TwoLabelTableViewCell
            cell.setupData(label1: Constants.description_registrationDate.localized, label2: Constants.loggedInUser?.createdDate?.toLocalDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "dd MMMM yyyy") ?? "")
            return cell
        //case .lastVisitedDate:
            //let cell = tableView.dequeueReusableCell(withIdentifier: TwoLabelTableViewCell.identifier) as! TwoLabelTableViewCell
            //cell.setupData(label1: Constants.description_lastVisitDate.localized, label2: Constants.loggedInUser?.modifiedDate?.toLocalDate(format: "yyyy-MM-dd HH:mm:ss")?.toString(format: "dd MMMM yyyy") ?? "")
           // return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch enumProfileTableRow(rawValue: indexPath.row)! {
        case .profile:
            break
       /* case .account:
            viewController.navigationController?.pushViewController(AccountDetailsViewController.loadFromNib(), animated: true)*/
        case .userProfile:
            viewController.navigationController?.pushViewController(UserProfileViewController.loadFromNib(), animated: true)
        case .contactInfo:
            viewController.navigationController?.pushViewController(ContactInfoViewController.loadFromNib(), animated: true)
        case .languageSettings:
            showLanguageSelections(sender: tableView.cellForRow(at: indexPath) ?? UIView())
        case .changePassword:
            viewController.navigationController?.pushViewController(ChangePasswordViewController.loadFromNib(), animated: true)
        default:
            break
        }
    }
}
