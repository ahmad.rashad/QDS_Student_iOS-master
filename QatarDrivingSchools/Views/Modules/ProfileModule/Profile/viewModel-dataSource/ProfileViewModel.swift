//
//  ProfileViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit
import NISLRequestPackages

class ProfileViewModel: UserViewModel {

    override init() {
        super.init()
    }
}
extension ProfileViewModel {
    func getUserName() -> String {
        Constants.loggedInUser?.fullNameEnglish ?? ""
    }
    func getEmail() -> String {
        Constants.loggedInUser?.emailId ?? ""
    }
    func getUserProfileURL() -> URL? {
        Constants.getPathOfMedia(value: Constants.loggedInUser?.profileImageName ?? "")
    }
}
