//
//  ProfileOverViewTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class ProfileOverViewTableViewCell: UITableViewCell {

    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var imageViewProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelEmail.font = CustomFont.semiBold()
        labelUserName.font = CustomFont.bold(ofSize: 20)
        labelEmail.textColor = .white
        labelUserName.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(username: String, email: String, profileURL: URL?) {
        labelEmail.text = email
        labelUserName.text = username
        imageViewProfile.sd_setImage(with: profileURL, placeholderImage: #imageLiteral(resourceName: "profile_placeholder1"), options: [], completed: nil)
    }
}
