//
//  LabelWithTitleTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class LabelWithTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    @IBOutlet weak var bgView: DesignableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        bgView.cornerRadius = 0
        labelTitle.font = CustomFont.regular()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:- Class methods
    func setupDate(image: UIImage, title: String){
        imageViewIcon.image = image
        labelTitle.text = title
    }
    func setTopCorner() {
        bgView.cornerRadius = 20
    }
}
