//
//  LanguageSettingsTableViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import UIKit

class LanguageSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonArabic: RoundButton!
    @IBOutlet weak var buttonEnglish: RoundButton!
    @IBOutlet weak var labelSettings: UILabel!
    
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    
    var languageChanged: ((_ language: Language)->Void)?
    var selectedLanguage = 0  // Tag 0 - English , 1 - Arabic
    
    enum enumLanguage: Int {
        case english = 0
        case arabic
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clearCell()
        labelSettings.text = Constants.description_languageSettings.localized
        labelSettings.font = CustomFont.bold(ofSize: 20)
        
        buttonEnglish.titleLabel?.font = CustomFont.regular()
        buttonArabic.titleLabel?.font = CustomFont.regular()
        
        buttonEnglish.setTitle(Constants.title_english, for: .normal)
        buttonArabic.setTitle(Constants.title_arabic, for: .normal)
        
        switch Language.language {
        case .ARABIC:
            selectedLanguage = 1
        default:
            selectedLanguage = 0
        }
        buttonEnglish.borderWidth = selectedLanguage == 0 ? 1 : 0
        buttonArabic.borderWidth = selectedLanguage == 1 ? 1 : 0
        
        if IS_IPAD {
            leading.priority = UILayoutPriority(1)
            trailing.priority = UILayoutPriority(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonChangeLanguageTouchUpInside(_ sender: RoundButton) {
        // Tag 0 - English , 1 - Arabic
        let language = enumLanguage.english.rawValue == sender.tag ? Constants.title_english : Constants.title_arabic
        let alertVC = UIAlertController(title: "", message: Constants.getLanguageChangeAlert(language: language), preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.title_yes, style:.destructive) { action in
            self.buttonEnglish.borderWidth = sender.tag == 0 ? 1 : 0
            self.buttonArabic.borderWidth = sender.tag == 1 ? 1 : 0
            if enumLanguage.english.rawValue == sender.tag {
                self.languageChanged?(Language.ENGLISH)
            }else{
                self.languageChanged?(Language.ARABIC)
            }
        }
        alertVC.addAction(action)
        let actionNo = UIAlertAction(title: Constants.title_no, style:.default) { action in
            
        }
        alertVC.addAction(actionNo)
        GlobalVariables.appDelegate?.window?.rootViewController?.present(alertVC, animated: true, completion: nil)
    }
    
}
