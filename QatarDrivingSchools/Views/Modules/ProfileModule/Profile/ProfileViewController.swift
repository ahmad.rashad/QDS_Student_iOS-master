//
//  ProfileViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//


import UIKit
import NotificationBannerSwift
class ProfileViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var dataSource : ProfileDataSource?
    lazy var viewModel = ProfileViewModel()
    
    //MARK:- Activity life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataSource = ProfileDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getUserDetails()
    }
    
    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    //MARK:- Class Methods
    func setupLayout() {
        viewModel.alertMessage.bind { (message) in
            let banner = NotificationBanner(title: message.body, style: .warning, colors: CustomBannerColors())
            banner.show()
        }
        viewModel.isUserDetailsFetched.bind { isFetched in
            if isFetched {
                self.tableView.reloadData()
            }
        }
        labelTitle.font = CustomFont.medium(ofSize: 18)
        labelTitle.text = Constants.title_profile.localized
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
