//
//  EBookViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 06/07/21.
//

import UIKit
import NISLRequestPackages

class EBookViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isEBookFetched: Dynamic<Bool> = Dynamic(true)
    var noEbookMessage: Dynamic<String> = Dynamic("")
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
        
    private var eBookList: [EBooks] = []
    private var isMoreDataAvailable = true
    private var offset = 0
    
    //MARK:- Dependancy Injection
    init() {
        
    }
    
    //MARK: - APIs
    func getEBooks() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":offset,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetAllEBooks, queryParameter: param) { (res: Swift.Result<BaseClass<EBooksBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.offset += 1
                    self.eBookList.append(contentsOf: data.data?.eBooks ?? [])
                    if self.eBookList.count == 0 {
                        self.noEbookMessage.value = Constants.description_noEbook
                        self.isMoreDataAvailable = false
                    }
                } else {
                    self.isMoreDataAvailable = false
                    self.noEbookMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isEBookFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isEBookFetched.value = false
            }
        }
    }
}
//MARK:- Getters and Setters
extension EBookViewModel {
    func getTotalNumberOfEBooks() -> Int {
        eBookList.count
    }
    func getEBook(at index: Int) -> EBooks {
        eBookList[index]
    }
    func isMoreDataAvailableInEBook() -> Bool {
        isMoreDataAvailable
    }
}

