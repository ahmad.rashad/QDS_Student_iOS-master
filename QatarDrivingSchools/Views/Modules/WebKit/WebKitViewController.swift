//
//  WebKitViewController.swift
//  Bubbles
//
//  Created by C100-107 on 8/26/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import WebKit
import PDFKit
class WebKitViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var buttonDone: RoundButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewNavigation: UIView!
    
    var url: URL?
    var titleString: String = ""
    var pdfView : PDFView?
    
    //MARK:- Activity life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.font = CustomFont.medium()
        labelTitle.text = titleString
        if url != nil {
            webView.load(URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0))
            //webView.load(URLRequest(url: url!))
        }
        buttonDone.titleLabel?.font = CustomFont.semiBold()
        buttonDone.setTitle(Constants.title_close.localized, for: .normal)
    }
    
    //MARK:- Class methods
    func setupPDF() {
        self.pdfView = PDFView()
        
        let documentToShow = PDFDocument(url: url!)
        
        self.pdfView!.document = documentToShow!
        //self.pdfView!.autoScales = true
        self.pdfView!.displayMode = .singlePageContinuous
        self.pdfView!.displayDirection = .vertical
        self.pdfView!.autoScales = true
        self.pdfView!.enableDataDetectors = true
        
        let width = self.view.frame.width
        let height = self.view.frame.height - 50
        self.pdfView!.frame = CGRect(x: 0, y: 0, width: width, height: height - 30)
        self.pdfView!.backgroundColor = .black
        self.view.addSubview(self.pdfView!)
        self.view.bringSubviewToFront(buttonDone)
    }
    func addBlurView() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = viewNavigation.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewNavigation.addSubview(blurEffectView)
        viewNavigation.sendSubviewToBack(blurEffectView)
    }
    
    //MARK:- Action methods
    @IBAction func buttonCloseTouchUpInside(_ sender: RoundButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
