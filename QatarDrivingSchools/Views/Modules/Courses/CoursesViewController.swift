//
//  CoursesViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class CoursesViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeader: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var imageViewPlaceHolder: UIImageView!
    
    var dataSource: CoursesDataSource?
    lazy var viewModel = CoursesViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.text = Constants.title_courses.localized
        labelTitle.font = CustomFont.semiBold()
        labelNoData.font = CustomFont.semiBold()
        
        dataSource = CoursesDataSource(collectionView: collectionView, collectionViewHeader: collectionViewHeader, viewModel: viewModel, viewController: self)
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
        collectionViewHeader.dataSource = dataSource
        collectionViewHeader.delegate = dataSource
        
        viewModel.noCourseMessage.bind { message in
           // self.labelNoData.text = message
            self.imageViewPlaceHolder.isHidden = message.isEmpty
        }
        viewModel.isCourseDataFetched.bind { isFetched in
            if isFetched {
                self.collectionView.reloadData()
            }
        }
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.activityIndicator.stopAnimating()
            }else{
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.getCourses()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonMenuTouchUpInside(_ sender: UIButton) {
        SideMenu.shared.showSideMenu(sender: sender)
    }
    @IBAction func buttonSearchTouchUpInside(_ sender: UIButton) {
        let questionVC = QuestionsViewController.loadFromNib()
        navigationController?.pushViewController(questionVC, animated: true)
       // self.navigationController?.pushViewController(SearchViewController.loadFromNib(), animated: true)
    }
}
