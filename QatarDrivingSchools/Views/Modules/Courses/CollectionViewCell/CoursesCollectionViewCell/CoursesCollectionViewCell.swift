//
//  CoursesCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class CoursesCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var progressView: CircularProgressView!
    @IBOutlet weak var labelPercentage: UILabel!
    @IBOutlet weak var buttonArrow: UIButton!
    @IBOutlet weak var labelEBooKTitle: UILabel!
    @IBOutlet weak var viewEbook: UIView!
    @IBOutlet weak var viewCourse: UIView!
    
    //MARK:- Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progressView.trackClr = UIColor(red: 198/255, green: 199/255, blue: 203/255, alpha:0.6)
        progressView.progressClr = Constants.color_applicationThemeColor //UIColor(red: 233/255, green: 172/255, blue: 0/255, alpha: 1.0)
        
        labelTitle.font = CustomFont.medium(ofSize: 20)
        labelPercentage.font = CustomFont.semiBold(ofSize: 10)
        labelEBooKTitle.font = CustomFont.semiBold(ofSize: 13)
        
        labelPercentage.textColor = Constants.color_darkApplicationThemeColor
        labelTitle.textColor = Constants.color_darkApplicationThemeColor
        
        progressView.isHidden = true
        buttonArrow.isHidden = false
        labelEBooKTitle.isHidden = true
        viewEbook.isHidden = true
        buttonArrow.isUserInteractionEnabled = false
    }

    //MARK:- Class methods
    func setProgressView(with percentage: Float){
        progressView.isHidden = false
        buttonArrow.isHidden = true
        progressView.setProgressWithAnimation(duration: 1.0, value: Float(Double(percentage)/100.0))
        labelPercentage.text = "\(percentage)%"
    }
    func setArrow(){
        progressView.isHidden = true
        buttonArrow.isHidden = false
        labelPercentage.text = ""
    }
    func setupUIForEBook(title: String){
        viewCourse.isHidden = true
        labelEBooKTitle.isHidden = false
        viewEbook.isHidden = false
        labelEBooKTitle.text = title
    }
    func setupUIForInvoice(title: String){
        viewCourse.isHidden = true
        labelEBooKTitle.isHidden = false
        viewEbook.isHidden = false
        labelEBooKTitle.text = title
    }
}
