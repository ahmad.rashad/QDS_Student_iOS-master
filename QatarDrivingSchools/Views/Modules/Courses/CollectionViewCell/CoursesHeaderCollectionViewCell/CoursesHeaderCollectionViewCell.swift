//
//  CoursesHeaderCollectionViewCell.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

class CoursesHeaderCollectionViewCell: UICollectionViewCell {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewBottom: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle.font = CustomFont.semiBold(ofSize: 12)
    }

}
