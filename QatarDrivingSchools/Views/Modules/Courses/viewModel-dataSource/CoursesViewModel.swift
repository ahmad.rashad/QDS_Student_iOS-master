//
//  CoursesViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit
import NISLRequestPackages

class CoursesViewModel {
    
    //MARK:- Variables
    private var selectedCategory = 0
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isCourseDataFetched: Dynamic<Bool> = Dynamic(true)
    var noCourseMessage: Dynamic<String> = Dynamic("")
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var coursesList: [Courses] = []
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK: - API
    func getCourses() {
        let param: Parameters = [
            "S_Code":Constants.loggedInUser?.scode ?? "",
            "PageOffset":0,
            "EmailId":Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetCourses, queryParameter: param) { (res: Swift.Result<BaseClass<CoursesBaseClass>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    self.coursesList.append(contentsOf: data.data?.courses ?? [])
                } else {
                    self.noCourseMessage.value = data.message ?? ""
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isCourseDataFetched.value = true
            case .failure(let message):
                self.isLoaderHidden.value = true
                self.noCourseMessage.value = message.body
                self.alertMessage.value = message
                self.isCourseDataFetched.value = false
            }
        }
    }
}
//MARK:- Getters and Setters
extension CoursesViewModel {
    func getSelectedCategory() -> Int {
        selectedCategory
    }
    func setSelectedCategory(value: Int) {
        selectedCategory = value
    }
    func getTotalNumberOfCourses() -> Int {
        coursesList.count
    }
    func getCourse(at index: Int) -> Courses {
        coursesList[index]
    }
}
