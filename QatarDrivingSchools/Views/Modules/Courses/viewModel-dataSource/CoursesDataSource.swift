//
//  CoursesDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import Foundation
import UIKit

class CoursesDataSource : NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let viewController: CoursesViewController
    private let collectionView: UICollectionView
    private let collectionViewHeader: UICollectionView
    private let viewModel: CoursesViewModel
    
    enum enumCollectionType: Int {
        case header
        case data
    }
    
    enum enumCoursesType: Int {
        case all
        case inProgress
        case saved
        case skills
        static let count: Int = {
            var max: Int = 0
            while let _ = enumCoursesType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(collectionView: UICollectionView,collectionViewHeader: UICollectionView, viewModel: CoursesViewModel, viewController: CoursesViewController) {
        self.viewController = viewController
        self.collectionView = collectionView
        self.collectionViewHeader = collectionViewHeader
        self.viewModel = viewModel
        super.init()
        setupCollectionView()
    }
    
    //MARK: - Class methods
    func setupCollectionView(){
        collectionView.tag = enumCollectionType.data.rawValue
        collectionViewHeader.tag = enumCollectionType.header.rawValue
        registerCollectionCell()
    }
    func registerCollectionCell(){
        collectionViewHeader.registerNib(nibNames: [CoursesHeaderCollectionViewCell.identifier])
        collectionView.registerNib(nibNames: [CoursesCollectionViewCell.identifier])
        collectionView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Collection Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch enumCollectionType(rawValue: collectionView.tag)! {
        case .header:
            return enumCoursesType.count
        case .data:
            return viewModel.getTotalNumberOfCourses()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch enumCollectionType(rawValue: collectionView.tag)! {
        case .header:
            return CGSize(width: collectionView.frame.width / 4, height: 44)
        case .data:
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: collectionView.frame.width / 4 - 8, height: 180)
            }
            return CGSize(width: collectionView.frame.width - 5, height: 250)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch enumCollectionType(rawValue: collectionView.tag)! {
        case .header:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoursesHeaderCollectionViewCell.identifier, for: indexPath) as! CoursesHeaderCollectionViewCell
            switch enumCoursesType(rawValue: indexPath.row)! {
            case .all:
                cell.labelTitle.text = Constants.description_all
            case .inProgress:
                cell.labelTitle.text = Constants.description_inProgress
            case .saved:
                cell.labelTitle.text = Constants.description_saved
            case .skills:
                cell.labelTitle.text = Constants.description_skills
            }
            if viewModel.getSelectedCategory() == indexPath.row {
                cell.imageViewBottom.backgroundColor = Constants.color_applicationThemeColor
                cell.labelTitle.textColor = .black
            }else{
                cell.imageViewBottom.backgroundColor = .clear
                cell.labelTitle.textColor = .gray
            }
            return cell
        case .data:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoursesCollectionViewCell.identifier, for: indexPath) as! CoursesCollectionViewCell
            let course = viewModel.getCourse(at: indexPath.row)
            cell.progressView.isHidden = true
            cell.setArrow()
            cell.imgView.sd_setImage(with: Constants.getPathOfMedia(value: course.courseImage), placeholderImage: Constants.image_noData, options: [], context: nil)
            cell.labelTitle.text = course.courseTitle ?? ""
            cell.setProgressView(with: course.progress ?? 0.0)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch enumCollectionType(rawValue: collectionView.tag)! {
        case .header:
            viewModel.setSelectedCategory(value: indexPath.row)
            collectionView.reloadData()
            self.collectionView.reloadData()
        case .data:
            let lessonsListVC = LessonsListViewController.loadFromNib()
            lessonsListVC.viewModel.setCourse(value: viewModel.getCourse(at: indexPath.row))
            self.viewController.navigationController?.pushViewController(lessonsListVC, animated: true)
        }
    }
}
