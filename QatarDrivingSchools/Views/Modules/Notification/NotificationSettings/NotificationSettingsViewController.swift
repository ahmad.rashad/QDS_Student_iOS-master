//
//  NotificationSettingsSettingsViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit

class NotificationSettingsViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: NotificationSettingsDataSource?
    lazy var viewModel = NotificationSettingsViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelTitle.text = Constants.title_settings.localized
        labelTitle.font = CustomFont.medium(ofSize: 18)
                
        dataSource = NotificationSettingsDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
