//
//  NotificationSettingsSettingsViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import Foundation
import NISLRequestPackages
class NotificationSettingsViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isPrefrenceUpdated: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var isPushNotificationEnabled: Bool = true
    private var isEmailNotificationEnabled: Bool = true
    
    //MARK:- Dependancy Injection
    init() {    }
    
    //MARK:- API
    func updateUserPrefrences() {
        let param: Parameters = [
            "S_Code": Constants.loggedInUser?.scode ?? "",
            "Emailid": Constants.getLoggedInUserEmailForAPI(),
            "EnableEmailNotification": getPushNotificationStatus() ? 1 : 0,
            "EnablePushNotification": getEmailNotificationStatus() ? 1 : 0
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.UpdateUserPrefrences, queryParameter: param) { (res: Swift.Result<BaseClass<Bool>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let _):
                self.isPrefrenceUpdated.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isPrefrenceUpdated.value = false
            }
        }
    }
    
}
//MARK:- Getters and Setters
extension NotificationSettingsViewModel {
    func getPushNotificationStatus() -> Bool {
        UserDefaults.standard.bool(forKey: enumUserDefaultsKey.pushNotification.rawValue)
    }
    func getEmailNotificationStatus() -> Bool {
        UserDefaults.standard.bool(forKey: enumUserDefaultsKey.emailNotification.rawValue)
    }
    func togglePushNotification() {
        UserDefaults.standard.setValue(!getPushNotificationStatus(), forKey: enumUserDefaultsKey.pushNotification.rawValue)
        updateUserPrefrences()
    }
    func toggleEmailNotification() {
        UserDefaults.standard.setValue(!getEmailNotificationStatus(), forKey: enumUserDefaultsKey.emailNotification.rawValue)
        updateUserPrefrences()
    }
}

