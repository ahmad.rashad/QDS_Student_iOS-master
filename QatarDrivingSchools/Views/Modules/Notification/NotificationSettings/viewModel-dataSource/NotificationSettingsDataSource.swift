//
//  NotificationSettingsSettingsDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import Foundation
import UIKit

class NotificationSettingsDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: NotificationSettingsViewController
    private let tableView: UITableView
    private let viewModel: NotificationSettingsViewModel
    
    enum enumNotificaionSettingsTableRow: Int {
        case push
        case mail
        
        static let count: Int = {
            var max: Int = 0
            while let _ = enumNotificaionSettingsTableRow(rawValue: max) { max += 1 }
            return max
        }()
    }
    //MARK:- Init
    init(tableView: UITableView, viewModel: NotificationSettingsViewModel, viewController: NotificationSettingsViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [LabelSwitchTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    @objc func switchToggled(_ sender: UISwitch) {
        switch enumNotificaionSettingsTableRow(rawValue: sender.tag)! {
        case .push:
            viewModel.togglePushNotification()
        case .mail:
            viewModel.toggleEmailNotification()
        }
        tableView.reloadData()
    }
    //MARK:- Table Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enumNotificaionSettingsTableRow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LabelSwitchTableViewCell.identifier, for: indexPath) as! LabelSwitchTableViewCell
        switch enumNotificaionSettingsTableRow(rawValue: indexPath.row)! {
        case .push:
            cell.labelTitle.text = Constants.description_enablePushNotification.localized
            cell.statusSwitch.tag = enumNotificaionSettingsTableRow.push.rawValue
            cell.statusSwitch.isOn = viewModel.getPushNotificationStatus()
        case .mail:
            cell.labelTitle.text = Constants.description_enableMailNotification.localized
            cell.statusSwitch.tag = enumNotificaionSettingsTableRow.mail.rawValue
            cell.statusSwitch.isOn = viewModel.getEmailNotificationStatus()
        }
        cell.statusSwitch.addTarget(self, action: #selector(switchToggled(_:)), for: .valueChanged)
        return cell
    }
}





