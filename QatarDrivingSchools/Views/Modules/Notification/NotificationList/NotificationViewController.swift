//
//  NotificationViewController.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import UIKit

class NotificationViewController: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: NotificationDataSource?
    lazy var viewModel = NotificationViewModel()
    
    //MARK:- Activity life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
                
        dataSource = NotificationDataSource(tableView: tableView, viewModel: viewModel, viewController: self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        setupLayout()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK:- Action Methods
    @IBAction func buttonBackTouchUpInside(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSettingTouchUpInside(_ sender: UIButton) {
        self.navigationController?.pushViewController(NotificationSettingsViewController.loadFromNib(), animated: true)
    }
    
    //MARK:- Class Methods
    func setupLayout() {
        labelTitle.text = Constants.title_notification.localized
        labelTitle.font = CustomFont.medium(ofSize: 18)
        
        viewModel.isLoaderHidden.bind { isHidden in
            if isHidden {
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.stopAnimating()
            }else{
                self.view.isUserInteractionEnabled = false
                self.activityIndicator.startAnimating()
            }
        }
        viewModel.isNotificationsFetched.bind { _ in
            self.tableView.reloadData()
        }
        
        viewModel.getNotifications()
    }
}
