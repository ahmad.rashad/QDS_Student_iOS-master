//
//  NotificationViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import Foundation
import NISLRequestPackages
class NotificationViewModel {
    
    //MARK:- Variables
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var isNotificationsFetched: Dynamic<Bool> = Dynamic(true)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    private var notifications : [Notifications] = []
    private var isMoreDataAvailable = true
    private var offset = 0
    
    //MARK:- Dependancy Injection
    init() {    }
    
    
    //MARK:- API
    func getNotifications() {
        let param: Parameters = [
            "S_Code": Constants.loggedInUser?.scode ?? "",
            "EmailId":Constants.getLoggedInUserEmailForAPI(),
            "PageOffset":offset
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetNotification, queryParameter: param) { (res: Swift.Result<BaseClass<NotificationBaseData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if self.offset == 0 {
                    self.isMoreDataAvailable = true
                    self.notifications.removeAll()
                }
                if data.status == Constants.api_success{
                    let list = data.data?.notifications ?? []
                    self.offset += 1
                    self.notifications.append(contentsOf: list)
                    if list.count == 0{
                        self.isMoreDataAvailable = false
                    }
                } else {
                    self.isMoreDataAvailable = false
                   // self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                self.isNotificationsFetched.value = true
            case .failure(let message):
                self.alertMessage.value = message
                self.isMoreDataAvailable = false
                self.isNotificationsFetched.value = false
            }
        }
    }
}

//MARK:- Getters and Setters
extension NotificationViewModel {
    func getTotalNotificationCount() -> Int {
        notifications.count
    }
    func getNotification(at index: Int) -> Notifications {
        notifications[index]
    }
    func isMoreDataAvailableInNotification() -> Bool {
        isMoreDataAvailable
    }
}


