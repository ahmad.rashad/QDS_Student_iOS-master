//
//  NotificationDataSource.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 08/07/21.
//

import Foundation
import UIKit

class NotificationDataSource : NSObject, UITableViewDelegate ,UITableViewDataSource {
    
    private let viewController: NotificationViewController
    private let tableView: UITableView
    private let viewModel: NotificationViewModel
    
    enum enumTableSectionType: Int {
        case noData = 0
        case notifications
        static let count: Int = {
            var max: Int = 0
            while let _ = enumTableSectionType(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    //MARK:- Init
    init(tableView: UITableView, viewModel: NotificationViewModel, viewController: NotificationViewController) {
        self.viewController = viewController
        self.tableView = tableView
        self.viewModel = viewModel
        super.init()
        setupTableView()
    }
    
    //MARK: - Class methods
    func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        registerTableCell()
    }
    func registerTableCell(){
        tableView.registerNib(nibNames: [NotificationTableViewCell.identifier,
                                         ImageTableViewCell.identifier])
        tableView.reloadData()
    }
    //MARK:- Action methods
    
    //MARK:- Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        enumTableSectionType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch enumTableSectionType(rawValue: section)! {
        case .noData:
            if viewController.activityIndicator.isAnimating {
                return 0
            }
            return viewModel.getTotalNotificationCount() == 0 ? 1 : 0
        case .notifications:
            return viewModel.getTotalNotificationCount()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch enumTableSectionType(rawValue: indexPath.section)! {
        case .noData:
            return tableView.frame.height
        case .notifications:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch enumTableSectionType(rawValue: indexPath.section)! {
        case .noData:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
            cell.setLottieAnimation()
            return cell
        case .notifications:
            if viewController.activityIndicator.isAnimating {
                let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
                cell.setLottieAnimation()
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier, for: indexPath) as! NotificationTableViewCell
            let notification = viewModel.getNotification(at: indexPath.row)
            cell.labelDescription.text = notification.notificationMessage ?? ""
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentRow = indexPath.row
        let count = viewModel.getTotalNotificationCount()
        
        let isLastCell = count - currentRow == 1
        let minY = tableView.contentOffset.y
        
        if viewModel.isMoreDataAvailableInNotification() && isLastCell && minY > 0 {
            viewModel.getNotifications()
        }
    }
}




