//
//  UserViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 27/09/21.
//

import Foundation
import NISLRequestPackages
class UserViewModel: BaseViewModel {
   
    // MARK: - Vars & Lets
    var isUserDetailsFetched: Dynamic<Bool> = Dynamic(false)
    private let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
    
    //MARK:- Class methods
    override init() {
        super.init()
    }
    //MARK:- API
    func getUserDetails() {
        let param: Parameters = [
            "EmailId": Constants.getLoggedInUserEmailForAPI()
        ]
        isLoaderHidden.value = false
        apiManager.call(type: RequestItemsType.GetUserProfile, queryParameter: param) { (res: Swift.Result<BaseClass<UserData>, AlertMessage>, response, jsonData) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                    if let user = data.data?.user?.first {
                        Constants.loggedInUser = user
                        self.isUserDetailsFetched.value = true
                    } else {
                        self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                    }
                } else {
                    self.alertMessage.value = AlertMessage(title: "", body: data.message ?? "")
                }
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }
}
