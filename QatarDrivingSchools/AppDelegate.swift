//
//  AppDelegate.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import UIKit
import Firebase
import MobileRTC
import IQKeyboardManager
import NISLRequestPackages
#if DEBUG
    import CocoaDebug
#endif

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let sdkKey = Constants.ZOOM_SDK_KEY
    private let sdkSecret = Constants.ZOOM_SDK_SECRET
    var window: UIWindow?
    var myOrientation: UIInterfaceOrientationMask = .portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        //IQKeyboardManager.shared().isEnabled = true
        FirebaseApp.configure()
        
        setupSDK(sdkKey: sdkKey, sdkSecret: sdkSecret)
        UIView.appearance().semanticContentAttribute = Language.language.semantic
        UITextField.appearance().textAlignment = Language.language.semantic == .forceLeftToRight ? .left : .right
        if Constants.isUserLoggedIn {
            SideMenu.shared.setSideMenu()
        }else{
            window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())
        }
        
        switch (Config.appConfiguration) {
        case .Debug:
            #if DEBUG
             CocoaDebugSettings.shared.enableLogMonitoring = false
            #endif
            RequestManager.networkEnvironment = .production
        case .TestFlight:
            RequestManager.networkEnvironment = .production
        default:
            RequestManager.networkEnvironment = .production
        }
        #if DEBUG
        //RequestManager.networkEnvironment = .qa
        #endif
        
        NotificationCenter.default.addObserver(self,selector: #selector(notificationStatusUnAuthorised(_:)), name:  Notification.Name(Constants.notification_status401), object: nil)
        let pushManager = PushNotificationManager()
        pushManager.registerForPushNotifications()
        return true
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    //MARK:- Class methods
    @objc func notificationStatusUnAuthorised(_ sender: NSNotification) {
        window?.rootViewController?.showAlertAndDoLogoutForUnAuthorise(message: Constants.alert_anotherLoginDetected)
    }
    func setupSDK(sdkKey: String, sdkSecret: String) {
        let context = MobileRTCSDKInitContext()
        context.domain = "zoom.us"
        context.enableLog = true
        
        let sdkInitializedSuccessfully = MobileRTC.shared().initialize(context)
        
        if sdkInitializedSuccessfully == true, let authorizationService = MobileRTC.shared().getAuthService() {
            authorizationService.clientKey = sdkKey
            authorizationService.clientSecret = sdkSecret
            authorizationService.delegate = self
            authorizationService.sdkAuth()
        }
    }
}
// MARK: - MobileRTCAuthDelegate
// Conform AppDelegate to MobileRTCAuthDelegate.
// 2. MobileRTCAuthDelegate listens to authorization events like SDK authorization, user login, etc.
extension AppDelegate: MobileRTCAuthDelegate {

    // Result of calling sdkAuth(). MobileRTCAuthError_Success represents a successful authorization.
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        switch returnValue {
        case .success:
            print("SDK successfully initialized.")
        case .keyOrSecretEmpty:
            assertionFailure("SDK Key/Secret was not-- provided. Replace sdkKey and sdkSecret at the top of this file with your SDK Key/Secret.")
        case .keyOrSecretWrong, .unknown:
            assertionFailure("SDK Key/Secret is not valid.")
        default:
            assertionFailure("SDK Authorization failed with MobileRTCAuthError: \(returnValue).")
        }
    }
}


