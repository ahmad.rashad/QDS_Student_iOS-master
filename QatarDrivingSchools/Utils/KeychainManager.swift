//
//  KeychainManager.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 11/03/21.
//

import Foundation

class KeyChain {

    class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]

        SecItemDelete(query as CFDictionary)

        return SecItemAdd(query as CFDictionary, nil)
    }
    class func delete(for key: String) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key ] as [String : Any]
        return SecItemDelete(query as CFDictionary)
    }

    class func load(key: String) -> Data? {
        
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]

        var dataTypeRef: AnyObject? = nil

        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }

    class func StringToData(string : String) -> Data
    {
        let data = Data(string.utf8)
        return data
    }

    class func DataToString(data: Data) -> String
    {
        let str = String(decoding: data, as: UTF8.self)
        return str
    }
    
    /*class func ArrayToData(arr : [[String : Any]]) -> Data {
        let data = NSKeyedArchiver.archivedData(withRootObject: arr)
        return data
    }
    
    class func DataToArray(data : Data) -> [[String:Any]] {
        let arrayFromData = NSKeyedUnarchiver.unarchiveObject(with: data) as! [[String:Any]]
        return arrayFromData
    }*/
}

extension Data {
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.load(as: T.self) }
    }
}
