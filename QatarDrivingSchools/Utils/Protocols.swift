//
//  Protocols.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 11/05/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit

protocol HomeScreenDelegate {
    func showMoreOptions()
}

protocol LocationUpdateDelegte {
    func locationDidChange()
}

protocol HeightForTextView {
    func heightOfTextView(height: CGFloat)
}
