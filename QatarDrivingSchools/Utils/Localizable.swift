//
//  String+Localizable.swift
//  Localizable
//
//  Created by  on 6/23/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit


let appleLanguagesKey = "AppleLanguages"


enum Language: String, CaseIterable {
    
    case DEFAULT_LANGUAGE, ENGLISH = "en"
    case ARABIC = "ar"
    case BANGALI = "bn"
    case SPANISH = "es"
    case PERSIAN = "fa"
    case HINDI = "hi"
    case MALAYALAM = "ml"
    case NEPALI = "ne"
    case PASHTO = "ps"
    case RUSSIAN = "ru"
    case SINHALA = "si"
    case TAMIL = "ta"
    case TELUGU = "te"
    case FILIPINO = "fil"
    case TURKISH = "tr"
    case URDU = "ur"
    case CHINESE = "zh"
    case FRENCH = "fr"
    
    var semantic: UISemanticContentAttribute {
        switch self {
        case .ARABIC, .PERSIAN, .PASHTO, .URDU:
            return .forceRightToLeft
        default:
            return .forceLeftToRight
        }
    }
    
    var description: String {
        switch self {
        case .DEFAULT_LANGUAGE:
            return "Default Language"
        case .ENGLISH:
            return "English"
        case .ARABIC:
            return "Arabic - عربي"
        case .BANGALI:
            return "Bangla - বাঙালি"
        case .SPANISH:
            return "Spanish - Española"
        case .PERSIAN:
            return "Persian - فارسی"
        case .HINDI:
            return "Hindi - हिंदी"
        case .MALAYALAM:
            return "Malayalam - മലയാളം"
        case .NEPALI:
            return "Nepali - नेपाली"
        case .PASHTO:
            return "Pashto - پښتو"
        case .RUSSIAN:
            return "Russian - русский"
        case .SINHALA:
            return "Sinhala - සිංහල"
        case .TAMIL:
            return "Tamil - தமிழ்"
        case .TELUGU:
            return "Telugu - తెలుగు"
        case .FILIPINO:
            return "Filipino - Filipino"
        case .TURKISH:
            return "Turkish - Türk"
        case .URDU:
            return "Urdu - اردو"
        case .CHINESE:
            return "Chinese - 中国人"
        case .FRENCH:
            return "French - français"
        }
    }
    
    static var language: Language {
        get {
            //print(UserDefaults.standard.string(forKey: appleLanguagesKey) ?? "ddd")
            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
                let language = Language(rawValue: languageCode) {
                return language
            } else {
                let preferredLanguage = NSLocale.preferredLanguages[0] as String
                //print("GET 1 " + preferredLanguage)
                //if preferredLanguage != "" {
//                    let index = preferredLanguage.index(
//                        preferredLanguage.startIndex,
//                        offsetBy: 2
//                    )
                    guard let localization = Language(rawValue: String(preferredLanguage)) else {//preferredLanguage.substring(to: index)) else {
                        return Language.DEFAULT_LANGUAGE
                    }
                //print("GET " + localization.description)
                //print("GET " + localization.rawValue)
                return localization
//                } else {
//                    return Language.ENGLISH
//                }

            }
        }
        set {
            guard language != newValue else {
                return
            }
            
            //if newValue != .DEFAULT_LANGUAGE {
                //change language in the app
                //the language will be changed after restart
                //print("SET " + newValue.description)
                //print("SET " + newValue.rawValue)
                UserDefaults.standard.set([newValue.rawValue], forKey: appleLanguagesKey)
                UserDefaults.standard.synchronize()
                
                //Changes semantic to all views
                //this hack needs in case of languages with different semantics: leftToRight(en/uk) & rightToLeft(ar)
                UIView.appearance().semanticContentAttribute = newValue.semantic
                UITextField.appearance().textAlignment = newValue.semantic == .forceLeftToRight ? .left : .right
                UITextView.appearance().textAlignment = newValue.semantic == .forceLeftToRight ? .left : .right
                //initialize the app from scratch
                //show initial view controller
                //so it seems like the is restarted
                //NOTE: do not localize storyboards
                //After the app restart all labels/images will be set
                //see extension String below
            
                if Constants.isUserLoggedIn {
                    SideMenu.shared.setSideMenu()
                }else{
                    GlobalVariables.appDelegate?.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())
                }
//            } else {
//                UserDefaults.standard.set(["en"], forKey: appleLanguagesKey)
//                UserDefaults.standard.synchronize()
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                UITextField.appearance().textAlignment = .left
//                UITextView.appearance().textAlignment = .left
//                if Constants.isUserLoggedIn {
//                    SideMenu.shared.setSideMenu()
//                }else{
//                    GlobalVariables.appDelegate?.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())
//                }
//            }

        }
    }
}


extension String {
    
    var localized: String {
        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
    var localizedImage: UIImage? {
        return localizedImage()
            ?? localizedImage(type: ".png")
            ?? localizedImage(type: ".jpg")
            ?? localizedImage(type: ".jpeg")
            ?? UIImage(named: self)
    }
    
    private func localizedImage(type: String = "") -> UIImage? {
        guard let imagePath = Bundle.localizedBundle.path(forResource: self, ofType: type) else {
            return nil
        }
        return UIImage(contentsOfFile: imagePath)
    }
}

extension Bundle {
    //Here magic happens
    //when you localize resources: for instance Localizable.strings, images
    //it creates different bundles
    //we take appropriate bundle according to language
    static var localizedBundle: Bundle {
        let languageCode = Language.language.rawValue
        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
            return Bundle.main
        }
        return Bundle(path: path)!
    }
}
