//
//  URL+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 9/9/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import AVKit
extension URL {
    func getSizeForVideo() -> CGSize? {
        guard let track = AVURLAsset(url: self).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: size.width, height: size.height)
    }
}
