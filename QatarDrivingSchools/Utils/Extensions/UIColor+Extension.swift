//
//  UIColor+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/05/21.
//
import UIKit
import Foundation
extension UIColor {
    public convenience init?(hexStr: String) {
        let r, g, b, a: CGFloat

        if hexStr.hasPrefix("#") {
            let start = hexStr.index(hexStr.startIndex, offsetBy: 1)
            let hexColor = String(hexStr[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}
