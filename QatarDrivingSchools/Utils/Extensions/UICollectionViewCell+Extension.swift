//
//  UICollectionViewCell+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 05/07/21.
//

import UIKit

extension UICollectionViewCell {
       
    static var identifier: String {
        return (self.description().split(separator: ".").last?.description) ?? ""
    }

}
