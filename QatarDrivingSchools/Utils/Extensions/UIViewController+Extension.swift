//
//  UIViewController+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/04/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import NISLRequestPackages
import UIKit

extension UIViewController {
    
    //MARK:- Static Function
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        return instantiateFromNib()
    }
    
    // MARK:- Public methods
    func showAlertWith(message: AlertMessage , style: UIAlertController.Style = .alert) {
        if message.body == Constants.description_malicious {
            showAlertAndDoLogout(message: Constants.description_malicious)
        }else{
            
            let alertController = UIAlertController(title: message.title, message: message.body, preferredStyle: style)
            let action = UIAlertAction(title: Constants.title_ok, style: .default) { (action) in
                if message.isPopRequired{
                    self.navigationController?.popViewController(animated: true)
                }
                if message.isDismissRequired{
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func showAlertAndDoLogout(message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.title_yes.localized, style: .destructive) { (action) in
            
            self.dismiss(animated: true, completion: nil)
            Constants.isUserLoggedIn = false
            Constants.loggedInUser = nil
            GlobalVariables.appDelegate?.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())
        }
        alertController.addAction(action)
        alertController.addAction(UIAlertAction(title: Constants.title_no.localized, style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlertAndDoLogoutForUnAuthorise(message: String) {
        let alertController = UIAlertController(title: Constants.description_appName, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.title_ok, style: .destructive) { (action) in
            self.dismiss(animated: true, completion: nil)
            Constants.isUserLoggedIn = false
            Constants.loggedInUser = nil
            GlobalVariables.appDelegate?.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.loadFromNib())
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    func shouldHideLoader(isHidden: Bool) {
        /*if isHidden {
            MBProgressHUD.hide(for: self.view, animated: true)
        } else {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }*/
    }
    //MARK:- API
    func logoutUser() {
        /*let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        let param: NSDictionary = [
            "userid": "\(Constants.getLoggedInUserId())"
        ]
        
        apiManager.upload(type: RequestItemsType.logout, params: param) { (res: Swift.Result<BaseClass<>, AlertMessage>) in
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                }
                break
            case .failure(_):
                break
            }
        }*/
    }
}
extension UIViewController {
    
    //MARK: Loader
    func showLoader()
    {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.color = UIColor.systemBlue
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)
    }
    func dismissLoader(functionToCall: MethodHandler2? = nil)
    {
        DispatchQueue.main.async {
            self.dismiss(animated: false) {
                if let fnCall = functionToCall {
                    fnCall()
                }
            }
        }
    }
    typealias MethodHandler2 = ()  -> Void
    
    //MARK: Alertview Validation Message
    func displayMessage(message:String) {
        let alertView = UIAlertController(title: "PHAsset Demo", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        self.present(alertView, animated: true, completion:nil)
    }
}
