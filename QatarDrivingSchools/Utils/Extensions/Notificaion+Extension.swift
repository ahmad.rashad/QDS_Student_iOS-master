//
//  Notificaion+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 24/07/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let getNewUserOnStream = Notification.Name("getNewUserOnStream")
    static let getOrientationDetailToSubscriber = Notification.Name("getOrientationDetailToSubscriber")

}
