//
//  UITableVIew+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 25/05/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit

extension UITableView {
    func registerNib(nibNames: [String]) {
        for nibName in nibNames {
            self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
        }
    }
}
