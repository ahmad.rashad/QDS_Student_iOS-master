//
//  UIImage+Extension.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 18/05/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit

extension UIImage {
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (self.size.equalTo(aSize)) {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: aSize.width, height: aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
