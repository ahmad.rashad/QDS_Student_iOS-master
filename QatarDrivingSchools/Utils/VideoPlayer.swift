//
//  VideoPlayer.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 20/08/21.
//

import Foundation
import AVFoundation

func encodeVideo(at videoURL: URL,presetName: String = AVAssetExportPresetPassthrough, completionHandler: ((URL?, Error?) -> Void)?) {
    let avAsset = AVURLAsset(url: videoURL, options: nil)
    
    //Creating temp path to save the converted video
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
    let filePath = documentsDirectory.appendingPathComponent("tempVideo.mp4")
    
    //Check if the file already exists then remove the previous file
    if FileManager.default.fileExists(atPath: filePath.path) {
        do {
            try FileManager.default.removeItem(at: filePath)
        } catch {
            completionHandler?(nil, error)
        }
    }
    
    let startDate = Date()
    
    //Create Export session
    guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: presetName) else {
        completionHandler?(nil, nil)
        return
    }
    
    exportSession.outputURL = filePath
    exportSession.outputFileType = AVFileType.mp4
    exportSession.shouldOptimizeForNetworkUse = true
    //exportSession.timeRange = getTimeDurationForCropVideo(avAsset: avAsset)
    
    exportSession.exportAsynchronously(completionHandler: {
        switch exportSession.status {
        case .failed:
            print(exportSession.error ?? "NO ERROR")
            completionHandler?(nil, exportSession.error)
            break
        case .cancelled:
            print("Export canceled")
            completionHandler?(nil, nil)
            break
        case .completed:
            //Video conversion finished
            let endDate = Date()
            let time = endDate.timeIntervalSince(startDate)
            print("Successfully Converted in ===>", time)
            completionHandler?(exportSession.outputURL, nil)
            break
        default:
            break
        }
        
    })
}
