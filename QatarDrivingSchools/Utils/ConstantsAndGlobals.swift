//
//  Constants.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/04/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import CoreLocation
import NISLRequestPackages
import NotificationBannerSwift

//MARK:- Classes
class Constants {
    
    // MARK: - Alerts
    static var alert_title_error = ""
    static var alert_title_alert : String { get { "Alert!".localized } }
    static var alert_title_warning : String { get { "Warning!".localized } }
    static var alert_title_saveError : String { get { "Save error".localized } }
    static var alert_title_saved : String { get { "Saved!".localized } }
    static var alert_title_chooseImage : String { get { "Choose Image".localized } }
    static var alert_tryToLogout : String { get { "Are you sure you want to logout?".localized } }
    static var alert_savedMessage : String { get { "Your media has been saved to your photos.".localized } }
    static var alert_emailMissing : String { get { "Please provide email id.".localized } }
    static var alert_emailIsNotValid : String { get { "Please provide the valid email id.".localized } }
    static var alert_passwordMissing : String { get { "Please provide your password.".localized } }
    static var alert_oldPasswordMissing : String { get { "Please provide your old password.".localized } }
    static var alert_newPasswordMissing : String { get { "Please provide your new password.".localized } }
    static var alert_confirmPasswordMissing : String { get { "Please provide your confirm password.".localized } }
    static var alert_forgotPasswordEmailSent : String { get { "A new password is sent to your email address. Please login with the sent password.".localized } }
    static var alert_genericErrorMessage : String { get { "Something went wrong, please try again.".localized } }
    static var alert_languageMissing : String { get { "Please select Language".localized } }
    static var alert_passwordIsNotValid : String { get { "Password Must be between 8 to 16 characters long and it should not contains white space.".localized } }
    static var alert_passwordDoesNotMatch : String { get { "New Password and re-entered password do not match.".localized } }
    static var alert_canNotSelectMoreThanGivenLimit : String { get { "You can not select more than 3 media.".localized } }
    static var alert_canNotSelectMoreThanGivenLimitOfVideo : String { get { "You can not select more than 1 video.".localized } }
    static var alert_confirmToSubmitAnswers : String { get { "Are you sure you want to submit answers?".localized } }
    static var alert_anotherLoginDetected : String { get { "Your session is expired, Please login again.".localized } }
    static var alert_exitExam : String { get { "Are you sure you want to exit exam?".localized } }
    static var alert_watchPreviousLessonBefore : String { get { "Watch previous lesson to unlock the next lesson!".localized } }
    static var alert_thereIsNoQuiz : String { get { "There is no quiz added to the lesson, please contact the school admin for more details".localized } }
    static var alert_noQuestions : String { get { "Sorry! This quiz has no questions.".localized } }
    static var alert_startQuiz : String { get { "Are you sure you want to start quiz ? Once started countdown timer will start.".localized } }
    static var alert_appointmentTitleMissing : String { get { "Please provide appointment title.".localized } }
    static var alert_appointmentAvailabilityDateMissing : String { get { "Please provide appointment availability date.".localized } }
    static var alert_appointmentAvailabilityDateShouldInFuture : String { get { "Availability date should be future date.".localized } }
    static var alert_appointmentAvailabilityTimeMissing : String { get { "Please provide appointment availability time.".localized } }
    static var alert_appointmentDescriptionMissing : String { get { "Please provide appointment description.".localized } }
    static var alert_appointmentEventTypeMissing : String { get { "Please provide appointment event type.".localized } }
    static var alert_pleaseSelectComplainCategory : String { get { "Please select complain category.".localized } }
    static var alert_complainDescMissing : String { get { "Please add your complain description.".localized } }
    static var alert_complainTitleMissing : String { get { "Please add your complain title.".localized } }
    static var alert_fullNameEnglishMissing : String { get { "Please add your full name in English.".localized } }
    static var alert_tryAgainAfterImageLoad : String { get { "Please try again once your certificate loaded.".localized } }
    static var alert_meetingDetailsMissing : String { get { "Zoom meeting details are not provided yet.".localized } }
    static var alert_completeLessonFirstForQuiz : String { get { "Please watch the lesson first to attempt the quiz.".localized } }
    static var alert_cancelAppointment : String { get { "Are you sure you want to cancel this appointment?".localized } }
    static var alert_correctAnswer : String { get { "Correct Answer".localized } }
    static var alert_wrongAnswer : String { get { "Sorry! Wrong Answer.".localized } }
    static var alert_textToSpeechNotSupported : String { get { "Text to speech is not supported for this language.".localized } } 
    
    //MARK:- Placeholder
    static var placeholder_emailOrQID : String { get { "Email/QID".localized } }
    static var placeholder_email : String { get { "Email".localized } }
    static var placeholder_language : String { get { "Language".localized } }
    static var placeholder_password : String { get { "Password".localized } }
    static var placeholder_firstName : String { get { "First Name".localized } }
    static var placeholder_lastName : String { get { "Last Name".localized } }
    static var placeholder_confirmPassword : String { get { "Confirm Password".localized } }
    static var placeholder_enterTitle : String { get { "Enter Title".localized } }
    static var placeholder_addLocation : String { get { "Add Location...".localized } }
    static var placeholder_date : String { get { "dd-mm-yyyy".localized } }
    static var placeholder_time : String { get { "00:00".localized } }
    static var placeholder_enterDescription : String { get { "Enter Description".localized } }
    static var placeholder_selectCategory : String { get { "- Select Category -".localized } }
    static var placeholder_enterDetails : String { get { "Enter Details...".localized } }
    static var placeholder_username : String { get { "User Name".localized } }
    static var placeholder_governmentWork : String { get { "Government's work".localized } }
    static var placeholder_dob : String { get { "Date of Birth".localized } }
    static var placeholder_fullNameInArabic : String { get { "Full Name (in Arabic)".localized } }
    static var placeholder_fullNameInEnglish : String { get { "Full Name (in English)".localized } }
    static var placeholder_personalQatariID : String { get { "Personal Qatari ID".localized } }
    static var placeholder_searchIn : String { get { "Search In".localized } }
    static var placeholder_what : String { get { "What".localized } }
    static var placeholder_whatDesc : String { get { "Keywords contained in event".localized } }
    static var placeholder_who : String { get { "Who".localized } }
    static var placeholder_whoDesc : String { get { "Enter participants or organiser".localized } }
    static var placeholder_where : String { get { "Where".localized } }
    static var placeholder_whereDesc : String { get { "Enter a location or room".localized } }
    static var placeholder_whereDescEvent : String { get { "Keywords not contained in event".localized } }
    static var placeholder_oldPassword : String { get { "Old Password".localized } }
    static var placeholder_newPassword : String { get { "New Password".localized } }
    static var placeholder_eventType : String { get { "Event Type".localized } }
    static var placeholder_complaintTitle : String { get { "Complain Title".localized } }
    
    //MARK:- Button Title
    static var title_finish : String { get { "Finish".localized } }
    static var title_forgot_password : String { get { "Forgot password?".localized } }
    static var title_login : String { get { "LOGIN".localized } }
    static var title_signUp : String { get { "Sign Up".localized } }
    static var title_rememberMe : String { get { "Remember Me".localized } }
    static var title_submit : String { get { "SUBMIT".localized } }
    static var title_ok : String { get { "Ok".localized } }
    static var title_addAttachment : String { get { "Add Attachment".localized } }
    static var title_male : String { get { "Male".localized } }
    static var title_female : String { get { "Female".localized } }
    static var title_save : String { get { "Save".localized } }
    static var title_saveInCaps : String { get { "SAVE".localized } }
    static var title_reset : String { get { "RESET".localized } }
    static var title_startLesson : String { get { "START LESSON".localized } }
    static var title_select : String { get { "SELECT".localized } }
    static var title_addAppointment : String { get { "Add Appointment".localized } }
    static var title_english : String { get { "English".localized } }
    static var title_arabic : String { get { "Arabic".localized } }
    static var title_cancel : String { get { "CANCEL".localized } }
    static var title_getCertificate : String { get { "GET CERTIFICATE".localized } }
    static var title_theoreticalTest : String { get { "THEORETICAL TEST".localized } }
    static var title_practicalExam : String { get { "PRACTICAL EXAM".localized } }
    static var title_previous : String { get { "PREVIOUS".localized } }
    static var title_next : String { get { "NEXT".localized } }
    static var title_yes : String { get { "Yes".localized } }
    static var title_no : String { get { "No".localized } }
    static var title_images : String { get { "Images".localized } }
    static var title_video : String { get { "Video".localized } }
    static var title_done : String { get { "Done".localized } }
    static var title_viewAll : String { get { "View All".localized } }
    static var title_completed : String { get { "COMPLETED".localized } }
    static var title_resume : String { get { "RESUME".localized } }
    static var title_showResult : String { get { "Show Result".localized } }
    static var title_join : String { get { "Join".localized } }
    static var title_pending : String { get { "Pending".localized } }
    static var title_close : String { get { "close".localized } }
    static var title_attendQuiz : String { get { "ATTEND QUIZ".localized } }
    static var title_viewResult : String { get { "VIEW RESULT".localized } }
    
    
    //MARK:- Screen Title
    static var title_forgotPassword : String { get { "Forgot Password".localized } }
    static var title_profile : String { get { "Profile".localized } }
    static var title_search : String { get { "Search".localized } }
    static var title_searchInCaps : String { get { "SEARCH".localized } }
    static var title_studentLearningProgress : String { get { "Students Learning Progress".localized } }
    static var title_newAppointment : String { get { "New Appointment".localized } }
    static var title_newAppointmentInCaps : String { get { "NEW APPOINTMENT".localized } }
    static var title_editAppointment : String { get { "Edit Appointment".localized } }
    static var title_complaints : String { get { "Complaints".localized } }
    static var title_newComplain : String { get { "New Complain".localized } }
    static var title_editComplain : String { get { "Edit Complain".localized } }
    static var title_contactInfo : String { get { "Contact Information".localized } }
    static var title_accountDetails : String { get { "Account Details".localized } }
    static var title_notification : String { get { "Notification".localized } }
    static var title_settings : String { get { "Settings".localized } }
    static var title_userProfile : String { get { "User Profile".localized } }
    static var title_appointmentSearch : String { get { "Appointment Search".localized } }
    static var title_selectLanguage : String { get { "Select Language".localized } }
    static var title_result : String { get { "Result".localized } }
    static var title_history : String { get { "History".localized } }
    static var title_contract : String { get { "Contract".localized } }
    static var title_invoice : String { get { "Invoice".localized } }
    static var title_contactUS : String { get { "Contact us".localized } }
    static var title_complainDetails : String { get { "Complain Details".localized } }
    static var title_courses : String { get { "Courses".localized } }
    static var title_courseDetails : String { get { "Course Details".localized } }
    
    //MARK:- descriptions (for labels)
    static var description_appName : String { get { "Qatar Driving School".localized } }
    static var description_malicious : String { get { "Malicious source detected".localized } }
    static var description_doNotHaveAnAccount : String { get { "Don't have an account? ".localized } }
    static var description_alreadyHaveAnAccount : String { get { "Already have an account? ".localized } }
    static var description_forgotPassword : String { get { "Please enter your email address. You will receive a link to create a new password via registered email.".localized } }
    static var description_chooseFromSavedAccount : String { get { "Choose from saved accounts".localized } }
    static var description_accountDetails : String { get { "Account Details".localized } }
    static var description_userProfile : String { get { "User Profile".localized } }
    static var description_contactInfo : String { get { "Contact Information".localized } }
    static var description_dashboard : String { get { "Dashboard".localized } }
    static var description_profile : String { get { "Profile".localized } }
    static var description_course : String { get { "Course".localized } }
    static var description_eBook : String { get { "E-Book".localized } }
    static var description_practiceQuestions : String { get { "Practice Questions".localized } }
    static var description_complain : String { get { "Complain".localized } }
    static var description_contract : String { get { "Contract".localized } }
    static var description_invoice : String { get { "Invoice".localized } }
    static var description_certificate : String { get { "Certificate".localized } }
    static var description_appointment : String { get { "Appointment".localized } }
    static var description_learningProgress : String { get { "Learning Progress".localized } }
    static var description_contactUS : String { get { "Contact Us".localized } }
    static var description_logout : String { get { "Logout".localized } }
    static var description_completedLessons : String { get { "Completed Lessons".localized } }
    static var description_passedQuiz : String { get { "Passed Quiz / Exams".localized } }
    static var description_result : String { get { "Result :".localized } }
    static var description_all : String { get { "All".localized } }
    static var description_inProgress : String { get { "In Progress".localized } }
    static var description_saved : String { get { "Saved".localized } }
    static var description_skills : String { get { "Skills".localized } }
    static var description_courseType : String { get { "Course Type".localized } }
    static var description_lessonProgress : String { get { "Lesson Progress".localized } }
    static var description_quizReview : String { get { "Quiz Review".localized } }
    static var description_quizTypeWiseEvaluation : String { get { "Quiz Type Wise Evaluation".localized } }
    static var description_title : String { get { "Title".localized } }
    static var description_addLocation : String { get { "Add Location".localized } }
    static var description_date : String { get { "Date".localized } }
    static var description_from : String { get { "From".localized } }
    static var description_to : String { get { "To".localized } }
    static var description_time : String { get { "Time".localized } }
    static var description_addDescription : String { get { "Add Description".localized } }
    static var description_color : String { get { "Color".localized } }
    static var description_complainDetails : String { get { "Complain Details".localized } }
    static var description_complainCategory : String { get { "Complain Category".localized } }
    static var description_complainDescription : String { get { "Complain Description".localized } }
    static var description_attachFile : String { get { "Attach File".localized } }
    static var description_mobile : String { get { "Mobile".localized } }
    static var description_address : String { get { "Address".localized } }
    static var description_otherDetails : String { get { "Other Details".localized } }
    static var description_registrationDate : String { get { "Registration Date".localized } }
    static var description_lastVisitDate : String { get { "Last Visit Date".localized } }
    static var description_enableMailNotification : String { get { "Enable mail notification".localized } }
    static var description_enablePushNotification : String { get { "Enable push notification".localized } }
    static var description_frontIdPhoto : String { get { "Front ID Photo".localized } }
    static var description_backIdPhoto : String { get { "Back ID Photo (optional)".localized } }
    static var description_gender : String { get { "Gender".localized } }
    static var description_mailSent : String { get { "Mail has been sent successfully!".localized } }
    static var description_mailSentDesc : String { get { "You have successfully sent the credentials to Jhon Dao's email address.".localized } }
    static var description_today : String { get { "Today".localized } }
    static var description_filter : String { get { "Filter".localized } }
    static var description_lessons : String { get { "Lessons".localized } }
    static var description_continueWatchingLesson : String { get { "Continue watching your lesson".localized } }
    static var description_quiz : String { get { "Quiz".localized } }
    static var description_examCategory : String { get { "Exam Category".localized } }
    static var description_dateTime : String { get { "Date and Time".localized } }
    static var description_numberOfQuestion : String { get { "Number of Questions".localized } }
    static var description_score : String { get { "Your Score".localized } }
    static var description_topic : String { get { "Topic".localized } }
    static var description_introduction : String { get { "Introduction".localized } }
    static var description_instructor : String { get { "Instructor".localized } }
    static var description_changePassword : String { get { "Change Password".localized } }
    static var description_complainType : String { get { "Complain Type:".localized } }
    static var description_dateTitle : String { get { "Date:".localized } }
    static var description_fileAttached : String { get { "file attached".localized } }
    static var description_noFilesAttached : String { get { "No file attached".localized } }
    static var description_filesAttached : String { get { "files attached".localized } }
    static var description_noComplaintsYet : String { get { "You didn't submit any complaints yet.".localized } }
    static var description_noEbook : String { get { "No E-Book available.".localized } }
    static var description_questions : String { get { "Questions".localized } }
    static var description_question : String { get { "Question".localized } }
    static var description_failedExam : String { get { "Sorry!! you have not passed the theory exam.".localized } }
    static var description_passedExam : String { get { "Congratulations you have passed the theory exam.".localized } }
    static var description_testTitle : String { get { "Qatar Driving Theory Test".localized } }
    static var description_duration : String { get { "Duration".localized } }
    static var description_upcomingExample : String { get { "UPCOMING EXAM".localized } }
    static var description_exam : String { get { "Exam".localized } }
    static var description_download : String { get { "DOWNLOAD".localized } }
    static var description_sendEmail : String { get { "SEND EMAIL".localized } }
    static var description_issueDate : String { get { "Issue Date".localized } }
    static var description_followUsOn : String { get { "Follow us on Facebook, Instagram, YouTube and Twitter".localized } }
    static var description_email : String { get { "Email ID : ".localized } }
    static var description_contact : String { get { "Contact Number : ".localized } }
    static var description_pending : String { get { "Pending".localized } }
    static var description_completed : String { get { "Completed".localized } }
    static var description_cancelled : String { get { "Cancelled".localized } }
    static var description_notAssigned : String { get { "Not Assigned".localized } }
    static var description_lessonQuiz : String { get { "Lessons Quiz".localized } }
    static var description_courseQuiz : String { get { "Course Quiz".localized } }
    static var description_pdf : String { get { "PDF".localized } }
    static var description_eventType : String { get { "Event Type".localized } }
    static var description_zzzz : String { get { "ZZZZZZZZZZZZZZZZZZ".localized } }
    static var description_languageSettings : String { get { "Language Settings".localized } }
    static var description_courseOverView : String { get { "Course overview".localized } }
    static var description_complaintOverView : String { get { "Complaint overview".localized } }
    static var description_courseLessonProgress : String { get { "Lesson Progress".localized } }
    static var description_courseQuizReview : String { get { "Course Review".localized } }
    static var description_courseDueFee : String { get { "Due Course Fee".localized } }
    static var description_totalComplain : String { get { "Total Complaints".localized } }
    static var description_pendingComplain : String { get { "Pending Complaints".localized } }
    static var description_completedComplain : String { get { "Completed complaints".localized } }
    static var description_courseCompleted : String { get { "Course Completed".localized } }
    static var description_coursePending : String { get { "Course Pending".localized } }
    static var description_courseInProgress : String { get { "Course In-Progress".localized } }
    static var description_marks : String { get { "Marks".localized } }
    
    //MARK:- Color
    static let color_applicationThemeColor = UIColor(named: "ApplicationTheme") ?? UIColor(red: 0, green: 102, blue: 204, alpha: 1.0)
    static let color_darkApplicationThemeColor = UIColor(named: "DarkApplicationColor") ?? UIColor(red: 0, green: 102, blue: 204, alpha: 1.0)
    static let color_applicationDisableColor = UIColor(named: "ApplicationDisableColor") ?? UIColor(red: 0, green: 102, blue: 204, alpha: 1.0)
    static let color_lightTransparent = UIColor(named: "LightTransparent") ?? UIColor(red: 0, green: 102, blue: 204, alpha: 1.0)
    
    static let color_greenText = UIColor(named: "GreenText") ?? UIColor(red: 34, green: 139, blue: 34, alpha: 1.0)
    static let color_darkGreen = UIColor(named: "DarkGreen") ?? UIColor(red: 0, green: 100, blue: 0, alpha: 1.0)
    static let color_yellow = UIColor(named: "Yellow") ?? UIColor(red: 255, green: 255, blue: 0, alpha: 1.0)
    
    //UIColor(named: "Yellow")!
    static let color_blue = UIColor(named: "Blue") ?? UIColor(red: 0, green: 0, blue: 255, alpha: 1.0)
    static let color_red = UIColor(named: "Red") ?? UIColor(red: 255, green: 0, blue: 0, alpha: 1.0)
    
    //MARK:- Images
    static let image_noProfilePlaceholder = #imageLiteral(resourceName: "placeholder_profile_ic")
    static let image_addImage = #imageLiteral(resourceName: "add1_ic")
    static let image_selectedRadio = #imageLiteral(resourceName: "checkbox_selected_ic")
    static let image_unselectedRadio = #imageLiteral(resourceName: "checkbox_deselected_ic")
    static let image_noData = #imageLiteral(resourceName: "Splash")
    static let image_noDataPlaceHolder = #imageLiteral(resourceName: "no_data_placeholder")
    static let image_invoice = #imageLiteral(resourceName: "ic_invoice")
    
    //MARK:- Zoom SDK
    static let ZOOM_SDK_KEY = "Zfd6jinvKMifs1pke1Pe9KGZl5xaQUdySE6V"
    static let ZOOM_SDK_SECRET = "JG3ZIv6XngPp4eGkM2glfSBVEZzzXOjugyzO"
    
    //MARK:- web configuration key
    static let web_configuration_videoStatus = "videoStatus"
    static let web_configuration_videoTime = "videoTime"
    static let web_configuration_videoSeek = "seek"
    static let web_configuration_setLanguage = "setLanguage"
    
    
    //MARK:- Device information
    static let device_type = "0" // iOS
    static var device_token = ""
    
    //MARK:- userDefaults
    static var isUserLoggedIn : Bool
    {
        get {
            UserDefaults.standard.bool(forKey: enumUserDefaultsKey.isLoggedIn.rawValue)
        }
        set{
            UserDefaults.standard.setValue(newValue, forKey: enumUserDefaultsKey.isLoggedIn.rawValue)
        }
    }
    static var loggedInUser: User?
    {
        get {
            UserDefaults.standard.getCustom(User.self, forKey: enumUserDefaultsKey.userData.rawValue)
        }
        set{
            UserDefaults.standard.setCustom(newValue, forKey: enumUserDefaultsKey.userData.rawValue)
        }
    }
    static var BEARER_TOKEN: String? {
        get {
            UserDefaults.standard.getCustom(String.self, forKey: enumUserDefaultsKey.bearerToken.rawValue)
        }
        set{
            UserDefaults.standard.setCustom(newValue, forKey: enumUserDefaultsKey.bearerToken.rawValue)
        }
    }
    //MARK:- Notification Names
    static let notification_status401 = "notification_status401" // When update please update in RequestManager to
        
    //MARK:- Google API
    static let google_api_key = "AIzaSyCkWarPn4LYhDzd0vLpcBK7_V3H9KK7CGM"
    
    //MARK:- Security
    
    //MARK:- API response
    static let api_success = 1
    static let api_failed = 0
    
    
    //MARK:- url
    static let url_privacyPolicy = URL(string: "https://www.abc.com/privacy_policy.pdf")
    static let url_termsAndConditions = URL(string: "https://www.abc.com/TC_trainer.pdf")
    
    //MARK:- Media paths
    
    static var PATH_MEDIA: String {
        get {
            switch RequestManager.networkEnvironment {
            case .dev: return "https://portal.syaaqh.qa/"
            case .stage: return "https://portal.syaaqh.qa/"
            case .production: return "https://portal.syaaqh.qa/"
            case .qa: return "https://portal.syaaqh.qa/"
            }
            
//            switch RequestManager.networkEnvironment {
//            case .dev: return "https//testqds.idealsolutions.com/"
//            case .stage: return "http://testqds.idealsolutions.com/"
//            case .production: return "http://testqds.idealsolutions.com/"
//            case .qa: return "http://testqds.idealsolutions.com/"
//            }
            
//            switch RequestManager.networkEnvironment {
//            case .dev: return "http//testqds.idealsolutions.com/"
//            case .stage: return "http://testqds.idealsolutions.com/"
//            case .production: return "http://testqds.idealsolutions.com/"
//            case .qa: return "http://testqds.idealsolutions.com/"
//            }
        }
    }
    
    //MARK:- tmp
    static var IS_FIRST_TIME_IMAGE_PICKER = true
    
    //MARK:- Functions
    static func getLanguageChangeAlert(language: String) -> String {
        "Are you sure you want to access the app in \(language) language?"
    }
    static func getPathOfMedia(value: String?) -> URL? {
        if let value = value {
            return URL(string: "\(PATH_MEDIA)\(value)")
        }
        return nil
    }
   
    static func getLoggedInUserId() -> Int{
        if Constants.loggedInUser == nil {
            return 0
        }
        return Constants.loggedInUser?.id ?? 0
    }
    static func getLoggedInUserEmailForAPI() -> String {
       "\(Constants.loggedInUser?.emailId ?? "")"
    }
    static func saveUserInfoAndProceed(user: User){
        Constants.isUserLoggedIn = true
        Constants.loggedInUser = user
        SideMenu.shared.setSideMenu()
    }
    static func setGradient(sender: UIView) {
        let topColor = Constants.color_applicationThemeColor
        let bottomColor = Constants.color_darkApplicationThemeColor
        let gradientLayer: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            gradientLayer.frame = CGRect.zero
            return gradientLayer
        }()
        gradientLayer.frame = sender.bounds
        sender.clipsToBounds = true
        sender.layer.insertSublayer(gradientLayer, at: 0)
    }
}

class GlobalVariables{
   // static var user: User!
    static var appDelegate = UIApplication.shared.delegate as? AppDelegate
}

class CustomFont {
    static func regular(ofSize: CGFloat = 16) -> UIFont {
        /*if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-Regular", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-Regular", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.systemFont(ofSize: ofSize)
    }
    static func bold(ofSize: CGFloat = 16) -> UIFont {
        /*if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-Bold", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-Bold", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.boldSystemFont(ofSize: ofSize)
    }
    static func light(ofSize: CGFloat = 16) -> UIFont {
        /*if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-Light", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-Light", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.systemFont(ofSize: ofSize)
    }
    static func semiBold(ofSize: CGFloat = 16) -> UIFont {
       /* if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-SemiBold", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-SemiBold", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.systemFont(ofSize: ofSize)
    }
    static func medium(ofSize: CGFloat = 16) -> UIFont {
       /* if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-Medium", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-Medium", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.systemFont(ofSize: ofSize)
    }
    static func black(ofSize: CGFloat = 16) -> UIFont {
        /*if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "Poppins-Black", size: ofSize + 10)!
        }*/
        return UIFont(name: "Poppins-Black", size: calculateFontForWidth(size: ofSize))!// ?? UIFont.systemFont(ofSize: ofSize)
    }
}
class CustomBannerColors: BannerColorsProtocol {

    internal func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:    return UIColor.red
        case .info:      return Constants.color_applicationThemeColor
        case .success:   return UIColor.green
        case .warning:   return Constants.color_applicationThemeColor
        case .none:      return UIColor.clear
        }
    }

}

//MARK:- Enums
enum enumViewControllerType: String{
    case edit
    case regular
    case viewOnly
}
enum enumMediaType: Int {
    case image = 1
    case video = 2
    case other = 3
}
enum enumUserDefaultsKey: String {
    case isLoggedIn
    case rememberedUserEmail
    case userData
    case bearerToken
    case pushNotification
    case emailNotification
    case fcmToken
}
enum enumLessonState: Int {
    case inactive = 0
    case active
    case resume
    case completed
}
enum enumAppointmentStatus: Int {
    case pending = 1
    case cancelled = 2
    case completed = 3
}
enum enumComplainStatus: Int {
    case pending = 1
    case inProgress = 2
    case done = 3
}
enum enumTestStatus: Int {
    case deactivate = 0
    case activate = 1
}
enum enumContractDocType: Int {
    case jpg = 0
    case png = 1
    case pdf = 2
}
enum enumResultType: Int {
    case pass = 1
    case failed = 0
}
/*enum enumModuleType: Int {
    case course = 1
    case lesson = 2
    case schoolGeneralAndInformation = 3
    case locationManagement = 4
    case staffManagementAndStaffAccess = 5
    case hallManagement = 6
    case masterForms = 7
    case questionAndQuiz = 8
    case complainManagement = 9
    case zoomRequest = 10
    case appointment = 11
    case report = 12
}*/
public enum enumModuleType: Int {
    case EventManagement = 1
    case RegistrationType = 2
    case CourseCategory = 3
    case Course = 4
    case Lesson = 5
    case Contract = 6
    case ManageStudent = 7
    case AuditLogs = 8
    case Complain = 9
    case Notification = 10
    case StudentActivity = 11
    case ManageReport = 12
    case Appointment = 13
    case ZoomMeetings = 14
    case StudentReport = 15
    case SchoolGeneral = 16
    case ContactInformation = 17
    case LocationManagement = 18
    case HallManagement = 19
    case DesignationManagement = 20
}
//MARK: - SCREEN BOUNDS
let SCREEN_WIDTH = UIScreen.main.bounds.size.width as CGFloat
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height as CGFloat
let SCREEN_SIZE = UIScreen.main.bounds.size

//MARK: - SCREEN WIDTH FOR ORIGINAL FONTS
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let SCREEN_WIDTH_FOR_ORIGINAL_FONT = IS_IPAD ? 1024 : 414 as CGFloat
let SCREEN_HEIGHT_FOR_ORIGINAL_FONT = IS_IPAD ? 1024 : 896 as CGFloat

//MARK: - DESIGNING FUNCTIONS

func calculateFontForWidth(size : CGFloat) -> CGFloat {
    return (SCREEN_WIDTH * size)/SCREEN_WIDTH_FOR_ORIGINAL_FONT
}

func calculateFontForHeight(size : CGFloat) -> CGFloat {
    return (SCREEN_HEIGHT * size)/SCREEN_HEIGHT_FOR_ORIGINAL_FONT
}

func scaleFont(byWidth control: UIView?) {
    if (control is UILabel) {
        (control as? UILabel)?.adjustsFontSizeToFitWidth = true
        (control as? UILabel)?.minimumScaleFactor = 0.5
        (control as? UILabel)?.baselineAdjustment = .alignCenters
        (control as? UILabel)?.lineBreakMode = .byClipping
    } else if (control is UIButton) {
        (control as? UIButton)?.titleLabel?.adjustsFontSizeToFitWidth = true
        (control as? UIButton)?.titleLabel?.minimumScaleFactor = 0.5
        (control as? UIButton)?.titleLabel?.baselineAdjustment = .alignCenters
        (control as? UIButton)?.titleLabel?.lineBreakMode = .byClipping
    } else if (control is UITextField) {
        (control as? UITextField)?.adjustsFontSizeToFitWidth = true
        (control as? UITextField)?.minimumFontSize = 0.5
    }
}
