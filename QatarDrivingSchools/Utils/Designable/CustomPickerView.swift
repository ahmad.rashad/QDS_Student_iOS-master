//
//  CustomPickerView.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/1/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
class CustomPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
    /**
     The data for the `UIPickerViewDelegate`

     Always needs to be an array of `String`! The `UIPickerView` can ONLY display Strings
     */
    public var data: [String]? {
        didSet {
            super.delegate = self
            super.dataSource = self
            self.reloadAllComponents()
        }
    }

    /**
     Stores the UITextField that is being edited at the moment
     */
    public var textFieldBeingEdited: UITextField?

    /**
     Get the selected Value of the picker
     */
    public var selectedValue: String {
        get {
            if data != nil {
                return data![selectedRow(inComponent: 0)]
            } else {
                return ""
            }
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if data != nil {
            return data!.count
        } else {
            return 0
        }
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if data != nil {
            return data![row]
        } else {
            return ""
        }
    }
}
