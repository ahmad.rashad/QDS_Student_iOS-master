//
//  ApplicationThemeButton.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 21/07/21.
//

import Foundation
import UIKit
@IBDesignable class ApplicationThemeButton: RoundButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
        let topColor = Constants.color_applicationThemeColor
        let bottomColor = Constants.color_darkApplicationThemeColor
        let gradientLayer: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            gradientLayer.frame = CGRect.zero
            return gradientLayer
        }()
        gradientLayer.frame = self.bounds
        self.clipsToBounds = true
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
