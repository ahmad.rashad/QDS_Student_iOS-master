//
//  RequestItemType.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 16/06/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import Foundation
import NISLRequestPackages

public enum RequestItemsType : String{
    case Account
    case ChangePassword
    case ForgetPassword
    case GetUserProfile
    case UpdateUserProfile
    case UpdateLastVisitDate
    
    case GetComplainList
    case GetComplainCategories
    case AddComplain
    case EditComplain
    case GetCourses
    case GetAllEBooks
    case SearchEBook
    case UpdateStudentsProgress
    
    case GetLessonByDay
    case GetLessonDetail
    case GetCompletedLessons
    case GetLearningProgress
    
    case GetAppointmentsCategories
    case GetAllAppointments
    case CreateAppointment
    case GetAppointmentsRequestList
    case UpdateAppointment
    case CancelAppointment
    
    case GetContract
    case GetInvoice
    case GetSchoolContact
    
    case GetCertificate
    case GetQuestionsForTest
    case GetDashboard
    case GetResult
    case GetQuiz
    case GetVideoQuestions
    
    case UpdateStudentActivity
    
    case GetNotification
    case UpdateUserPrefrences
    case AddPushToken
    case Logout
}

//MARK: - Extension
extension RequestItemsType: EndPointType {
    
    // MARK: - Vars & Lets
    public var baseURL: String {
        switch RequestManager.networkEnvironment {
            case .dev: return "https:api.syaaqh.qa/api/"
            case .stage: return ""
            case .production: return "https:api.syaaqh.qa/api/"
            case .qa: return "https:api.syaaqh.qa/api/"
        }
        
//        switch RequestManager.networkEnvironment {
//            case .dev: return "http://testqds.api.idealsolutions.com/api/"
//            case .stage: return ""
//            case .production: return "http://testqds.api.idealsolutions.com/api/"
//            case .qa: return "http://testqds.api.idealsolutions.com/api/"
//        }
        
//        switch RequestManager.networkEnvironment {
//        case .dev: return "http://192.168.2.28:44221/api/"
//        case .stage: return ""
//        case .production: return "http://192.168.2.28:44221/api/"
//        case .qa: return "http://192.168.2.28:44221/api/"
//        }
    }
    
    public var version: String {
        switch RequestManager.networkEnvironment {
        case .dev: return ""
        case .stage: return ""
        case .production: return ""
        case .qa: return ""
        }
    }
    
    public var path: String {
        switch self {
        case .ChangePassword, .ForgetPassword, .Logout , .GetUserProfile, .UpdateUserProfile, .UpdateLastVisitDate:
            return "Account/" + self.rawValue
        case .GetCourses, .GetCertificate, .UpdateStudentsProgress , .GetInvoice:
            return "Course/" + self.rawValue
        case .GetLessonByDay, .GetLessonDetail, .GetCompletedLessons, .GetAllEBooks, .SearchEBook, .GetVideoQuestions:
            return "Lesson/" + self.rawValue
        case .GetQuestionsForTest, .GetResult, .GetQuiz:
            return "Exam/" + self.rawValue
        case .GetLearningProgress, .GetSchoolContact, .UpdateStudentActivity, .UpdateUserPrefrences, .AddPushToken:
            return "Student/" + self.rawValue
        case .GetDashboard:
            return "Dashboard/" + self.rawValue
        case .GetAppointmentsCategories, .GetAllAppointments, .CreateAppointment, .GetAppointmentsRequestList, .CancelAppointment, .UpdateAppointment:
            return "Appoinment/" + self.rawValue
        case .GetContract:
            return "Contract/" + self.rawValue
        case .GetNotification:
            return "Notification/" + self.rawValue
        default:
            return self.rawValue
        }
    }
    
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .GetUserProfile, .GetComplainList, .GetComplainCategories, .GetCourses, .GetLessonByDay, .GetLessonDetail, .GetCompletedLessons, .GetQuestionsForTest, .GetAllEBooks, .SearchEBook, .GetLearningProgress, .GetDashboard, .GetAppointmentsCategories, .GetCertificate, .GetAllAppointments, .GetAppointmentsRequestList, .GetSchoolContact, .GetContract, .GetInvoice, .GetQuiz , .GetVideoQuestions, .GetNotification:
            return .get
        default:
            return .post
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        case .Account:
            return ["Content-Type": "application/json",
                    "User-Agent": "iOS",
                    "X-Requested-With": "XMLHttpRequest"]
        default:
            return ["Content-Type": "application/json",
                    "User-Agent": "iOS",
                    "X-Requested-With": "XMLHttpRequest",
                    "Authorization":Constants.BEARER_TOKEN ?? ""]
        }
    }
    
    public var url: URL {
        switch self {
        default:
            return URL(string: self.baseURL + self.version + self.path)!
        }
    }
    
    public var encoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
}
