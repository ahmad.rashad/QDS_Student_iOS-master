//
//  BaseViewModel.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 8/29/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
import NISLRequestPackages

class BaseViewModel {
    
    // MARK: - Vars & Lets
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var notificationCountUpdated: Dynamic<Int> = Dynamic(0)
    
    var langaguesList: [String] = []
    
    init(){
        for lang in Language.allCases {
            langaguesList.append(lang.rawValue)
        }
        langaguesList.sort()
    }
    
    //MARK:- API used from multiple place
    class func updateStudentActivity(moduleId: Int, action: String, description: String) {
        let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
        let param: Parameters = [
            "email": Constants.getLoggedInUserEmailForAPI(),
            "moduleId": moduleId,
            "action": action,
            "description": description,
            "s_Code": Constants.loggedInUser?.scode ?? ""
        ]
        apiManager.call(type: RequestItemsType.UpdateStudentActivity, params: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                }
            case .failure(let message):
                print(message)
            }
        }
    }
     
    class func updateAppToken(token: String){
        let apiManager = RequestManager(sessionManager: SessionManager(), retrier: RequestManagerRetrier())
        let param: Parameters = [
            "Emailid": Constants.getLoggedInUserEmailForAPI(),
            "Token": token,
            "S_Code": Constants.loggedInUser?.scode ?? ""
        ]
        apiManager.call(type: RequestItemsType.AddPushToken, queryParameter: param) { (res: Swift.Result<BaseClass<AppointmentsBaseData>, AlertMessage>, response, jsonData) in
            switch res {
            case .success(let data):
                if data.status == Constants.api_success{
                }
            case .failure(let message):
                print(message)
            }
        }
    }
}
//MARK:- Getteres and setters
extension BaseViewModel {
    func getLanguagesCount() -> Int {
        langaguesList.count
    }
    func getLanguage(at index: Int) -> String {
        langaguesList[index]
    }
    
}
