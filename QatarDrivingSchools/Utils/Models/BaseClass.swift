//
//  BaseClass.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import Foundation

struct BaseClass<U:Codable>: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data = "data"
    }
    
    var status: Int?
    var message: String?
    var data: U?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        data = try container.decodeIfPresent(U.self, forKey: .data)
    }
}
