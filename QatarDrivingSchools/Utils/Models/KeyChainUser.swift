//
//  KeyChainUser.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 02/07/21.
//

import Foundation
import UIKit

struct Struct_KeyChainUser: Codable {
    
    enum CodingKeys: String, CodingKey {
        case email
        case password
    }
    
    init() {
        
    }
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    var email: String?
    var password: String?
  
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        password = try container.decodeIfPresent(String.self, forKey: .password)
    }
}
