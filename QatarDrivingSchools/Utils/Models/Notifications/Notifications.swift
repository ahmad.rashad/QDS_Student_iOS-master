//
//  Notifications.swift
//
//  Created by C100-107 on 07/10/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Notifications: Codable {
    
    enum CodingKeys: String, CodingKey {
        case notificationMessage
        case notificationType
    }
    
    var notificationMessage: String?
    var notificationType: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        notificationMessage = try container.decodeIfPresent(String.self, forKey: .notificationMessage)
        notificationType = try container.decodeIfPresent(Int.self, forKey: .notificationType)
    }
    
}
