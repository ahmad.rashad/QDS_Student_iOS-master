//
//  Data.swift
//
//  Created by C100-107 on 07/10/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationBaseData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case notifications
    }
    
    var notifications: [Notifications]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        notifications = try container.decodeIfPresent([Notifications].self, forKey: .notifications)
    }
    
}
