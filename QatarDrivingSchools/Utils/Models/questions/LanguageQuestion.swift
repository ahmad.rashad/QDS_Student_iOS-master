//
//  LanguageQuestion.swift
//
//  Created by C100-07 on 03/02/22
//  Copyright (c) . All rights reserved.
//

import Foundation

struct LanguageQuestion: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case question = "Question"
        case lessonTitle = "LessonTitle"
        /*case option4 = "Option4"
        case option1 = "Option1"
        case option3 = "Option3"
        case option2 = "Option2"*/
        case option = "Option"
    }
    
    var code: String?
    var question: String?
    /*var option1: String?
    var option2: String?
    var option3: String?
    var option4: String?*/
    var lessonTitle: String?
    var option: Option?

    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decodeIfPresent(String.self, forKey: .code)
        question = try container.decodeIfPresent(String.self, forKey: .question)
        /*option4 = try container.decodeIfPresent(String.self, forKey: .option4)
        option1 = try container.decodeIfPresent(String.self, forKey: .option1)
        option2 = try container.decodeIfPresent(String.self, forKey: .option2)
        option3 = try container.decodeIfPresent(String.self, forKey: .option3)*/
        lessonTitle = try container.decodeIfPresent(String.self, forKey: .lessonTitle)
        option = try container.decodeIfPresent(Option.self, forKey: .option)
    }
}

struct Option: Codable {

  enum CodingKeys: String, CodingKey {
    case option1 = "Option1"
    case option2 = "Option2"
    case option4 = "Option4"
    case option3 = "Option3"
  }

  var option1: String?
  var option2: String?
  var option4: String?
  var option3: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    option1 = try container.decodeIfPresent(String.self, forKey: .option1)
    option2 = try container.decodeIfPresent(String.self, forKey: .option2)
    option4 = try container.decodeIfPresent(String.self, forKey: .option4)
    option3 = try container.decodeIfPresent(String.self, forKey: .option3)
  }

}
