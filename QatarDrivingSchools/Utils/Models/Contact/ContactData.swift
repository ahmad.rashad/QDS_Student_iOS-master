//
//  Data.swift
//
//  Created by C100-107 on 27/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ContactData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case contact
    }
    
    var contact: [Contact]?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        contact = try container.decodeIfPresent([Contact].self, forKey: .contact)
    }
    
    
}
