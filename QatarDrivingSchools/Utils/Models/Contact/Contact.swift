//
//  Contact.swift
//
//  Created by C100-107 on 27/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Contact: Codable {
    
    enum CodingKeys: String, CodingKey {
        case socialMediaLinks
        case emailid
        case contactNumber
    }
    
    var socialMediaLinks: [SocialMediaLinks]?
    var emailid: String?
    var contactNumber: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        socialMediaLinks = try container.decodeIfPresent([SocialMediaLinks].self, forKey: .socialMediaLinks)
        emailid = try container.decodeIfPresent(String.self, forKey: .emailid)
        contactNumber = try container.decodeIfPresent(String.self, forKey: .contactNumber)
    }
    
}
