//
//  SocialMediaLinks.swift
//
//  Created by C100-107 on 27/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SocialMediaLinks: Codable {
    
    enum CodingKeys: String, CodingKey {
        case twitter
        case facebook
        case instagram
        case youtube
    }
    
    var twitter: String?
    var facebook: String?
    var instagram: String?
    var youtube: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        twitter = try container.decodeIfPresent(String.self, forKey: .twitter)
        facebook = try container.decodeIfPresent(String.self, forKey: .facebook)
        instagram = try container.decodeIfPresent(String.self, forKey: .instagram)
        youtube = try container.decodeIfPresent(String.self, forKey: .youtube)
    }
    
}
