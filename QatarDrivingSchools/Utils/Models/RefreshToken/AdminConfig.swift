//
//  AdminConfig.swift
//
//  Created by C100-107 on 27/04/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AdminConfig: Codable {

  enum CodingKeys: String, CodingKey {
    case userAgent
    case globalPassword
    case tempToken
  }

  var userAgent: String?
  var globalPassword: String?
  var tempToken: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    userAgent = try container.decodeIfPresent(String.self, forKey: .userAgent)
    globalPassword = try container.decodeIfPresent(String.self, forKey: .globalPassword)
    tempToken = try container.decodeIfPresent(String.self, forKey: .tempToken)
  }

}
