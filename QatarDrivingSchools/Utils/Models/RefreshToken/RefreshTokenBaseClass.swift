//
//  BaseClass.swift
//
//  Created by C100-107 on 27/04/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct RefreshTokenBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case message
        case adminConfig
        case tempToken
        case status
    }
    
    var message: String?
    var adminConfig: [AdminConfig]?
    var tempToken: String?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        message = try container.decodeIfPresent(String.self, forKey: .message)
        adminConfig = try container.decodeIfPresent([AdminConfig].self, forKey: .adminConfig)
        tempToken = try container.decodeIfPresent(String.self, forKey: .tempToken)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
    }
    
}
