//
//  Data.swift
//
//  Created by C100-107 on 03/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct EBooksBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case eBooks
    }
    
    var eBooks: [EBooks]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        eBooks = try container.decodeIfPresent([EBooks].self, forKey: .eBooks)
    }
    
}
