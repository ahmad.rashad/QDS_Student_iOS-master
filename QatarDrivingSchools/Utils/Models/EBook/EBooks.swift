//
//  EBooks.swift
//
//  Created by C100-107 on 03/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct EBooks: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lessonId
        case bookName
        case thumbnailImage
        case pdfLink
    }
    
    var lessonId: Int?
    var bookName: String?
    var thumbnailImage: String?
    var pdfLink: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lessonId = try container.decodeIfPresent(Int.self, forKey: .lessonId)
        bookName = try container.decodeIfPresent(String.self, forKey: .bookName)
        thumbnailImage = try container.decodeIfPresent(String.self, forKey: .thumbnailImage)
        pdfLink = try container.decodeIfPresent(String.self, forKey: .pdfLink)
    }
    
}
