//
//  Tests.swift
//
//  Created by C100-107 on 09/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Tests: Codable {
    
    enum CodingKeys: String, CodingKey {
        case totalQuestion
        case numberOfQuestionsToDisplay
        case passingLimitAnswer
        case examName
        case examId
        case timeDuration
        case status
        case certificate
        case resultStatus
        case errorMesg
    }
    
    var totalQuestion: Int?
    var numberOfQuestionsToDisplay: Int?
    var passingLimitAnswer: Int?
    var examName: String?
    var examId: Int?
    var timeDuration: String?
    var status: Int?
    var certificate: [Certificates]?
    var resultStatus: Int?
    var errorMesg: String?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        totalQuestion = try container.decodeIfPresent(Int.self, forKey: .totalQuestion)
        numberOfQuestionsToDisplay = try container.decodeIfPresent(Int.self, forKey: .numberOfQuestionsToDisplay)
        passingLimitAnswer = try container.decodeIfPresent(Int.self, forKey: .passingLimitAnswer)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
        examId = try container.decodeIfPresent(Int.self, forKey: .examId)
        timeDuration = try container.decodeIfPresent(String.self, forKey: .timeDuration)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        certificate = try container.decodeIfPresent([Certificates].self, forKey: .certificate)
        resultStatus = try container.decodeIfPresent(Int.self, forKey: .resultStatus)
        errorMesg = try container.decodeIfPresent(String.self, forKey: .errorMesg)
    }
    
}
