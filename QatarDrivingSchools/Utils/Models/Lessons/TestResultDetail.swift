//
//  TestResultDetail.swift
//
//  Created by C100-07 on 15/12/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct TestResultDetail: Codable {
    
    enum CodingKeys: String, CodingKey {
        case submittedDate
        case totalQuestion
        case numberOfQuestionsToDisplay
        case examName
        case resultStatus
        case score
        case result
        case examId
        case timeDuration
        case image
    }
    
    var submittedDate: String?
    var totalQuestion: Int?
    var numberOfQuestionsToDisplay: Int?
    var examName: String?
    var resultStatus: Int?
    var score: Int?
    var result: Float?
    var examId: Int?
    var timeDuration: String?
    var image: String?
    
    init() {
    
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        submittedDate = try container.decodeIfPresent(String.self, forKey: .submittedDate)
        totalQuestion = try container.decodeIfPresent(Int.self, forKey: .totalQuestion)
        numberOfQuestionsToDisplay = try container.decodeIfPresent(Int.self, forKey: .numberOfQuestionsToDisplay)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
        resultStatus = try container.decodeIfPresent(Int.self, forKey: .resultStatus)
        score = try container.decodeIfPresent(Int.self, forKey: .score)
        result = try container.decodeIfPresent(Float.self, forKey: .result)
        examId = try container.decodeIfPresent(Int.self, forKey: .examId)
        timeDuration = try container.decodeIfPresent(String.self, forKey: .timeDuration)
        image = try container.decodeIfPresent(String.self, forKey: .image)
    }
    
}
