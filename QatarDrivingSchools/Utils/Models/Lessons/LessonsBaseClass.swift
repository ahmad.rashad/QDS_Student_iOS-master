//
//  Data.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct LessonsBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lessons
    }
    
    var lessons: [Lessons]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lessons = try container.decodeIfPresent([Lessons].self, forKey: .lessons)
    }
    
}


struct LessonDetailBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lessonDetail
    }
    
    var lessonDetail: [Lessons]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lessonDetail = try container.decodeIfPresent([Lessons].self, forKey: .lessonDetail)
    }
    
}
