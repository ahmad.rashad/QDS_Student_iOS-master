//
//  Lessons.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation
import Lottie

struct Lessons: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case pausedFrom
        case thumbnailImage
        case descriptionValue = "description"
        case courseCompleted
        case lessonTitle
        case tests
        case progress
        case lessonId
        case courseTitle
        case assignDay
        case duration
        case courseId
        case pauseFrom
        case vidyardLink
        case testResultDetail
        case translation
    }
    
    var status: Int?
    var pausedFrom: String?
    var thumbnailImage: String?
    var descriptionValue: String?
    var courseCompleted: Float?
    var lessonTitle: String?
    var tests: [Tests]?
    var progress: Float?
    var lessonId: Int?
    var courseTitle: String?
    var assignDay: Int?
    var duration: String?
    var courseId: Int?
    var pauseFrom: String?
    var vidyardLink: String?
    var testResultDetail: [TestResultDetail]?
    var translation : String?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        pausedFrom = try container.decodeIfPresent(String.self, forKey: .pausedFrom)
        thumbnailImage = try container.decodeIfPresent(String.self, forKey: .thumbnailImage)
        descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        courseCompleted = try container.decodeIfPresent(Float.self, forKey: .courseCompleted)
        lessonTitle = try container.decodeIfPresent(String.self, forKey: .lessonTitle)
        tests = try container.decodeIfPresent([Tests].self, forKey: .tests)
        progress = try container.decodeIfPresent(Float.self, forKey: .progress)
        lessonId = try container.decodeIfPresent(Int.self, forKey: .lessonId)
        courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)
        assignDay = try container.decodeIfPresent(Int.self, forKey: .assignDay)
        duration = try container.decodeIfPresent(String.self, forKey: .duration)
        courseId = try container.decodeIfPresent(Int.self, forKey: .courseId)
        pauseFrom = try container.decodeIfPresent(String.self, forKey: .pauseFrom)
        vidyardLink = try container.decodeIfPresent(String.self, forKey: .vidyardLink)
        testResultDetail = try container.decodeIfPresent([TestResultDetail].self, forKey: .testResultDetail)
        translation = try container.decodeIfPresent(String.self, forKey: .translation)
    }
}
