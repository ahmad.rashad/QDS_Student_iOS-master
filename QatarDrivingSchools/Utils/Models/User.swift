//
//  User.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 01/07/21.
//

import Foundation

struct UserData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case user
    }
    
    var user: [User]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        user = try container.decodeIfPresent([User].self, forKey: .user)
    }
}


struct User: Codable {
    
    enum CodingKeys: String, CodingKey {
        case createdDate
        case emailId
        case role
        case userName
        case id
        case isActive
        case scode
        case address
        case modifiedDate
        case token
        case phoneNumber
        case profileImage
        case profileImageName
        case fullNameEnglish
        case fullNameArabic
        case gender
        case language
        case nationality
        case dateOfBirth
        case personalQatariID
        case idPhotoFront = "idPhoto_front"
        case idPhotoBack = "idPhoto_back"
    }
    
    var createdDate: String?
    var emailId: String?
    var role: String?
    var userName: String?
    var id: Int?
    var isActive: Bool?
    var scode: String?
    var address: String?
    var modifiedDate: String?
    var token: String?
    var phoneNumber: String?
    var profileImage: String?
    var profileImageName: String?
    var fullNameEnglish: String?
    var fullNameArabic: String?
    var gender: Bool?
    var language: String?
    var nationality: String?
    var dateOfBirth: String?
    var personalQatariID: String?
    var idPhotoFront: String?
    var idPhotoBack: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        createdDate = try container.decodeIfPresent(String.self, forKey: .createdDate)
        emailId = try container.decodeIfPresent(String.self, forKey: .emailId)
        role = try container.decodeIfPresent(String.self, forKey: .role)
        userName = try container.decodeIfPresent(String.self, forKey: .userName)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        isActive = try container.decodeIfPresent(Bool.self, forKey: .isActive)
        scode = try container.decodeIfPresent(String.self, forKey: .scode)
        address = try container.decodeIfPresent(String.self, forKey: .address)
        modifiedDate = try container.decodeIfPresent(String.self, forKey: .modifiedDate)
        token = try container.decodeIfPresent(String.self, forKey: .token)
        phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        profileImage = try container.decodeIfPresent(String.self, forKey: .profileImage)
        profileImageName = try container.decodeIfPresent(String.self, forKey: .profileImageName)
        fullNameEnglish = try container.decodeIfPresent(String.self, forKey: .fullNameEnglish)
        fullNameArabic = try container.decodeIfPresent(String.self, forKey: .fullNameArabic)
        gender = try container.decodeIfPresent(Bool.self, forKey: .gender)
        language = try container.decodeIfPresent(String.self, forKey: .language)
        nationality = try container.decodeIfPresent(String.self, forKey: .nationality)
        dateOfBirth = try container.decodeIfPresent(String.self, forKey: .dateOfBirth)
        personalQatariID = try container.decodeIfPresent(String.self, forKey: .personalQatariID)
        idPhotoFront = try container.decodeIfPresent(String.self, forKey: .idPhotoFront)
        idPhotoBack = try container.decodeIfPresent(String.self, forKey: .idPhotoBack)
    }
    
}
