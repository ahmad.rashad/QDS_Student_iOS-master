//
//  Entity.swift
//
//  Created by C100-07 on 10/02/22
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Entity: Codable {
    
    enum CodingKeys: String, CodingKey {
        case examId = "ExamId"
        case totalQuestion = "TotalQuestion"
        case passingLimitAnswer = "PassingLimitAnswer"
        case examName = "ExamName"
    }
    
    var examId: Int?
    var totalQuestion: Int?
    var passingLimitAnswer: Int?
    var examName: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        examId = try container.decodeIfPresent(Int.self, forKey: .examId)
        totalQuestion = try container.decodeIfPresent(Int.self, forKey: .totalQuestion)
        passingLimitAnswer = try container.decodeIfPresent(Int.self, forKey: .passingLimitAnswer)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
    }
    
}
struct EntityTranslation: Codable {
    
    enum CodingKeys: String, CodingKey {
        case examName = "ExamName"
        case code = "Code"
    }
    
    var examName: String?
    var code: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
        code = try container.decodeIfPresent(String.self, forKey: .code)
    }
    
}
