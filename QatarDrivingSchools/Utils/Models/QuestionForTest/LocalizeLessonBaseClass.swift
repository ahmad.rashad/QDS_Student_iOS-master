//
//  LocalizeLessonBaseClass.swift
//
//  Created by C100-07 on 10/02/22
//  Copyright (c) . All rights reserved.
//

import Foundation

struct LocalizeLessonBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lessonTitle = "LessonTitle"
        case entity = "Entity"
        case description = "Description"
        case courseTitle = "CourseTitle"
        case code = "Code"
    }
    
    var lessonTitle: String?
    var entity: [Entity]?
    var description: String?
    var courseTitle: String?
    var code: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lessonTitle = try container.decodeIfPresent(String.self, forKey: .lessonTitle)
        entity = try container.decodeIfPresent([Entity].self, forKey: .entity)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)
        code = try container.decodeIfPresent(String.self, forKey: .code)
    }
    
}
