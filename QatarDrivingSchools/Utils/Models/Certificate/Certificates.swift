//
//  Certificates.swift
//
//  Created by C100-107 on 17/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Certificates: Codable {
    
    enum CodingKeys: String, CodingKey {
        case studentName
        case certificateLink
        case certificateDate
        case id
    }
    
    var studentName: String?
    var certificateLink: String?
    var certificateDate: String?
    var id: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        studentName = try container.decodeIfPresent(String.self, forKey: .studentName)
        certificateLink = try container.decodeIfPresent(String.self, forKey: .certificateLink)
        certificateDate = try container.decodeIfPresent(String.self, forKey: .certificateDate)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
    }
    
}
