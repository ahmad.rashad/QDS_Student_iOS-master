//
//  CertificatesData.swift
//
//  Created by C100-107 on 17/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CertificatesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case certificates
    }
    
    var certificates: [Certificates]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        certificates = try container.decodeIfPresent([Certificates].self, forKey: .certificates)
    }
    
}
