//
//  ComplaintsCategoryBaseClass.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ComplaintsCategoryBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case complaintsCategory
    }
    
    var complaintsCategory: [ComplaintsCategory]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        complaintsCategory = try container.decodeIfPresent([ComplaintsCategory].self, forKey: .complaintsCategory)
    }
    
}
