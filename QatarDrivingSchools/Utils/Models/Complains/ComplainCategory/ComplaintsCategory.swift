//
//  ComplaintsCategory.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ComplaintsCategory: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case complainType
    }
    
    var id: Int?
    var complainType: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        complainType = try container.decodeIfPresent(String.self, forKey: .complainType)
    }
    
}
