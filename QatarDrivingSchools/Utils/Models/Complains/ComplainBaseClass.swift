//
//  ComplainBaseClass.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ComplainBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case complainData
    }
    
    var complainData: [ComplainData]?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        complainData = try container.decodeIfPresent([ComplainData].self, forKey: .complainData)
    }
    
}
