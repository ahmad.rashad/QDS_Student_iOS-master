//
//  Complainlist.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

/*import Foundation

struct ComplainDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case categoryID
        case complainDate
        case descriptionValue = "description"
        case countOfAttechment
        case categoryName
    }
    
    var status: String?
    var categoryID: Int?
    var complainDate: String?
    var descriptionValue: String?
    var countOfAttechment: Int?
    var categoryName: String?

    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        categoryID = try container.decodeIfPresent(Int.self, forKey: .categoryID)
        complainDate = try container.decodeIfPresent(String.self, forKey: .complainDate)
        descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        countOfAttechment = try container.decodeIfPresent(Int.self, forKey: .countOfAttechment)
        categoryName = try container.decodeIfPresent(String.self, forKey: .categoryName)
    }
    
    init(){
        status = ""
        categoryID = 0
        complainDate = ""
        descriptionValue = ""
        countOfAttechment = 0
        categoryName = ""
    }
}
*/
