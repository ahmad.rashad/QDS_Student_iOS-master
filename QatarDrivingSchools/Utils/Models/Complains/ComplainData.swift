//
//  ComplainData.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ComplainData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case emailId
        case media
        case complainId
        case studentName
        case complainDate
        case categoryName
        case studentId
        case descriptionValue = "description"
        case categoryId
        case complainTitle
        case response
    }
    
    var status: Int?
    var emailId: String?
    var media: [Media]?
    var complainId: Int?
    var studentName: String?
    var complainDate: String?
    var categoryName: String?
    var studentId: Int?
    var descriptionValue: String?
    var categoryId: Int?
    var complainTitle: String?
    var response: String?
    
    init() {}
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        emailId = try container.decodeIfPresent(String.self, forKey: .emailId)
        media = try container.decodeIfPresent([Media].self, forKey: .media)
        complainId = try container.decodeIfPresent(Int.self, forKey: .complainId)
        studentName = try container.decodeIfPresent(String.self, forKey: .studentName)
        complainDate = try container.decodeIfPresent(String.self, forKey: .complainDate)
        categoryName = try container.decodeIfPresent(String.self, forKey: .categoryName)
        studentId = try container.decodeIfPresent(Int.self, forKey: .studentId)
        descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        categoryId = try container.decodeIfPresent(Int.self, forKey: .categoryId)
        complainTitle = try container.decodeIfPresent(String.self, forKey: .complainTitle)
        response = try container.decodeIfPresent(String.self, forKey: .response)
    }
    
}
