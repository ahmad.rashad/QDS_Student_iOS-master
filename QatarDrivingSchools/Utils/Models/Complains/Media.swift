//
//  Media.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Media: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
    }
    
    var id: Int?
    var name: String?
    var type: Int?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        type = try container.decodeIfPresent(Int.self, forKey: .type)
    }
    
}
