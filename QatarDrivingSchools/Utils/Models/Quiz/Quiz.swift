//
//  Quiz.swift
//
//  Created by C100-107 on 29/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Quiz: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lessonQuiz
        case courseQuiz
    }
    
    var lessonQuiz: [Tests]?
    var courseQuiz: [Tests]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lessonQuiz = try container.decodeIfPresent([Tests].self, forKey: .lessonQuiz)
        courseQuiz = try container.decodeIfPresent([Tests].self, forKey: .courseQuiz)
    }
    
}
