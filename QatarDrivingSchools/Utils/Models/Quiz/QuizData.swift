//
//  QuizData.swift
//
//  Created by C100-107 on 29/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct QuizData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case quiz
    }
    
    var quiz: [Quiz]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        quiz = try container.decodeIfPresent([Quiz].self, forKey: .quiz)
    }
    
}
