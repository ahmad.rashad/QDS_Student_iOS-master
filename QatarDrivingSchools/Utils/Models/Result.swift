//
//  Result.swift
//
//  Created by C100-107 on 31/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResultData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case result
    }
    
    var result: [TestResultDetail]?
    
    init(result: [TestResultDetail])
    {
        self.result = result
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        result = try container.decodeIfPresent([TestResultDetail].self, forKey: .result)
    }
    
}

struct Result: Codable {
    
    enum CodingKeys: String, CodingKey {
        case examId
        case score
        case examName
        case totalQuestion
        case numberOfQuestionsToDisplay
        case resultStatus
        case result
        case submittedDate
    }
    
    var examId: Int?
    var score: Int?
    var examName: String?
    var totalQuestion: Int?
    var numberOfQuestionsToDisplay: Int?
    var resultStatus: Int?
    var result: Double?
    var submittedDate: String?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        examId = try container.decodeIfPresent(Int.self, forKey: .examId)
        score = try container.decodeIfPresent(Int.self, forKey: .score)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
        totalQuestion = try container.decodeIfPresent(Int.self, forKey: .totalQuestion)
        numberOfQuestionsToDisplay = try container.decodeIfPresent(Int.self, forKey: .numberOfQuestionsToDisplay)
        resultStatus = try container.decodeIfPresent(Int.self, forKey: .resultStatus)
        result = try container.decodeIfPresent(Double.self, forKey: .result)
        submittedDate = try container.decodeIfPresent(String.self, forKey: .submittedDate)
    }
    
}
