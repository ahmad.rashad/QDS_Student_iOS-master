//
//  Data.swift
//
//  Created by C100-107 on 27/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct QuestionsBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case questions
        case resultStatus
    }
    
    var questions: [Questions]?
    var resultStatus: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        questions = try container.decodeIfPresent([Questions].self, forKey: .questions)
        resultStatus = try container.decodeIfPresent(Int.self, forKey: .resultStatus)
    }
    
}
