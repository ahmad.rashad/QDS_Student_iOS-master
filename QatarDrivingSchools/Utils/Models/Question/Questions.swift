//
//  Questions.swift
//
//  Created by C100-107 on 27/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Questions: Codable {
    
    enum CodingKeys: String, CodingKey {
        case pauseTimeForQuestion
        case backWardTime
        case correctAnswer
        case sign
        case lessonTitle
        case questionId
        case videoId
        case lessonId
        case question
        case options
        case translation
    }
    
    var pauseTimeForQuestion: String?
    var backWardTime: String?
    var correctAnswer: String?
    var sign: String?
    var lessonTitle: String?
    var questionId: Int?
    var videoId: String?
    var lessonId: Int?
    var question: String?
    var translation: String?
    var options: [Options]?
    var selectedAnswer: Int?
    
    init(from decoder: Decoder) throws {
        print(decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        print(container)
        pauseTimeForQuestion = try container.decodeIfPresent(String.self, forKey: .pauseTimeForQuestion)
        backWardTime = try container.decodeIfPresent(String.self, forKey: .backWardTime)
        correctAnswer = try container.decodeIfPresent(String.self, forKey: .correctAnswer)
        sign = try container.decodeIfPresent(String.self, forKey: .sign)
        lessonTitle = try container.decodeIfPresent(String.self, forKey: .lessonTitle)
        questionId = try container.decodeIfPresent(Int.self, forKey: .questionId)
        videoId = try container.decodeIfPresent(String.self, forKey: .videoId)
        lessonId = try container.decodeIfPresent(Int.self, forKey: .lessonId)
        question = try container.decodeIfPresent(String.self, forKey: .question)
        translation = try container.decodeIfPresent(String.self, forKey: .translation)
        options = try container.decodeIfPresent([Options].self, forKey: .options)
        
    }
    
}
