//
//  Options.swift
//
//  Created by C100-107 on 07/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Options: Codable {
    internal init(option: String? = nil, text: String? = nil, type: String? = nil, imageUrl: String? = nil) {
        self.option = option
        self.text = text
        self.type = type
        self.imageUrl = imageUrl
    }
    
    
    enum CodingKeys: String, CodingKey {
        case option
        case text
        case type
        case imageUrl
    }
    
    var option: String?
    var text: String?
    var type: String?
    var imageUrl: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        option = try container.decodeIfPresent(String.self, forKey: .option)
        text = try container.decodeIfPresent(String.self, forKey: .text)
        type = try container.decodeIfPresent(String.self, forKey: .type)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
    }
}
