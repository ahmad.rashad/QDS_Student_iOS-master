//
//  Invoice.swift
//
//  Created by C100-107 on 28/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Invoice: Codable {
    
    enum CodingKeys: String, CodingKey {
        case invoiceDoc
    }
    
    var invoiceDoc: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        invoiceDoc = try container.decodeIfPresent(String.self, forKey: .invoiceDoc)
    }
    
}
