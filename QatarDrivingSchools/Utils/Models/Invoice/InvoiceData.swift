//
//  InvoiceData.swift
//
//  Created by C100-107 on 28/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct InvoiceData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case invoice
    }
    
    var invoice: [Invoice]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        invoice = try container.decodeIfPresent([Invoice].self, forKey: .invoice)
    }
    
}
