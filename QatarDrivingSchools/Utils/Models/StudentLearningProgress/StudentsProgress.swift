//
//  StudentsProgress.swift
//
//  Created by C100-107 on 22/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StudentsProgress: Codable {
    
    enum CodingKeys: String, CodingKey {
        case pausedFrom
        case courseCategoryId
        case lessonId
        case lessonTitle
        case courseCategory
        case lessonProgress
        case courseTitle
        case courseId
        case courseProgress
    }
    
    var pausedFrom: String?
    var courseCategoryId: Int?
    var lessonId: Int?
    var lessonTitle: String?
    var courseCategory: String?
    var lessonProgress: Float?
    var courseTitle: String?
    var courseId: Int?
    var courseProgress: Float?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        pausedFrom = try container.decodeIfPresent(String.self, forKey: .pausedFrom)
        courseCategoryId = try container.decodeIfPresent(Int.self, forKey: .courseCategoryId)
        lessonId = try container.decodeIfPresent(Int.self, forKey: .lessonId)
        lessonTitle = try container.decodeIfPresent(String.self, forKey: .lessonTitle)
        courseCategory = try container.decodeIfPresent(String.self, forKey: .courseCategory)
        lessonProgress = try container.decodeIfPresent(Float.self, forKey: .lessonProgress)
        courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)
        courseId = try container.decodeIfPresent(Int.self, forKey: .courseId)
        courseProgress = try container.decodeIfPresent(Float.self, forKey: .courseProgress)
    }
    
}
