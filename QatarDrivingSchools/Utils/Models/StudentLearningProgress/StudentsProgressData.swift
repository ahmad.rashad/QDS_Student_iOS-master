//
//  Data.swift
//
//  Created by C100-107 on 22/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StudentsProgressData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case studentsProgress
    }
    
    var studentsProgress: [StudentsProgress]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        studentsProgress = try container.decodeIfPresent([StudentsProgress].self, forKey: .studentsProgress)
    }
    
}
