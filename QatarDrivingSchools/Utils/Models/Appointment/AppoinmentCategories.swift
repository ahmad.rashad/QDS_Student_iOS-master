//
//  AppoinmentCategories.swift
//
//  Created by C100-107 on 14/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AppoinmentCategories: Codable {
    
    enum CodingKeys: String, CodingKey {
        case colorCode
        case id
        case type
    }
    
    var colorCode: String?
    var id: Int?
    var type: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        colorCode = try container.decodeIfPresent(String.self, forKey: .colorCode)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        type = try container.decodeIfPresent(String.self, forKey: .type)
    }
    
    init(colorCode: String, type: String, id: Int) {
        self.colorCode = colorCode
        self.type = type
        self.id = id
    }
}
