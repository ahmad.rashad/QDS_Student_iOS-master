//
//  Appointments.swift
//
//  Created by C100-107 on 16/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Appointments: Codable {
    
    enum CodingKeys: String, CodingKey {
        case appointmentId
        case response
        case staffId
        case eventName
        case eventTypeId
        case meetingPassword
        case meetingDate
        case requestDescription
        case availiblityTime
        case eventColorCode
        case meetingId
        case availiblityDate
        case studentId
        case meetingTime
        case zoomLink
        case creatorEmailId
        case status
        case title
    }
    
    var appointmentId: Int?
    var response: String?
    var staffId: Int?
    var eventName: String?
    var eventTypeId: Int?
    var meetingPassword: String?
    var meetingDate: String?
    var requestDescription: String?
    var availiblityTime: String?
    var eventColorCode: String?
    var meetingId: String?
    var availiblityDate: String?
    var studentId: Int?
    var meetingTime: String?
    var zoomLink: String?
    var creatorEmailId: String?
    var status: Int?
    var title: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appointmentId = try container.decodeIfPresent(Int.self, forKey: .appointmentId)
        response = try container.decodeIfPresent(String.self, forKey: .response)
        staffId = try container.decodeIfPresent(Int.self, forKey: .staffId)
        eventName = try container.decodeIfPresent(String.self, forKey: .eventName)
        eventTypeId = try container.decodeIfPresent(Int.self, forKey: .eventTypeId)
        meetingPassword = try container.decodeIfPresent(String.self, forKey: .meetingPassword)
        meetingDate = try container.decodeIfPresent(String.self, forKey: .meetingDate)
        requestDescription = try container.decodeIfPresent(String.self, forKey: .requestDescription)
        availiblityTime = try container.decodeIfPresent(String.self, forKey: .availiblityTime)
        eventColorCode = try container.decodeIfPresent(String.self, forKey: .eventColorCode)
        meetingId = try container.decodeIfPresent(String.self, forKey: .meetingId)
        availiblityDate = try container.decodeIfPresent(String.self, forKey: .availiblityDate)
        studentId = try container.decodeIfPresent(Int.self, forKey: .studentId)
        meetingTime = try container.decodeIfPresent(String.self, forKey: .meetingTime)
        zoomLink = try container.decodeIfPresent(String.self, forKey: .zoomLink)
        creatorEmailId = try container.decodeIfPresent(String.self, forKey: .creatorEmailId)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        title = try container.decodeIfPresent(String.self, forKey: .title)
    }
    
}
