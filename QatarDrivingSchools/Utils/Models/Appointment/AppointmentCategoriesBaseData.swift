//
//  Data.swift
//
//  Created by C100-107 on 14/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AppointmentCategoriesBaseData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case appoinmentCategories
    }
    
    var appoinmentCategories: [AppoinmentCategories]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appoinmentCategories = try container.decodeIfPresent([AppoinmentCategories].self, forKey: .appoinmentCategories)
    }
    
}
