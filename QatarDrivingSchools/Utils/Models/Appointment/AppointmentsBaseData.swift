//
//  Data.swift
//
//  Created by C100-107 on 14/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AppointmentsBaseData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case appointments
    }
    
    var appointments: [Appointments]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appointments = try container.decodeIfPresent([Appointments].self, forKey: .appointments)
    }
    
}
