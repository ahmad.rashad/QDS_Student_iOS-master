//
//  Data.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CoursesBaseClass: Codable {
    
    enum CodingKeys: String, CodingKey {
        case courses
    }
    
    var courses: [Courses]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        courses = try container.decodeIfPresent([Courses].self, forKey: .courses)
    }
    
}
