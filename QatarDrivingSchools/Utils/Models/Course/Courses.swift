//
//  Courses.swift
//
//  Created by C100-107 on 20/08/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Courses: Codable {
    
    enum CodingKeys: String, CodingKey {
        case courseId
        case daysProgressed
        case courseCategoryId
        case courseTitle
        case progress
        case courseCategory
        case courseImage
        case totalNumberOfDays
    }
    
    var courseId: Int?
    var daysProgressed: Int?
    var courseCategoryId: Int?
    var courseTitle: String?
    var progress: Float?
    var courseCategory: String?
    var courseImage: String?
    var totalNumberOfDays: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        courseId = try container.decodeIfPresent(Int.self, forKey: .courseId)
        daysProgressed = try container.decodeIfPresent(Int.self, forKey: .daysProgressed)
        courseCategoryId = try container.decodeIfPresent(Int.self, forKey: .courseCategoryId)
        courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)
        progress = try container.decodeIfPresent(Float.self, forKey: .progress)
        courseCategory = try container.decodeIfPresent(String.self, forKey: .courseCategory)
        courseImage = try container.decodeIfPresent(String.self, forKey: .courseImage)
        totalNumberOfDays = try container.decodeIfPresent(Int.self, forKey: .totalNumberOfDays)
    }
    
}
