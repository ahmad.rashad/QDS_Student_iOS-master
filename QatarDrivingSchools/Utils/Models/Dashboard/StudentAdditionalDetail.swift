//
//  StudentAdditionalDetail.swift
//
//  Created by C100-07 on 29/12/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StudentAdditionalDetail: Codable {
    
    enum CodingKeys: String, CodingKey {
      //  case staffId
        case completedComplaints
        case dueCourseFees
        case zoomLink
        case zoomMeetingDate
        case quizReview
        case totalComplaints
        case zoomTitle
        case zoomMeetingNumber
        case zoomMeetingTime
        case pendingComplaints
        case lessonProgress
    }
    
 //   var staffId: Int?
    var completedComplaints: Int?
    var dueCourseFees: Int?
    var zoomLink: String?
    var zoomMeetingDate: String?
    var quizReview: String?
    var totalComplaints: Int?
    var zoomTitle: String?
    var zoomMeetingNumber: String?
    var zoomMeetingTime: String?
    var pendingComplaints: Int?
    var lessonProgress: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
      //  staffId = try container.decodeIfPresent(Int.self, forKey: .staffId)
        completedComplaints = try container.decodeIfPresent(Int.self, forKey: .completedComplaints)
        dueCourseFees = try container.decodeIfPresent(Int.self, forKey: .dueCourseFees)
        zoomLink = try container.decodeIfPresent(String.self, forKey: .zoomLink)
        zoomMeetingDate = try container.decodeIfPresent(String.self, forKey: .zoomMeetingDate)
        quizReview = try container.decodeIfPresent(String.self, forKey: .quizReview)
        totalComplaints = try container.decodeIfPresent(Int.self, forKey: .totalComplaints)
        zoomTitle = try container.decodeIfPresent(String.self, forKey: .zoomTitle)
        zoomMeetingNumber = try container.decodeIfPresent(String.self, forKey: .zoomMeetingNumber)
        zoomMeetingTime = try container.decodeIfPresent(String.self, forKey: .zoomMeetingTime)
        pendingComplaints = try container.decodeIfPresent(Int.self, forKey: .pendingComplaints)
        lessonProgress = try container.decodeIfPresent(String.self, forKey: .lessonProgress)
    }
    
}
