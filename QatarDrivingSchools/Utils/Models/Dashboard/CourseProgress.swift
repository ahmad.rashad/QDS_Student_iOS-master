//
//  CourseProgress.swift
//
//  Created by C100-107 on 10/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CourseProgress: Codable {
    
    enum CodingKeys: String, CodingKey {
        case courseProgress
        case courseTitle
    }
    
    var courseProgress: Float?
    var courseTitle: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        courseProgress = try container.decodeIfPresent(Float.self, forKey: .courseProgress)
        courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)
    }
    
}
