//
//  DashboardData.swift
//
//  Created by C100-107 on 10/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DashboardData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case quizResult
        case inprogressLesson
        case courseProgress
        case studentAdditionalDetail
        //case upComingAppointment
        //case upComingExam
    }
    
    var quizResult: [QuizResult]?
    var inprogressLesson: [Lessons]?
    var courseProgress: [CourseProgress]?
    var studentAdditionalDetail : [StudentAdditionalDetail]?
    //var upComingAppointment: Any?
    //var upComingExam: Any?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        quizResult = try container.decodeIfPresent([QuizResult].self, forKey: .quizResult)
        inprogressLesson = try container.decodeIfPresent([Lessons].self, forKey: .inprogressLesson)
        courseProgress = try container.decodeIfPresent([CourseProgress].self, forKey: .courseProgress)
        studentAdditionalDetail = try container.decodeIfPresent([StudentAdditionalDetail].self, forKey: .studentAdditionalDetail)
        //upComingAppointment = try container.decodeIfPresent([].self, forKey: .upComingAppointment)
       // upComingExam = try container.decodeIfPresent([].self, forKey: .upComingExam)
    }
    
}
