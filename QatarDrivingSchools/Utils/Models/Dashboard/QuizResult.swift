//
//  QuizResult.swift
//
//  Created by C100-107 on 10/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct QuizResult: Codable {
    
    enum CodingKeys: String, CodingKey {
        case examName
        case examDate
        case examId
        case score
        case seatNo
    }
    
    var examName: String?
    var examDate: String?
    var examId: Int?
    var score: Int?
    var seatNo: String?
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        examName = try container.decodeIfPresent(String.self, forKey: .examName)
        examDate = try container.decodeIfPresent(String.self, forKey: .examDate)
        examId = try container.decodeIfPresent(Int.self, forKey: .examId)
        score = try container.decodeIfPresent(Int.self, forKey: .score)
        seatNo = try container.decodeIfPresent(String.self, forKey: .seatNo)
    }
    
}
