//
//  Data.swift
//
//  Created by C100-107 on 10/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DashboardBaseData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case dashboardData
    }
    
    var dashboardData: [DashboardData]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dashboardData = try container.decodeIfPresent([DashboardData].self, forKey: .dashboardData)
    }
    
}
