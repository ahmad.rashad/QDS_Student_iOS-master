//
//  ContractsData.swift
//
//  Created by C100-107 on 28/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ContractsData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case contracts
    }
    
    var contracts: [Contracts]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        contracts = try container.decodeIfPresent([Contracts].self, forKey: .contracts)
    }
    
}
