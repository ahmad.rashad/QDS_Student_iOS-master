//
//  Contracts.swift
//
//  Created by C100-107 on 28/09/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Contracts: Codable {
    
    enum CodingKeys: String, CodingKey {
        case details
        case contractPdf
        case contractDocType
        case title
    }
    
    var details: String?
    var contractPdf: String?
    var contractDocType: Int?
    var title: String?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        details = try container.decodeIfPresent(String.self, forKey: .details)
        contractPdf = try container.decodeIfPresent(String.self, forKey: .contractPdf)
        contractDocType = try container.decodeIfPresent(Int.self, forKey: .contractDocType)
        title = try container.decodeIfPresent(String.self, forKey: .title)
    }
    
}
