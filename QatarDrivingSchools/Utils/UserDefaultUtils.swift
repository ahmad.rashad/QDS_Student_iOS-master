//
//  UserDefaultUtils.swift
//  QatarDrivingSchools
//
//  Created by C100-107 on 10/04/20.
//  Copyright © 2020 C100-107. All rights reserved.
//

import UIKit
extension UserDefaults{
    func setCustom<T: Codable>(_ value: T?, forKey: String){
        let data = try? JSONEncoder().encode(value)
        set(data, forKey: forKey)
    }
    
    func setCustomArr<T: Codable>(_ value: [T], forKey: String) {
        let data = value.map { try? JSONEncoder().encode($0) }
        
        set(data, forKey: forKey)
    }
    
    func getCustom<T>(_ type: T.Type, forKey: String) -> T? where T : Decodable {
        guard let encodedData = data(forKey: forKey) else {
            return nil
        }
        
        return try! JSONDecoder().decode(type, from: encodedData)
    }
    
    func getCustomArr<T>(_ type: T.Type, forKey: String) -> [T] where T : Decodable {
        guard let encodedData = array(forKey: forKey) as? [Data] else {
            return []
        }
        
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
}

