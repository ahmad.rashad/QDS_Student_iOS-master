//
//  AppDelegate+Notfication.swift
//  QatarDrivingSchools
//
//  Created by C100-07 on 17/11/21.
//

import Foundation
import UIKit
import UserNotifications
import UserNotificationsUI
import FirebaseMessaging

//MARK: - Notification Delegate & Other Methods
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        //Constants.device_token = token
        print("====Device Token : \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote push notification")
        #if DEBUG
        Messaging.messaging().apnsToken = Data()
        //Constants.device_token = "simulator"
        #endif
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 1
        //print("didReceiveRemoteNotification: \(userInfo)")
        
       /* var isDisplayNotification = false
        
        if let apsObj = userInfo["aps"] as? NSDictionary,
            let actionId = apsObj["category"] as? String {
            
            /*if let enumNoti = enumNotificationCategory(rawValue: actionId) {
                print(enumNoti)
                isDisplayNotification = true
                //isDisplayNotification = !(enumNoti == .AddBid)
            } else {
                isDisplayNotification = true
            }*/
        }
        
        if isDisplayNotification {
            completionHandler(.newData)
        } else {
            completionHandler(.noData)
        }
        */
        completionHandler(.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let isDisplayNotification = true
        let userInfo = notification.request.content.userInfo
        print("willPresent: \(userInfo)")
        
        /*if let apsObj = userInfo["aps"] as? NSDictionary,
            let actionId = apsObj["category"] as? String {
            
            /*if let enumNoti = enumNotificationCategory(rawValue: actionId) {
                print(enumNoti)
//                isDisplayNotification = !(enumNoti == .AddBid)
//                eventDetailsDelegate?.eventDetailsCallback(enumNoti: enumNoti, userInfo: userInfo)
                
            }*/
        }*/
        
        if isDisplayNotification {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let userInfo = response.notification.request.content.userInfo
        let isDisplayNotification = true
        print("didReceive: \(userInfo)")
        if let dic = userInfo as? [String : Any] {
            if let type = dic["notification_type"] as? String, type == "message" {
                
            }
        }
        if isDisplayNotification {
            completionHandler()
        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                
                //print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            //print("Notification settings: \(settings.authorizationStatus)")
            
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func unregisterForRemoteNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
}

